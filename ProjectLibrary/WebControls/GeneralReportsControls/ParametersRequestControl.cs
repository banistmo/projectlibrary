using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Web.Caching;
//using Vision.DA;
//using Vision.Defs;
using ProjectLibrary.Components;
using ProjectLibrary.DA;
using System.Linq;
using System.Configuration;

namespace ProjectLibrary.WebControls.GeneralReportsControls
{
    /// <summary>
    /// This class inherits from the WebControl Class and its responsability is to 
    /// request the required parameters values in order to pass them to a display page which will 
    /// build the specified report based on this parameters.
    /// </summary>
    [DefaultProperty("Text"),
    ToolboxData("<{0}:ParametersRequestControl runat=server></{0}:ParametersRequestControl>")]
    public class ParametersRequestControl : System.Web.UI.WebControls.WebControl
    {
        private string text;
        ReportsConfiguration reportOptions = new ReportsConfiguration();
        //ImplementationProvider  myProvider = 	new  ImplementationProvider("DSNREPORTS");
        ODPProvider myProvider = new ODPProvider("DSNREPORTS");

        string reportname = "";
        [Bindable(true),
        Category("Appearance"),
        DefaultValue("")]
        public string Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
            }
        }


        /// <summary>
        /// This Method is used in order to render the validation code on the parameters catching.
        /// This method check for the report parameters data types and generates the validation conditions 
        /// for each parameter.
        /// </summary>
        /// <returns>this method returns a JavaScript code which will be writed in the page that contains this control.</returns>
        public string GetValidationCode()
        {
            
            string returnedValue = "";
            foreach (ReportsConfiguration.ParametersRow row in reportOptions.Parameters)
            {
                if (row.ReportRow["Name"].ToString() == reportname)
                {
                    switch (row.type)
                    {
                        case "integer":
                            returnedValue += "     if(!ValidateInteger(GetObject('" + row.name + "').value)){SetErrorMsg('El campo " + row.description + " contiene datos invalidos.');return false;}";
                            if (row.required == "true")
                                returnedValue += "     if(GetObject('" + row.name + "').value==''){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            break;
                        case "string":
                            returnedValue += "     if(!ValidateString(GetObject('" + row.name + "').value)){SetErrorMsg('El campo " + row.description + " contiene datos invalidos.');return false;}";
                            if (row.required == "true")
                                returnedValue += "     if(GetObject('" + row.name + "').value==''){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            break;
                        case "likestring":
                            returnedValue += "     if(!ValidateString(GetObject('" + row.name + "').value)){SetErrorMsg('El campo " + row.description + " contiene datos invalidos.');return false;}";
                            if (row.required == "true")
                                returnedValue += "     if(GetObject('" + row.name + "').value==''){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            break;
                        case "date":
                            if (row.required == "true")
                                returnedValue += "     if(GetObject('" + row.name + "').value==''){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            break;
                        case "list":
                            if (row.required == "true")
                            {
                                string ReportsDefaultValueInLists = "";
                                if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                                    ReportsDefaultValueInLists = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                                returnedValue += "     if(GetObject('" + row.name + "').value=='" + ReportsDefaultValueInLists + "'){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            }
                            break;
                        case "bindlist":
                            if (row.required == "true")
                            {
                                string ReportsDefaultValueInLists = "";
                                if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                                    ReportsDefaultValueInLists = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                                returnedValue += "     if(GetObject('" + row.name + "').value=='" + ReportsDefaultValueInLists + "' || GetObject('" + row.name + "').value==''){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            }
                            break;
                        case "bool":
                            if (row.required == "true")
                            {
                                string ReportsDefaultValueInLists = "";
                                if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                                    ReportsDefaultValueInLists = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                                returnedValue += "     if(GetObject('" + row.name + "').value=='" + ReportsDefaultValueInLists + "'){SetErrorMsg('El campo " + row.description + " es requerido.');return false;}";
                            }
                            break;
                    }
                }
            }
            returnedValue += "\n return true;";
            return returnedValue;
        }


        /// <summary>
        /// This method just constructs a code which build an string made of parameters values in the client-side.  
        /// This string has the a querystring structure and
        /// is passed throw the browser to the report display page.
        /// </summary>
        /// <returns>Returns the code which build the parameters querystring in order to be passed to the Display Report page </returns>
        public string GetParametersBuildingCode()
        {
            string returnedValue = "";
            returnedValue += "var returnedValue=\"\";\n";
            string ReportsDefaultResult = "-1";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultResult"] != null)
                ReportsDefaultResult = Configuration.SettingsManager.Keys["ReportsDefaultResult"];
            foreach (ReportsConfiguration.ParametersRow row in reportOptions.Parameters)
            {
                if (row.ReportRow["Name"].ToString() == reportname)
                {
                    //returnedValue += "if(GetObject(\"" + row.name + "\").value!=-1)\n returnedValue += \"" + row.name + "=\" + GetObject(\"" + row.name + "\").value+ \"&\";\n else \n returnedValue += \"" + row.name + "=&\";\n";
                    //returnedValue += "if(GetObject(\"" + row.name + "\").value!=\"\")\n returnedValue += \"" + row.name + "=\" + GetObject(\"" + row.name + "\").value+ \"&\";\n else \n returnedValue += \"" + row.name + "=-1&\";\n";
                    returnedValue += "if(GetObject(\"" + row.name + "\").value!=\"\")\n returnedValue += \"" + row.name + "=\" + GetObject(\"" + row.name + "\").value+ \"&\";\n else \n returnedValue += \"" + row.name + "=" + ReportsDefaultResult + "&\";\n";
                }
            }
            returnedValue += "returnedValue=returnedValue.substring(0,returnedValue.length -1);";
            returnedValue += "return returnedValue;";
            return returnedValue;
        }

        /// <summary>
        /// This method generate a string that contains the definition of all <b>BindList</b> parameters values in the
        /// specified report.
        /// </summary>
        /// <returns>Returns the generated string</returns>
        public string GetAllRequiredArrays()
        {
            reportname = this.Page.Request.QueryString["report"];
            string returnedValue = "";
            foreach (ReportsConfiguration.ParametersRow row in reportOptions.Parameters)
            {
                if (row.ReportRow["Name"].ToString() == reportname)
                {
                    if (row.type == "bindlist")
                        returnedValue += GetBindListArray(row);
                }
            }
            return returnedValue;
        }


        /// <summary>
        /// Returns the speficic array creation code of the bindlist parameters in the renderedRows input
        /// </summary>
        /// <param name="renderedRow">The row in a DataTable with the ReportsCofiguration schema.
        /// This row must be a bindlist parameter type</param>
        /// <returns>The code to build in the client-side the array option in order to the specified parameter.</returns>
        private string GetBindListArray(ReportsConfiguration.ParametersRow renderedRow)
        {
            string returnedValue = "";
            DataTable QueryResult = myProvider.RunQuery(renderedRow.query, "listQuery").Tables[0];
            returnedValue += "<script language=\"JavaScript\">";
            returnedValue += "var " + renderedRow.name + "List= [";
            //			returnedValue += "[-1,\"-------------------\",-1],";
            for (int i = 0; i < QueryResult.Rows.Count; i++)
            {
                returnedValue += "[";
                DataRow row = QueryResult.Rows[i];
                returnedValue += row["Id"] + ",";
                returnedValue += "\"" + row["Description"] + "\",";
                returnedValue += row["FilterField"];
                returnedValue += "]";
                if (i < QueryResult.Rows.Count - 1)
                    returnedValue += ",";

            }
            returnedValue += "]";
            returnedValue += "";
            returnedValue += "</script>";
            returnedValue = RemoveLineEndings(returnedValue);
            return returnedValue;
        }

        private string RemoveLineEndings(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(lineSeparator, string.Empty).Replace(paragraphSeparator, string.Empty);
        }


        /// <summary>
        /// Its function is to render the HTML code for each input parameter specified in the renderRow input.
        /// </summary>
        /// <param name="renderedRow">This is the parameter row to render.</param>
        /// <returns>Th HTML code to be parsed by the browser</returns>
        private string RenderField(ReportsConfiguration.ParametersRow renderedRow)
        {
            DataTable QueryResult = null;

            string returnedValue = "";
            returnedValue += "<tr><td style='padding: 7;'><b>" + renderedRow.description + ":&nbsp;</b></td>";
            switch (renderedRow.type.ToLower())
            {
                case "likestring":
                    returnedValue += "<td><INPUT class=\"mask BgInput\" id=\"" + renderedRow.name + "\" name=\"" + renderedRow.name + "\" runat=\"server\" preset=\"texto\"></td>";
                    break;
                case "string":
                    returnedValue += "<td><INPUT class=\"mask BgInput\" id=\"" + renderedRow.name + "\" name=\"" + renderedRow.name + "\" runat=\"server\" preset=\"texto\"></td>";
                    break;
                case "integer":
                    returnedValue += "<td><INPUT class=\"mask BgInput\" id=\"" + renderedRow.name + "\" name=\"" + renderedRow.name + "\" runat=\"server\" preset=\"int\"></td>";
                    break;
                case "date":
                    returnedValue += "<td><INPUT class=\"mask BgInput\" id=\"" + renderedRow.name + "\" name=\"" + renderedRow.name + "\" runat=\"server\" preset=\"shortdate\" readOnly=\"true\"><A class=\"so-BtnLink\" onclick=\"showCalendarMod('frm','" + renderedRow.name + "','BTN_date',false, event);return false;\" href=\"javascript:void(0);\"><IMG height=\"17\" src=\"../../img/tools/cal.gif\" width=\"22\" align=\"absMiddle\" border=\"0\" name=\"BTN_date\" preset=\"shortdate\"></A><td>";
                    break;
                case "list":
                    string changeString = "";
                    if (renderedRow.childcombos != "")
                        foreach (string fieldn in renderedRow.childcombos.Split(','))
                            changeString += "FillCombo('" + fieldn + "', this.options[this.selectedIndex].value);";

                    //returnedValue += "<td><select id=\""+ renderedRow.name +"\"  name=\""+ renderedRow.name +"\"  onchange=\""+changeString +"\" ><OPTION value='-1'>---------------------</OPTION>";

                    string ReportsDefaultValueInLists = "";
                    if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                        ReportsDefaultValueInLists = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                    returnedValue += "<td><select id=\"" + renderedRow.name + "\"  name=\"" + renderedRow.name + "\"  onchange=\"" + changeString + "\" ><OPTION value='" + ReportsDefaultValueInLists + "'>---------------------</OPTION>";
                    QueryResult = myProvider.RunQuery(renderedRow.query, "listQuery").Tables[0];
                    foreach (DataRow row in QueryResult.Rows)
                    {
                        returnedValue += "<option value=\"" + row["id"] + "\" >";
                        returnedValue += row["description"];
                        returnedValue += "</option>";
                    }
                    returnedValue += "</select></td>";
                    break;
                case "bindlist":
                    //returnedValue += "<td><select id=\"" + renderedRow.name + "\"  name=\"" + renderedRow.name + "\"><OPTION value='-1'>-------------------</OPTION></select></td>";

                    string changeString2 = "";
                    if (renderedRow.childcombos != "")
                        foreach (string fieldn in renderedRow.childcombos.Split(','))
                            changeString2 += "FillCombo('" + fieldn + "', this.options[this.selectedIndex].value);";

                    string ReportsDefaultValueInLists3 = "";
                    if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                        ReportsDefaultValueInLists = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                    returnedValue += "<td><select id=\"" + renderedRow.name + "\"  name=\"" + renderedRow.name + "\"  onchange=\"" + changeString2 + "\" ><OPTION value='" + ReportsDefaultValueInLists3 + "'>-------------------</OPTION></select></td>";
                    break;
                case "bool":
                    string ReportsDefaultValueInLists2 = "";
                    if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultValueInLists"] != null)
                        ReportsDefaultValueInLists2 = Configuration.SettingsManager.Keys["ReportsDefaultValueInLists"];
                    returnedValue += "<td><select id=\"" + renderedRow.name + "\"  name=\"" + renderedRow.name + "\"  ><OPTION value='" + ReportsDefaultValueInLists2 + "'>---------------------</OPTION>";
                    returnedValue += "<option value=\"" + "1" + "\" >";
                    returnedValue += "Si";
                    returnedValue += "</option>";
                    returnedValue += "<option value=\"" + "0" + "\" >";
                    returnedValue += "No";
                    returnedValue += "</option>";
                    returnedValue += "</select></td>";
                    break;
            }
            returnedValue += "</tr>";
            return returnedValue;
        }

        /// <summary> 
        /// Render all this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            output.Write("<table>");
            foreach (ReportsConfiguration.ParametersRow row in reportOptions.Parameters)
            {
                if (row.ReportRow["Name"].ToString() == reportname)
                {
                    output.Write(RenderField(row));
                }
            }

            output.Write("</table>");
            if ((this.Page.Request.QueryString["report"] != null) && (this.Page.Request.QueryString["report"] != ""))
            {
                //				output.Write("<input type='button' class='sbttn' id='ShowReportButton' runat='server' value='Ver Reporte' Width='109px' Height='20px' onclick='SendParameters()'>");

                output.Write("<div style='height:10'></div>");
                output.Write("<TABLE>");
                output.Write("<TR>");
                output.Write("<TD>");
                output.Write("<table cellpadding='0' cellspacing='0' border='0' height='21px'>");
                output.Write("	<tr>");
                output.Write("<td width='2px' valign='top'>");
                output.Write("	<img src='../../img/WorkPortal/WPLeftButton.gif' border='0' width='2' height='21'>");
                output.Write("	</td><td width='50px' nowrap>");
                output.Write("	<a class='WPButton' >");
                output.Write("	<input id='ShowReportButton' type='button' class='WPButtonI BAMnColor' value='Ver Informe'  onclick='SendParameters(); '>");
                output.Write("	</td>");
                output.Write("	<td width='2px' valign='top'>");
                output.Write("	<img src='../../img/WorkPortal/WPRightButton.gif' border='0' width='2' height='21'>");
                output.Write("	</td>");
                output.Write("	</tr>");
                output.Write("	</table>");
                output.Write("</TD>");
                output.Write("<TD>");
                output.Write("<table cellpadding='0' cellspacing='0' border='0' height='21px'>");
                output.Write("	<tr>");
                output.Write("<td width='2px' valign='top'>");
                output.Write("	<img src='../../img/WorkPortal/WPLeftButton.gif' border='0' width='2' height='21'>");
                output.Write("	</td><td width='50px' nowrap>");
                output.Write("	<a class='WPButton' >");
                output.Write("	<input id='ExportToExcelButton' type='button' class='WPButtonI BAMnColor' value='Exportar a Excel' onclick='ExportToExcel()'>");
                output.Write("	</td>");
                output.Write("	<td width='2px' valign='top'>");
                output.Write("	<img src='../../img/WorkPortal/WPRightButton.gif' border='0' width='2' height='21'>");
                output.Write("	</td>");
                output.Write("	</tr>");
                output.Write("	</table>");
                output.Write("</TD>");
                output.Write("</TR>");
                output.Write("</TABLE>");
                //output.Write("<input type='button' class='sbttn' id='ExportToExcelButton' runat='server' value='Export To Excel' Width='109px' Height='20px' onclick='ExportToExcel()'>");
            }



        }

        /// <summary>
        /// Load the configuration file in a dataset with the required ReportConfiguration schema.
        /// </summary>
        /// <param name="e">EventArgs by default</param>
        protected override void OnLoad(EventArgs e)
        {
            reportname = this.Page.Request.QueryString["report"];
            int EnableOnTheFlyReports = 0;
            if (System.Configuration.ConfigurationManager.AppSettings["EnableOnTheFlyReports"] != null)
                EnableOnTheFlyReports = Convert.ToInt32(Configuration.SettingsManager.Keys["EnableOnTheFlyReports"]);
            string IdUser = convertirGuidUserPorUserId(decodestring(this.Page.Request.QueryString["tokenb"]));
            if (EnableOnTheFlyReports == 1)
                reportOptions = GetReportsWithParameters(IdUser);
            else
                reportOptions = GetReportsWithParameters();
        }
        public string decodestring(string cadena)
        {
            string returnValue = "";
            if (cadena != null)
            {
                if (cadena != "")
                {
                    byte[] encodedDataAsBytes = System.Convert.FromBase64String(cadena);
                    returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
                }
            }
            return returnValue;
        }

        public static ReportsConfiguration reportOptionsCache;

        public static ReportsConfiguration GetReportsWithParameters()
        {

            if (System.Web.HttpRuntime.Cache["reportOptions"] != null)
                return reportOptionsCache;
            //ImplementationProvider myProvider =  new ImplementationProvider("DSNREPORTS");
            ODPProvider myProvider = new ODPProvider("DSNREPORTS");
            ReportsConfiguration reportOptions = new ReportsConfiguration();
            DataSet reportds = new DataSet();
            string ReportsParametersQuery = "Select idReport, name ,description,type,query,childcombos,filterfield,required from parameters";
            string ReportsReportsQuery = "Select idReport,Description,Groups,Queries,Name from report";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsParametersQuery"] != null)
                ReportsParametersQuery = Configuration.SettingsManager.Keys["ReportsParametersQuery"];
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsReportsQuery"] != null)
                ReportsReportsQuery = Configuration.SettingsManager.Keys["ReportsReportsQuery"];
            myProvider.RunQuery(ReportsReportsQuery, "Report", true, reportds);
            myProvider.RunQuery(ReportsParametersQuery, "parameters", true, reportds);
            DataRow[] drParamReports;
            ReportsConfiguration.ReportRow drReportNew;
            ReportsConfiguration.ParametersRow drParamNew;
            foreach (DataRow drReport in reportds.Tables[0].Rows)
            {
                drReportNew = reportOptions.Report.NewReportRow();
                drReportNew.Description = drReport["Description"].ToString();
                drReportNew.Groups = drReport["Groups"].ToString();
                drReportNew.Queries = drReport["Queries"].ToString();
                drReportNew.Name = drReport["Name"].ToString();

                drParamReports = reportds.Tables[1].Select("idReport=" + drReport["idReport"].ToString());
                reportOptions.Report.Rows.Add(drReportNew);
                foreach (DataRow drParam in drParamReports)
                {

                    drParamNew = reportOptions.Parameters.NewParametersRow();
                    drParamNew.name = drParam["name"].ToString();
                    drParamNew.description = drParam["description"].ToString();
                    drParamNew.type = drParam["type"].ToString();
                    drParamNew.query = drParam["query"].ToString();
                    drParamNew.childcombos = drParam["childcombos"].ToString();
                    drParamNew.filterfield = drParam["filterfield"].ToString();
                    drParamNew.required = drParam["required"].ToString();
                    drParamNew.ReportRow = drReportNew;

                    reportOptions.Parameters.Rows.Add(drParamNew);
                }
            }

            int cacheMinutes = 5;
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsCacheMinutes"] != null)
                cacheMinutes = Convert.ToInt32(Configuration.SettingsManager.Keys["ReportsCacheMinutes"]);
            if (cacheMinutes > 0)
            {
                System.Web.HttpRuntime.Cache.Add("reportOptions", reportOptions, null, DateTime.Now.AddMinutes(cacheMinutes), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
                reportOptionsCache = reportOptions;
            }
            return reportOptions;

        }

        public static ReportsConfiguration GetReportsWithParameters(string UserId)
        {
            if (System.Web.HttpRuntime.Cache["reportOptions"] != null)
                return reportOptionsCache;
            //ImplementationProvider myProvider = new ImplementationProvider("DSNREPORTS");
            ODPProvider myProvider = new ODPProvider("DSNREPORTS");
            //DataTable idDBUser = myProvider.RunQuery("SELECT * FROM WFUSER WHERE GUIDUSER='" + UserId + "'", "idDBUser").Tables[0];
            //string idUser = idDBUser.Rows[0]["IDUSER"].ToString();
            ReportsConfiguration reportOptions = new ReportsConfiguration();
            DataRow[] drParamReports;
            ReportsConfiguration.ReportRow drReportNew;
            ReportsConfiguration.ParametersRow drParamNew;

            //Init Include On The Fly

            string RegionalFilterSubcategory = "select Distinct IDPCATEGORIADELSERVICIO from ( select T.IDPCATEGORIADELSERVICIO from PRG_ClaseServicio C inner join (select Max(idp_prg_tipodeservicio) as TS, idp_prg_claseservicio from p_PRG_TipodeServicio group by idp_prg_claseservicio) TM ON tm.idp_prg_claseservicio = c.idprg_claseservicio inner join p_PRG_TipodeServicio T on T.idp_prg_tipodeservicio = TM.TS inner join p_PRG_UsuarioCon U on U.p_Prg_TipodeServicio = T.idp_prg_tipodeservicio where T.BSeguridadCon = 1 and U.idUser = @UserId union all select T.IDPCATEGORIADELSERVICIO from PRG_ClaseServicio C inner join (select Max(idp_prg_tipodeservicio) as TS, idp_prg_claseservicio from p_PRG_TipodeServicio group by idp_prg_claseservicio) TM ON tm.idp_prg_claseservicio = c.idprg_claseservicio inner join p_PRG_TipodeServicio T on T.idp_prg_tipodeservicio = TM.TS where (T.BSeguridadCon = 0 or T.BSeguridadCon is null) and (T.IDPAIS=(select idpais from wfuser where iduser = @UserId) OR T.IDPAIS is null) ) where IDPCATEGORIADELSERVICIO is not null";
            RegionalFilterSubcategory = RegionalFilterSubcategory.Replace("@UserId", UserId);
            DataSet reportOTFds = new DataSet();
            string OTFReportsQuery = ProjectLibrary.Properties.PLResources.ResourceManager.GetString("OnTheFlyReportsListQuery2");
            //string OTFReportsQuery = Configuration.SettingsManager.Keys["OnTheFlyReportsListQuery"];
            if (System.Configuration.ConfigurationManager.AppSettings["OnTheFlyReportsListQuery2"] != null)
                OTFReportsQuery = Configuration.SettingsManager.Keys["OnTheFlyReportsListQuery2"];
            OTFReportsQuery = OTFReportsQuery.Replace("@UserId", UserId);
            drReportNew = reportOptions.Report.NewReportRow();
            drReportNew.Description = "Proceso Requerimiento General";//drReport["Description"].ToString();
            drReportNew.Groups = "";
            drReportNew.Name = "OTFR_000001";// + drReport["idReport"].ToString();
            reportOptions.Report.Rows.Add(drReportNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "serviceSubCategory";
            drParamNew.description = "Sub categor�a";
            drParamNew.type = "list";
            drParamNew.required = "true";
            drParamNew.childcombos = "serviceType";
            drParamNew.query = "select IDP_PRG_CATEGORIASERVICIO as id, SDESCRIPCION as description from p_prg_categoriaservicio where IDP_PRG_CATEGORIASERVICIO IN(" + RegionalFilterSubcategory + ") order by SDESCRIPCION";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "serviceType";
            drParamNew.description = "Tipo de Servicio";
            drParamNew.type = "bindlist";
            drParamNew.required = "true";
            drParamNew.childcombos = "";
            drParamNew.query = OTFReportsQuery;
            drParamNew.filterfield = "IDPCATEGORIADELSERVICIO";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "CreationDateFrom";
            drParamNew.description = "Fecha creaci�n caso (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "CreationDateTo";
            drParamNew.description = "Fecha creaci�n caso (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "ServiceDateFrom";
            drParamNew.description = "Fecha servicio (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "ServiceDateTo";
            drParamNew.description = "Fecha servicio (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "EndServiceDateFrom";
            drParamNew.description = "Fecha fin servicio (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "EndServiceDateTo";
            drParamNew.description = "Fecha fin servicio (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            /* ------- Por solicitud de OBELIX se comenta esta secci�n -----
            //Historico estados
            drReportNew = reportOptions.Report.NewReportRow();
            drReportNew.Description = "Historico de estados del servicio";//drReport["Description"].ToString();
            drReportNew.Groups = "";
            drReportNew.Name = "OTFR_000002";// + drReport["idReport"].ToString();
            reportOptions.Report.Rows.Add(drReportNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "serviceSubCategory";
            drParamNew.description = "Sub categor�a";
            drParamNew.type = "list";
            drParamNew.required = "false";
            drParamNew.childcombos = "serviceType";
            drParamNew.query = "select IDP_PRG_CATEGORIASERVICIO as id, SDESCRIPCION as description from p_prg_categoriaservicio where IDP_PRG_CATEGORIASERVICIO IN(" + RegionalFilterSubcategory + ") order by SDESCRIPCION ";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "serviceType";
            drParamNew.description = "Tipo de Servicio";
            drParamNew.type = "bindlist";
            drParamNew.required = "false";
            drParamNew.childcombos = "";
            drParamNew.query = OTFReportsQuery;
            drParamNew.filterfield = "IDPCATEGORIADELSERVICIO";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "CaseNumber";
            drParamNew.description = "N�mero del servicio";
            drParamNew.type = "string";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "Activity";
            drParamNew.description = "Actividad";
            drParamNew.type = "list";
            drParamNew.required = "false";
            drParamNew.childcombos = "";
            drParamNew.query = "select IDP_PRG_ACTIVIDADSERVICIO id, SDESCRIPCION description from P_PRG_ACTIVIDADSERVICIO order by 1";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "StatusResult";
            drParamNew.description = "Resultado estado";
            drParamNew.type = "list";
            drParamNew.required = "false";
            drParamNew.childcombos = "";
            drParamNew.query = "select IDP_PRG_RESULTADODELESTADO id, SDESCRIPCION description from P_PRG_RESULTADODELESTADO order by 2";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "PerformerUser";
            drParamNew.description = "Usuario ejecutor estado";
            drParamNew.type = "string";
            drParamNew.required = "false";
            drParamNew.query = "";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "CreationDateFrom";
            drParamNew.description = "Fecha creaci�n caso (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "CreationDateTo";
            drParamNew.description = "Fecha creaci�n caso (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "ServiceDateFrom";
            drParamNew.description = "Fecha servicio (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "ServiceDateTo";
            drParamNew.description = "Fecha servicio (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "EndServiceDateFrom";
            drParamNew.description = "Fecha fin servicio (desde)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

            drParamNew = reportOptions.Parameters.NewParametersRow();
            drParamNew.name = "EndServiceDateTo";
            drParamNew.description = "Fecha fin servicio (hasta)";
            drParamNew.type = "date";
            drParamNew.required = "false";
            drParamNew.ReportRow = drReportNew;
            reportOptions.Parameters.Rows.Add(drParamNew);

             ------- FIN --- > Por solicitud de OBELIX se comenta esta secci�n ----- */

            //End include On The Fly


            DataSet reportds = new DataSet();
            string ReportsParametersQuery = "Select idReport, name ,description,type,query,childcombos,filterfield,required from parameters";
            string ReportsReportsQuery = "Select idReport,Description,Groups,Queries,Name from report";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsParametersQuery"] != null)
                ReportsParametersQuery = Configuration.SettingsManager.Keys["ReportsParametersQuery"];
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsReportsQuery"] != null)
                ReportsReportsQuery = Configuration.SettingsManager.Keys["ReportsReportsQuery"];
            myProvider.RunQuery(ReportsReportsQuery, "Report", true, reportds);
            myProvider.RunQuery(ReportsParametersQuery, "parameters", true, reportds);

            foreach (DataRow drReport in reportds.Tables[0].Rows)
            {
                drReportNew = reportOptions.Report.NewReportRow();
                drReportNew.Description = drReport["Description"].ToString();
                drReportNew.Groups = drReport["Groups"].ToString();
                drReportNew.Queries = drReport["Queries"].ToString();
                drReportNew.Name = drReport["Name"].ToString();

                drParamReports = reportds.Tables[1].Select("idReport=" + drReport["idReport"].ToString());
                reportOptions.Report.Rows.Add(drReportNew);
                foreach (DataRow drParam in drParamReports)
                {

                    drParamNew = reportOptions.Parameters.NewParametersRow();
                    drParamNew.name = drParam["name"].ToString();
                    drParamNew.description = drParam["description"].ToString();
                    drParamNew.type = drParam["type"].ToString();
                    drParamNew.query = drParam["query"].ToString().Replace("@idUser", UserId);
                    drParamNew.childcombos = drParam["childcombos"].ToString();
                    drParamNew.filterfield = drParam["filterfield"].ToString();
                    drParamNew.required = drParam["required"].ToString();
                    drParamNew.ReportRow = drReportNew;

                    reportOptions.Parameters.Rows.Add(drParamNew);
                }
            }
            

            int cacheMinutes = 5;
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsCacheMinutes"] != null)
                cacheMinutes = Convert.ToInt32(Configuration.SettingsManager.Keys["ReportsCacheMinutes"]);
            if (cacheMinutes > 0)
            {
                System.Web.HttpRuntime.Cache.Add("reportOptions", reportOptions, null, DateTime.Now.AddMinutes(cacheMinutes), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
                reportOptionsCache = reportOptions;
            }
            
            return reportOptions;

        }
        private string convertirGuidUserPorUserId(string GuidUser)
        {
            DataTable idDBUser = myProvider.RunQuery("SELECT * FROM WFUSER WHERE GUIDUSER='" + GuidUser + "'", "idDBUser").Tables[0];
            return idDBUser.Rows[0]["IDUSER"].ToString();
        }
    }
}
