using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;

namespace ProjectLibrary.WebControls.ComboQuery
{
	/// <summary>
	/// Summary description for ComboQueryControl.
	/// </summary>
	[DefaultProperty("Text"), 
		ToolboxData("<{0}:ComboQueryControl runat=server></{0}:ComboQueryControl>")]
	public class ComboQueryControl : System.Web.UI.WebControls.WebControl
	{
	}
}
