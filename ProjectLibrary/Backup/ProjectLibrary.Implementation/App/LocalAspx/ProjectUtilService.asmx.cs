﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ProjectLibrary.DA;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script;
using System.Text;

namespace ProjectLibrary.Implementation.App.LocalAspx
{
    /// <summary>
    /// Summary description for ProjectUtilService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ProjectUtilService : System.Web.Services.WebService
    {
        ODPProvider myProvider = new ODPProvider("DSNREPORTS");

        [WebMethod]
        public string GetGDFCategories(GDFOptions options)
        {
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            //GDFOptions oData = (GDFOptions)json_serializer.Deserialize(options,typeof(GDFOptions));
            DataTable dt = new DataTable();
            if (options.countryId == null)
                options.countryId = 0; 
            StringBuilder queryCategories = new StringBuilder();
            queryCategories.Append("select distinct idp_prg_clasificacion id,sdescripcion description from ");
            queryCategories.Append("( ");
            queryCategories.Append("select distinct cat.idp_prg_clasificacion, cat.sdescripcion from p_PRG_Clasificacion CAT ");
            queryCategories.Append("inner join p_PRG_CategoriaServicio SUB on cat.idp_prg_clasificacion = sub.id_prg_clasificacion ");
            queryCategories.Append("inner join p_PRG_TipodeServicio TS on ts.idpcategoriadelservicio = sub.idp_prg_categoriaservicio ");
            queryCategories.Append("where (ts.bseguridadrad is null or ts.bseguridadrad = 0) and ts.bservicioactivo=1 and (ts.idpais = "+options.countryId+" or idpais is null)");
            queryCategories.Append("union all ");
            queryCategories.Append("select distinct cat.idp_prg_clasificacion, cat.sdescripcion from p_PRG_Clasificacion CAT ");
            queryCategories.Append("inner join p_PRG_CategoriaServicio SUB on cat.idp_prg_clasificacion = sub.id_prg_clasificacion ");
            queryCategories.Append("inner join p_PRG_TipodeServicio TS on ts.idpcategoriadelservicio = sub.idp_prg_categoriaservicio ");
            queryCategories.Append("inner join p_PRG_UsuarioRad U on u.p_prg_tipodeservicio = ts.idp_prg_tipodeservicio ");
            queryCategories.Append("where ts.bseguridadrad = 1 and ts.bservicioactivo=1 and u.iduser = " + options.userId);
            queryCategories.Append(") order by 2  ");
            dt = myProvider.RunQuery(queryCategories.ToString(), "diplayTable").Tables[0];
            string result = JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.None);
            return result;
        }
        [WebMethod]
        public string GetGDFSubCategories(GDFOptions options)
        {
            DataTable dt = new DataTable();
            if (options.countryId == null)
                options.countryId = 0; 
            StringBuilder querySubCategories = new StringBuilder();
            querySubCategories.Append("select distinct idp_prg_categoriaservicio id,sdescripcion description from  ");
            querySubCategories.Append("(  ");
            querySubCategories.Append("select distinct sub.idp_prg_categoriaservicio, sub.sdescripcion from  p_PRG_CategoriaServicio SUB  ");
            querySubCategories.Append("inner join p_PRG_TipodeServicio TS on ts.idpcategoriadelservicio = sub.idp_prg_categoriaservicio  ");
            querySubCategories.Append("where (ts.bseguridadrad is null or ts.bseguridadrad = 0) and ts.bservicioactivo=1 AND sub.id_prg_clasificacion =" + options.categoryId + " and (ts.idpais = "+options.countryId+" or idpais is null)");
            querySubCategories.Append("union all  ");
            querySubCategories.Append("select distinct sub.idp_prg_categoriaservicio, sub.sdescripcion from  p_PRG_CategoriaServicio SUB  ");
            querySubCategories.Append("inner join p_PRG_TipodeServicio TS on ts.idpcategoriadelservicio = sub.idp_prg_categoriaservicio  ");
            querySubCategories.Append("inner join p_PRG_UsuarioRad U on u.p_prg_tipodeservicio = ts.idp_prg_tipodeservicio  ");
            querySubCategories.Append("where ts.bseguridadrad = 1 and ts.bservicioactivo=1 and u.iduser = " + options.userId+"  AND sub.id_prg_clasificacion ="+options.categoryId);
            querySubCategories.Append(")  ");
            querySubCategories.Append("order by 2  ");
            dt = myProvider.RunQuery(querySubCategories.ToString(), "diplayTable").Tables[0];
            string result = JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.None);
            return result;
        }
        [WebMethod]
        public string GetGDFServices(GDFOptions options)
        {
            DataTable dt = new DataTable();
            if (options.countryId == null)
                options.countryId = 0; 
            StringBuilder queryServices = new StringBuilder();
            queryServices.Append("select distinct idp_prg_tipodeservicio id,snombreservicio description,smensajedeayuda helptext from  ");
            queryServices.Append("(  ");
            queryServices.Append("select distinct ts.idp_prg_tipodeservicio, ts.snombreservicio, ts.smensajedeayuda from  p_PRG_TipodeServicio TS  ");
            queryServices.Append("where (ts.bseguridadrad is null or ts.bseguridadrad = 0) AND  ts.bservicioactivo=1 AND ts.idpcategoriadelservicio = " + options.subcategoryId + " and (ts.idpais = "+options.countryId+" or idpais is null)");
            queryServices.Append("union all  ");
            queryServices.Append("select distinct ts.idp_prg_tipodeservicio, ts.snombreservicio, ts.smensajedeayuda from  p_PRG_TipodeServicio TS  ");
            queryServices.Append("inner join p_PRG_UsuarioRad U on u.p_prg_tipodeservicio = ts.idp_prg_tipodeservicio  ");
            queryServices.Append("where ts.bseguridadrad = 1 and ts.bservicioactivo=1 and u.iduser = " + options.userId + "  AND ts.idpcategoriadelservicio = " + options.subcategoryId);
            queryServices.Append(")  ");
            queryServices.Append("order by 2  ");
            dt = myProvider.RunQuery(queryServices.ToString(), "diplayTable").Tables[0];
            string result = JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.None);
            return result;
        }
    }
    public class GDFOptions
    {
        private string userid;
        public string userId
        {
            get
            {
                return userid;
            }
            set
            {
                userid = value;
            }
        }
        private int countryid;
        public int countryId
        {
            get
            {
                return countryid;
            }
            set
            {
                countryid = value;
            }
        }
        private int categoryid;
        public int categoryId
        {
            get
            {
                return categoryid;
            }
            set
            {
                categoryid = value;
            }
        }
        private int subcategoryid;
        public int subcategoryId
        {
            get
            {
                return subcategoryid;
            }
            set
            {
                subcategoryid = value;
            }
        }
    }
}
