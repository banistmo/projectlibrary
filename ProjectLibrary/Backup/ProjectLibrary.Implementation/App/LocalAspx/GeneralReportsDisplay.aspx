﻿<%@ Page language="c#" Codebehind="GeneralReportsDisplay.aspx.cs" AutoEventWireup="True" Inherits="ProjectLibrary.Implementation.App.LocalAspx.GeneralReportsDisplay" %>
<HTML>
	<HEAD>

<%WriteHead();%>

<style>
.gridline1 td {
    font-size: 12px;
    border-right: black;
    border-style: solid;
    border-width: 1px;
    min-width:150;
}

.gridline2 td {
    font-size: 12px;
    border-right: black;
    border-style: solid;
    border-width: 1px;
    min-width:150;
}

</style>
	</HEAD>
	<body class="ui-bizagi-old-render @font-face" style="text-align:center">
		<form runat="server">
			<table width="90%" border="0" cellspacing="2" cellpadding="2" align="center" style="margin:3">
				<TR>
					<TD align="center">
						<asp:DataGrid id="DataGridReport" runat="server" AllowPaging="True" BorderColor="White" BorderWidth="2px"
							BackColor="White" CellPadding="4" BorderStyle="solid" PageSize="10">
							<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="gridline2" HorizontalAlign="Center"></AlternatingItemStyle>
							<ItemStyle CssClass="gridline1" HorizontalAlign="Center"></ItemStyle>
							<HeaderStyle CssClass="headerlinksNI" HorizontalAlign="Center"></HeaderStyle>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="headerlinksNI" Mode="NumericPages"></PagerStyle>
						</asp:DataGrid>
						<span id="SpanMessage" runat="server"></span>
					</TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
