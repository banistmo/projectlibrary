﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

namespace ProjectLibrary
{
    public class Cryptography
    {
        private static byte[] m_Key = new byte[8];
        private static byte[] m_IV = new byte[8];
        private UTF8Encoding m_oEncoding = new UTF8Encoding();

		//private static string m_sAPP_BASE_PATH = null;
		protected static readonly string m_sCryptKey = "B20E89B5-74AA-4DB3-ABB4-6A605537A918";
		protected static readonly string m_sCryptPrefix = "CRYPT.1:";
		//protected EAppSettingsSource appSettingsSrc;
		protected Hashtable htSections;
/*		public static string APP_BASE_PATH
		{
			get
			{
				if (CAppSettings.m_sAPP_BASE_PATH == null)
				{
					CAppSettings.m_sAPP_BASE_PATH = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath;
					if (CAppSettings.m_sAPP_BASE_PATH[CAppSettings.m_sAPP_BASE_PATH.Length - 1] != Path.DirectorySeparatorChar)
					{
						CAppSettings.m_sAPP_BASE_PATH += Path.DirectorySeparatorChar.ToString();
					}
				}
				return CAppSettings.m_sAPP_BASE_PATH;
			}
		}
 
		public new abstract string this[string name]
		{
			get;
			set;
		}
		public new abstract string this[int index]
		{
			get;
		}

        public static string ConfigurationFile
		{
			get
			{
				return AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			}
		}
		public static bool IsWebApplication
		{
			get
			{
				return AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.ToLower().EndsWith("web.config");
			}
		}
		public static eBizAgiApplication BizAgiApplicationContext
		{
			get
			{
				string text = AppDomain.CurrentDomain.FriendlyName.ToLower();
				text = text.Replace("vshost.", string.Empty);
				string a;
				if ((a = text) != null)
				{
					if (a == "bizagistudio.exe")
					{
						return eBizAgiApplication.Studio;
					}
					if (a == "bizagimc.exe")
					{
						return eBizAgiApplication.ManagementConsole;
					}
					if (a == "bizagi.scheduler.services.exe")
					{
						return eBizAgiApplication.Scheduler;
					}
					if (a == "bizagi.serveroperationsservice.exe")
					{
						return eBizAgiApplication.ServerOperationsService;
					}
				}
				if (CAppSettings.IsWebApplication)
				{
					return eBizAgiApplication.Web;
				}
				return eBizAgiApplication.Unknown;
			}
		}
		public CAppSettings()
		{
		}
		public CAppSettings(EAppSettingsSource appSettingsSrc)
		{
			this.appSettingsSrc = appSettingsSrc;
		}
 */ 
//		public abstract void AddHashTableParams(Hashtable htParameters);
		public static string DecryptString(string sKey, string sValue)
		{
			string result;
			try
			{
				result = DecryptString(sValue);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("Unable to get key setting '{0}'. {1}", sKey, ex.Message), ex);
			}
			return result;
		}
		public static string DecryptString(string sValue)
		{
			if (IsEncrypted(sValue))
			{
				Cryptography cryptography = new Cryptography();
				return cryptography.DecryptData(m_sCryptKey, sValue.Substring(m_sCryptPrefix.Length));
			}
			return sValue;
		}
		public static string EncryptString(string sValue)
		{
			string result;
			try
			{
				Cryptography cryptography = new Cryptography();
				result = m_sCryptPrefix + cryptography.EncryptData(m_sCryptKey, sValue);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("Unable to encrypt text. {0}", ex.Message), ex);
			}
			return result;
		}
		public static bool IsEncrypted(string sValue)
		{
			return sValue.Length >= m_sCryptPrefix.Length && sValue.Substring(0, m_sCryptPrefix.Length) == m_sCryptPrefix;
		}


        public string EncryptData(string strKey, string strData)
        {
            string text;
            if (strData.Length > 50000000)
            {
                text = "Error. Data String too large. Keep within 50Mb.";
                throw new Exception(text);
            }
            if (this.m_oEncoding.GetType().Equals(typeof(ASCIIEncoding)))
            {
                int i = 0;
                int length = strData.Length;
                while (i < length)
                {
                    if (strData[i] >= '\u0080')
                    {
                        text = string.Format("Error. Character '{0}' is not supported for encryption.", strData[i]);
                        throw new Exception(text);
                    }
                    i++;
                }
            }
            if (!Cryptography.InitKey(strKey))
            {
                text = "Error. Fail to generate key for encryption";
                throw new Exception(text);
            }
            strData = string.Format("{0,5:00000}", Math.Min(strData.Length, 99999)) + strData;
            byte[] bytes = this.m_oEncoding.GetBytes(strData);
            DESCryptoServiceProvider dESCryptoServiceProvider = new DESCryptoServiceProvider();
            ICryptoTransform transform = dESCryptoServiceProvider.CreateEncryptor(Cryptography.m_Key, Cryptography.m_IV);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();
            if (memoryStream.Length == 0L)
            {
                text = "";
            }
            else
            {
                text = Convert.ToBase64String(memoryStream.GetBuffer(), 0, Convert.ToInt32(memoryStream.Length));
            }
            cryptoStream.Close();
            return text;
        }
        public string DecryptData(string strKey, string strData)
        {
            if (!Cryptography.InitKey(strKey))
            {
                string text = "Error. Fail to generate key for decryption";
                throw new Exception(text);
            }
            DESCryptoServiceProvider dESCryptoServiceProvider = new DESCryptoServiceProvider();
            ICryptoTransform transform = dESCryptoServiceProvider.CreateDecryptor(Cryptography.m_Key, Cryptography.m_IV);
            byte[] buffer;
            try
            {
                buffer = Convert.FromBase64CharArray(strData.ToCharArray(), 0, strData.Length);
            }
            catch (Exception innerException)
            {
                string text = "Error. Input Data is not base64 encoded.";
                throw new Exception(text, innerException);
            }
            MemoryStream stream = new MemoryStream(buffer);
            CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
            string result;
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                StreamWriter streamWriter = new StreamWriter(memoryStream);
                streamWriter.Write(new StreamReader(stream2).ReadToEnd());
                streamWriter.Flush();
                string text = this.m_oEncoding.GetString(memoryStream.ToArray());
                streamWriter.Close();
                text = text.Substring(5);
                result = text;
            }
            catch (Exception innerException2)
            {
                string text = "Error. Decryption Failed. Possibly due to incorrect Key or corrputed data";
                throw new Exception(text, innerException2);
            }
            return result;
        }
        private static bool InitKey(string strKey)
        {
            bool result;
            try
            {
                byte[] array = new byte[strKey.Length];
                ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
                aSCIIEncoding.GetBytes(strKey, 0, strKey.Length, array, 0);
                SHA1CryptoServiceProvider sHA1CryptoServiceProvider = new SHA1CryptoServiceProvider();
                byte[] array2 = sHA1CryptoServiceProvider.ComputeHash(array);
                for (int i = 0; i < 8; i++)
                {
                    Cryptography.m_Key[i] = array2[i];
                }
                for (int i = 8; i < 16; i++)
                {
                    Cryptography.m_IV[i - 8] = array2[i];
                }
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        public string Hash2EncryptData(string strData)
        {
            SHA512 sHA = new SHA512Managed();
            byte[] bytes = this.m_oEncoding.GetBytes(strData);
            byte[] array = sHA.ComputeHash(bytes);
            StringBuilder stringBuilder = new StringBuilder(array.Length);
            for (int i = 0; i < array.Length; i++)
            {
                stringBuilder.Append(array[i].ToString("X2"));
            }
            return stringBuilder.ToString();
        }
    }
}
