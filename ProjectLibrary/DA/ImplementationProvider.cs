using System;
using System.Data;
using System.Data.OleDb;
using ProjectLibrary.Configuration;
using ProjectLibrary.ExceptionHandling;

namespace ProjectLibrary.DA
{
	/// <summary>
	/// Summary description for ImplementationProvider.
	/// </summary>
	public class ImplementationProvider
	{
		
		protected string _server;
		protected string _database;
		protected string _user;
		protected string _password;
		protected string _connectionString;

		protected  OleDbConnection conn= null;
		protected System.Collections.ArrayList parameters= new System.Collections.ArrayList();
		public string Server 
		{
			set{_server=value;}
			get{return _server;}
		}

		public string Database 
		{
			set{_database=value;}
			get{return _database;}
		}

		public string User 
		{
			set{_user=value;}
			get{return _user;}
		}

		public string Password 
		{
			set{_password=value;}
			get{return _password;}
		}

		public string ConnectionString 
		{
			set{_connectionString=value;}
			get{return _connectionString;}
		}

		public ImplementationProvider ()
		{
				ConnectionString=SettingsManager.Keys["DSNDB"];
				conn= new OleDbConnection(this.ConnectionString);
		}

		/// <summary>
		/// Constructor OverLoad sending Key conection string
		/// </summary>
		/// <param name="keyConection">Key .config to extract the conection string</param>
		public ImplementationProvider (string keyConection)
		{
			ConnectionString= Cryptography.DecryptString(SettingsManager.Keys[keyConection]);
            //Get String unencrypted!!
			conn= new OleDbConnection(this.ConnectionString);
		}


		public DataSet RunQuery(string query, string tableName)
		{
			OleDbCommand comm= new OleDbCommand(query,conn);
			for(int i=0; i<parameters.Count;i++)
			{
			query=query.Replace(((OleDbParameter)parameters[i]).ParameterName,((OleDbParameter)parameters[i]).Value.ToString());
			comm.Parameters.Add((OleDbParameter)parameters[i]);
			
			}
			comm.CommandText=query;
			
			//comm.CommandType=CommandType.StoredProcedure;
			OleDbDataAdapter da=new OleDbDataAdapter(comm);
			
			DataSet ds= new DataSet();
            try
            {
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();
            }
            catch (Exception ex)
            {
                ExceptionHandler.Throw(ExceptionTypes.Data, "Couldn't fill the dataset with the query : " + query + "on the table : " + tableName + 
                    Environment.NewLine + ex.Message);
            }            
            return ds;
					
		}

		public void RunQuery(string query, string tableName, bool bfillDataset, DataSet dataset)
		{
		
			DataSet ds=RunQuery(query,tableName);
			if(bfillDataset)
			{
				dataset.Tables.Add(ds.Tables[0].Copy());
			}
		
		}

		public DataSet RunProc(string storeprocedure, string tableName)
		{
			OleDbCommand comm= new OleDbCommand(storeprocedure,conn);
			for(int i=0; i<parameters.Count;i++)
			{
				
				comm.Parameters.Add((OleDbParameter)parameters[i]);
			
			}
			comm.CommandType=CommandType.StoredProcedure;
			
			//comm.CommandType=CommandType.StoredProcedure;
			OleDbDataAdapter da=new OleDbDataAdapter(comm);
			
			DataSet ds= new DataSet();
            try
            {
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();
            }catch(Exception ex)
            {
                ExceptionHandler.Throw(ExceptionTypes.Data, "Couldn't fill the dataset with the stored procedure : " + storeprocedure + 
                    "on the table : " + tableName + Environment.NewLine + ex.Message);
            }

            return ds;
					
		}
		public void AddParameter(string sParamName, string sParamValue)
		{
			OleDbParameter param= new OleDbParameter(sParamName,sParamValue);
			parameters.Add(param);
		}
		public void ClearParameter()
		{
			parameters= new System.Collections.ArrayList();
		}
	}
}
