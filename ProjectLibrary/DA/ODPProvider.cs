using System;
using System.Data;
using ProjectLibrary.Configuration;
using ProjectLibrary;
using Oracle.DataAccess.Client;
using System.Data.OleDb;
using ProjectLibrary.Components;

namespace ProjectLibrary.DA
{
	/// <summary>
	/// Summary description for ImplementationProvider.
	/// </summary>
	public class ODPProvider
	{
		
		protected string _server;
		protected string _database;
		protected string _user;
		protected string _password;
		protected string _connectionString;
        protected bool _isOracle;

		protected  OracleConnection conn= null;
        protected OleDbConnection connOledb = null;
		protected System.Collections.ArrayList parameters= new System.Collections.ArrayList();
		public string Server 
		{
			set{_server=value;}
			get{return _server;}
		}

        public bool IsOracle
        {
            set { _isOracle = value; }
            get { return _isOracle; }
        }

		public string Database 
		{
			set{_database=value;}
			get{return _database;}
		}

		public string User 
		{
			set{_user=value;}
			get{return _user;}
		}

		public string Password 
		{
			set{_password=value;}
			get{return _password;}
		}

		public string ConnectionString 
		{
			set{_connectionString=value;}
			get{return _connectionString;}
		}

		public ODPProvider ()
		{
            if (SettingsManager.Keys["PROVIDERTYPE"] == "Oracle")
            {
                ConnectionString = SettingsManager.Keys["DSNDB"];
                conn = new OracleConnection(this.ConnectionString);
            }
            else
            {
                ConnectionString = SettingsManager.Keys["DSNDB"];
                connOledb = new OleDbConnection(this.ConnectionString);            
            }
		}

		/// <summary>
		/// Constructor OverLoad sending Key conection string
		/// </summary>
		/// <param name="keyConection">Key .config to extract the conection string</param>
        public ODPProvider(string keyConection)
		{
            if (SettingsManager.Keys["PROVIDERTYPE"] == "Oracle")
            {
                IsOracle = true;
                string OldConnectionString = Cryptography.DecryptString(SettingsManager.Keys[keyConection]);
                System.Data.Common.DbConnectionStringBuilder dbConnectionStringBuilder = new System.Data.Common.DbConnectionStringBuilder();
                dbConnectionStringBuilder.ConnectionString = OldConnectionString;
                string sUsername = Cryptography.DecryptString(dbConnectionStringBuilder["User ID"].ToString());
                string sPassword = Cryptography.DecryptString(dbConnectionStringBuilder["Password"].ToString());
                string sServer = Cryptography.DecryptString(dbConnectionStringBuilder["Data Source"].ToString());
                string sNewConnectionString = "Data Source=" + sServer + ";User ID=" + sUsername + ";Password=" + sPassword + ";";
                ConnectionString = sNewConnectionString;
                //Get String unencrypted!!            
                conn = new OracleConnection(this.ConnectionString);
            }
            else
            {
                string OldConnectionString = Cryptography.DecryptString(SettingsManager.Keys[keyConection]);
                System.Data.Common.DbConnectionStringBuilder dbConnectionStringBuilder = new System.Data.Common.DbConnectionStringBuilder();
                dbConnectionStringBuilder.ConnectionString = OldConnectionString;
                //"Provider=SQLOLEDB;Data Source=SER-ANDREAP\SQLEXPRESS; Initial Catalog=Nativa_Local; User ID=sa;Password=BizagiBPM123;"
                string sUsername = Cryptography.DecryptString(dbConnectionStringBuilder["User ID"].ToString());
                string sPassword = Cryptography.DecryptString(dbConnectionStringBuilder["Password"].ToString());
                string sServer = Cryptography.DecryptString(dbConnectionStringBuilder["Data Source"].ToString());
                string sProvider = Cryptography.DecryptString(dbConnectionStringBuilder["Provider"].ToString());
                string sCatalog = Cryptography.DecryptString(dbConnectionStringBuilder["Initial Catalog"].ToString());

                string sNewConnectionString = "Provider=" + sProvider + ";Data Source=" + sServer + ";Initial Catalog=" + sCatalog + ";User ID=" + sUsername + ";Password=" + sPassword + ";";
                ConnectionString = sNewConnectionString;
                //Get String unencrypted!!            
                connOledb = new OleDbConnection(this.ConnectionString);
            }
		}


		public DataSet RunQuery(string query, string tableName)
		{
            if (IsOracle)
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ReportsSchema"] != null)
                {
                    string sAlter = "ALTER SESSION SET CURRENT_SCHEMA=" + System.Configuration.ConfigurationManager.AppSettings["ReportsSchema"];
                    OracleCommand commAlter = new OracleCommand(sAlter, conn);
                    conn.Open();
                    commAlter.ExecuteNonQuery();
                }

                OracleCommand comm = new OracleCommand(query, conn);

                for (int i = 0; i < parameters.Count; i++)
                {
                    query = query.Replace(((OracleParameter)parameters[i]).ParameterName, ((OracleParameter)parameters[i]).Value.ToString());
                    comm.Parameters.Add((OracleParameter)parameters[i]);
                }
                Utils.LogFile(query);
                comm.CommandText = query;

                //comm.CommandType=CommandType.StoredProcedure;
                OracleDataAdapter da = new OracleDataAdapter(comm);

                DataSet ds = new DataSet();
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                return ds;
            }
            else
            {
                OleDbCommand comm = new OleDbCommand(query, connOledb);
                for (int i = 0; i < parameters.Count; i++)
                {
                    query = query.Replace(((OleDbParameter)parameters[i]).ParameterName, ((OleDbParameter)parameters[i]).Value.ToString());
                    comm.Parameters.Add((OleDbParameter)parameters[i]);

                }
                comm.CommandText = query;

                //comm.CommandType=CommandType.StoredProcedure;
                OleDbDataAdapter da = new OleDbDataAdapter(comm);

                DataSet ds = new DataSet();
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();

                return ds;            
            }
					
		}

		public void RunQuery(string query, string tableName, bool bfillDataset, DataSet dataset)
		{
		
			DataSet ds=RunQuery(query,tableName);
			if(bfillDataset)
			{
				dataset.Tables.Add(ds.Tables[0].Copy());
			}
		
		}

		public DataSet RunProc(string storeprocedure, string tableName)
		{
            if (IsOracle)
            {
                OracleCommand comm = new OracleCommand(storeprocedure, conn);
                for (int i = 0; i < parameters.Count; i++)
                {
                    comm.Parameters.Add((OracleParameter)parameters[i]);
                }
                comm.CommandType = CommandType.StoredProcedure;

                //comm.CommandType=CommandType.StoredProcedure;
                OracleDataAdapter da = new OracleDataAdapter(comm);

                DataSet ds = new DataSet();
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();

                return ds;
            }
            else
            {
                OleDbCommand comm = new OleDbCommand(storeprocedure, connOledb);
                for (int i = 0; i < parameters.Count; i++)
                {

                    comm.Parameters.Add((OleDbParameter)parameters[i]);

                }
                comm.CommandType = CommandType.StoredProcedure;

                //comm.CommandType=CommandType.StoredProcedure;
                OleDbDataAdapter da = new OleDbDataAdapter(comm);

                DataSet ds = new DataSet();
                da.Fill(ds);
                ds.Tables[0].TableName = tableName;
                ClearParameter();

                return ds;            
            }
					
		}
		public void AddParameter(string sParamName, string sParamValue)
		{
            if (IsOracle)
            {
                OracleParameter param = new OracleParameter(sParamName, sParamValue);
                parameters.Add(param);
            }
            else
            {
                OleDbParameter param = new OleDbParameter(sParamName, sParamValue);
                parameters.Add(param);            
            }
		}
		public void ClearParameter()
		{
			parameters= new System.Collections.ArrayList();
		}
	}
}
