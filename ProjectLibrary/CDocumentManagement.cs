using System;
using System.IO; 
using System.Web; 
using BizAgi.Interop.Excel;

namespace ProjectLibrary
{

	/// <summary>
	/// This class contains functions for document management, such as retrieving specific 
	/// document paths, duplicating and copying documents.
	/// The class is not intended to read or modify document content; there should be some 
	/// other classes to achieve these functionallity.
	/// </summary>
	public class CDocumentManagement
	{
			
		#region Constructor
			
		public CDocumentManagement()
		{			
		}

		#endregion


		#region General document management functions



		/// <summary>
		/// Returns trhe virtual directory for this application
		/// </summary>
		/// <returns></returns>
		private static string GetVirtualDir()
		{
			string ProjectName;
			try
			{
				ProjectName			= System.Configuration.ConfigurationSettings.AppSettings["Project"];   
	 
			}
			catch
			{
				ProjectName = "BizAgi";                
			}

			string CurVirtDir	= System.Web.HttpContext.Current.Server.MapPath(null);

			string LowerCVD		= CurVirtDir.ToLower();
			string LowerPRN		= ProjectName.ToLower();
			int IndexPrjName	= LowerCVD.IndexOf(LowerPRN);
				
			if(IndexPrjName<0)
			{
				return "";
			}

			string VirtualDir	=	CurVirtDir.Substring(0, IndexPrjName + ProjectName.Length);

			return VirtualDir;

		}


		/// <summary>
		/// Erases all content in the specified directory
		/// </summary>
		/// <param name="destDir"></param>
		private static void DeleteDirContent(string destDir)
		{
			string[] DirFiles	=	Directory.GetFiles(destDir, "*.*"); 
			foreach (string fileName in DirFiles)
			{
				File.Delete(fileName);  
			}
		}


		/// <summary>
		/// Returns the directory for a specific document, given its identification data
		/// </summary>
		/// <param name="idEnt">Entity id</param>
		/// <param name="idAttrib">Attrib id</param>
		/// <param name="Key">Primary key value in entity</param>
		/// <returns></returns>
		private static string GetDocDir( int idEnt, int idAttrib, int Key )
		{
			string VirtDir	=	GetVirtualDir();
			int Div1000		=	(int)(Key / 1000);

			string DocDir	=	VirtDir + "\\docs\\" + idEnt + "\\" + 
				Div1000.ToString()  + "\\" +
				Key.ToString()  + "\\" +
				idAttrib.ToString();
			return DocDir;
		}


		/// <summary>
		/// Returns the path for a specific document, given its identification data.
		/// If the path is not found returns an empty string.
		/// </summary>
		/// <param name="idEnt">Entity id</param>
		/// <param name="idAttrib">Attrib id</param>
		/// <param name="Key">Primary key value in entity</param>
		/// <returns></returns>
		private static string GetDocPath( int idEnt, int idAttrib, int Key )
		{
			string DocDir		=	GetDocDir(idEnt, idAttrib, Key);
			string[] DirFiles	=	Directory.GetFiles(DocDir, "*.*"); 
			if(DirFiles.Length==0)
				return "";

			return DirFiles[0];
		}


		/// <summary>
		/// Replaces all files in destDir by the file specified in srcPath. 
		/// destDir will end up with a unique file inside it.
		/// </summary>
		/// <param name="srcPath"></param>
		/// <param name="destDir"></param>
		/// <returns></returns>
		private static string CopyFile( string srcPath, string destDir )
		{
			return CopyFile(srcPath, destDir, true);
		}


		/// <summary>
		/// Copies a file into a specific directory
		/// </summary>
		/// <param name="srcPath">source file path</param>
		/// <param name="destDir">destination directory</param>
		/// <param name="delDirContent">Indicates if content in destination dir must be delete before copying </param>
		/// <returns></returns>
		private static string CopyFile( string srcPath, string destDir, bool delDirContent )
		{

			int nameStart	=	srcPath.LastIndexOf("\\") + 1; 
			string fileName	=	srcPath.Substring(nameStart); 
			string destPath	=	destDir + "\\" + fileName;
				
			Directory.CreateDirectory(destDir); 
			if(delDirContent)
				DeleteDirContent(destDir);

			File.Copy(srcPath, destPath, true); 

			return destPath;
		}



		#endregion			

		
		#region Document Neuroscore
/*
		/// <summary>
		/// Returns the path for document SolicitudMontoIndem (entity LesionesyMuertes), given its key
		/// </summary>
		/// <param name="idLesionesyMuertes"></param>
		/// <returns></returns>
		public static string Get_SolicitudMI1_XLS_Dir( int idSolicitud )
		{
			
			int id_ent_Solicitud			= 57;
			int id_attr_NeuroscoreFile		= 395;

			string DocDir = GetDocDir(id_ent_Solicitud, id_attr_NeuroscoreFile, idSolicitud);
			
			return DocDir;
		}


		/// <summary>
		/// Finds the directory for document SolicitudMontoIndem (entity LesionesyMuertes), given its key
		/// </summary>
		/// <param name="idLesionesyMuertes"></param>
		/// <returns></returns>
		public static string Get_SolicitudMI1_XLS_Path( int idSolicitud )
		{
			int id_ent_Solicitud		= 57;
			int id_attr_NeuroscoreFile	= 395;

			return GetDocPath(id_ent_Solicitud, id_attr_NeuroscoreFile, idSolicitud);
		}

							 
	/// <summary>
		/// Makes a copy of the SolicitudMontoIndem document template (entity LesionesyMuertes)
		/// to one specific directory, given its key
		/// </summary>
		/// <param name="idLesionesyMuertes"></param>
		/// <returns></returns>
		public static string Copy_SolicitudMI1_XLS(int idSolicitud)
		{
			string VirtDir		= GetVirtualDir();
			string TemplDirPath = VirtDir + "\\app\\cartas\\templates";
			string TemplatePath = TemplDirPath + "\\NeuroscoreFile.xls";

			string DestDirPath	= Get_SolicitudMI1_XLS_Dir(idSolicitud);
			string DestPath		= DestDirPath + "\\NeuroscoreFileSol.xls";
			
			Directory.CreateDirectory(DestDirPath); 
			File.Copy(TemplatePath, DestPath, true); 

			return DestPath;
		}*/


		#endregion	
	}
}
