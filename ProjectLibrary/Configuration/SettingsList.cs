using System;
using System.Configuration;
using ProjectLibrary.ExceptionHandling;


namespace ProjectLibrary.Configuration
{
	/// <summary>
	/// This is an internal class. There are no documentation about it
	/// </summary>
	public class SettingsList
	{
		/// <summary>
		/// This is an internal constructor there are no documentation about it.
		/// </summary>
		public SettingsList()
		{
		}


		/// <summary>
		/// This is an internal indexer there are no documentation about it.
		/// </summary>
		public string this[string key]
		{
			get
			{
				object oKey = ConfigurationManager.AppSettings[key];
				if(oKey == null)
					ExceptionHandler.Throw("No se ha podido encontrar la llave '" +key + "'.  Contacte al administrador para una revisión de configuración de la aplicación.");
				return oKey.ToString();
			}
		}


	}
}
