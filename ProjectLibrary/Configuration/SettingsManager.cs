using System;
using System.IO;
using ProjectLibrary.ExceptionHandling;

namespace ProjectLibrary.Configuration
{
	/// <summary>
	/// This class implements methods used to manage settings for application in an easy way.
	/// With this class we will be able to check for web.config key values.
	/// </summary>
	public class SettingsManager
	{
		/// <summary>
		/// This is a static private variable asociated with the Keys property for this class.
		/// </summary>
		private static SettingsList keys = new SettingsList();
		
		/// <summary>
		/// This static property represents a collection of all key registered as configuration parameters
		/// for the current application. Use this property insted of .Net classes due to this property manage
		/// the missing keys exceptions and register its into the Windows Event viewer.
		/// </summary>
		public static SettingsList Keys
		{
			get
			{
				return keys;
			}
		}
	

		/// <summary>
		/// This methos read a boolean value from the Keys collection extracted from the configuration
		/// file related to this aplication.  Note that this methos cast the registered key value, if 
		/// you spefify a value diferent than true or false it must throw a InvalidCastException.
		/// </summary>
		/// <param name="keyName">The boolean key value</param>
		/// <returns>true if there is a true value registered in the configuration, false if the key value if false.</returns>
		public static bool ReadBoolean(string keyName)
		{
			bool retval  = false;
			try
			{
				retval = bool.Parse(Keys[keyName]);
			}
			catch(Exception mie)
			{
				ExceptionHandler.Throw("Los valores permitidos para la configuración de la llave '"+ keyName +"' son (true|false). La excepción arrojada fue:" + mie.Message);
			}
			return retval;
		}

		
		/// <summary>
		/// This method read a Int32 value from the Keys collection extracted from the configuration
		/// file related to this aplication.  Note that this method cast the registered key value, if 
		/// you spefify an Int32 inconsistent value it must throw an InvalidCastException.
		/// </summary>
		/// <param name="keyName">The Int32 key name</param>
		/// <returns>true if there is a true value registered in the configuration, false if the key value if false.</returns>
		public static int ReadInt32(string keyName)
		{
			int retval = 0;

			try
			{
				retval = int.Parse(keyName);
			}
			catch(Exception mie)
			{
				ExceptionHandler.Throw("La llave de configuración '"+ keyName +"' debe contener valores enteros en el atributo value. Exepción:" + mie.Message);
			}

			return retval;
		}

		
		/// <summary>
		/// This method read a Decimal value from the Keys collection extracted from the configuration
		/// file related to this aplication.  Note that this method cast the registered key value, if 
		/// you spefify an Decimal inconsistent value it must throw an InvalidCastException.
		/// </summary>
		/// <param name="keyName">The Decimal key name</param>
		/// <returns>true if there is a true value registered in the configuration, false if the key value if false.</returns>
		public static decimal ReadDecimal(string keyName)
		{
			decimal retval = 0.0m;
			string v = Keys[keyName];
			try
			{
				retval = decimal.Parse(v);
			}
			catch(Exception mie)
			{
				ExceptionHandler.Throw("La llave '"+ keyName +"' debe contener un valor de tipo decimal. Exepción: " + mie.Message);

            }

			return retval;
		}

		
		/// <summary>
		/// This method build a string with the filename registered in the specified Key and use
		/// the ConfigurationFilesFolder key to locate that file name.
		/// </summary>
		/// <param name="keyName">The key which contains the file name to be processed</param>
		/// <returns>A path or directory for the file located in the configuration files folder defined 
		/// in the aplication settings file</returns>
		public static string GetConfigFilePath(string keyName)
		{
			return Path.Combine(keys["ConfigurationFilesFolder"], keys[keyName]);
		}

		public static string GetConfigFilePathByFileName(string fileName)
		{
			return Path.Combine(keys["ConfigurationFilesFolder"], fileName);
		}




		/// <summary>
		/// This method build a string with the filename registered in the specified Key and use
		/// the LogFilesFolder key to locate that file name.
		/// </summary>
		/// <param name="keyName">The key which contains the file name to be processed</param>
		/// <returns>A path or directory for the file located in the Log files folder defined 
		/// in the aplication settings file</returns>
		public static string GetLogFilePath(string keyName)
		{
			return Path.Combine(keys["LogFilesFolder"], keys[keyName]);
		}

	
		public static bool Exist(string keyName)
		{
            if (keys[keyName] != null)
                return true;
            else
                return false;
		}

	}
}
