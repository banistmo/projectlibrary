using System;
using System.IO;
using System.Xml;

//using java.io;
//using  java.util;
namespace ProjectLibrary
{
	#region Class CBizAgiUtils
	/// <summary>
	/// Public Class with all public methods
	/// </summary>
	public class CBizAgiUtils
	{
		/// <summary>
		/// Builds a PDF File
		/// </summary>
		/// <param name="sFileContent">Content of the FO File</param>
		/// <returns>sbyte Array, PDF File Content</returns>
		public static sbyte [] BuildPDFScopes(string sFileContent, string sPathFOP, bool nocopy, bool noedit, bool noannotations, bool noprint)
		{
			return CBizAgiReports.BuildPDFScopes(sFileContent,sPathFOP, nocopy, noedit, noannotations, noprint);
		}
		/// <summary>
		/// Builds a RTF File
		/// </summary>
		/// <param name="sFileContent">Content of the File</param>
		/// <returns>sbyte Array, RTF File Content</returns>
		public static sbyte [] BuildRTFScopes(string sFileContent)
		{
			return CBizAgiReports.BuildRTFScopes(sFileContent);
		}

		/// <summary>
		/// Gets the Name of the File embebed in a XML
		/// </summary>
		/// <param name="sXml">XML Content</param>
		/// <returns>Name of the File</returns>
		public static string GetInnerXml(string sXml, string xPath)
		{
			return CBizAgiInterfaces.GetInnerXml(sXml, xPath);
		}
		/// <summary>
		/// Gets the Content of the File embebed in a XML
		/// </summary>
		/// <param name="sXml">XML Content</param>
		/// <returns>Content of the File</returns>
		public static sbyte[] GetFileContent(string sFileContent)
		{
			return CBizAgiInterfaces.GetFileContent(sFileContent);
		}

        /// <summary>
        /// Converts an integer to string (spanish)
        /// </summary>
        /// <param name="sXml">Integer</param>
        /// <returns>Number in text (spanish)</returns>
        public static string NumToText(long value)
        {
            string Num2Text = "";
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + CBizAgiUtils.NumToText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + CBizAgiUtils.NumToText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = CBizAgiUtils.NumToText((long)(Math.Truncate((double)value / 10) * 10)) + " Y " + CBizAgiUtils.NumToText(value%10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + CBizAgiUtils.NumToText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = CBizAgiUtils.NumToText((long)(Math.Truncate((double)value / 100))) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = CBizAgiUtils.NumToText((long)Math.Truncate((double)value / 100) * 100) + " " + CBizAgiUtils.NumToText(value%100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + CBizAgiUtils.NumToText(value%1000);
            else if (value < 1000000)
            {
               Num2Text = CBizAgiUtils.NumToText((long)Math.Truncate((double)value / 1000)) + " MIL";
               if ((value%1000) > 0) Num2Text = Num2Text + " " + CBizAgiUtils.NumToText(value%1000);
            }
            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + CBizAgiUtils.NumToText(value%1000000);
            else if (value < 1000000000000)
            {
               Num2Text = CBizAgiUtils.NumToText((long)Math.Truncate((double)value / 1000000)) + " MILLONES";
               if ((value - Math.Truncate((double)value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + CBizAgiUtils.NumToText(value - ((long)Math.Truncate((double)value / 1000000) * 1000000));
            }
            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + CBizAgiUtils.NumToText(value - (long)Math.Truncate((double)value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = CBizAgiUtils.NumToText((long)Math.Truncate((double)value / 1000000000000)) + " BILLONES";
                if ((value - (long)Math.Truncate((double)value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + CBizAgiUtils.NumToText(value - (long)Math.Truncate((double)value / 1000000000000) * 1000000000000);
            }
        return Num2Text;            
        }
	
	}
	#endregion
	#region Class CBizAgiReports
	/// <summary>
	/// Creates PDF, HTML, RTF Files using XML as Input
	/// </summary>
	class CBizAgiReports
	{
		#region Internal Methods
		/// <summary>
		/// Builds a PDF File
		/// </summary>
		/// <param name="sFileContent">Content of the FO File</param>
		/// <returns>sbyte Array, PDF File Content</returns>
		internal static sbyte [] BuildPDFScopes(string sFileContent, string sPathFOP, bool nocopy, bool noedit, bool noannotations, bool noprint)
		{				
			string fopPath = sPathFOP;
			string foPath = System.IO.Path.GetTempPath() + "tmp"+DateTime.Now.Minute+DateTime.Now.Second+DateTime.Now.Millisecond+".fo";
			string pdfPath = System.IO.Path.GetTempPath() + "tmp"+DateTime.Now.Minute+DateTime.Now.Second+DateTime.Now.Millisecond+".pdf";

			StreamWriter tmpWriter = new StreamWriter(foPath);
			tmpWriter.Write(sFileContent);
			tmpWriter.Close();

			try
			{
				
				BuildPDF(sPathFOP, foPath, pdfPath,  nocopy, noedit, noannotations, noprint);

		
				FileStream fs = new FileStream(pdfPath, FileMode.Open);
				//int Size = 50000;
				byte []data = CBizAgiIO.ReadFully(fs);
				//int b = fs.Read(data,0,Size);
				
				
				//while(b!=0)
				//	b = fs.Read(data,0,Size);					
				
				fs.Close();

				sbyte[] s = new sbyte[data.Length];
				System.Buffer.BlockCopy(data, 0, s, 0, data.Length);

				
				System.IO.File.Delete(foPath);
				System.IO.File.Delete(pdfPath);

				return s;
							
			} 
			catch (Exception ex) 
			{
				throw new ApplicationException (ex.Message);
			} 
		}

		/// <summary>
		/// Builds a RTF File
		/// </summary>
		/// <param name="sFileContent">Content of the File</param>
		/// <returns>sbyte Array, RTF File Content</returns>
		internal static sbyte [] BuildRTFScopes(string sFileContent)
		{
			try
			{
				sFileContent = ReplaceEspecialCharacters(sFileContent);

				string tmpPathandFileName= System.IO.Path.GetTempPath() + "tmp"+DateTime.Now.Minute+".rtf";
				StreamWriter tmpWriter = new StreamWriter(tmpPathandFileName);
				tmpWriter.Write(sFileContent);
				tmpWriter.Close();

				FileStream fs = new FileStream(tmpPathandFileName, FileMode.Open);
				
				byte []data = CBizAgiIO.ReadFully(fs);
					//new byte[Size];
				//int b = fs.Read(data,0,Size);
				
				//while(b!=0)
				//	b = fs.Read(data,0,Size);					
				
				fs.Close();

				sbyte[] s = new sbyte[data.Length];
				System.Buffer.BlockCopy(data, 0, s, 0, data.Length);

				System.IO.File.Delete(tmpPathandFileName);

				return s;
			} 
			catch (Exception ex) 
			{
				throw new ApplicationException (ex.Message);
			} 
		}
	
		#endregion
		#region Support Methods
		/// <summary>
		/// Replace Special characters for RTF Files
		/// </summary>
		/// <param name="sFileRTFContents">Content of the File</param>
		/// <returns>Content of the File with all special characters replaced</returns>
		private static string ReplaceEspecialCharacters(string sFileRTFContents)
		{
			try
			{
				sFileRTFContents = sFileRTFContents.Replace("�","\\'e1");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'e9");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ed");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'f3");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'fa");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'f1");

				sFileRTFContents = sFileRTFContents.Replace("�","\\'c1");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'c9");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'cd");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'d3");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'da");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'d1");

				sFileRTFContents = sFileRTFContents.Replace("�","\\'e4");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'eb");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ef");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'f6");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'fc");

				sFileRTFContents = sFileRTFContents.Replace("�","\\'c4");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'cb");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'cf");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'d6");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'dc");

				sFileRTFContents = sFileRTFContents.Replace("�","\\'e2");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ea");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ee");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'f4");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'fb");

				sFileRTFContents = sFileRTFContents.Replace("�","\\'c2");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ca");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'ce");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'d4");
				sFileRTFContents = sFileRTFContents.Replace("�","\\'db");

				return sFileRTFContents;
			}
			catch (Exception ex) 
			{
				throw new ApplicationException ("RTF-Replacing Special Characters: " + ex.Message);
			} 

		}
	
		/// <summary>
		/// Calls FO.BAT to generate PDF File
		/// </summary>
		/// <param name="sFopPath">Path of FO.BAT</param>
		/// <param name="sFoPath">Path of FO File</param>
		/// <param name="sPdfPath">Path of PDF File</param>
		private static void BuildPDF(string sFopPath, string sFoPath, string sPdfPath,  bool nocopy, bool noedit, bool noannotations, bool noprint)
		{
			try
			{
				string securityOptions = "";
				if(noedit)
					securityOptions += " -noedit";
				if(nocopy)
					securityOptions += " -nocopy";
				if(noprint)
					securityOptions += " -noprint";
				if(noannotations)
					securityOptions += " -noannotations";

				System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
				si.FileName = sFopPath;						
				si.Arguments = securityOptions +  " \""+sFoPath+"\"" + " " + "\""+sPdfPath+"\"";
				si.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
				si.UseShellExecute = false;
				System.Diagnostics.Process p = new System.Diagnostics.Process();				
				p.StartInfo = si;
				p.Start();
				p.WaitForExit();	
			}
			catch(Exception ex)
			{
				throw new ApplicationException ("Error calling FOP.bat: " + ex.Message);
			}

		}

 	#endregion
	}
	#endregion
	#region Class CBizAgiInterfaces
	/// <summary>
	/// Interface Methods
	/// </summary>
	class CBizAgiInterfaces
	{
		#region Internal Methods

		/// <summary>
		/// Gets the Content of the File embebed in a XML
		/// </summary>
		/// <param name="sXml">XML Content</param>
		/// <returns>Content of the File</returns>
		internal static sbyte[] GetFileContent(string sFileContent)
		{
			try
			{
				byte[] byteContent = new byte[sFileContent.Length];
				byteContent = Convert.FromBase64String(sFileContent);

				sbyte[] sbyteContent = new sbyte[byteContent.Length];
				System.Buffer.BlockCopy(byteContent, 0, sbyteContent, 0, sbyteContent.Length);

				return sbyteContent;
			}
			catch (Exception ex) 
			{
				throw new ApplicationException (ex.Message);
			} 
		}
		/// <summary>
		/// Gets the Name of the File embebed in a XML
		/// </summary>
		/// <param name="sXml">XML Content</param>
		/// <returns>Name of the File</returns>
		internal static string GetInnerXml(string sXml, string xPath)
		{
			try
			{
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(sXml);
				return xmlDoc.SelectSingleNode(xPath).InnerText;
			}
			catch (Exception ex) 
			{
				throw new ApplicationException (ex.Message);
			} 
		}
		#endregion
	}
	#endregion
	#region Class CBizAgiIO
	/// <summary>
	/// Public Class for files and directories IO operations
	/// </summary>
	public class CBizAgiIO
	{
		public static FileInfo [] GetFilesList(string sPath)
		{
			DirectoryInfo di;
			FileInfo [] fi;
			if(Directory.Exists(sPath))
			{
				di = new DirectoryInfo(sPath);
				fi = di.GetFiles();
			}
			else
			{
				fi = null;
			}
			return fi;
		}
		public static sbyte [] GetFileContent(string sPath)
		{
			FileStream fs = new FileStream(sPath, FileMode.Open);
			//int Size = fs.Length;
			byte []data = ReadFully(fs);
			//int b = fs.Read(data,0,Size);
				
			//while(b!=0)
			//	b = fs.Read(data,0,Size);					

			sbyte[] s = new sbyte[data.Length];
			fs.Close();
			System.Buffer.BlockCopy(data, 0, s, 0, data.Length);

			return s;
		}
		public static byte[] ReadFully (Stream stream)
		{
			byte[] buffer = new byte[32768];
			using (MemoryStream ms = new MemoryStream())
			{
				while (true)
				{
					int read = stream.Read (buffer, 0, buffer.Length);
					if (read <= 0)
						return ms.ToArray();
					ms.Write (buffer, 0, read);
				}
			}
		}
	}
	#endregion
}