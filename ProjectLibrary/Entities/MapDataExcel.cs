﻿/* COPYRIGHT - BIZAGI
 * Autor: Diego David Pandales Perez
 * Date: DIC-2007
 */

using System.Xml.Serialization;
namespace ProjectLibrary.Entities
{

	/// <summary>
	/// Class utils to mainupulate excel files
	/// </summary>
	
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.bizagi.com/schemas/MapDataExcel.xsd")]
	[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.bizagi.com/schemas/MapDataExcel.xsd", IsNullable=false)]
	[System.Xml.Serialization.XmlInclude(typeof(CMapDataExcel ))]
	public class CConfigMapDataExcel 
	{
		private CMapDataExcel [] _MapsConfig;
		public CMapDataExcel [] MapsConfig
		{
			get{return _MapsConfig;}
			set{_MapsConfig=value;}

		}

	
	}

	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.bizagi.com/schemas/MapDataExcel.xsd")]
	[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.bizagi.com/schemas/MapDataExcel.xsd", IsNullable=false)]
	[System.Xml.Serialization.XmlInclude(typeof(CMapDataExcel ))]
	public class CMapDataExcel 
	{
		
		/// <remarks/>
		/// Name Sheet in excel file, should correspond to TableName in dataset
		/// </remarks>
		private string _NameSheet;
		public string NameSheet
		{
			get {return _NameSheet;}
			set{_NameSheet=value;}
		}
		/// <remarks/>
		private CFieldDataExcel [] _Fields;
		/// <summary>
		/// Set fields to map dataset vs excel file
		/// </summary>
		public CFieldDataExcel [] Fields
		{
			get
			{
				return _Fields;
			}
			set
			{
				_Fields=value;
			}
		}

		private string _cellOffset="0";

		/// <remarks/>
		/// Cell in sheet  begin to fill the DataTable
		/// </remarks>
		public string CellOffset
		{
			get
			{
				return _cellOffset;
			}
			set
			{
				_cellOffset=value;
			}
		}
	}

	/// <remarks/>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.bizagi.com/schemas/MapDataExcel.xsd")]
	
	[System.Xml.Serialization.XmlInclude(typeof(CFieldDataExcel ))]
	public class CFieldDataExcel 
	{
    
		private string _NameCell;
		/// <remarks>
		/// Cell name in excel file
		///</remarks>
		
		public string NameCell
		{
			get{return _NameCell;}
			set{_NameCell=value;}
		}
    
		/// <remarks/>
		/// Name Field in DataSet
		/// </remarks>
		private string _NameData;
		public string NameData
		{
			get{return _NameData;}
			set{_NameData=value;}
		}
    
		/// <remarks/>
		private string _idCell;
		/// <remarks/>
		/// ID Cell in excel file  (i.e: A1, B12...)
		/// </remarks>
		public string IdCell
		{
			get{return _idCell;}
			set{_idCell=value;}
		}
    
		
	}
}