/* COPYRIGHT - BIZAGI
 * Autor: Diego David Pandales Perez
 * Date: NOV-2007
 */
using System;
using System.Web;
using ProjectLibrary.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Web.Configuration;

namespace ProjectLibrary.Components
{
	/// <summary>
	/// Class utilities
	/// </summary>
	public class Utils
	{
		public Utils()
		{
		}


		public static string RootApplication()
		{
			if(HttpContext.Current!=null)
				return HttpContext.Current.Request.ApplicationPath;
			else
				return SettingsManager.Keys["AppBaseDirectory"];;
		}


		public static string GetUNCPathFromKeyName(string keyname)
		{
			if(HttpContext.Current!= null)
			{
				return HttpContext.Current.Server.MapPath(RootApplication() + SettingsManager.Keys[keyname]);
			}
			else
			{
				return SettingsManager.Keys["AppBaseDirectory"] + SettingsManager.Keys[keyname] ;
			}
		}


		public static string Server()
		{
			return HttpContext.Current.Server.MachineName;
		}

		public static string UrlToUNC(string url)
		{
			string Unc;
			try
			{
				Unc = HttpContext.Current.Server.MapPath(url);
			}
			catch
			{
				Unc = null;
			}
			return Unc;
		}

		public static string ConvertToDB(object fieldParameter)
		{
			string returnedString = "";
			//			Type objecttype = fieldParameter.GetType();
			switch(fieldParameter.GetType().Name)
			{
				case  "Single":
					float floatfield = (float)fieldParameter;
					if(floatfield==int.MinValue)
						returnedString = " NULL ";
					else
						returnedString = floatfield.ToString();
					break;
				case "Int32":
					int intfield = (int)fieldParameter;
					if(intfield==int.MinValue)
						returnedString = " NULL ";
					else
						returnedString = intfield.ToString();
					break;
				case "DateTime":
					DateTime datetimefield = (DateTime) fieldParameter;
					if(datetimefield.ToString("yyyy-MM-dd")=="0001-01-01")
						returnedString =" NULL ";
					else
						returnedString= "'"+datetimefield.ToString("yyyy-MM-dd")+"'";
					break;
			}
			return returnedString;
		}

        /// <summary>
        /// Funci�n para escribir un archivo de log
        /// </summary>
        /// <param name="identificadorLog">Nombre del texto a escribir</param>
        /// <param name="textoLog">Texto a escribir en el log</param>
        /// <param name="rutaLog">Ruta del archivo</param>
        public static void LogFile(string identificadorLog, string textoLog)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(WebConfigurationManager.AppSettings["rutaLogReportes"].ToString()))
                {
                    sw.WriteLine("Fecha:" + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString());
                    sw.WriteLine(identificadorLog);
                    sw.WriteLine(textoLog);
                    sw.WriteLine("-----------------------------------------------------------------------------------------------------------------------------");
                    sw.WriteLine("-----------------------------------------------------------------------------------------------------------------------------");
                    sw.Dispose();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Funci�n para escribir un archivo de log
        /// </summary>
        /// <param name="textoLog">Texto a escribir en el log</param>
        /// <param name="rutaLog">Ruta del archivo</param>
        public static void LogFile(string textoLog)
        {
            if (CrearLog())
            {
                try
                {
                    using (StreamWriter sw = File.AppendText(WebConfigurationManager.AppSettings["rutaLogReportes"].ToString()))
                    {
                        sw.WriteLine("Fecha:" + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString());
                        sw.WriteLine(textoLog);
                        sw.WriteLine("-----------------------------------------------------------------------------------------------------------------------------");
                        sw.WriteLine("-----------------------------------------------------------------------------------------------------------------------------");
                        sw.Dispose();
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Funci�n para determinar si se crea el log
        /// </summary>
        /// <returns>Booleano</returns>
        private static bool CrearLog()
        {
            bool generarLogReportes = false;
            if (!String.IsNullOrEmpty(WebConfigurationManager.AppSettings["generarLogReportes"]))
            {
                generarLogReportes = bool.Parse(WebConfigurationManager.AppSettings["generarLogReportes"].ToString());
            };
            return generarLogReportes;
        }

        /// <summary>
        /// Funci�n para remover acentos
        /// </summary>
        /// <param name="text">Texto a evaluar, para remover los acentos que contengan</param>
        /// <returns>Retorna el texto sin los acentos</returns>
        public static string RemoveAccents(string text)
        {
            return text.Replace("�", "A").Replace("�", "E").Replace("�", "I").Replace("�", "O").Replace("�", "U").Replace("�", "a").Replace("�", "e").Replace("�", "i").Replace("�", "o").Replace("�", "u").Replace("�", "N").Replace("�", "n").Replace("�", "").Replace("!", "").Replace("�", "").Replace("?", "");
        }

        /// <summary>
        /// Funci�n para obtener la configuraci�n de la llave ReportsClientFormatDate
        /// </summary>
        /// <returns>Retorna la configuraci�n de la llave ReportsClientFormatDate</returns>
        public static string GetReportsClientFormatDate()
        {
            string reportsClientFormatDate = "dd/MM/yyyy";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsClientFormatDate"] != null)
            {
                reportsClientFormatDate = Configuration.SettingsManager.Keys["ReportsClientFormatDate"];
            }
            return reportsClientFormatDate;
        }

        #region Serialize and Deserialize

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private static string UTF8ByteArrayToString(byte[] characters)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			string constructedString = encoding.GetString(characters);
			constructedString = constructedString.Remove(0, constructedString.IndexOf('<')); //eliminar caracteres no legibles binarios
			return (constructedString);
		}

		/// <summary>
		/// Converts the String to UTF8 Byte array and is used in De serialization
		/// </summary>
		/// <param name="pXmlString"></param>
		/// <returns></returns>
		private static Byte[] StringToUTF8ByteArray(string pXmlString)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		}

		/// <summary>
		/// Serialize an object into an XML string
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static string SerializeObject(Object obj)
		{
				
				string xmlString = null;
				MemoryStream memoryStream = new MemoryStream();
				XmlSerializer xs = new XmlSerializer(obj.GetType());
				StreamWriter xmlTextWriter = new StreamWriter(memoryStream, Encoding.UTF8);
				xs.Serialize(xmlTextWriter, obj);
				memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
				xmlString = UTF8ByteArrayToString(memoryStream.ToArray()); return xmlString;
			
		}

		/// <summary>
		/// Reconstruct an object from an XML string
		/// </summary>
		/// <param name="xml"></param>
		/// <returns></returns>
		public static Object  DeserializeObject(string xml,System.Type objType)
		{
			XmlSerializer xs = new XmlSerializer(objType);
			MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));
			XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
			return xs.Deserialize(memoryStream);
		}

		#endregion

	}
}
