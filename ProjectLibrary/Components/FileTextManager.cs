using System;
using System.Data;
using System.IO;
using ProjectLibrary;
namespace ProjectLibrary.Components
{
	/// <summary>
	/// Summary description for FileTextManager.
	/// </summary>
	public class FileTextManager
	{
		public FileTextManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		private static System.Text.Encoding _Encoding=null;
		protected static System.Text.Encoding Encoding
		{
			get
			{ 
				if( _Encoding==null) 
					return System.Text.Encoding.ASCII ;
				else 
					return _Encoding;
			}
			set
			{
				_Encoding=	value;
			}
		}
		
		private static DataTable GetFileTextContentWithSchema(string [] strSchema,string strPathFile )
		{
		
			TextReader myReader ;
			
			string strLine;
			DataTable dtFilesContent= new DataTable("ContentFiles");
			DataRow rowLine;	
			//Get number of columns to data table content
			int iColumns=strSchema.Length;
				
			//Build Columns in file content data table
			for(int count=0; count<iColumns; count++)
				dtFilesContent.Columns.Add("Field"+Convert.ToString(count+1));
			
		

			try
			{
				//Open file and read first line to determine how many fields there are.
				myReader = new StreamReader(strPathFile,Encoding ,false);
			

				while((	strLine= myReader.ReadLine())!=null)
				{
					if(strLine!="")
					{
						//Create data columns accordingly
						rowLine= dtFilesContent.NewRow();
						rowLine.BeginEdit();
						for( int i=0; i<iColumns; i++)
						{
					
							rowLine[i]=strLine.Substring(0,int.Parse(strSchema[i]));
							strLine=strLine.Remove(0,int.Parse(strSchema[i]));
					
						}
						rowLine.EndEdit();

						dtFilesContent.Rows.Add(rowLine);
						
					}
				}//while( myReader.Peek() != -1);
				myReader.Close();
				myReader=null;
				return dtFilesContent;
			}
			catch (Exception exc)
			{
				ExceptionHandling.ExceptionHandler.ThrowNoLog(ExceptionHandling.ExceptionTypes.Data,"Error abriendo/leyendo el archivo "+ strPathFile + "\\n"+exc.Message );
				
				myReader=null;
				return null;
			}
			finally
			{

				myReader=null;
			
			}
			


		}
		
        public static DataTable GetFileTextContent(byte[] FileBytes, string sEncoding, string sSeparator, int iColumns)
        {

            TextReader myReader;

            switch (sEncoding.ToUpper())
            {
                case "ASCII": Encoding = System.Text.Encoding.ASCII; break;
                case "UNICODE": Encoding = System.Text.Encoding.Unicode; break;
                case "UTF7": Encoding = System.Text.Encoding.UTF7; break;
                case "UTF8": Encoding = System.Text.Encoding.UTF8; break;
                default: Encoding = System.Text.Encoding.ASCII; break;
            }

            string strLine;
            DataTable dtFilesContent = new DataTable("ContentFiles");
            DataRow rowLine;
            //Get number of columns to data table content
            //int iColumns = strSchema.Length;

            //Build Columns in file content data table
            for (int count = 0; count < iColumns; count++)
                dtFilesContent.Columns.Add("Field" + Convert.ToString(count));
            
            dtFilesContent.Columns.Add("Line");
            dtFilesContent.Columns.Add("Error");
            dtFilesContent.Columns.Add("ErrorDetail");
            char[] delimiters;
            switch (sSeparator.ToUpper())
            {
                case "COMA": delimiters = new char[] { ',' }; break;
                case "TAB": delimiters = new char[] {'\t'}; break;
                case "PIPE": delimiters = new char[] { '|' }; break;
                case "PUNTO Y COMA": delimiters = new char[] { ';' }; break;
                default: delimiters = new char[] { '\t' }; break;
            }            

            try
            {
                myReader = new StreamReader(new System.IO.MemoryStream(FileBytes), Encoding, false);

                while ((strLine = myReader.ReadLine()) != null)
                {
                    if (strLine != "")
                    {
                        String[] segments = strLine.Split(delimiters, StringSplitOptions.None);

                        if (segments.Length == iColumns)
                        {
                            //Create data columns accordingly
                            rowLine = dtFilesContent.NewRow();
                            rowLine.BeginEdit();
                            for (int i = 0; i < iColumns; i++)
                            {
                                rowLine[i] = segments[i].Trim();
                            }
                            rowLine[iColumns] = strLine;
                            rowLine.EndEdit();

                            dtFilesContent.Rows.Add(rowLine);
                        }
                        else
                        {
                            //Create data columns accordingly
                            rowLine = dtFilesContent.NewRow();
                            rowLine.BeginEdit();
                            rowLine[iColumns] = strLine;
                            rowLine[iColumns+1] = "1";
                            rowLine[iColumns + 2] = "N�mero de columnas incorrecto: " + segments.Length.ToString();
                            rowLine.EndEdit();
                            dtFilesContent.Rows.Add(rowLine);                        
                        }

                    }
                }
                myReader.Close();
                myReader = null;
                return dtFilesContent;
            }
            catch (Exception exc)
            {
                ExceptionHandling.ExceptionHandler.ThrowNoLog(ExceptionHandling.ExceptionTypes.Data, "Error abriendo/leyendo el archivo " + exc.Message);

                myReader = null;
                return null;
            }
            finally
            {

                myReader = null;

            }

        }

        public static DataTable GetFileTextContentFixWidth(byte[] FileBytes, string sEncoding, string sSchema, int iColumns)
        {

            TextReader myReader;
            char[] delimiters = new char[] { ',' };
            string[] strSchema = sSchema.Split(delimiters);
            int iLineSize = 0;
            for (int i = 0; i < strSchema.Length; i++)
            {
                iLineSize +=  Convert.ToInt32(strSchema[i]);
            }
            switch (sEncoding.ToUpper())
            {
                case "ASCII": Encoding = System.Text.Encoding.ASCII; break;
                case "UNICODE": Encoding = System.Text.Encoding.Unicode; break;
                case "UTF7": Encoding = System.Text.Encoding.UTF7; break;
                case "UTF8": Encoding = System.Text.Encoding.UTF8; break;
                default: Encoding = System.Text.Encoding.ASCII; break;
            }

            string strLine;
            DataTable dtFilesContent = new DataTable("ContentFiles");
            DataRow rowLine;
            //Get number of columns to data table content
            //int iColumns = strSchema.Length;

            //Build Columns in file content data table
            for (int count = 0; count < iColumns; count++)
                dtFilesContent.Columns.Add("Field" + Convert.ToString(count));

            dtFilesContent.Columns.Add("Line");
            dtFilesContent.Columns.Add("Error");
            dtFilesContent.Columns.Add("ErrorDetail");
            
            try
            {
                myReader = new StreamReader(new System.IO.MemoryStream(FileBytes), Encoding, false);

                while ((strLine = myReader.ReadLine()) != null)
                {
                    if (strLine != "")
                    {
                        if (strLine.Length == iLineSize)
                        {
                            rowLine = dtFilesContent.NewRow();
                            rowLine.BeginEdit();
                            rowLine[iColumns] = strLine;
                            for (int i = 0; i < iColumns; i++)
                            {

                                rowLine[i] = strLine.Substring(0, int.Parse(strSchema[i]));
                                strLine = strLine.Remove(0, int.Parse(strSchema[i]));

                            }
                            rowLine.EndEdit();

                            dtFilesContent.Rows.Add(rowLine);
                        }
                        else
                        {
                            //Create data columns accordingly
                            rowLine = dtFilesContent.NewRow();
                            rowLine.BeginEdit();
                            rowLine[iColumns] = strLine;
                            rowLine[iColumns + 1] = "1";
                            rowLine[iColumns + 2] = "Tama�o de l�nea incorrecto: " + strLine.Length.ToString();
                            rowLine.EndEdit();
                            dtFilesContent.Rows.Add(rowLine);
                        }
                    }
                }
                myReader.Close();
                myReader = null;
                return dtFilesContent;
            }
            catch (Exception exc)
            {
                ExceptionHandling.ExceptionHandler.ThrowNoLog(ExceptionHandling.ExceptionTypes.Data, "Error abriendo/leyendo el archivo " + exc.Message);

                myReader = null;
                return null;
            }
            finally
            {

                myReader = null;

            }

        }

        public static bool SaveDatatableToPlainText(DataTable datatable, string file, string delimited, bool exportcolumnsheader)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter str = new StreamWriter(ms, System.Text.Encoding.Default);
            if (exportcolumnsheader)
            {
                string Columns = string.Empty;
                foreach (DataColumn column in datatable.Columns)
                {
                    Columns += column.ColumnName + delimited;
                }
                str.WriteLine(Columns.Remove(Columns.Length - 1, 1));
            }
            foreach (DataRow datarow in datatable.Rows)
            {
                string row = string.Empty;
                foreach (object items in datarow.ItemArray)
                {
                    row += items.ToString().Replace("|","") + delimited;
                }
                str.WriteLine(row.Remove(row.Length - 1, 1));
            }
            str.Flush();
            FileStream oFile = new FileStream(file, FileMode.Create, FileAccess.Write);
            ms.WriteTo(oFile);
            oFile.Close();
            ms.Close();
            str.Close();
            return true;
        }
	}
}
