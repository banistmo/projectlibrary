/* COPYRIGHT - BIZAGI
 * Autor: Diego David Pandales Perez
 * Date: DIC-2007
 */
using System;
//using CarlosAg.ExcelXmlWriter;
//using CarlosAg.ExcelXmlWriter.Schemas;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
namespace ProjectLibrary.Components
{
	/// <summary>
	/// Class to manipulate excel files, fill with datasources.
	/// </summary>
	public class ExcelUtils: IDisposable
	{
		
		#region Properties
		private string _connectionString ;
		private string _pathFile;
		private bool disposing;
		OleDbConnection _connection ;
		public  string ConnectionString
		{
			get
			{
				return @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+PathFile+";Extended Properties=\"Excel 8.0;HDR=YES;\"";
			}
			set
			{
				_connectionString=value;
			}
		}

		public  string ConnectionStringNoHeader
		{
			get
			{
				return @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+PathFile+";Extended Properties=\"Excel 8.0;HDR=NO;\"";
			}
		
		}
		public  string PathFile
		{
			get
			{
				return _pathFile;
			}
			set
			{
				_pathFile=value;
			}
		}
		#endregion

		#region Constructors

		/// <summary>
		/// Constructor create physical file with binary file
		/// </summary>
		/// <param name="binaryFile">content binary file</param>
		public ExcelUtils(string pathFile,byte [] binaryFile)
		{
			this.PathFile=pathFile;
			this.CreateFile(binaryFile);
			_connection= new OleDbConnection(this.ConnectionString);
		}

		/// <summary>
		/// Constructor with the pathfile, assume file exist 
		/// </summary>
		
		public ExcelUtils(string pathFile)
		{
			this.PathFile=pathFile;
			_connection= new OleDbConnection(this.ConnectionString);
		}

		/// <summary>
		/// Constructor with the pathfile, assume file exist 
		/// </summary>
		
		public ExcelUtils()
		{
			
			//FileInfo fi=new FileInfo(Configuration.SettingsManager.Keys["PathDataSetExcelFile"]);
			string strPath=System.IO.Path.GetTempPath() + "tempExcelUtils"+DateTime.Now.ToUniversalTime().ToString("MMddyyyyhhmmss") +".xls";
			File.Copy(Configuration.SettingsManager.Keys["PathDataSetExcelFile"],strPath,true);
			this.PathFile=strPath;
			_connection= new OleDbConnection(this.ConnectionString);
		}

		#endregion
		
		#region Private Methods
		/// <summary>
		/// Generate file excel with binari content
		/// </summary>
		/// <param name="binaryFile">conten binary file</param>
		private void CreateFile(byte[] binaryFile)
		{
			try
			{
				Stream stream = new FileStream(this.PathFile, FileMode.Open, FileAccess.Write);
				BinaryWriter writer = new BinaryWriter(stream);
				writer.Write(binaryFile);
				writer.Close();
			}
			catch
			{
				ExceptionHandling.ExceptionHandler.ThrowNoLog(ExceptionHandling.ExceptionTypes.FileSystemAccess,"Ocurrio un error creando el archivo de excel");
			}


		
		}
		#endregion

		#region Public Methods
		/*	/// <summary>
			/// Create a file with datasource
			/// </summary>
			/// <returns>Path to file</returns>
			public static string CreateBookWithDataset(DataSet dsData, string path)
			{
				string [] hojas= new string [dsData.Tables.Count];
				WorksheetRow row ;
				Worksheet sheet;
				Workbook book = new Workbook();
				book.Properties.Author="BizAgi";
				book.Properties.Company="Vision Software S.A";
				
				// Add some styles to the Workbook
				WorksheetStyle style = book.Styles.Add("HeaderStyle");
				style.Font.FontName = "Tahoma";
				style.Font.Size = 14;
				style.Font.Bold = true;
				style.Alignment.Horizontal = StyleHorizontalAlignment.Center;
				style.Font.Color = "White";
				style.Interior.Color = "Blue";
				style.Interior.Pattern = StyleInteriorPattern.DiagCross;

				foreach(DataTable dt in dsData.Tables)
				{
					//make sheet by table in dataset
					sheet = book.Worksheets.Add(dt.TableName);
					//make header = columns name
					row =  sheet.Table.Rows.Add();
			
				
				
					foreach(DataColumn dc in dt.Columns)
					{
						row.Cells.Add(new WorksheetCell(dc.ColumnName,"HeaderStyle"));
					}
				
					//fill sheet witdh value columns in table
					foreach (DataRow dr in dt.Rows)
					{
						row =  sheet.Table.Rows.Add();
						for(int i=0; i<dt.Columns.Count;i++)
						{		
							row.Cells.Add(dr[i].ToString());
						}
					}
											  
				
				}
			
			
				book.Save(path);
			
				return path;
		
			}*/

		

		public  DataSet ReadExcel(string query)
		{
			OleDbCommand command = new OleDbCommand(query,_connection);
			OleDbDataAdapter adapter= new OleDbDataAdapter(command);
			DataSet ds= new DataSet();
			adapter.Fill(ds);
			return ds;
		}


		private  void ExecuteExcel( string queryExec)
		{
			
			
			
			_connection.Open();
			try
			{
				OleDbCommand command = new OleDbCommand(queryExec,_connection);
				command.ExecuteNonQuery();
			}
			catch(Exception exc)
			{
				throw new ApplicationException("Ocurrio un error realizando la actualizacion al archivo:  " +queryExec,exc);
			}
			finally
			{
				_connection.Close();
			}
					
		}

		/// <summary>
		/// Fill excel fiels with dataset
		/// </summary>
		/// <param name="dsData">Data</param>
		/// <param name="xmlDocFields">Configs Parameters</param>
		public  void DataSetToExcel(DataSet dsData, System.Xml.XmlDocument xmlDocFields)
		{
			
			Entities.CConfigMapDataExcel   objMapDataExcel=  (Entities.CConfigMapDataExcel )Utils.DeserializeObject(xmlDocFields.OuterXml, typeof(Entities.CConfigMapDataExcel));
			
			System.Text.StringBuilder query;
			System.Text.StringBuilder fields;
			System.Text.StringBuilder values;
			
			DataTable dt;
			foreach (Entities.CMapDataExcel objMap in objMapDataExcel.MapsConfig)
			{
				dt= dsData.Tables[objMap.NameSheet];
				
				foreach(DataRow dr  in dt.Rows)
				{
					query= new System.Text.StringBuilder( "insert into ["+objMap.NameSheet+"$] (");
					fields= new System.Text.StringBuilder();
					values=new System.Text.StringBuilder();
			
					for (int i=0; i< objMap.Fields.Length; i++)
					{
						fields.AppendFormat("{0},",objMap.Fields[i].NameCell); 
						values.AppendFormat("{0},","\"" + dr[objMap.Fields[i].NameData].ToString() +"\""); 

									
					
				
					}
					query.Append(fields.ToString().Remove(fields.ToString().Length-1,1 ));
					query.Append(") values (");
					query.AppendFormat("{0} )",values.ToString().Remove(values.ToString().Length-1,1 ));
					this.ExecuteExcel(query.ToString());
				}
		
			}
			
		}

		/// <summary>
		/// Update Cell excel fields with dataset, using optionally celloffset to fill
		/// </summary>
		/// <param name="dsData">Data</param>
		/// <param name="xmlDocFields">Configs Parameters</param>
		public  sbyte[] DataSetToExcelUpdate(DataSet dsData, System.Xml.XmlDocument xmlDocFields)
		{
			
			Entities.CConfigMapDataExcel   objMapDataExcel=  (Entities.CConfigMapDataExcel )Utils.DeserializeObject(xmlDocFields.OuterXml, typeof(Entities.CConfigMapDataExcel));
			
			this._connection.ConnectionString=this.ConnectionStringNoHeader;

			System.Text.StringBuilder query;
			System.Text.StringBuilder fields;
			System.Text.StringBuilder values;
			
			DataTable dt;
			string cell;
			int cellInt;
			int regsCount;
			string cellFirst;
			char cellChar;
			string celloffset;
			foreach (Entities.CMapDataExcel objMap in objMapDataExcel.MapsConfig)
			{
				dt= dsData.Tables[objMap.NameSheet];
				
				cell=objMap.CellOffset;
				regsCount=0;
				

				foreach(DataRow dr  in dt.Rows)
				{
					cellFirst="";
			
					for (int i=0; i< objMap.Fields.Length; i++)
					{
						query= new System.Text.StringBuilder( "UPDATE ["+objMap.NameSheet +"$");
						fields= new System.Text.StringBuilder();
						values=new System.Text.StringBuilder();
					
						if(cell=="0") //si no tiene offset
						{
							fields.AppendFormat("{0}:{1}",objMap.Fields[i].IdCell,objMap.Fields[i].IdCell); 
						}
						else //si tiene offset es para rellenar apartir del offset, se ignora el idCell se usa el offset
						{
							cell=objMap.Fields[i].IdCell;
							if(Char.IsNumber(  cell.Substring(1,1).ToCharArray()[0])) //es una sola letra i.g A1, B12
							{
								/*cellInt=(int)Char.Parse(cell.Substring(0,1))+i;
								cellChar=(char)cellInt;
								
								if(cellChar >'Z')
								{
									if(cellFirst!="")
										cellFirst= Convert.ToString(((char)(((int)Char.Parse(cellFirst) )+ 1)));
									else
										cellFirst="A";

									cellChar='A';
								}*/
								cellInt=(int)Char.Parse(cell.Substring(0,1));
								cellChar=(char)cellInt;

								celloffset=cellFirst+ cellChar.ToString()+  Convert.ToString((int.Parse(cell.Substring(1))+regsCount));
								fields.AppendFormat("{0}:{1}",celloffset,celloffset); 
							}
							else //son dos letras AA12, BA1
							{

								//cellInt=(int)Char.Parse(cell.Substring(1,1))+i;
								cellInt=(int)Char.Parse(cell.Substring(1,1));
								cellChar=(char)cellInt;
								cellFirst=cell.Substring(0,1) ;

								/*if(cellChar >'Z')
								{
										cellFirst= Convert.ToString(((char)(((int)Char.Parse(cellFirst) )+ 1)));
									cellChar='A';
								}*/

								celloffset=cellFirst+cellChar.ToString()+  Convert.ToString((int.Parse(cell.Substring(2))+regsCount));
								fields.AppendFormat("{0}:{1}",celloffset,celloffset); 
							}


						}
						
						values.AppendFormat(" SET F1 ={0} "," \"" + dr[objMap.Fields[i].NameData].ToString() +"\" "); 
						query.AppendFormat("{0}]",fields.ToString());
						

						query.AppendFormat("{0}",values.ToString());
						
						this.ExecuteExcel(query.ToString());
				
					}
					regsCount++;
				}
				
			}

			sbyte []fileReturn  =ProjectLibrary.CBizAgiIO.GetFileContent(this.PathFile);
			File.Delete(this.PathFile);
			return fileReturn  ;
			
			
		}

		
		/// <summary>
		/// Update Cell excel fields with dataset, using optionally celloffset to fill
		/// </summary>
		/// <param name="dsData">Data</param>
		/// <param name="strXmlDocFields">Configs Parameters</param>
		public  sbyte[] DataSetToExcelUpdate(DataSet dsData, string strXmlDocFields)
		{
			System.Xml.XmlDocument xmlDocFields= new System.Xml.XmlDocument();
			xmlDocFields.LoadXml(strXmlDocFields);
			return DataSetToExcelUpdate(dsData,xmlDocFields);
			
		}
		#endregion

		#region IDisposable Members
		/// <summary>
		/// M�todo de IDisposable para desechar la clase.
		/// </summary>
		public void Dispose()
		{
			// Llamo al m�todo que contiene la l�gica
			// para liberar los recursos de esta clase.
			Dispose(true);
		}

		/// <summary>
		/// M�todo sobrecargado de Dispose que ser� el que
		/// libera los recursos, controla que solo se ejecute
		/// dicha l�gica una vez y evita que el GC tenga que
		/// llamar al destructor de clase.
		/// </summary>
		/// <param name=�b�></param>
		protected virtual void Dispose(bool b)
		{
			// Si no se esta destruyendo ya�
			if (!disposing)
			{
				// La marco como desechada � desechandose,
				// de forma que no se puede ejecutar este c�digo
				// dos veces.
				disposing = true;
				if(_connection.State ==ConnectionState.Open)
					_connection.Close();
				// Indico al GC que no llame al destructor
				// de esta clase al recolectarla.
				GC.SuppressFinalize(this);
 
				// � libero los recursos� 
			}
		}

		/// <summary>
		/// Destructor de clase.
		/// En caso de que se nos olvide �desechar� la clase,
		/// el GC llamar� al destructor, que tamb�n ejecuta la l�gica
		/// anterior para liberar los recursos.
		/// </summary>
		~ExcelUtils()
		{
			// Llamo al m�todo que contiene la l�gica
			// para liberar los recursos de esta clase.
			Dispose(true);
		}



		#endregion
	}
}
