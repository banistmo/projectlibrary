using System;

namespace ProjectLibrary.ExceptionHandling
{
	/// <summary>
	/// Summary description for ExceptionTypes.
	/// </summary>
	public enum ExceptionTypes
	{
		Core,
		SubCore,
		Configuration,
		Data,
		Component,
		FileSystemAccess
	}
}
