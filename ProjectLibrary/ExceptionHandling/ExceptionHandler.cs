using System;
using System.Web;
using System.Diagnostics;

namespace ProjectLibrary.ExceptionHandling
{
	/// <summary>
	/// The ExceptionHandler Class will manage exception for this project.
	/// </summary>
	public class ExceptionHandler
	{
		public static void Throw(string errorMessage)
		{
			Throw(ExceptionTypes.Component, errorMessage);
		}

		public static void Throw(Exception exception)
		{
			Throw(ExceptionTypes.Component, exception.Message);
		}

		public static void Throw(Exception exception, Exception inner)
		{
			Throw(ExceptionTypes.Component, exception.Message, inner);
		}

	
		public static string GetErrorHeader(ExceptionTypes type)
		{
			string errorheader = "(Unspefied error)";
			switch(type)
			{
				case ExceptionTypes.Core:
					errorheader = "BizAgi Core Error";
					break;
				case ExceptionTypes.SubCore:
					errorheader = "Project Sub-Core Error";
					break;
				case ExceptionTypes.Data:
					errorheader = "Project Data Error";	
					break;
				case ExceptionTypes.Component:
					errorheader = "Project Component Error";
					break;
				case ExceptionTypes.Configuration:
					errorheader = "Application Configuration Error";
					break;
				case ExceptionTypes.FileSystemAccess:
					errorheader = "File Access Error";
					break;
			}

			errorheader += ":["+ Process.GetCurrentProcess().ProcessName +"]:";

			return errorheader;
		}

		
		public static void Throw(ExceptionTypes type, string errorMessage, Exception inner)
		{
			string errorheader = GetErrorHeader(type);
			EventLog.WriteEntry("BizAgi", errorheader +":" +  errorMessage + " INNER EXCEPTION:" +inner.ToString(), EventLogEntryType.Error);

			throw new Exception(errorheader +":" +  errorMessage, inner);
		}

		public static void Throw(ExceptionTypes type, string errorMessage)
		{
			string errorheader = GetErrorHeader(type);

			EventLog.WriteEntry("BizAgi" , errorheader +":" +  errorMessage, EventLogEntryType.Error);

			throw new Exception(errorheader +":" +  errorMessage);
		}

		public static void ThrowNoLog(ExceptionTypes type, string errorMessage)
		{
			throw new Exception(GetErrorHeader(type) + errorMessage);
		}

	}
}
