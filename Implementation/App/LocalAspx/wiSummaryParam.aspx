<%@ Page language="c#" Codebehind="wiSummaryParam.aspx.cs" AutoEventWireup="false" Inherits="MonitorMovilidadAdmon.wiSummaryParam" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>wiSummaryParam</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--Estilos en produccion--><LINK href="../../css/estilos.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/WPCustomStyles.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/BAWindow.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table height="21" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPLeftButton.gif" width="2" border="0">
					</td>
					<td noWrap width="50"><a class="WPButton" style="BACKGROUND-IMAGE: url(../../img/WorkPortal/WPMiddleButton.jpg)"><asp:button id="buttonLoad" style="Z-INDEX: 101" accessKey="L" runat="server" Text="Actualizar"
								CssClass="WPButtonI BAMnColor"></asp:button></a></td>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPRightButton.gif" width="2" border="0">
					</td>
				</tr>
			</table>
			<asp:datagrid id=dg style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 48px" runat="server" Width="888px" Height="72px" DataSource="<%# oraDS_wiSummaryParam %>" DataKeyField="ROWID" DataMember="WISUMMARYPARAM" AutoGenerateColumns="False" GridLines="None">
				<SelectedItemStyle HorizontalAlign="Right"></SelectedItemStyle>
				<EditItemStyle HorizontalAlign="Right"></EditItemStyle>
				<AlternatingItemStyle HorizontalAlign="Right" CssClass="gridline1"></AlternatingItemStyle>
				<ItemStyle HorizontalAlign="Right" CssClass="gridline2"></ItemStyle>
				<HeaderStyle CssClass="ListHeaderLinks"></HeaderStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Actualizar" CancelText="Cancelar" EditText="Editar">
						<HeaderStyle Width="3cm"></HeaderStyle>
					</asp:EditCommandColumn>
					<asp:BoundColumn Visible="False" DataField="ROWID" ReadOnly="True" HeaderText="ROWID"></asp:BoundColumn>
					<asp:BoundColumn DataField="IREINTENTOS" HeaderText="M&#225;ximo reintentos"></asp:BoundColumn>
					<asp:BoundColumn DataField="IBORRARDIASMANTENER" HeaderText="# dias a mantener"></asp:BoundColumn>
					<asp:BoundColumn DataField="IULTIMOENVIO" ReadOnly="True" HeaderText="Ultimo enviado"></asp:BoundColumn>
				</Columns>
			</asp:datagrid></form>
	</body>
</HTML>
