<%@ Page language="c#" Codebehind="wiSummary.aspx.cs" AutoEventWireup="false" Inherits="MonitorMovilidadAdmon.wiSummary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>wiSummary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--Estilos en produccion--><LINK href="../../css/estilos.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/WPCustomStyles.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/BAWindow.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table height="21" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPLeftButton.gif" width="2" border="0">
					</td>
					<td noWrap width="50"><a class="WPButton" style="BACKGROUND-IMAGE: url(../../img/WorkPortal/WPMiddleButton.jpg)"><asp:button id="buttonLoad" accessKey="L" runat="server" CssClass="WPButtonI BAMnColor" Text="Actualizar"></asp:button></a></td>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPRightButton.gif" width="2" border="0">
					</td>
				</tr>
			</table>
			<asp:datagrid id=dg style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 48px" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" DataMember="WISUMMARY" AllowSorting="True" DataKeyField="IDSUMMARY" DataSource="<%# oraDS_wiSummary %>" Height="24px" Width="992px" GridLines="None" CellPadding="0">
				<AlternatingItemStyle CssClass="gridline1"></AlternatingItemStyle>
				<ItemStyle CssClass="gridline2"></ItemStyle>
				<HeaderStyle CssClass="ListHeaderLinks"></HeaderStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Actualizar" CancelText="Cancelar" EditText="Editar">
						<HeaderStyle Width="3cm"></HeaderStyle>
					</asp:EditCommandColumn>
					<asp:TemplateColumn HeaderText="Eliminar">
						<ItemTemplate>
							<asp:CheckBox id=cbBorrar runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BBORRAR").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox id=cbBorrarEdit runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.BBORRAR").ToString()=="1" %>'>
							</asp:CheckBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Forzar intento">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemTemplate>
							<asp:CheckBox id=cbForzar runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BFORZARINTENTO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox id=cbForzarEdit runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.BFORZARINTENTO").ToString()=="1" %>'>
							</asp:CheckBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="IDSUMMARY" ReadOnly="True" HeaderText="Consecutivo">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="STIPO" ReadOnly="True" HeaderText="Tipo"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDWORKITEM" ReadOnly="True" HeaderText="Workitem"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDTASK" ReadOnly="True" HeaderText="Task"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDCASE" ReadOnly="True" HeaderText="Caso"></asp:BoundColumn>
					<asp:BoundColumn DataField="SCODSOCIEDAD" ReadOnly="True" HeaderText="Sociedad"></asp:BoundColumn>
					<asp:BoundColumn DataField="SDESCPROCACT" ReadOnly="True" HeaderText="Proc.-Act.">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SRADNUMBER" ReadOnly="True" HeaderText="N&#176; de alta">
						<HeaderStyle Wrap="False" Width="2cm"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="IDUSER" ReadOnly="True" HeaderText="Usuario">
						<HeaderStyle Wrap="False"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SUSERNAME" ReadOnly="True" HeaderText="Nombre">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SDOMAIN" ReadOnly="True" HeaderText="Dominio">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="DFECHAHORA" ReadOnly="True" HeaderText="Fecha">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Enviado">
						<ItemTemplate>
							<asp:CheckBox id=cbEnviado runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BENVIADO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="DENVIO" ReadOnly="True" HeaderText="F.env&#237;o">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Usuario habilitado">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemTemplate>
							<asp:CheckBox id=cbUsuarioHabilitado runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BUSUARIOHABILITADO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Error">
						<ItemTemplate>
							<asp:CheckBox id=cbError runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container,"DataItem.BERROR").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="SERROR" ReadOnly="True" HeaderText="Mensaje">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="IINTENTOS" ReadOnly="True" HeaderText="Intentos"></asp:BoundColumn>
					<asp:BoundColumn DataField="DULTIMOINTENTO" ReadOnly="True" HeaderText="F. &#250;ltimo intento">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle Mode="NumericPages"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
