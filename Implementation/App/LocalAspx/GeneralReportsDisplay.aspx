﻿<%@ Page language="c#" Codebehind="GeneralReportsDisplay.aspx.cs" AutoEventWireup="True" Inherits="ProjectLibrary.Implementation.App.LocalAspx.GeneralReportsDisplay" %>
<HTML>
	<HEAD>

<%WriteHead();%>

<script>
function openBACaseReport(h,b)
{
	parent.bizagiWorkportal.mainController.publish("executeAction", {
        action: parent.bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
        idCase: h
    });
	var x = parent.$(".css_myFrame");
	var t= parent.$(x[x.length-1].parentElement.parentElement);
	t[0].firstElementChild.childNodes[1].click();
	x.remove();
}
</script>

<style>
.gridline1 td {
    font-size: 12px;
    border-right: black;
    border-style: solid;
    border-width: 1px;
    min-width:150;
}

.gridline2 td {
    font-size: 12px;
    border-right: black;
    border-style: solid;
    border-width: 1px;
    min-width:150;
}

</style>
	</HEAD>
	<body class="ui-bizagi-old-render @font-face" style="text-align:center">
		<form runat="server">
			<table width="90%" border="0" cellspacing="2" cellpadding="2" align="center" style="margin:3">
				<TR>
					<TD align="center">
						<asp:DataGrid id="DataGridReport" runat="server" AllowPaging="True" BorderColor="White" BorderWidth="2px"
							BackColor="White" CellPadding="4" BorderStyle="solid" PageSize="10">
							<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="gridline2" HorizontalAlign="Center"></AlternatingItemStyle>
							<ItemStyle CssClass="gridline1" HorizontalAlign="Center"></ItemStyle>
							<HeaderStyle CssClass="headerlinksNI" HorizontalAlign="Center"></HeaderStyle>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="headerlinksNI" Mode="NumericPages"></PagerStyle>
						</asp:DataGrid>
						<span id="SpanMessage" runat="server"></span>
					</TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
