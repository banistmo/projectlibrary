using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ProjectLibrary.DA;

namespace ProjectLibrary.Implementation.App.LocalAspx
{
	/// <summary>
	/// Summary description for Noticias.
	/// </summary>
	public class Noticias : System.Web.UI.Page
	{
		private ImplementationProvider myProvider = new ImplementationProvider();
		private void Page_Load(object sender, System.EventArgs e)
		{
			string sQuery = "Select iCaso, sRadNumber FROM Noticia";
			DataSet ds = myProvider.RunQuery(sQuery,"Noticias");
			Response.Write("Lista de Noticias");
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				sHtml = "<a href=javascript:openBACase('" + dr["iCaso"].ToString() + ", '../ListaDetalle/Detalle.aspx?idCase=" + dr["iCaso"].ToString() + "')>"+dr["sRadNumber"].ToString()+"</a><br>";
				Response.Write(sHtml);
			}

			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
