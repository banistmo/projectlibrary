using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using BizAgi.BL.User;
//using Vision.DA;
//using Vision.Defs;
using ProjectLibrary.Components;
using ProjectLibrary.DA;
using ProjectLibrary.WebControls.GeneralReportsControls;

namespace ProjectLibrary.Implementation.App.LocalAspx
{
	/// <summary>
	/// Summary description for GeneralReportsParameters.
	/// </summary>
	public partial class GeneralReportsParameters : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.DropDownList DropDownRegionals;
		protected System.Web.UI.WebControls.DropDownList DropDownOffices;
		protected System.Web.UI.WebControls.Button ShowReportButton;
		protected System.Web.UI.HtmlControls.HtmlSelect reportType;
		protected System.Web.UI.HtmlControls.HtmlInputText CaseBox;
		protected System.Web.UI.HtmlControls.HtmlInputText InitRadicationDate;
		protected System.Web.UI.HtmlControls.HtmlInputText FinRadicationDate;
		protected System.Web.UI.HtmlControls.HtmlInputText InitCloseDate;
		protected System.Web.UI.HtmlControls.HtmlInputText FinCloseDate;
		//private   ImplementationProvider   myProvider =  new ImplementationProvider("DSNREPORTS");
        private ODPProvider myProvider = new ODPProvider("DSNREPORTS");
		protected System.Web.UI.WebControls.Label Clabel1;
		protected System.Web.UI.WebControls.Label lblHelp;//new TransparentDataProvider(); // new BizAgiConavi.DA.TransparentDataProvider();

		private string IdUser;
		
		public string GetReportName()
		{
			return Request.QueryString["report"];
		}

		public string GetReportUrl()
		{
            return "GeneralReportsParameters.aspx?tokenb=" + Request.QueryString["tokenb"] + "&report=";
		}
		public string GetReportDisplayUrl()
		{
            string userId = convertirGuidUserPorUserId(this.IdUser);
            return "GeneralReportsDisplay.aspx?idUser="+ userId + "&report=";
		}
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (Request.UrlReferrer != null)
            {
                //if (Request.UrlReferrer.ToString().Contains("Default.aspx") || Request.UrlReferrer.ToString().Contains("GeneralReportsParameters.aspx"))
                this.IdUser = decodestring(Request.QueryString["tokenb"]);
                    //else
                //     Response.Redirect("../../default.aspx");
            }
            else
            {
                Response.Redirect("../../default.aspx");
            }
		}
        public string decodestring(string cadena)
        {
            string returnValue = "";
            if (cadena != null)
            {
                if (cadena != "")
                {
                    byte[] encodedDataAsBytes = System.Convert.FromBase64String(cadena);
                    returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
                }
            }
            return returnValue;
        }

		public void GetReportsCombo()
		{
			//			Response.Write("<div id=\"Panel1\" style=\"background-color:#1253ca;border-color:Black;height:80px;width:100%;\">");
			//			Response.Write("<img id=\"Image1\" src=\"../../img/principal/LogoConavi.gif\" border=\"0\" style=\"height:63px;\" />");
			//			Response.Write("<P><FONT size=\"2\"></FONT>&nbsp;</P>");	
			//			Response.Write("</div>");
			Response.Write("<select name=\"ProcessCombo\" id=\"ProcessCombo\" onchange=\"GotoProcessReport(GetObject('ProcessCombo').value)\">");
            string userid = convertirGuidUserPorUserId(this.IdUser);
            int EnableOnTheFlyReports = 0;
            if (System.Configuration.ConfigurationManager.AppSettings["EnableOnTheFlyReports"] != null)
                EnableOnTheFlyReports = Convert.ToInt32(Configuration.SettingsManager.Keys["EnableOnTheFlyReports"]);
            ReportsConfiguration mydataset;
            if (EnableOnTheFlyReports == 1)
                mydataset = ParametersRequestControl.GetReportsWithParameters(userid);
            else
                mydataset = ParametersRequestControl.GetReportsWithParameters();

			
			//			mydataset.ReadXml(mem );//			mydataset.ReadXml( Utils.GetUNCPathFromKeyName("ReportsFilePath")+ "\\" + "ReportsConfig.xml");
			Response.Write("<OPTION value=''>----------------------</OPTION>");

			bool userallowed=false;
			//DataTable idDBUser = myProvider.RunQuery("SELECT * FROM WFUSER WHERE GUIDUSER='" + IdUser + "'", "idDBUser").Tables[0];
			//object idUserDB = idDBUser.Rows[0]["IDUSER"];

            DataTable usergroups = myProvider.RunQuery("SELECT * FROM USERROLE WHERE IdUser=" + userid, "groupsTable").Tables[0];
			foreach(ReportsConfiguration.ReportRow row in mydataset.Report.Rows)
			{
                if (row.Groups.ToString() == "")
                {
                    userallowed = true;
                    Response.Write("<option value=" + row.Name + ">" + row.Description + "</option>");
                }
                else
                {
                    DataRow[] rolerows = usergroups.Select("idRole in(" + row.Groups.ToString() + ")");
                    userallowed = false;
                    if (rolerows.Length > 0)
                        userallowed = true;
                    if (userallowed)
                        Response.Write("<option value=" + row.Name + ">" + row.Description + "</option>");
                    else
                    {
#if DEBUG
                        Response.Write("<option value=" + row.Name + ">" + row.Description + "</option>");
#endif
                    }
                }
			}
			Response.Write("</select>");
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowReportButton_Click(object sender, System.EventArgs e)
		{
		
		}

		public void WriteHead()
		{
        string ReportsClientFormatDate = "dd/MM/yyyy";
        string ReportsCulture = "es-CO";
        string ReportsCurrencyCulture = "en-US";
        if (System.Configuration.ConfigurationManager.AppSettings["ReportsClientFormatDate"] != null)
            ReportsClientFormatDate = Configuration.SettingsManager.Keys["ReportsClientFormatDate"];
        if (System.Configuration.ConfigurationManager.AppSettings["ReportsCulture"] != null)
            ReportsCulture = Configuration.SettingsManager.Keys["ReportsCulture"];
        if (System.Configuration.ConfigurationManager.AppSettings["ReportsCurrencyCulture"] != null)
            ReportsCurrencyCulture = Configuration.SettingsManager.Keys["ReportsCurrencyCulture"];
		this.Response.Write("<title>Informes</title>");
        this.Response.Write("\n");
		this.Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">");
        this.Response.Write("\n");
		this.Response.Write("<meta http-equiv=\"expires\" content=\"Wed, 26 Feb 1997 08:00:00 GMT\">");
        this.Response.Write("\n");
		this.Response.Write("<META http-equiv=\"Pragma\" content=\"no-cache\">");
        this.Response.Write("\n");
		this.Response.Write("<meta content=\"Microsoft Visual Studio 7.0\" name=\"GENERATOR\">");
        this.Response.Write("\n");
		this.Response.Write("<meta content=\"C#\" name=\"CODE_LANGUAGE\">");
        this.Response.Write("\n");
		this.Response.Write("<meta content=\"JavaScript\" name=\"vs_defaultClientScript\">");
        this.Response.Write("\n");
		this.Response.Write("<LINK href=\"../../css/estilos.css\" type=\"text/css\" rel=\"stylesheet\">");
        this.Response.Write("\n");
		this.Response.Write("<LINK href=\"../../css/calendar.css\" type=\"text/css\" rel=\"stylesheet\">");
        this.Response.Write("\n");
        this.Response.Write("<LINK href=\"css/implementationcss.css\" type=\"text/css\" rel=\"stylesheet\">");
        this.Response.Write("\n");
		this.Response.Write("<LINK href=\"../../css/WorkPortal/WPCustomStyles.css\" type=\"text/css\" rel=\"stylesheet\">");
        this.Response.Write("\n");
		this.Response.Write("<LINK href=\"../../css/WorkPortal/BAWindow.css\" type=\"text/css\" rel=\"stylesheet\">");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/implementation.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/BizAgiAJAX.js\"></script>");
        this.Response.Write("\n");    
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WorkPortal/BAWindows/prototype.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WorkPortal/BAWindows/window.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WorkPortal/BAWindows/BAwindow.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../Localization/LocalizationES.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/pupdate1.js\" type=\"text/javascript\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WPSettings.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/scripts.js\" type=\"text/javascript\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/wizard.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WMask.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/pupdate1.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\" src=\"../../js/Cache.js\"></script>");
        this.Response.Write("\n");
		this.Response.Write("<script language=\"JavaScript\">");
        this.Response.Write("\n");
        this.Response.Write("var BA_DATE_FORMAT_MASK = \""+ReportsClientFormatDate+"\";");
        this.Response.Write("\n");
		this.Response.Write("var BA_TIME_FORMAT_MASK = \"hh:mm tt\";");
        this.Response.Write("\n");
		this.Response.Write("var BA_DIGITS_AFTER_DECIMAL = \"2\";");
        this.Response.Write("\n");
		this.Response.Write("var BA_DECIMAL_SEPARATOR = \".\";");
        this.Response.Write("\n");
		this.Response.Write("var BA_GROUP_SEPARATOR = \",\";");
        this.Response.Write("\n");
		this.Response.Write("var BA_GROUP_SIZE = \"3\";");
        this.Response.Write("\n");
        this.Response.Write("var BA_CULTURE = \"" + ReportsCulture + "\";");
        this.Response.Write("\n");
        this.Response.Write("var BA_CURRENCY_CULTURE = \"" + ReportsCurrencyCulture + "\";");
        this.Response.Write("\n");
		this.Response.Write("var bOnSubmit = false;");
        this.Response.Write("\n");
        this.Response.Write("</script>");
        this.Response.Write("\n");

		}
        private string convertirGuidUserPorUserId(string GuidUser)
        {
            DataTable idDBUser = myProvider.RunQuery("SELECT * FROM WFUSER WHERE GUIDUSER='" + GuidUser + "'", "idDBUser").Tables[0];
           return idDBUser.Rows[0]["IDUSER"].ToString();
        }
    }
}