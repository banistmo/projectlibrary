using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Vision.DA;
//using Vision.Defs;
using System.Globalization;
using ProjectLibrary.Components;
using System.Reflection;
using ProjectLibrary.DA;
using Aspose.Cells;
using System.IO;
using System.Resources;
using System.Configuration;

namespace ProjectLibrary.Implementation.App.LocalAspx
{
	/// <summary>
	/// Summary description for GeneralReportsDisplay.
	/// </summary>
	public partial class GeneralReportsDisplay : System.Web.UI.Page // BizAgi.UI.WFBiz.CSite
	{

		//ImplementationProvider  myProvider =  new ImplementationProvider("DSNREPORTS");
        ODPProvider myProvider = new ODPProvider("DSNREPORTS");
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
		
			if(!IsPostBack)
			{
				LoadReport();
			}

		}

		private bool IsDateField (DataRow report, string FieldName)
		{
			bool retval= false;
			DataRow[] childparameters = report.GetChildRows(report.Table.ChildRelations[0]);
			foreach(DataRow row in childparameters)
			{
				if(Convert.ToString(row["name"]).ToLower() ==FieldName.ToLower() && Convert.ToString(row["type"]).ToLower()=="date")
					retval = true;
			}
			return retval;
		}

		private bool IsStringField (DataRow report, string FieldName)
		{
			bool retval= false;
			DataRow[] childparameters = report.GetChildRows(report.Table.ChildRelations[0]);
			foreach(DataRow row in childparameters)
			{
				if(Convert.ToString(row["name"]).ToLower() ==FieldName.ToLower() && Convert.ToString(row["type"]).ToLower()=="string")
					retval = true;
			}
			return retval;
		}

		private bool IsLikeStringField (DataRow report, string FieldName)
		{
			bool retval= false;
			DataRow[] childparameters = report.GetChildRows(report.Table.ChildRelations[0]);
			foreach(DataRow row in childparameters)
			{
				if(Convert.ToString(row["name"]).ToLower() ==FieldName.ToLower() && Convert.ToString(row["type"]).ToLower()=="likestring")
					retval = true;
			}
			return retval;
		}

        private void configResponse(String reportName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", reportName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm") + ".xlsx"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            //Response.Charset = "";
            //Response.ContentEncoding = System.Text.Encoding.Unicode;
            //Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            EnableViewState = false;
        }


		private void LoadTrueReport()
		{


			string reportname = Request.QueryString["report"] ;
            if (string.IsNullOrEmpty(reportname))
            {
                reportname = "1";
            }
          
            string idUser = Request.QueryString["idUser"];
            int EnableOnTheFlyReports = 0;
            if (System.Configuration.ConfigurationManager.AppSettings["EnableOnTheFlyReports"] != null)
                EnableOnTheFlyReports = Convert.ToInt32(Configuration.SettingsManager.Keys["EnableOnTheFlyReports"]);
            ProjectLibrary.WebControls.GeneralReportsControls.ReportsConfiguration config;
            if (EnableOnTheFlyReports == 1)
                config = ProjectLibrary.WebControls.GeneralReportsControls.ParametersRequestControl.GetReportsWithParameters(idUser);
            else
                config = ProjectLibrary.WebControls.GeneralReportsControls.ParametersRequestControl.GetReportsWithParameters();

            //			config.ReadXml( Utils.GetUNCPathFromKeyName("ReportsFilePath")+ "\\" + "ReportsConfig.xml");
            DataRow currentreport = config.Report.Select("Name='"+ reportname+"'")[0];

            if (Request.QueryString["ExpExcel"] == "true")
            {
                if (EnableOnTheFlyReports == 1)
                {
                    string OTFRname = currentreport["Description"].ToString().Replace(" ", "");
                    configResponse(OTFRname);
                }
                else
                    configResponse(reportname);
            }
            
			//TODO config culture
            string ReportsCulture = "es-CO";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsCulture"] != null)
                ReportsCulture = Configuration.SettingsManager.Keys["ReportsCulture"];

            CultureInfo ci = new CultureInfo(ReportsCulture, true);
			
			//			ci.ShortDatePattern = "dd/MM/yyyy";

            string ReportsClientFormatDate = "dd/MM/yyyy";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsClientFormatDate"] != null)
                ReportsClientFormatDate = Configuration.SettingsManager.Keys["ReportsClientFormatDate"];
            
            string ReportsServerFormatDate = "MM/dd/yyyy";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsServerFormatDate"] != null)
                ReportsServerFormatDate = Configuration.SettingsManager.Keys["ReportsServerFormatDate"];
            
            string ReportsDefaultResult = "-1";
            if (System.Configuration.ConfigurationManager.AppSettings["ReportsDefaultResult"] != null)
                ReportsDefaultResult = Configuration.SettingsManager.Keys["ReportsDefaultResult"];
            
            string OTFReport = "";
            if (EnableOnTheFlyReports == 1)
            {
                string idService = Request.QueryString["serviceType"];
                if (reportname.StartsWith("OTFR_000001"))
                {
                    OTFReport = GetOTFQuery(idService, reportname);
                }
                if (reportname.StartsWith("OTFR_000002"))
                {
                    OTFReport = GetOTFQuery(idService, reportname);
                }
            }

            ci.DateTimeFormat.ShortDatePattern = ReportsClientFormatDate;
			for(int i=0;i<Request.QueryString.Count;i++)
                //if (Request.QueryString.GetKey(i) != "report" && Request.QueryString.GetKey(i) != "idUser" && Request.QueryString.GetKey(i) != "ExpExcel")
				if(Request.QueryString.GetKey(i)!="report" && Request.QueryString.GetKey(i)!="ExpExcel")
				{
					if(IsDateField(currentreport,  Request.QueryString.GetKey(i)))
					{
						string datetoconvert = Request.QueryString[i];
                        if(datetoconvert=="")
                            datetoconvert=ReportsDefaultResult;
                        if (datetoconvert != ReportsDefaultResult)
                            myProvider.AddParameter("@" + Request.QueryString.GetKey(i), "'" + DateTime.Parse(datetoconvert, ci).ToString(ReportsServerFormatDate) + "'");
						else
                            myProvider.AddParameter("@" + Request.QueryString.GetKey(i), "'" + ReportsDefaultResult + "'");
					}
					else if(IsStringField(currentreport,  Request.QueryString.GetKey(i)))
					{
                        myProvider.AddParameter("@" + Request.QueryString.GetKey(i), (Request.QueryString[i] == ReportsDefaultResult) ? "'" + ReportsDefaultResult + "'" : "'" + Request.QueryString[i] + "'");
					}
					else if(IsLikeStringField(currentreport,  Request.QueryString.GetKey(i)))
					{
                        myProvider.AddParameter("@" + Request.QueryString.GetKey(i), (Request.QueryString[i] == ReportsDefaultResult) ? "'" + ReportsDefaultResult + "'" : "'%" + Request.QueryString[i] + "%'");
					}
					else
					{
                        //myProvider.AddParameter("@" + Request.QueryString.GetKey(i), (Request.QueryString[i] == ReportsDefaultResult) ? ReportsDefaultResult : Request.QueryString[i]);
                        myProvider.AddParameter("@" + Request.QueryString.GetKey(i), (Request.QueryString[i] == ReportsDefaultResult) ? "'" + ReportsDefaultResult + "'" : "'" + Request.QueryString[i] + "'");
					}
				}

            DataTable myTable;
            if (EnableOnTheFlyReports == 1 && reportname.StartsWith("OTFR_"))
            {
                myTable = myProvider.RunQuery(OTFReport, "diplayTable").Tables[0];
            }
            else
            {
                myTable = myProvider.RunQuery(currentreport["Queries"].ToString(), "diplayTable").Tables[0];
            }

			if(Request.QueryString["ExpExcel"]== "true")
			{
                string ReportsClientFormatDateExcel = Utils.GetReportsClientFormatDate();
                string ReportsTemplatePath = "";
                Workbook oWorkbook;
                if (System.Configuration.ConfigurationManager.AppSettings["ReportsTemplatePath"] != null)
                    ReportsTemplatePath = Configuration.SettingsManager.Keys["ReportsTemplatePath"];
                if (ReportsTemplatePath != "")
                {
                    string templateFile = ReportsTemplatePath + reportname + ".xlsx";
                    if (File.Exists(templateFile))
                    {
                        FileStream fsFile = new FileStream(templateFile, FileMode.Open, FileAccess.Read);
                        oWorkbook = new Workbook(fsFile);
                        Worksheet oSheet = oWorkbook.Worksheets[0];
                        bool insert = false;
                        bool encabezados = true;
                        oSheet.Cells.ImportDataTable(myTable, encabezados, 0, 0, insert);
                        oSheet.AutoFitColumns();
                        //Date Style
                        Aspose.Cells.Style datestyle = oWorkbook.CreateStyle();
                        datestyle.Custom = ReportsClientFormatDateExcel;
                        StyleFlag datestyleFlag = new StyleFlag();
                        datestyleFlag.NumberFormat = true;

                        foreach (DataColumn DC in myTable.Columns)
                        {
                            if (DC.DataType == System.Type.GetType("System.DateTime"))
                            {
                                oSheet.Cells.Columns[DC.Ordinal].ApplyStyle(datestyle, datestyleFlag);
                            }
                        }
                        oWorkbook.CalculateFormula();
                        fsFile.Close();
                        //End date style
                    }
                    else
                    {
                        oWorkbook = new Workbook();
                        Worksheet oSheet = oWorkbook.Worksheets[0];
                        oSheet.Cells.ImportDataTable(myTable, true, "A1");
                        //oSheet.Cells.ImportDataTable(myTable, true, 1, 1, 1, 1, false, "dd/MM/yyyy");
                        oSheet.AutoFitColumns();
                        //Date Style
                        Aspose.Cells.Style datestyle = oWorkbook.CreateStyle();
                        datestyle.Custom = ReportsClientFormatDateExcel;
                        StyleFlag datestyleFlag = new StyleFlag();
                        datestyleFlag.NumberFormat = true;

                        foreach (DataColumn DC in myTable.Columns)
                        {
                            if (DC.DataType == System.Type.GetType("System.DateTime"))
                            {
                                oSheet.Cells.Columns[DC.Ordinal].ApplyStyle(datestyle, datestyleFlag);
                            }
                        }

                        //End date style
                        Aspose.Cells.Style style = oWorkbook.CreateStyle();
                        style.VerticalAlignment = TextAlignmentType.Center;
                        style.HorizontalAlignment = TextAlignmentType.Center;
                        style.Font.Color = Color.Black;
                        style.Font.IsBold = true;
                        style.ShrinkToFit = true;
                        style.Borders[BorderType.BottomBorder].Color = Color.Black;
                        style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Medium;
                        StyleFlag styleFlag = new StyleFlag();
                        styleFlag.HorizontalAlignment = true;
                        styleFlag.VerticalAlignment = true;
                        styleFlag.ShrinkToFit = true;
                        styleFlag.Borders = true;
                        styleFlag.FontColor = true;
                        Row row = oSheet.Cells.Rows[0];
                        row.ApplyStyle(style, styleFlag);  
                    }
                }
                else
                {
                    oWorkbook = new Workbook();
                    Worksheet oSheet = oWorkbook.Worksheets[0];
                    oSheet.Cells.ImportDataTable(myTable, true, "A1");
                    //oSheet.Cells.ImportDataTable(myTable, true, 1, 1, 1, 1, false, "dd/MM/yyyy");
                    oSheet.AutoFitColumns();
                    //Date Style
                    Aspose.Cells.Style datestyle = oWorkbook.CreateStyle();
                    datestyle.Custom = ReportsClientFormatDateExcel;
                    StyleFlag datestyleFlag = new StyleFlag();
                    datestyleFlag.NumberFormat = true;

                    foreach (DataColumn DC in myTable.Columns)
                    {
                        if (DC.DataType == System.Type.GetType("System.DateTime"))
                        {
                            oSheet.Cells.Columns[DC.Ordinal].ApplyStyle(datestyle, datestyleFlag);
                        }
                    }

                    //End date style
                    Aspose.Cells.Style style = oWorkbook.CreateStyle();
                    style.VerticalAlignment = TextAlignmentType.Center;
                    style.HorizontalAlignment = TextAlignmentType.Center;
                    style.Font.Color = Color.Black;
                    style.Font.IsBold = true;
                    style.ShrinkToFit = true;
                    style.Borders[BorderType.BottomBorder].Color = Color.Black;
                    style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Medium;
                    StyleFlag styleFlag = new StyleFlag();
                    styleFlag.HorizontalAlignment = true;
                    styleFlag.VerticalAlignment = true;
                    styleFlag.ShrinkToFit = true;
                    styleFlag.Borders = true;
                    styleFlag.FontColor = true;
                    Row row = oSheet.Cells.Rows[0];
                    row.ApplyStyle(style, styleFlag);                
                }


                MemoryStream msout = new System.IO.MemoryStream();
                oWorkbook.Save(msout, Aspose.Cells.SaveFormat.Xlsx);
                Byte[] oBytes = msout.ToArray();
                msout.Close();
                Response.BinaryWrite(oBytes);
                Response.End();
			}
			else
			{
				Response.Write("<CENTER>");
				if(myTable.Rows.Count>0)
				{
					DataGridReport.DataSource=myTable;
					DataGridReport.DataBind();
				}
				else
				{
					SpanMessage.InnerText="La busqueda no arroj� resultados.";
				}

				
				Response.Write("</CENTER>");
			}

		}

		public void LoadReport()
		{
		
				if(Request.QueryString["report"] != null )
				{
					
					LoadTrueReport();
				}
		

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
		//	this.m_bIgnoreReferer = true;
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DataGridReport.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DataGridReport_PageIndexChanged);

		} 
		#endregion

		private void DataGridReport_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			DataGridReport.CurrentPageIndex=e.NewPageIndex;
			if(Request.QueryString["report"] != null)
				LoadTrueReport();

		}

        public void WriteHead()
        {
            if (Request.QueryString["ExpExcel"] != "true")
            {
                string ReportsClientFormatDate = "dd/MM/yyyy";
                string ReportsCulture = "es-CO";
                string ReportsCurrencyCulture = "en-US";
                if (System.Configuration.ConfigurationManager.AppSettings["ReportsClientFormatDate"] != null)
                    ReportsClientFormatDate = Configuration.SettingsManager.Keys["ReportsClientFormatDate"];
                if (System.Configuration.ConfigurationManager.AppSettings["ReportsCulture"] != null)
                    ReportsCulture = Configuration.SettingsManager.Keys["ReportsCulture"];
                if (System.Configuration.ConfigurationManager.AppSettings["ReportsCurrencyCulture"] != null)
                    ReportsCurrencyCulture = Configuration.SettingsManager.Keys["ReportsCurrencyCulture"];

                this.Response.Write("<title>Informes</title>");
                this.Response.Write("\n");
                this.Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">");
                this.Response.Write("\n");
                this.Response.Write("<meta http-equiv=\"expires\" content=\"Wed, 26 Feb 1997 08:00:00 GMT\">");
                this.Response.Write("\n");
                this.Response.Write("<META http-equiv=\"Pragma\" content=\"no-cache\">");
                this.Response.Write("\n");
                this.Response.Write("<meta content=\"Microsoft Visual Studio 7.0\" name=\"GENERATOR\">");
                this.Response.Write("\n");
                this.Response.Write("<meta content=\"C#\" name=\"CODE_LANGUAGE\">");
                this.Response.Write("\n");
                this.Response.Write("<meta content=\"JavaScript\" name=\"vs_defaultClientScript\">");
                this.Response.Write("\n");
                this.Response.Write("<LINK href=\"../../css/estilos.css\" type=\"text/css\" rel=\"stylesheet\">");
                this.Response.Write("\n");
                this.Response.Write("<LINK href=\"../../css/calendar.css\" type=\"text/css\" rel=\"stylesheet\">");
                this.Response.Write("\n");
                this.Response.Write("<LINK href=\"../../css/WorkPortal/WPCustomStyles.css\" type=\"text/css\" rel=\"stylesheet\">");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../js/implementation.js\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../js/BizAgiAJAX.js\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../Localization/LocalizationES.js\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../js/WPSettings.js\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../js/scripts.js\" type=\"text/javascript\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\" src=\"../../js/pupdate1.js\"></script>");
                this.Response.Write("\n");
                this.Response.Write("<script language=\"JavaScript\">");
                this.Response.Write("\n");
                this.Response.Write("var BA_DATE_FORMAT_MASK = \"" + ReportsClientFormatDate + "\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_TIME_FORMAT_MASK = \"hh:mm tt\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_DIGITS_AFTER_DECIMAL = \"2\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_DECIMAL_SEPARATOR = \".\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_GROUP_SEPARATOR = \",\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_GROUP_SIZE = \"3\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_CULTURE = \"" + ReportsCulture + "\";");
                this.Response.Write("\n");
                this.Response.Write("var BA_CURRENCY_CULTURE = \"" + ReportsCurrencyCulture + "\";");
                this.Response.Write("\n");
                this.Response.Write("var bOnSubmit = false;");
                this.Response.Write("\n");
                this.Response.Write("</script>");
                this.Response.Write("\n");
                this.Response.Write("<style>#floater1Div { LEFT: 0px; VISIBILITY: hidden; POSITION: absolute; TOP: 0px }");
                this.Response.Write("\n");
                this.Response.Write("</style>");
                this.Response.Write("\n");
            }
        }

        public string GetPivotFilter(string idReport)
        {
            string sFinalPivotFilter = "'No Columns'";
            string sPivotFilter = "SELECT TRIM(DA.SLABELATRIBUTO) AS SLABELATRIBUTO, ES.IORDEN AS ORDEN1, DA.IORDEN AS ORDEN2, TS.IDP_PRG_TIPODESERVICIO AS TS FROM P_PRG_DEFINICIONATRIBUTO DA ";
            sPivotFilter += "  INNER JOIN P_PRG_ESTADOSSERVICIO ES ON ES.IDP_PRG_ESTADOSSERVICIO = DA.P_PRG_ESTADOSSERVICIO ";
            sPivotFilter += "  INNER JOIN P_PRG_TIPODESERVICIO TS ON TS.IDP_PRG_TIPODESERVICIO = ES.P_PRG_TIPODESERVICIO ";
            sPivotFilter += "  WHERE DA.SLABELATRIBUTO IS NOT NULL AND TS.IDP_PRG_CLASESERVICIO = " + idReport;
            sPivotFilter += "  order BY 4 Desc,2 ASC, 3 ASC";
            DataSet pivotFilterds = new DataSet();
            myProvider.RunQuery(sPivotFilter, "Filters", true, pivotFilterds);

            ArrayList arrFilters = new ArrayList();
            ArrayList arrAlias = new ArrayList();

            arrAlias.Add("CASO NO");
            arrAlias.Add("SERVICIO");
            arrAlias.Add("FECHA SERVICIO");
            arrAlias.Add("ACTIVIDAD");
            arrAlias.Add("ESTADO DE LA TAREA");
            arrAlias.Add("FECHA CREACI�N PROCESO");
            arrAlias.Add("ACTIVIDAD VENCE EN");
            arrAlias.Add("FECHA SOLUCI�N");
            arrAlias.Add("USUARIO ASIGNADO");
            arrAlias.Add("FECHA CREACI�N CASO");
            arrAlias.Add("FECHA FIN SERVICIO");
            arrAlias.Add("N�MERO DE RADICADO PADRE");
            arrAlias.Add("CATEGOR�A");
            arrAlias.Add("SUB CATEGOR�A");
            arrAlias.Add("TIPO DE SERVICIO");
            arrAlias.Add("SUB PROCESO MACRO");
            arrAlias.Add("ESTADO ACTUAL DEL SERVICIO");
            arrAlias.Add("USUARIO ATENCI�N");
            arrAlias.Add("USUARIO GESTI�N");
            arrAlias.Add("RESULTADO DE LA APROBACI�N");
            arrAlias.Add("OBSERVACIONES DE APROBACI�N");
            arrAlias.Add("RESULTADO DE LA ATENCI�N");
            arrAlias.Add("RESULTADO DE LA GESTI�N");
            arrAlias.Add("RESULTADO DE LA VALIDACI�N");
            arrAlias.Add("NOMBRE USUARIO RADICADOR");
            arrAlias.Add("USUARIO DE RED");
            arrAlias.Add("CARGO USUARIO RADICADOR");
            arrAlias.Add("DEPENDENCIA");
            arrAlias.Add("MOTIVO CANCELACI�N");

            sFinalPivotFilter = "";
            foreach (DataRow drPivotFilter in pivotFilterds.Tables[0].Rows)
            {
                string sLabel = drPivotFilter["SLABELATRIBUTO"].ToString().Replace("'", "").Replace("\"", "");
                string sAlias = sLabel.Length >= 30 ? sAlias = sLabel.Substring(0, 28) : sLabel;
                sAlias = Utils.RemoveAccents(sAlias);
                if (!arrFilters.Contains(sLabel.ToUpper()))
                {
                    arrFilters.Add(sLabel.ToUpper());
                    if (!arrAlias.Contains(sAlias.ToUpper()))
                    {
                        arrAlias.Add(sAlias.ToUpper());
                        sLabel = "'" + sLabel + "' AS \"" + sAlias + "\"";
                    }
                    else
                    { 
                        Boolean bContinueFinding = true;
                        int key = 2;
                        while (bContinueFinding)
                        {
                            string sNewAlias = sAlias;
                            int iSize = sAlias.Length;
                            if (iSize <= 27)
                            {
                                sNewAlias = sAlias + "_" + key.ToString();
                                if (!arrAlias.Contains(sNewAlias.ToUpper()))
                                {
                                    sAlias = sNewAlias;
                                    arrAlias.Add(sAlias.ToUpper());
                                    bContinueFinding = false;
                                }
                            }
                            else
                            {
                                sNewAlias = sAlias.Substring(0, 27) +"_" + key.ToString();
                                if (!arrAlias.Contains(sNewAlias.ToUpper()))
                                {
                                    sAlias = sNewAlias;
                                    arrAlias.Add(sAlias.ToUpper());
                                    bContinueFinding = false;
                                }
                            }

                            key = key + 1;
                        }
                        sLabel = "'" + sLabel + "' AS \"" + sAlias + "\"";
                    }
                    sFinalPivotFilter += sLabel + ", ";
                }
            }
            if (sFinalPivotFilter != "")
                sFinalPivotFilter = sFinalPivotFilter.Substring(0, sFinalPivotFilter.Length - 2);
            return sFinalPivotFilter;
        }
        public string GetOTFQuery(string idService,string reportName)
        {
            string OTFQuery = ProjectLibrary.Implementation.App.LocalAspx.LocalResources.ResourceManager.GetString(reportName);
            OTFQuery = OTFQuery.Replace("@idService", idService);
            if (reportName == "OTFR_000001")
            {
                string pivotFilter = GetPivotFilter(idService);
                OTFQuery = OTFQuery.Replace("DateFormaterRG", Utils.GetReportsClientFormatDate());
                OTFQuery = OTFQuery.Replace("OTFQUERYFIELDSFILTER", pivotFilter);
            }
            return OTFQuery;
        }
	}
}
