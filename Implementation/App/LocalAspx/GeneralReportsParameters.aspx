﻿<%@ Register tagprefix="MyTag"   Namespace="ProjectLibrary.WebControls.GeneralReportsControls" Assembly="ProjectLibrary" %>
<%@ Page language="c#" Codebehind="GeneralReportsParameters.aspx.cs" AutoEventWireup="True" Inherits="ProjectLibrary.Implementation.App.LocalAspx.GeneralReportsParameters" %>
<html>
	<head>
		<%WriteHead();%>
        <script type="text/javascript">

        function showCalendarMod(frmName, dteBox, btnImg, bHour, bAMPM, event, MnDt, MnMo, MnYr, MxDt, MxMo, MxYr, runFuncs) {
            initCalendar();

            bhour = bHour;
            bAmPm = bAMPM;

            FuncsToRun = runFuncs;
            calfrmName = frmName;
            if (IsCalendarVisible) {
                hideCalendar();
            }
            else {
                if (document.images['calbtn1'] != null) document.images['calbtn1'].src = img_del.src;
                if (document.images['calbtn2'] != null) document.images['calbtn2'].src = img_close.src;
                if (bHideDropDowns) { toggleDropDowns('hidden'); }
                if ((MnDt != null && MnMo != null && MnYr != null) || (MxDt != null && MxMo != null && MxYr != null)) {
                    IsUsingMinMax = true;

                    if (MnDt != null && MnMo != null && MnYr != null) {
                        minDate.setFullYear(MnYr);
                        minDate.setDate(15);
                        minDate.setMonth(MnMo - 1);
                        minDate.setDate(MnDt);
                    } else {
                        minDate = new Date("1/1/" + minYearList);
                    }
                    minDate.setHours(0, 0, 0);

                    if (MxDt != null && MxMo != null && MxYr != null) {
                        maxDate.setFullYear(MxYr);
                        maxDate.setDate(15);
                        maxDate.setMonth(MxMo - 1);
                        maxDate.setDate(MxDt);
                    } else {
                        maxDate = new Date("12/31/" + maxYearList);
                    }
                    maxDate.setHours(23, 59, 59);
                }
                else {
                    IsUsingMinMax = false;
                }
                curImg = btnImg;
                curDateBox = dteBox;

                if (ppcIE && !checkIsIE11()) {
                    ppcX = getOffsetLeft(document.images[btnImg]);
                    ppcY = getOffsetTop(document.images[btnImg]) + document.images[btnImg].height;
                }
                else if (ppcNN) {
                    ppcX = document.images[btnImg].x;
                    ppcY = document.images[btnImg].y + document.images[btnImg].height;
                }

                var toShowDate = todayDate = new Date();
                if (document.getElementsByName(curDateBox)[0]) {
                    var sCurrDate = document.getElementsByName(curDateBox)[0].value;
                    if (sCurrDate.length > 0) {
                        curDate = getDateFromFormat(sCurrDate, BA_DATE_FORMAT_MASK + (bhour ? ' ' + BA_TIME_FORMAT_MASK : ''), bhour);
                        if (curDate == 0 && bhour == false) {
                            curDate = getDateFromFormat(sCurrDate, BA_DATE_FORMAT_MASK + ' ' + BA_TIME_FORMAT_MASK, true);
                        }
                        toShowDate = curDate;
                    } else {
                        toShowDate = todayDate;
                    }
                }

                if (IsUsingMinMax && minDate > todayDate)
                    toShowDate = minDate;

                //		if(IsUsingMinMax &&  toShowDate > maxDate)
                //			toShowDate = maxDate;

                var scrollTop = 0;

                if (document.documentElement && document.body.scrollTop == 0) {
                    scrollTop = document.documentElement.scrollTop;
                }
                else {
                    scrollTop = document.body.scrollTop;
                }

                curScrollY = scrollTop;

                //Detects the calendar icon position, to calculate the screen coordinates and reposition the calendar element in the absolute position
                var obj;
                var currentTargetElement;

                //use the bAMPM element as reference for the click event
                if (event == null){
                    obj = {x: bAMPM.layerX, y: bAMPM.layerY};
                }
                else{
                    currentTargetElement = (event.currentTarget) ? event.currentTarget : event.srcElement;
            
                    obj = getScreenCordinates(event,currentTargetElement);

                    //Add the icon's dimention as an offset
                    obj.x += currentTargetElement.offsetWidth * .5;
                    obj.y += currentTargetElement.offsetHeight * .5;
                }
        
                domlay('popupcalendar', 1, obj.x, obj.y, Calendar(toShowDate.getMonth(), toShowDate.getFullYear(), toShowDate.getDate(), bhour, bAmPm));

                IsCalendarVisible = true;

            }
        }

		function ValidateString(sText)
		{
			var ValidChars = "ñÑ-_%.,/ ";
			var IsNumber=true;
			var Char;
			for(i=0;i<sText.length && IsNumber == true; i++) 
			{ 
				Char = sText.charAt(i); 
				if (!((Char >= '0' && Char <= '9')||(Char >= 'a' && Char <= 'z')||(Char >= 'A'&& Char <= 'Z')||(ValidChars.indexOf(Char)!=-1)))
					IsNumber = false;
			}
			return IsNumber;		
		}
			
		function ValidateInteger(sText)
		{
			var ValidChars = "0123456789";
			var IsNumber=true;
			var Char;
			for(i=0;i<sText.length && IsNumber == true; i++) 
			{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
				{
					IsNumber = false;
				}
			}
			return IsNumber;
		}
		function ValidateReal(sText)
		{
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;
			for(i=0;i<sText.length && IsNumber == true; i++) 
			{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
				{
					IsNumber = false;
				}
			}
			return IsNumber;
		}
		
		function ValidateInputData(){  <%=myControl.GetValidationCode() %>	}
	
		function ExportToExcel()
		{
		    //Git Cargando
		    document.getElementById("register_p").style.display = "block";
            document.getElementById("resultsFrame").src = "";
            document.getElementById("resultsDiv").style.display="none";            			
		    amp = "&";
		    if(GetParametersString()=="")
		       amp = "";

		    var urlresults = "<%= GetReportDisplayUrl()%>"+document.getElementById("ProcessCombo").value+ "&ExpExcel=true" + amp + GetParametersString();
		       
		    if (ValidateInputData()) {
		        EnableWizzard(false);
		        document.getElementById("resultsFrame").src = urlresults;
		        //window.open(urlresults, "xtlswin");
		    } else {
		        document.getElementById('register_p').style.display = 'none';
		    }
			finTimer();
		}
	
            function finTimer()
            {
                //   alert("Alert");
                setTimeout("document.getElementById('register_p').style.display = 'none';", 5000);
                //document.getElementById("register_p").style.display = "none";
            }

		function SendParameters()
            {
		    //Git Cargando
		    document.getElementById("register_p").style.display = "block";
		    
            document.getElementById("resultsFrame").src = "";
            document.getElementById("resultsDiv").style.display="none";
			if(ValidateInputData())
			{
                EnableWizzard(false);
			    amp = "&";
			    if(GetParametersString()=="")
				       amp = "";
			
			    var urlresults = "<%= GetReportDisplayUrl()%>"+document.getElementById("ProcessCombo").value+ "&ExpExcel=false" + amp + GetParametersString();
                var w = this.windowSize()[0] * 0.90;
                var h = this.windowSize()[1] * 0.90;
				//En nueva ventana
                //ShowBAWindowModal('*'+document.getElementById("ProcessCombo").options[document.getElementById("ProcessCombo").selectedIndex].text+'*',w,h,urlresults, null);

                //en iframe
                document.getElementById("resultsDiv").style.width = document.getElementById("resultsDiv").parentElement.offsetWidth - document.getElementById("parametersDiv").offsetWidth -84;
                document.getElementById("resultsFrame").src = urlresults;
                document.getElementById("resultsDiv").style.display = "block";
                       

			} else {
			    document.getElementById('register_p').style.display = 'none';
			}
		    finTimer();
		   
		}

		
		function GetParametersString()
		{
			<%=myControl.GetParametersBuildingCode() %>				
		}
			
		function SetOptionByValue(value)
		{
			if(value =="" || value == null)
                document.getElementById("parametersDiv").style.display = "none";
            else
                document.getElementById("parametersDiv").style.display = "block";
			var i=0;
			var mycombo = GetObject("ProcessCombo");
			for(i=0;i<mycombo.options.length;i++)
			{
				if(mycombo.options[i].value==value)
				{
					mycombo.options.selectedIndex = i;
				}
			}
			
		}
	
		function FillCombo(comboname, filtervalue)
		{
			var combotofill = GetObject(comboname);
			combotofill.options.length=0;
			var nullOption = document.createElement('OPTION');
					combotofill.options.add(nullOption);
					nullOption.innerText = "-------------------";
					nullOption.value = "";
			var i;
			for(i=0;i<eval(comboname+"List.length");i++)
			{
				if(eval(comboname+"List["+i+"][2]")== filtervalue)
				{
					var oOption = document.createElement('OPTION');
					combotofill.options.add(oOption);
					oOption.innerText = eval(comboname+"List["+i+"][1]");
					oOption.value = eval(comboname+"List["+i+"][0]");
				}
			}
            combotofill.onchange();
		}

		function PageStartPoint()
		{ 
			BAonload();
			SetOptionByValue ("<%=GetReportName()%>");
		}

		function GotoProcessReport(reportname)
		{
		    //alert(reportname);
			var url = "<%=GetReportUrl()%>";
			window.location=url+reportname+"&culture=<%=decodestring(Request.QueryString["tokena"])%>";
	
		}
    </script>
		<style>
		
		</style>
    <%=myControl.GetAllRequiredArrays()%>
	</head>
	<body onload="//PageStartPoint(); ">
        <% //this.Response.Write("Ingreso al modulo"); Response.End(); %>
    		<table width="90%">
			<tr>
				<td colspan="3">
					<img src="../../img/WorkPortal/WPReportes.gif" width="48" height="48" alt="" border="0"
						align="top" style="float:right">
					<span style="width:150"><% //= Response.Write(BizAgi.Resources.CResourceManager.Instance)%>
                        <% BizAgi.Resources.CResourceManager.Instance.GetString("EXT_" + "REP_Description", System.Globalization.CultureInfo.CreateSpecificCulture(decodestring(Request.QueryString["tokena"])), "Seleccione el informe y diligencie los parametros de consulta. El informe puede ser visto en pantalla o exportado a Excel"); %>

					</span>
                                                    <!--BizAgi.Resources.CResourceManager.Instance.GetString("EXT_"+"REP_Description",System.Globalization.CultureInfo.CreateSpecificCulture(decodestring(Request.QueryString["tokena"])),"Seleccione el informe y diligencie los parametros de consulta. El informe puede ser visto en pantalla o exportado a Excel") %></span>-->
				</td>
			</tr>
			<tr>
				<td valign="Top" class="Header" colspan="3">
					&nbsp;
				<table>
					<tr>
						<td>
							<b><span><%= BizAgi.Resources.CResourceManager.Instance.GetString("EXT_"+"REP_Combo",System.Globalization.CultureInfo.CreateSpecificCulture(decodestring(Request.QueryString["tokena"])),"Informe a Consultar:") %> </span></b>
                            <br>
						</td>
						<td>
							<% GetReportsCombo();%>
						</td>
						<td>
                        <P></P>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
        <div id="parametersDiv" style="display: block; float:left; margin:2; padding:5" class="ui-bizagi-container ui-bizagi-container-form ui-widget-content ui-bizagi-container-queryform ui-bizagi-rendering-mode-execution ui-widget-content">
		<form id="frm" name="frm" method="post" runat="server">

				<!--			<div class='tb'>
				<span class='btn' onclick='window.top.min(1)' title='Minimize Left'>Min</span>
				<span class='btn' onclick='window.top.res()' title='Restore'>Res</span>
			</div>
-->

            <div style="height:10"></div>

				<MyTag:ParametersRequestControl id="myControl" runat="server" Height="39" Width="194" />
				<asp:DataGrid id="dgResult" runat="server" Visible="False" BackColor="#CCCCCC" BorderColor="#999999"
					BorderStyle="Solid" CellSpacing="2" BorderWidth="3px" CellPadding="4" ForeColor="Black" Height="101px"
					Width="152px">
					<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
					<ItemStyle BackColor="White"></ItemStyle>
					<HeaderStyle Font-Bold="True" ForeColor="Black" BackColor="Gold"></HeaderStyle>
					<FooterStyle BackColor="#CCCCCC"></FooterStyle>
					<PagerStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#CCCCCC" Mode="NumericPages"></PagerStyle>
				</asp:DataGrid>
			
            

			<br>
		</form>
         
		<div class="text" id="popupcalendar"></div>
        </div>
        <div id="register_p"  style="display: none; width: 100%; height: 100%; background-color: #f2f2f2; position: absolute;  z-index: 1; text-align:center;">
            <h3>Cargando ...</h3><br />
            <img src="Ajax-loader.gif" alt="Loading" />

        </div>
        <div id="resultsDiv" style="display: none; float: right; top: -20px; width: 70%; height: 70%;" class="ui-bizagi-container ui-bizagi-container-form ui-widget-content ui-bizagi-container-queryform ui-bizagi-rendering-mode-execution ui-bizagi-container-group-wrapper ui-widget-content">
        <iframe name="resultsFrame" id="resultsFrame" style="height: 100%; width: 100%;">
        </iframe>
        </div>

		<script language='javascript'>
			
            document.sDefHelpTitle='Informes';
			document.sDefHelpText='';
			document.iDefHelpType=1;
			printWizardMagic("merlin1.gif");
			setHelp(document.sDefHelpTitle, "", document.iDefHelpType);
			EnableWizzard(false);
            
			var ex;
			try
			{
				document.getElementById("ShowReportButton").value = '<%= BizAgi.Resources.CResourceManager.Instance.GetString("EXT_"+"REP_BtnView",System.Globalization.CultureInfo.CreateSpecificCulture(decodestring(Request.QueryString["tokena"])),"Ver Informe") %>';
				document.getElementById("ExportToExcelButton").value = '<%= BizAgi.Resources.CResourceManager.Instance.GetString("EXT_"+"REP_BtnExport",System.Globalization.CultureInfo.CreateSpecificCulture(decodestring(Request.QueryString["tokena"])),"Exportar a Excel")%>';				
			}
			catch (ex)
			{
			}
		</script>
	</body>
</HTML>
