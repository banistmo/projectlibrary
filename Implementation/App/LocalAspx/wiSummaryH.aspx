<%@ Page language="c#" Codebehind="wiSummaryH.aspx.cs" AutoEventWireup="false" Inherits="MonitorMovilidadAdmon.wiSummaryH" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>wiSummaryH</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--Estilos en produccion--><LINK href="../../css/estilos.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/WPCustomStyles.css" type="text/css" rel="stylesheet"><LINK href="../../css/WorkPortal/BAWindow.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table height="21" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPLeftButton.gif" width="2" border="0">
					</td>
					<td noWrap width="50"><a class="WPButton" style="BACKGROUND-IMAGE: url(../../img/WorkPortal/WPMiddleButton.jpg)"><asp:button id="buttonLoad" style="Z-INDEX: 101" accessKey="L" runat="server" Text="Actualizar"
								CssClass="WPButtonI BAMnColor"></asp:button></a></td>
					<td vAlign="top" width="2"><IMG height="21" src="../../img/WorkPortal/WPRightButton.gif" width="2" border="0">
					</td>
				</tr>
			</table>
			<asp:datagrid id=dg style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 48px" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataMember="WISUMMARYH" AllowSorting="True" DataKeyField="IDSUMMARY" DataSource="<%# oraDS_wiSummaryH %>" Height="24px" Width="832px" GridLines="None" CellPadding="0" PageSize="20">
				<AlternatingItemStyle CssClass="gridline1"></AlternatingItemStyle>
				<ItemStyle CssClass="gridline2"></ItemStyle>
				<HeaderStyle CssClass="ListHeaderLinks"></HeaderStyle>
				<Columns>
					<asp:ButtonColumn Text="Eliminar" CommandName="Delete"></asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Eliminado">
						<ItemTemplate>
							<asp:CheckBox id=cbBorrar runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BBORRAR").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Forzar intento">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemTemplate>
							<asp:CheckBox id=cbForzar runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BFORZARINTENTO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="IDSUMMARY" ReadOnly="True" HeaderText="Consecutivo">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="STIPO" HeaderText="Tipo"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDWORKITEM" ReadOnly="True" HeaderText="Workitem"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDTASK" ReadOnly="True" HeaderText="Task"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="IDCASE" ReadOnly="True" HeaderText="Case"></asp:BoundColumn>
					<asp:BoundColumn DataField="SCODSOCIEDAD" ReadOnly="True" HeaderText="Sociedad"></asp:BoundColumn>
					<asp:BoundColumn DataField="SDESCPROCACT" ReadOnly="True" HeaderText="Proc.-Act.">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SRADNUMBER" ReadOnly="True" HeaderText="N&#176; alta">
						<HeaderStyle Wrap="False" Width="2cm"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="IDUSER" ReadOnly="True" HeaderText="Usuario">
						<HeaderStyle Wrap="False"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SUSERNAME" ReadOnly="True" HeaderText="Nombre">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SDOMAIN" ReadOnly="True" HeaderText="Dominio">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="DFECHAHORA" ReadOnly="True" HeaderText="Fecha">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Enviado">
						<ItemTemplate>
							<asp:CheckBox id=cbEnviado runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BENVIADO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="DENVIO" ReadOnly="True" HeaderText="F.env&#237;o">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Usuario habilitado">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemTemplate>
							<asp:CheckBox id=cbUsuarioHabilitado runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BUSUARIOHABILITADO").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Error">
						<ItemTemplate>
							<asp:CheckBox id=cbError runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.BERROR").ToString()=="1" %>'>
							</asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="SERROR" ReadOnly="True" HeaderText="Mensaje">
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="IINTENTOS" ReadOnly="True" HeaderText="Intentos">
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="DULTIMOINTENTO" ReadOnly="True" HeaderText="F.&#250;ltimo intento">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle Mode="NumericPages"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
