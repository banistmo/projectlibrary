<%@ Page Language="c#" CodeBehind="AlarmsAdmin.aspx.cs" AutoEventWireup="false" Inherits="BizAgiBPM.App.Admin.AlarmsAdmin" %>

<%@ Register TagPrefix="UI" Namespace="BizAgi.UI.WFBase" Assembly="BizAgi.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>AlarmsAdmin</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0"/>
    <meta name="CODE_LANGUAGE" content="C#"/>
    <meta name="vs_defaultClientScript" content="JavaScript"/>
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
    <link href="../../css/estilos.css" type="text/css" rel="stylesheet"/>
    <link href="../../css/WorkPortal/WPCustomStyles.css" type="text/css" rel="stylesheet"/>
    <link href="../../css/Admin/Common.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../../Localization/LocalizationEN.js"></script>
    <script language="javascript" type="text/javascript" src="../../js/scripts.js"></script>
</head>
<body ms_positioning="FlowLayout">
    <% Header(); %>
    <form id="AlarmsAdmin" method="post" runat="server">
    <table cellspacing="0" cellpadding="2" width="550" align="center">
        <tr>
            <td class="header">
                <!--Label1-->
                <UI:CLabel runat="server" Text="AlarmManagement" />
            </td>
            <script language="javascript">
						BASetLocationFromMain("<%= BizAgi.UI.WFBase.CResourceManager.RM.GetString("AlarmManagement") %>");
            </script>
        </tr>
        <tr>
            <td>
                <span id="SpanMessages" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="header">
                <!--Label2-->
                <UI:CLabel runat="server" Text="AlarmTasks" />
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="2" cellpadding="2" width="100%" align="center">
                    <tr>
                        <td class="ListHeaderLinks" style="background-image: none;" width="20%">
                            <!--Label5-->
                            <UI:CLabel runat="server" Text="AlarmApplication" />
                        </td>
                        <td class="ListHeaderLinks" style="background-image: none;" width="35%">
                            <!--Label6-->
                            <UI:CLabel runat="server" Text="AlarmWorkflow" />
                        </td>
                        <td class="ListHeaderLinks" style="background-image: none;" width="10%">
                            <!--Label6-->
                            <UI:CLabel runat="server" Text="AlarmVersion" />
                        </td>
                        <td class="ListHeaderLinks" style="background-image: none;" width="35%">
                            <!--Label7-->
                            <UI:CLabel runat="server" Text="AlarmTask" />
                        </td>
                    </tr>
                    <tr>
                        <td align="middle">
                            <asp:ListBox ID="ListBoxApplication" runat="server" Width="100%" Rows="15" AutoPostBack="True">
                            </asp:ListBox>
                        </td>
                        <td align="middle">
                            <asp:ListBox ID="ListBoxWFClass" runat="server" Width="100%" Rows="15" AutoPostBack="True">
                            </asp:ListBox>
                        </td>
                        <td align="middle" width="15%">
                            <asp:ListBox ID="ListBoxWFVersion" runat="server" Width="100%" Rows="15" AutoPostBack="True">
                            </asp:ListBox>
                        </td>
                        <td align="middle">
                            <asp:ListBox ID="ListBoxWorkFlow" runat="server" Width="100%" Rows="15" AutoPostBack="True">
                            </asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <UI:CWPHtmlInputButton runat="server" type="button" value="BtnSeeAlarms" name="HtmlInputButtonDisplayAlarmsForTask"
                    class="sbttn" ID="HtmlInputButtonDisplayAlarmsForTask">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
