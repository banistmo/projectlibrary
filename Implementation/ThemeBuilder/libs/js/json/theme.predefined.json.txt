{
  "themes": [
    {
      "id": "00000000-0000-0000-0000-000000000000",
      "name": "Bizagi",
      "type": "predefined",
      "image": "libs/css/img/themes/bizagi_go.png",
      "value": "{\"@skeuomorphic\":\"active\",\"@flat\":\"inactive\",\"@lineal\":\"inactive\"}",
      "thumbs" :["#36454E","#2E516B","#5D6771","#DEDEDE"],
      "version": "1.0.2"
    },
    {
      "id": "86b8d526-5ff2-4513-b206-2cc77cf2a4cc",
      "name": "Lost Woods",
      "type": "predefined",
      "description": "",
      "image": "libs/css/img/themes/lost_woods.png",
      "version": "1.0.2",
      "value": "{\"@background\":\"#86a022\",\"@text\":\"#404c10\",\"@title\":\"#2e370c\",\"@navigation-border\":\"#98b526\",\"@navigation\":\"#516115\",\"@navigation-activate\":\"#637619\",\"@navigation-link\":\"#f4f9e2\",\"@content-border\":\"#b4c76c\",\"@pane\":\"#d7d6cb\",\"@content\":\"#d8dacf\",\"@content-link\":\"#637619\",\"@menu-start\":\"#86a022\",\"@menu-end\":\"#748b1e\",\"@inbox-start\":\"#052b34\",\"@inbox-end\":\"#0e404a\",\"@footer-start\":\"#536c20\",\"@footer-end\":\"#86a022\",\"@modal\":\"#e1e2df\",\"@modal-border\":\"#eeefeb\",\"@tooltip\":\"#ffffff\",\"@button\":\"#fcfdf7\",\"@button-text\":\"#637619\",\"@form-container\":\"#d3d8c0\",\"@form-text\":\"#404c10\",\"@form-group-border\":\"#c5d685\",\"@form-group\":\"#d3d8c0\",\"@form-tab-border\":\"#c5d685\",\"@form-tab\":\"#d3d8c0\",\"@form-grid-border\":\"#c5d685\",\"@form-grid\":\"#d3d8c0\",\"@form-input-border\":\"#c5d685\",\"@form-input\":\"#c8d2a2\",\"@font-family\":\"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif\",\"@active-text-shadow\":\"false\",\"@active-box-shadow\":\"false\",\"@active-border-radius\":\"true\",\"@skeuomorphic\":\"inactive\",\"@flat\":\"active\",\"@lineal\":\"inactive\"}"
    },
    {
      "id": "87dd146b-c877-4ea2-83a7-7a49acb237c5",
      "name": "Beezagi",
      "type": "predefined",
      "description": "",
      "image": "libs/css/img/themes/beezagi.png",
      "version": "1.0.2",
      "value": "{\"@background\":\"#71502e\",\"@text\":\"#291d10\",\"@title\":\"#161009\",\"@navigation-border\":\"#835d35\",\"@navigation\":\"#3b2a18\",\"@navigation-activate\":\"#4d361f\",\"@navigation-link\":\"#ebdccd\",\"@content-border\":\"#cfb281\",\"@pane\":\"#f4e5cb\",\"@content\":\"#d6cbbd\",\"@content-link\":\"#4d361f\",\"@menu-start\":\"#71502e\",\"@menu-end\":\"#432f17\",\"@inbox-start\":\"#de991c\",\"@inbox-end\":\"#d47900\",\"@footer-start\":\"#5f4327\",\"@footer-end\":\"#71502e\",\"@modal\":\"#dcc0a5\",\"@modal-border\":\"#fde9cd\",\"@tooltip\":\"#eed3af\",\"@button\":\"#f2e9df\",\"@button-text\":\"#4d361f\",\"@form-container\":\"#fde9cd\",\"@form-text\":\"#291d10\",\"@form-group-border\":\"#b79c81\",\"@form-group\":\"#eed3af\",\"@form-tab-border\":\"#b79c81\",\"@form-tab\":\"#eed3af\",\"@form-grid-border\":\"#b79c81\",\"@form-grid\":\"#eed3af\",\"@form-input-border\":\"#b79c81\",\"@form-input\":\"#dcb684\",\"@font-family\":\"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif\",\"@active-text-shadow\":\"false\",\"@active-box-shadow\":\"false\",\"@active-border-radius\":\"true\",\"@skeuomorphic\":\"inactive\",\"@flat\":\"active\",\"@lineal\":\"inactive\"} "
    },
    {
        "id": "faeccc1e-2e4c-4d9b-a4cc-1d1c03cee13e",
        "name": "Fires of Mount Doom",
        "type": "predefined",
        "description": "",
        "active":"false",
        "image": "libs/css/img/themes/fires_of_mount_doom.png",
        "version": "1.0.2",
        "value": "{\"@background\":\"#cf2400\",\"@text\":\"#691200\",\"@title\":\"#500e00\",\"@navigation-border\":\"#5f2649\",\"@navigation\":\"#a00007\",\"@navigation-activate\":\"#d42904\",\"@navigation-link\":\"#f6cac3\",\"@content-border\":\"#bca2a2\",\"@pane\":\"#ffeae2\",\"@content\":\"#ffe7e7\",\"@content-link\":\"#7a1f0c\",\"@menu-start\":\"#7c0203\",\"@menu-end\":\"#33021d\",\"@inbox-start\":\"#d42904\",\"@inbox-end\":\"#a00007\",\"@footer-start\":\"#7c0203\",\"@footer-end\":\"#33021d\",\"@modal\":\"#f1e1dd\",\"@modal-border\":\"#f9f0ef\",\"@tooltip\":\"#ffffff\",\"@button\":\"#ffffff\",\"@button-text\":\"#9c1b00\",\"@form-container\":\"#f4c9c1\",\"@form-text\":\"#691200\",\"@form-group-border\":\"#f48a74\",\"@form-group\":\"#f8dcd6\",\"@form-tab-border\":\"#f48a74\",\"@form-tab\":\"#f8dcd6\",\"@form-grid-border\":\"#f48a74\",\"@form-grid\":\"#e9927f\",\"@form-input-border\":\"#f48a74\",\"@form-input\":\"#eca495\",\"@font-family\":\"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif\",\"@active-text-shadow\":\"false\",\"@active-box-shadow\":\"true\",\"@active-border-radius\":\"true\",\"@skeuomorphic\":\"active\",\"@flat\":\"inactive\",\"@lineal\":\"inactive\"}"
    },
    {
        "id": "35161aa3-b684-45ed-965e-4329a752cd28",
        "name": "Brightest Day",
        "type": "predefined",
        "description": "",
        "active":"false",
        "image": "libs/css/img/themes/brightest_day.png",
        "version": "1.0.2",
        "value": "{\"@background\":\"#dadada\",\"@text\":\"#888888\",\"@title\":\"#888888\",\"@navigation-border\":\"#e7e7e7\",\"@navigation\":\"#c1c1c1\",\"@navigation-activate\":\"#cecece\",\"@navigation-link\":\"#888888\",\"@content-border\":\"#d4d4d4\",\"@pane\":\"#ededed\",\"@content\":\"#e7e7e7\",\"@content-link\":\"#888888\",\"@menu-start\":\"#efefef\",\"@menu-end\":\"#d1d1d1\",\"@inbox-start\":\"#c1c1c1\",\"@inbox-end\":\"#cecece\",\"@footer-start\":\"#d8d8d8\",\"@footer-end\":\"#efefef\",\"@modal\":\"#ffffff\",\"@modal-border\":\"#ffffff\",\"@tooltip\":\"#ffffff\",\"@button\":\"#ffffff\",\"@button-text\":\"#888888\",\"@form-container\":\"#eeeeee\",\"@form-text\":\"#888888\",\"@form-group-border\":\"#ffffff\",\"@form-group\":\"#ffffff\",\"@form-tab-border\":\"#ffffff\",\"@form-tab\":\"#ffffff\",\"@form-grid-border\":\"#ffffff\",\"@form-grid\":\"#ffffff\",\"@form-input-border\":\"#ffffff\",\"@form-input\":\"#ffffff\",\"@font-family\":\"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif\",\"@active-text-shadow\":\"false\",\"@active-box-shadow\":\"true\",\"@active-border-radius\":\"true\",\"@skeuomorphic\":\"active\",\"@flat\":\"inactive\",\"@lineal\":\"inactive\"}"    
    }
  ]
}