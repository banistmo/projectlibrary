/*
 *   Name: BizAgi Theme Builder Services
 *   Author: David Niño (based on Edward Morales and Diego Parra version)
 *   Comments:
 *   -   This class will provide a facade to access to workportal REST services
 */

$.Class.extend("bizagi.themebuilder.services.service", {}, {
    /* 
     *   Constructor
     */
    init: function(params) {
        params = typeof (params) == "object" ? params : {};

        params.proxyPrefix = "../";

        this.serviceLocator = new bizagi.themebuilder.services.context(params);
    },
    /**
     * This method will return the url defined as endPoint using the parameter "endPoint" name
     */
    getUrl: function(params) {
        var self = this;
        var url = self.serviceLocator.getUrl(params.endPoint);
        return url;
    },
    
    /*
    *  Get Menu Authorization
    */
    getMenuAuthorization: function () {
        var self = this;

        return $.read(
                self.serviceLocator.getUrl("getMenuAuthorization")
                );
    },

    /**
     * This service get json of theme definition
     *@return {json} theme definition
     */
    getCurrentTheme: function() {
        var self = this;
        var url = self.serviceLocator.getUrl("getCurrentTheme");

        return $.read(url).pipe(function(data) {
            return data;
        });
    },

    /**
     * Get list of defined themes
     * @return {json} object with themes definicion
     */
    getAllThemes: function() {
        var self = this;
        var url = self.serviceLocator.getUrl("getAllThemes");

        return $.read(url).pipe(function(data) {
            return data;
        });
    },

    /*
     *
     */
    createTheme: function(params) {
        var self = this;
        var url = self.serviceLocator.getUrl("createTheme");
        var data = {};
        params = params || {};

        data.json   = params || "";

        return $.create(url, data);
    },

    /*
     *
     */
    publishTheme: function(params) {
        var self = this;
        var url = self.serviceLocator.getUrl("publishTheme");
        var data = {};
        params = params || {};

        data.json = params.json || "";
        
        return $.update(url, data);
    },

    /*
     *
     */
    updateTheme: function(params) {
        var self = this;
        var url = self.serviceLocator.getUrl("updateTheme");
        var data = {};
        
        url = printf(url, params.id);

        data.json = params.json || "";
        
        return $.update(url, data);
    },

    /*
     *
     */
    deleteTheme: function(params) {
        var self = this;
        var url = self.serviceLocator.getUrl("deleteTheme");
        var data = {};
        params = params || {};

        url = printf(url, params.id);
        
        return $.destroy(url);
    },
    /**
     * Get dummy json with themes definitions
     * @return {json} object with content of the theme.dummy.json file
     */
    getDummyThemes: function() {
        return $.ajax({
            dataType: "json",
            url: "libs/js/json/theme.dummy.json.js"
        }).pipe(function(data) {
            return data;
        });
    }
});