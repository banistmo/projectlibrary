﻿/*
*   Name: BizAgi Workportal Render Webpart
*   Author: Ricardo Perez
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.summaryCaseWindows8", {}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        var params = initialParams || {};
        // Call base
        this._super(workportalFacade, dataService, initialParams);

        self.container = initialParams.container || document;

        self.container.bind("ui-bizagi-show-summary-win-8", function (e, params) {
            if (typeof (params.idWorkitem) !== "undefined" && params.idWorkitem != 0) {

                initialParams.idWorkitem = params.idWorkitem;
                self.refresh($.extend(initialParams, params));

                //Animation efect on title
                $(".pagetitle", self.container).fadeOut(400, function () {
                    this.textContent = (typeof params.taskState !== "undefined") ? params.taskState : self.data.process;
                    $(this).fadeToggle(400);
                });
            }
        });
    },

    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var content = self.content || $();
        return content;
    },

    /*
    *   Customize the web part in each device
    */

    postRender: function (params) {
        var self = this;
        params = params || {};
        var defer = new $.Deferred();

        params.canvas.empty();

        // Display progress ring
        self.showProgressRing(params);

        self.dataService.summaryCaseDetails({
            idCase: params.idCase,
            eventAsTasks: params.eventAsTasks || false,
            onlyUserWorkItems: params.onlyUserWorkItems || false,
            idWorkitem: params.idWorkitem,
            isOfflineForm: params.isOfflineForm || false
        }).done(function (data) {
            data.params = params;
            self.data = data;

            self.renderCurrentTab(params, data);

            if (data.currentState && data.currentState[0] && data.currentState[0].allowReleaseActivity) {
                $('.bz-sc-release', self.content).bind('click', function () {
                    self.release(params);
                });
            }

            defer.resolve(data);

        }).fail(function (error) {
            // Case not exist
            self.hiddenContainerSummary();

            defer.reject(error);
        });

        return defer.promise();
    },

    /*
    * Action release
    */
    release: function (params) {
        var self = this;
        var defered = new $.Deferred();
        var buttons = [{ 'label': self.getResource("workportal-widget-dialog-box-release-ok"), 'action': 'resolve' }, { 'label': self.getResource("workportal-widget-dialog-box-release-cancel")}];
        $.when(bizagi.showConfirmationBox(self.getResource("workportal-widget-dialog-box-release"), self.getResource("render-actions-release"), '', buttons)).done(function () {
            bizagi.util.smartphone.startLoading();
            $.when(self.dataService.releaseActivity({
                idCase: params.idCase,
                idWorkItem: params.idWorkitem
            })).done(function (data) {
                var status = (data && data.status) ? data.status : 'Error';
                switch (status) {
                    case "Success":
                        WinJS.Navigation.navigate("/pages/groupedItems/groupedItems.html");
                        break;
                    case "ConfigurationError":
                        bizagi.showMessageBox(self.getResource("workportal-widget-dialog-box-release-configuration-error-message").replace("{0}", params.idWorkitem), self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                        break;
                    case "Error":
                    default:
                        bizagi.showMessageBox(self.getResource("workportal-widget-dialog-box-release-error-message").replace("{0}", params.idWorkitem), self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                        break;
                }
                bizagi.util.smartphone.stopLoading();
                defered.resolve();
            }).fail(function () {
                var message = self.getResource("workportal-widget-dialog-box-release-error-message").replace("{0}", params.idWorkitem);
                bizagi.showMessageBox(message, self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                bizagi.util.smartphone.stopLoading();
                defered.reject();
            });
        });
        return defered.promise();
    },

    showProgressRing: function (params) {
        var progress = document.createElement("progress");

        progress.className = "win-ring win-large ringProgressWebparts";
        $(progress).appendTo(params.canvas);
    },

    hiddenContainerSummary: function () {
        var self = this;

        var canvasRender = self.container.context.querySelector(".formRenderContent");
        var summarySlider = self.container.context.querySelector(".summarySlider");
        var progress = self.container.context.querySelector(".ringProgressWebparts");

        progress.parentNode.removeChild(progress);

        var collapseAnimation = WinJS.UI.Animation.createRepositionAnimation(canvasRender, summarySlider);
        canvasRender.style.msGridColumn = "1";
        canvasRender.style.msGridColumnSpan = "2";
        summarySlider.classList.add("closed");

        // Execute the collapse animation.
        collapseAnimation.execute();

        $(summarySlider).css('visibility', 'hidden');
    },

    /*
    *   Renders the tabs component of the webpart
    */
    renderCurrentTab: function (argsParams, argsData) {
        var self = this;
        var params = argsParams || {};
        var data = argsData;
        var content = self.getContent();

        var cache = {};
        var htmlContent = "";

        //data.showComments = true;// Functionality not implemented
        self.content = $.tmpl(self.workportalFacade.getTemplate("summaryCaseWindows8"), data);

        //save opacity to check if we must animate enter contetn or not
        var opacity = params.canvas.style("opacity");

        //Animation efect on webpart
        if (opacity !== "0") {
            WinJS.UI.Animation.exitContent(params.canvas.context, null).done(function () {
                params.canvas.empty();
                self.content.appendTo(params.canvas);
                WinJS.UI.Animation.enterContent(params.canvas.context);
            });
        } else {
            params.canvas.empty();
            self.content.appendTo(params.canvas);
        }

        // Delegate events for parent process
        $("#details", self.content).delegate(".summaryLink", "click", function () {
            self.container.triggerHandler("navigate-from.webpart-win8", { idCase: data.parentProcess.idCase, isOpen: false, radNumber: data.radNumberParentCase, title: data.parentProcess.displayName });
        });

        // jQuery-ui tabs
        $("#ui-bizagi-details-tabs", self.content).tabs();

        $("#ui-bizagi-details-tabs", self.content).bind("tabsbeforeactivate", function (event, ui) {
            bizagi.util.setContext({
                commentsFocus: false
            });
            switch (ui.newPanel.attr("id")) {
                case "comments":
                    if (cache["comments"] == undefined) {
                        // Extend render with comments
                        $.extend(self, {}, bizagi.workportal.comments);
                        // Define canvas
                        data.canvas = $("#comments", ".summaryCase");
                        data.readOnly = ($(ui.tab).data('isclosed')) ? true : false;
                        $.when(self.renderComments(data))
                            .done(function (htmlContent) {
                                cache["comments"] = htmlContent;
                            });
                    }

                    bizagi.util.setContext({
                        commentsFocus: true
                    });
                    break;
                case "subprocess":
                    if (cache["subprocess"] == undefined) {
                        $.when(
                            self.dataService.summarySubProcess({
                                idCase: self.params.idCase
                            })
                        ).done(function (subprocess) {
                            htmlContent = $.tmpl(self.workportalFacade.getTemplate("subprocessSummaryCaseWindows8"), subprocess);
                            htmlContent.appendTo($("#subprocess", ".summaryCase"));
                            cache["subprocess"] = htmlContent;
                            // Define Events for subprocess tab
                            htmlContent.delegate(".summaryLink", "click", function () {
                                var index = $(this).find("#indexSubProcess").val() || $(this).find("#index").val();
                                self.container.triggerHandler("navigate-from.webpart-win8", { idCase: subprocess.subProcesses[index].idCase, isOpen: subprocess.subProcesses[index].isOpen == "true", radNumber: subprocess.subProcesses[index].radNumber, title: subprocess.subProcesses[index].displayName, item: subprocess.subProcesses[index] });
                            });
                        });
                    }
                    break;
                case "assignees":
                    if (cache["assignees"] == undefined) {

                        $.when(
                            self.dataService.summaryAssigness({
                                idCase: self.params.idCase
                            })
                        ).done(function (assignees) {
                            htmlContent = $.tmpl(self.workportalFacade.getTemplate("assigneesSummaryCaseWindows8"), assignees);
                            htmlContent.appendTo($("#assignees", ".summaryCase"));
                            cache["assignees"] = htmlContent;
                        });
                    }
                    break;
                case "events":
                    if (cache["events"] == undefined) {
                        $.when(
                            self.dataService.summaryCaseEvents({
                                idCase: self.params.idCase
                            })
                        ).done(function (events) {
                            htmlContent = $.tmpl(self.workportalFacade.getTemplate("eventsSummaryCaseWindows8"), events);
                            htmlContent.appendTo($("#events", ".summaryCase"));
                            cache["events"] = htmlContent;
                            htmlContent.delegate(".summaryLink", "click", function () {
                                var element = $(this);
                                var index = element.parent().find("#indexEvent").val();

                                var idCase = element.data('case') || self.params.idCase;
                                var idWorkItem = element.data('workitem');
                                var displayName = element.data('displayname') || '';
                                var idTask = element.data('task');
                                var item = events.events[index][displayName];

                                self.container.triggerHandler("navigate-from.webpart-win8", { idCase: idCase, idWorkitem: idWorkItem, idTask: idTask, isOpen: false, title: displayName, item: item });
                            });
                        });
                    }
                    break;
                case "activities":
                    if (cache["activities"] == undefined) {
                        $.when(
                            self.dataService.summaryActivities({
                                data: data,
                                idWorkitem: self.params.idWorkitem
                            })
                        ).done(function (activities) {
                            activities["idCase"] = self.params.idCase;
                            htmlContent = $.tmpl(self.workportalFacade.getTemplate("activitiesSummaryCaseWindows8"), activities);
                            htmlContent.appendTo($("#activities", ".summaryCase"));
                            cache["activities"] = htmlContent;
                            htmlContent.delegate(".summaryLink", "click", function () {
                                var element = $(this);
                                var index = element.find("#indexActivity").val();
                                var item = activities.currentState[index];

                                var idCase = element.data('case') || self.params.idCase;
                                var idWorkItem = element.data('workitem');
                                var displayName = element.data('displayname') || item.state;
                                var idTask = element.data('task');

                                self.container.triggerHandler("navigate-from.webpart-win8", { idCase: idCase, idWorkitem: idWorkItem, idTask: idTask, isOpen: false, title: displayName, item: item });
                            });
                        });
                    }
                    break;
            }
        }); //End of tabselector bind
    }
});