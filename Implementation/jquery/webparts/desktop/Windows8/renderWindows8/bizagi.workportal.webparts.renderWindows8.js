﻿/*
*   Name: BizAgi Workportal Render Webpart
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.renderWindows8", {
    BIZAGI_WORKPORTAL_WIDGET_RENDER: "activityform",
    BIZAGI_WORKPORTAL_WIDGET_OLDRENDERINTEGRATION: "oldrenderintegration",
    BIZAGI_WORKPORTAL_WIDGET_ASYNC: "async",
    BIZAGI_WORKPORTAL_WIDGET_ROUTING: "routing",
    ASYNC_CHECK_TIMER: 3000
}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        this._super(workportalFacade, dataService, initialParams);

        self.container = initialParams.container || document;

        self.container.bind("ui-bizagi-show-render", function (e, triggerParams) {
            self.renderForm(triggerParams);
        });
    },

    renderContent: function (params) {
        var self = this;
        var template = self.workportalFacade.getTemplate("renderWindows8");
        var content = self.content = $.tmpl(template);
        return content;
    },

    /*
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;
        var content = self.getContent();

        params = params || {};
        params.context = "sharepoint";
        params.sharepointProxyPrefix = self.sharepointProxyPrefix;

        // Render form 
        params.database = self.dataService.database;

        // Attach handler to render container to subscribe for routing events>
        content.bind("routing", function (e, triggerParams) {
            self.performRouting(triggerParams || params);
        });

        if (params.idWorkitem) {
            self.renderingExecute(params);
        } else {
            self.performRouting(params);
        }
    },

    /*
    * Listener to ui-bizagi-show-render event
    */
    renderForm: function (params) {
        var self = this;
        self.refresh(params);
    },

    /*
    * Executes the action
    */
    performRouting: function (params) {
        var self = this;
        var content = self.getContent();

        $.when(self.dataService.routing.getRoute(params)).done(function (route) {
            route = route || {};
            if (params.isOfflineForm != "undefined" && params.isOfflineForm == true) {
                route.moduleParams = $.extend(route.moduleParams, { formsRenderVersion: self.params.formsRenderVersion, isOfflineForm: self.params.isOfflineForm, idCase: self.params.idCase, idWorkitem: self.params.idWorkitem }) || {};
            }
            else {
                route.moduleParams = route.moduleParams || {};
            }

            switch (route.module) {
                case self.Class.BIZAGI_WORKPORTAL_WIDGET_RENDER:
                    if (typeof route.moduleParams.displayName !== 'undefined') {
                        route.moduleParams.taskState = route.moduleParams.displayName;
                    }

                    self.container.triggerHandler("ui-bizagi-show-summary-win-8", route.moduleParams);
                    if (route.moduleParams.idWorkitem) {
                        self.renderForm(route.moduleParams);
                    } else {
                        if (route.moduleParams.messageForm || route.moduleParams.withOutGlobalForm) {
                            self.showFinishMessage(route.moduleParams);
                        } else {
                            //Render summary form
                            params.idWorkitem = null;
                            params.data = null;
                            params.idCase = route.moduleParams.idCase;
                            self.renderingExecute(params);
                        }
                    }
                    break;
                case self.Class.BIZAGI_WORKPORTAL_WIDGET_OLDRENDERINTEGRATION:
                    var errorTemplate = self.workportalFacade.getTemplate("infoRenderFormWindows8");
                    var message = self.resources.getResource("workportal-widget-oldrenderintegration-mobile");
                    var errorHtml = $.tmpl(errorTemplate, {
                        message: message
                    });

                    //Animation efect on webpart to finish case
                    content.fadeOut(400, function () {
                        $(this).empty();
                        errorHtml.appendTo($(this));
                        $(this).fadeToggle(400);
                    });
                    break;
                case self.Class.BIZAGI_WORKPORTAL_WIDGET_ASYNC:
                    //TODO: Implement Async
                    self.executeAsync(route.moduleParams);
                    break;
                case self.Class.BIZAGI_WORKPORTAL_WIDGET_ROUTING:
                    // it has many workItems or subprocess
                    // show routing window
                    var workItemsTemplate = self.workportalFacade.getTemplate("workitemsWindows8");
                    var workItemsHtml = $.tmpl(workItemsTemplate, {
                        workitems: route.moduleParams.data.workItems
                    });

                    self.container.triggerHandler("ui-bizagi-show-summary-win-8", { idCase: route.moduleParams.idCase });

                    $(".ui-bizagi-webpart-workitems-pivot", workItemsHtml).bind("MSPointerDown", onPointerDown).bind("MSPointerUp MSPointerOut", onPointerUp).bind("click", function () {
                        var item = $(this);
                        var idCaseToSend = item.data("idcase") ? item.data("idcase") : route.moduleParams.idCase;
                        //only for win 8

                        self.renderForm({ idWorkitem: item.data("idworkitem"), idCase: idCaseToSend });
                        self.container.triggerHandler("ui-bizagi-show-summary-win-8", { idCase: idCaseToSend, idWorkitem: item.data("idworkitem"), taskState: item.data("displayname") });
                    });

                    // Remove loading icon from summary container
                    content.empty();
                    workItemsHtml.appendTo(content);
                    break;
            }
        });

        function onPointerDown(evt) {
            WinJS.UI.Animation.pointerDown(evt.currentTarget);
        }

        function onPointerUp(evt) {
            WinJS.UI.Animation.pointerUp(evt.currentTarget);
        }
    },

    /*
    * Executes the rendering
    */
    renderingExecute: function (params) {
        var self = this;
        var content = self.getContent();

        var rendering = new bizagi.rendering.facade(params);
        var progress = document.createElement("progress");

        progress.className = "win-ring win-large ringProgressWebparts";
        $(progress).appendTo(content);

        bizagi.context.isOfflineForm = params.isOfflineForm || false;
        bizagi.context.idCase = params.idCase;

        // Executes rendering into render container
        content.fadeOut(400, function () {
            rendering.execute($.extend(params, {
                canvas: content
            })).done(function (data) {
                if (progress.parentNode)
                    progress.parentNode.removeChild(progress);
                self.container.triggerHandler("render-ready");
            });

            //Animnation to show the content again
            $(content).fadeToggle(400);
        });
    },

    /*
    * Show Message
    */
    showFinishMessage: function (params) {
        var self = this;
        var content = self.getContent();
        var errorTemplate = self.workportalFacade.getTemplate("infoRenderFormWindows8");
        var message = self.resources.getResource("render-without-globalform");
        var btn = self.resources.getResource("render-without-globalform-button");

        if (typeof params.messageForm != "undefined" && params.messageForm != "") {
            message = params.messageForm;
        }

        if (typeof self.params != "undefined" && typeof self.params.isOfflineForm !== "undefined" && self.params.isOfflineForm == true) {
            message = self.resources.getResource("render-without-globalform-offline");
        }

        var errorHtml = $.tmpl(errorTemplate, {
            message: message,
            button: btn
        });

        $(".ui-bizagi-info-message-button", errorHtml).click(function () {
            WinJS.Navigation.navigate("/pages/groupedItems/groupedItems.html");
        });

        //Animation efect on webpart to finish case
        content.fadeOut(400, function () {
            $(this).empty();
            errorHtml.appendTo($(this));
            $(this).fadeToggle(400);
        });
    },

    executeAsync: function (params) {
        var self = this;
        var defer = new $.Deferred();
        self.checkAsycnExecutionState(defer, params);
        return defer.promise();
    },

    checkAsycnExecutionState: function (defer, params) {
        var self = this;
        var async = this;
        var content = self.getContent();

        $.when(self.dataService.getAsynchExecutionState({
            idCase: self.params.idCase
        })).done(function (response) {
            var template = self.workportalFacade.getTemplate("asyncWindows8");

            // verify errors in response
            if (response.state == "Error" && response.errorMessage != undefined) {
                // Change default error
                response.errorMessage = bizagi.localization.getResource("render-async-error");
            }

            // Render base template
            var asyncHTML = $.tmpl(template, response);

            // Check for finish signal
            if (response.state == "Processing") {
                // Check for non-error message
                if (response.errorMessage.length == 0) {
                    setTimeout(function () {
                        // Refresh widget after a timer
                        async.checkAsycnExecutionState(defer, params);

                    }, self.Class.ASYNC_CHECK_TIMER);
                }

            } else if (response.state == "Finished") {
                // Execute routing
                self.performRouting({ action: self.Class.BIZAGI_WORKPORTAL_WIDGET_ROUTING, idCase: self.params.idCase });
            }
            //Animation efect on webpart to finish case
            content.fadeOut(400, function () {
                $(this).empty();
                asyncHTML.appendTo(content);
                $(this).fadeToggle(400);
            });
            // Resolve main deferred
            defer.resolve(content);
        });
    },


    /*
    *  Get the render version from server or if is offline set to 2
    */
    getFormRenderVersion: function (params) {
        var self = this;
        if (typeof params.isOfflineForm != "undefined")
            return { formsRenderVersion: 2 };
        return self.dataService.getCaseFormsRenderVersion(params);
    }

});
