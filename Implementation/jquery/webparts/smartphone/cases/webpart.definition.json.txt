﻿{
	"name": "cases",
	"js": [{"src":"jquery.inview.js"}, {"src": "bizagi.workportal.webparts.cases.js"}],
	"css": [ {"src": "bizagi.webpart.cases.css"} ],
	"tmpl": [ 
		{alias: "inbox-mail",  "src": "cases.tmpl.html#ui-bz-wp-widget-inbox-mail"},
		{alias: "item-loadmore",  "src": "cases.tmpl.html#ui-bz-wp-widget-inbox-mail-item-loadmore"} ,
		{alias: "item",  "src": "cases.tmpl.html#ui-bz-wp-widget-inbox-mail-item"},
		{alias: "summary", src:"cases.tmpl.html#ui-bz-wp-widget-inbox-mail-summary"}
	]
}