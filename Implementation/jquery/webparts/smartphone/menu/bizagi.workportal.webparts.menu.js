
/*
*   Name: BizAgi Workportal Cases Webpart
*   Author: Diego Parra
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.menu", {}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        this._super(workportalFacade, dataService, initialParams);

        // Set listeners
        //   this.subscribe("bz-mobile-show-cases", function (e, params) { self.showCases(params); });
        this.isFirtsPageLoad = true;
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        //Override the login function, this is only necesary in WorkPortal
        bizagi.services.ajax.loginPage = function () { };
        var self = this;
        var template = self.workportalFacade.getTemplate("menu-base");
        var defer = new $.Deferred();
        $.when(self.dataService.getCurrentUser()).done(function (data) {
            bizagi.currentUser = data;
            var content = self.content = $.tmpl(template, $.extend(data, {
                environment: bizagi.loader.environment || "",
                build: bizagi.loader.build,
                base64image: "",
                showAboutMenu: bizagi.override.enableShowAboutMenu
            }));
            defer.resolve(content);
        });
        return defer.promise();
    },
    /*
    *   Customize the web part in each device  
    */
    postRender: function (params) {
        var self = this;
        self.enableKendoDemo();
        self.enableItemsMenu();
         

    },

    enableKendoDemo: function () {
        var self = this;
        if (typeof bizagi.kendoMobileApplication === "undefined") {
            bizagi.kendoMobileApplication = new kendo.mobile.Application($("body"), {
                transition: "slide",
                skin: "flat",
                //initial: "initKendo",
                //webAppCapable: false ,
                // serverNavigation : true,
                init: function () {
                    //  bizagi.kendoMobileApplication.showLoading();
                }

            });

        }

    },



    enableItemsMenu: function () {
        var self = this;
        var content = self.getContent();
        var menuItemsTmpl = self.workportalFacade.getTemplate("menu-items");
        $.when(self.getSecurityMenu()).done(function (menuItems) {
            menuItems.authCurrentUserAdministration = false;
            menuItems.authAnalysisReports = false;
            menuItems.authAdmin = false;
            menuItems.enableLogOut = true;
            //todo incorporar template kendo tagregarle capacidad del l10n
            $.tmpl(menuItemsTmpl, menuItems).appendTo("[data-bizagi-component*='menu-items']", content);
            $("[data-bizagi-component*='menu-items']", content).kendoMobileListView({
                appendOnRefresh: false,
                pullToRefresh: false
            }).delegate("[data-icon='new-case']", "click", function (e) {
                e.preventDefault();
                alert("nc");
            }).delegate("[data-icon='inbox']", "click", function (e) {
                e.preventDefault();
                // bizagi.kendoMobileApplication.navigate("workarea");
                alert("inbox");
            }).delegate("[data-icon='logout']", "click", function (e) {
                e.preventDefault();
                alert("logout");
            });

        });

        /*$(document).on("click", "a#bz-workarea-newcase", function (e) {
        e.preventDefault();
        alert("nc");
        });  */


    },
    getSecurityMenu: function () {
        var self = this;
        var authMenu = {};
        var df = new $.Deferred();
        self.security = {};
        self.jsonSecurityList = {};

        $.when(self.dataService.getMenuAuthorization()).done(function (data) {
            self.convertSecurityData(data);
            self.jsonSecurityList = data;
            authMenu.authNewCase = self.security.NewCase || false;
            authMenu.authAnalysisReports = self.checkRootCategory(data, "AnalysisReports");
            authMenu.authAdmin = self.checkRootCategory(data, "Admin");
            authMenu.authCurrentUserAdministration = self.security.CurrentUser;
            $.each(self.security, function (key, value) {
                authMenu[key] = value;
            });
            df.resolve(authMenu);
        });
        return df.promise();
    },

    convertSecurityData: function (data) {
        var self = this;
        data = data || {};
        for (var i in data) {
            if (!data.hasOwnProperty(i)) continue;
            if (typeof data[i] == 'object') {
                self.convertSecurityData(data[i]);
            }
            else if (data[i] != undefined) {
                self.security[data[i]] = true;
            }
        }
    },

    checkRootCategory: function (data, key) {
        if (data.permissions != undefined) {
            for (var i = 0; i < data.permissions.length; i++) {
                if (data.permissions[i][key] != undefined) {
                    return true;
                }
            }
        }
        return false;
    }

});
