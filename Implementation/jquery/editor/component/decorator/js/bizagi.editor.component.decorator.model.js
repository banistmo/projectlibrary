﻿/*
 * Author : Alexande Mejia
 * Date   : 13 Ago 2012
 * Comments:
 *     Define the model of the decorator component
 *
 */

$.Class.extend("bizagi.editor.component.decorator.model", {


	/*
	 *   @static
	 *   Return the model for a specified control
	 */
	getSpecificModel: function(control) {
		var self = this;
		var type = control.type || control.name;


		self.models = {
			"grid": { specificOptions: [
				{ id: "ConfigGrid", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-editcolumns")}
			] },
			"offlinegrid": { specificOptions: [
				{ id: "ConfigGrid", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-editcolumns")}
			] },
			"groupedgrid": { specificOptions: [
				{ id: "ConfigGrid", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-editcolumns")}
			] },
			"nestedform": { standardOptions: [
				{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
				{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
			],
				specificOptions: [
					{ id: "EditNestedForm", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-editform")}
				]
			},
			"querynestedform": { standardOptions: [
				{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
				{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
			],
				specificOptions: [
					{ id: "EditNestedForm", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-editform")}
				]
			},
			"horizontal": { standardOptions: [
				{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
				{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
			]
			},
			"container": {
				standardOptions: [
					{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
					{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
				]
			},
			"formbutton": {
				standardOptions: [
					{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
					{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
				]
			},
			"query": {
				standardOptions: [
					{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
					{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
				]
			}

		};

		self.standardOptions = [
			{ allowProperties: true, id: "gear", displayName: bizagi.localization.getResource("bizagi-editor-properties-caption") },
			{ allowXpath: true, id: "bind", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-bind") },
			{ allowDelete: true, id: "delete", displayName: bizagi.localization.getResource("bizagi-editor-decorator-model-allowdelete")}
		];


		if(type == "queryInternal" || type == "query") {
			var model = self.models["query"] || { standardOptions: self.standardOptions };
		}
		else {
			model = self.models[control.name] || self.models[type] || { standardOptions: self.standardOptions };
		}

		if(!model.standardOptions) model.standardOptions = self.standardOptions;

		return model;
	}
}, {});
