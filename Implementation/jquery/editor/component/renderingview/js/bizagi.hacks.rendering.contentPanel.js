
/*
*   Name: BizAgi Form Modeler Panel Extension
*   Author: Christian Collazos
*   Comments:
*   -   This script will redefine the panel class to adjust to form modeler
*/

// Auto extend
bizagi.rendering.contentPanel.extend("bizagi.rendering.contentPanel", {}, {
    /* 
    *   Template method to implement in each device to customize each container after processed
    */
    postRenderContainer: function (panel) {
        var self = this;

        self._super(panel);


        //bind header double click
        var header = panel.find(".ui-bizagi-container-contentpanel-header");


        header.dblclick(function () {

            header.hide();

            // Publish label edition event
            self.triggerGlobalHandler("startlabeledition");

            // Create editable label component
            var presenter = new bizagi.editor.component.editableLabel.presenter({
                label: header,
                value: self.properties.displayName
            });

            // Bind change event
            presenter.subscribe("change", function (ev, args) {
                self.triggerGlobalHandler("changelabel", { guid: self.properties.guid, value: args.value });
            });

            // Render label
            $.when(presenter.render()).done(function () {
                if (self.getElement().find(".ui-bizagi-container-input-editable").length == 0) {
                    setTimeout(function () {
                        header.hide();
                        self.adjustEditLabel();
                    }, 100);
                } else {
                    self.adjustEditLabel();
                }
            });
        });
    },

    adjustEditLabel: function () {
        var self = this;

        self.getElement().find(".ui-bizagi-container-input-editable").css({
            "width": "95%",
            "background-color": "orange",
            "top": "-18px",
            "border-radius": "10px"
        });

        self.getElement().find(".ui-bizagi-container-input-editable input").css({
            "margin-left": "5px",
            "width": "90%"
        });

        self.getElement().find(".ui-bizagi-render-confirmation").css({
            "right": "10px",
            "top": "5px"
        });
    },

    showElementLabelEditor: function () {
        var self = this;

        if (!(self.container.find(".ui-bizagi-container-input-editable > input.ui-bizagi-input-editable").length > 0)) {
            self.container.find("span:first").trigger('dblclick');
        }
    }
})