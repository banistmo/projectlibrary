﻿/*
*   Name: BizAgi Editor Component Properties  Commands Group by
*   Author: Alexander Mejia
*   Comments:
*   -   This script will define basic stuff for command
*/

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.groupby", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.showcolumn", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The showcolumn property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.displaytype", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.isadministrable", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var entityType = self.element.triggerGlobalHandler("getContextEntityType");        
        if (entityType != "parameter")
            self.indexedProperty.notShow = true;
               
    }
});


bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.selectProcess", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var entityType = self.element.triggerGlobalHandler("getContextEntityType");

        if (entityType != "application")
            self.indexedProperty.notShow = true;
        else {           
            self.indexedProperty.allowClick = self.element.triggerGlobalHandler("getControllerInfo", { type: "isNewForm" });
        }

    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.allowdecimals", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node.getRenderType() == "oracleNumber") {
                self.indexedProperty.notShow = true;
                self.properties.allowdecimals = false;
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.defaultinclude", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid() || !self.properties.selectable )
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.numberrange", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node.getRenderType() == "oracleNumber") {
                self.properties.allowdecimals = false;
                self.indexedProperty.notShow = true;                
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.defaultvalue", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node.getRenderType() == "oracleNumber") {
                self.indexedProperty.notShow = true;
            }
        }
    }
});