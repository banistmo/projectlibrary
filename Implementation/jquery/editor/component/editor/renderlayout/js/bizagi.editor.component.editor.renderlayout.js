/*
@title: Editor renderlayout
@authors: Rhony Pedraza /Ramiro Gomez
@date: 31-may-12
*/
bizagi.editor.component.editor(
    "bizagi.editor.component.editor.renderlayout", {
        listensTo: ["innerPropertyChange"]
    }, {
        init: function (canvas, model, controller) {
            this._super(canvas, model, controller);
        },
        renderEditor: function (container, data) {
            var elEditor,
            subpropertiesLength,
            i,
            subproperty,
            calculatedValues,
            names,
            self = this;

            self.properties = [];

            subpropertiesLength = data.subproperties.length;
            calculatedValues = self.calculateValues(data.subproperties).replace(/\%/gi, '');
            this.values = self.retrieveValues(calculatedValues);
            names = self.retrieveNames(data.subproperties);

            data.caption = data.caption || "Layout";

            $.extend(data, { calculatedValues: calculatedValues }, this.values, names);
            elEditor = $.tmpl(self.getTemplate("frame"), data);

            elEditor.find(".renderlayout-group").hide().addClass("renderlayout-close");

            for (i = 0; i < subpropertiesLength; i++) {
                subproperty = data.subproperties[i].property;
                self.renderSubproperty(elEditor.find(".renderlayout-group"), subproperty);
            }
            elEditor.appendTo(container);

            // saves input value
            self.inputValue = [];
            self.inputValue.push(self.properties[0].options.value);
            self.inputValue.push(self.properties[1].options.value);


            self.createDialogEditor(data);

        },
        createDialogEditor: function (data) {
            var self = this, elPopUpContent, elPopUpActions, elSplitterButton, elCloseButton, elCancelButton, elOkButton, elApplyButton;
            /** 
            CREACION DEL DIALOG DE EDICION DE PROPIEDADES 
            **/
            elPopUpContent = $.tmpl(self.getTemplate("content-popup"), data);
            elPopUpActions = $.tmpl(self.getTemplate("actions-popup"), data);

            $(".renderlayout-box1", elPopUpContent).text(self.properties[0].options.value + "%").attr("data-value", self.properties[0].options.value + "%"); ;
            $(".renderlayout-box2", elPopUpContent).text(self.properties[1].options.value + "%").attr("data-value", self.properties[1].options.value + "%"); ;

            elOkButton = $('.biz-action-btn', elPopUpActions).eq(1);
            elOkButton.click(function (event) { self.responseOkButton.apply(self, [elOkButton, event]); });

            elCancelButton = $('.biz-action-btn', elPopUpActions).eq(2);
            elCancelButton.click(function (event) { self.responseCancelButton.apply(self, [elCancelButton, event]); });

            elApplyButton = $('.biz-action-btn', elPopUpActions).eq(0);
            elApplyButton.click(function (event) { self.responseApplyButton.apply(self, [elApplyButton, event]); });



            elCloseButton = $('.biz-icon.ui-close-btn', elPopUpContent).eq(0);
            elCloseButton.click(function (event) { self.responseCloseButton.apply(self, [elCloseButton, event]); });

            /***/

            // calculate values in box
            elSplitterButton = $(".renderlayout-split", elPopUpContent);

            elSplitterButton.change(function () {
                var porcentaje1 = $(this).val();
                var porcentaje2 = 100 - porcentaje1;
                $(".renderlayout-box1", elPopUpContent).text(porcentaje1 + "%").attr("data-value", porcentaje1);
                $(".renderlayout-box2", elPopUpContent).text(porcentaje2 + "%").attr("data-value", porcentaje2);
            });

            self.modalReference = self.createDialog(elPopUpContent, elPopUpActions);
        },
        renderSubproperty: function (container, data) {
            var elProperty, property;
            elProperty = $.tmpl(this.getTemplate("frame-property"), data);

            data.value = parseFloat(data.value);

            $.extend(data, { "editor-parameters": { suffix: "%", range: { min: 0, max: 100}} });

            property = new bizagi.editor.component.editor.int(elProperty, data, this.controller);

            this.properties.push(property);

            elProperty.appendTo(container);
            property.render();
        },
        calculateValues: function (subproperties) {
            var subpropertiesLength, calculatedValues = "", i, defaultValues = [30, 70];
            subpropertiesLength = subproperties.length;
            calculatedValues += "";
            for (i = 0; i < subpropertiesLength; i++) {
                if (subproperties[i].property.hasOwnProperty("value") & subproperties[i].property.value != null) {
                    calculatedValues += subproperties[i].property.value;
                } else {
                    if (subproperties[i].property.hasOwnProperty("default") & subproperties[i].property["default"] != null) {
                        calculatedValues += subproperties[i].property["default"];
                    } else {
                        calculatedValues += defaultValues[i];
                        subproperties[i].property.value = defaultValues[i];
                    }
                }
                if (i == subpropertiesLength - 1) {
                    calculatedValues += "";
                } else {
                    calculatedValues += ", ";
                }
            }
            calculatedValues += "";
            return calculatedValues;
        },
        retrieveValues: function (calculatedValues) {
            var values = calculatedValues.replace(/\[|\]|,/g, "").trim().split(" ");
            return { per1: values[0], per2: values[1] };
        },
        retrieveNames: function (subproperties) {
            var i, names = [], subpropertiesLength = subproperties.length;
            for (i = 0; i < subpropertiesLength; i++) {
                names[i] = subproperties[i].property.caption;
            }
            return { caption1: names[0], caption2: names[1] };
        },
        remove: function () {
            this.element.hide();
            this.element.empty();
        },
        loadTemplates: function () {
            var deferred = $.Deferred();
            $.when(
                this.loadTemplate("frame", bizagi.getTemplate("bizagi.editor.component.editor.renderlayout").concat("#renderlayout-frame")),
                this.loadTemplate("frame-property", bizagi.getTemplate("bizagi.editor.component.editor.renderlayout").concat("#renderlayout-frame-property")),
                this.loadTemplate("content-popup", bizagi.getTemplate("bizagi.editor.component.editor.renderlayout").concat("#renderlayout-frame-popup")),
                this.loadTemplate("actions-popup", bizagi.getTemplate("bizagi.editor.component.editor.renderlayout").concat("#renderlayout-frame-popup-actions"))
                ).done(function () {
                    deferred.resolve();
                });
            return deferred.promise();
        },
        responseCloseDialog: function () {
            this.element.empty();
            this.renderEditor(this.element, this.options);
        },

        /*
        *
        */
        ".renderlayout-value click": function (el) {
            this.showDialogBox(el);
        },

        /*
        *
        */
        ".renderlayout-data > .biz-ico.ui-control-modal click": function (el) {
            this.showDialogBox(el);
        },

        /*
        *
        */
        showDialogBox: function (el) {
            var self = this;
            self.showDialog(el).done(function () {
                // align splitter
                var splitter = $(".renderlayout-split", self.modalReference);
                var value = self.inputValue[0];
                splitter.val(value);
            });
        },
        responseCloseButton: function () { // close box
            var self = this;

            self.hideDialog();

            $(".renderlayout-box1", self.modalReference).attr("data-value", self.values.per1).text(self.values.per1 + "%");
            $(".renderlayout-box2", self.modalReference).attr("data-value", self.values.per2).text(self.values.per2 + "%");
        },
        responseOkButton: function () {
            var self = this,
            firstSubproperty,
            secondSubproperty;

            firstSubproperty = self.options.subproperties[0];
            secondSubproperty = self.options.subproperties[1];

            firstSubproperty.property.value = $(".renderlayout-box1", self.modalReference).attr("data-value");
            secondSubproperty.property.value = $(".renderlayout-box2", self.modalReference).attr("data-value");

            var properties = [];
            properties.push({ property: firstSubproperty.property.name, value: firstSubproperty.property.value.replace(/\%/gi, '') });
            properties.push({ property: secondSubproperty.property.name, value: secondSubproperty.property.value.replace(/\%/gi, '') });

            self.controller.publish("propertyEditorChanged", {
                typeEvent: bizagi.editor.component.properties.events.PROPERTIES_CHANGE_MULTIPLE_PROPERTIES,
                properties: properties,
                id: this.element.closest(".bizagi_editor_component_properties").data("id")
            });

            self.hideDialog(function () {
                self.responseCloseDialog();
            });
        },
        responseApplyButton: function () {
            var self = this,
            firstSubproperty,
            secondSubproperty;

            firstSubproperty = self.options.subproperties[0];
            secondSubproperty = self.options.subproperties[1];

            firstSubproperty.property.value = $(".renderlayout-box1", self.modalReference).attr("data-value");
            secondSubproperty.property.value = $(".renderlayout-box2", self.modalReference).attr("data-value");

            var properties = [];
            properties.push({ property: firstSubproperty.property.name, value: firstSubproperty.property.value.replace(/\%/gi, '') });
            properties.push({ property: secondSubproperty.property.name, value: secondSubproperty.property.value.replace(/\%/gi, '') });

            self.controller.publish("propertyEditorChanged", {
                typeEvent: bizagi.editor.component.properties.events.PROPERTIES_CHANGE_MULTIPLE_PROPERTIES,
                properties: properties,
                id: this.element.closest(".bizagi_editor_component_properties").data("id")
            });
        },
        responseCancelButton: function () {
            var self = this;

            self.hideDialog();
            $(".renderlayout-box1", self.modalReference).attr("data-value", this.values.per1).text(this.values.per1 + "%");
            $(".renderlayout-box2", self.modalReference).attr("data-value", this.values.per2).text(this.values.per2 + "%");
        }
    }
    );