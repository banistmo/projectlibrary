
/*
 * Author : Alexander Mejia Garzon
 * Date   : 30mar12 
 * Comments:
 *     Define the node of model in the xpath navigator component
 *
 */

bizagi.editor.observableClass.extend("bizagi.editor.component.xpathnavigator.node", {}, {

    /*
    *   Creates a model for a single node
    */
    init: function (node) {
        var self = this;

        // call base
        self._super();

        self.nodes = null;
        self.data = node;

        if (node) self.processData(node);
    },

    /*
    *   Process the node data
    */
    processData: function (node) {
        var self = this;

        self.id = Math.guid();
        self.canHaveChildren = node.canHasChildren === undefined ? false : node.canHasChildren; // manage default values
        self.displayName = node.displayName;
        self.guid = node.guid;
        self.isDraggable = node.isDragabble === undefined ? true : node.isDragabble; // manage default values
        self.style = node.style;
        self.renderType = node.renderType;
        self.xpath = node.xpath;
        self.isScopeAttribute = node.isScopeAttribute === undefined ? false : node.isScopeAttribute; // manage default values
        self.guidRelatedEntity = node.guidRelatedEntity;
        self.hasRelatedEntity = self.guidRelatedEntity && self.guidRelatedEntity != "00000000-0000-0000-0000-000000000000" ? true : false;
        self.contextScope = node.contextScope;
        self.formVersion = node.formVersion;
        self.maxLength = node.maxLength;
        self.contextEntityType = node.contextEntityType;


        // TODO: Data definition must be changed in order to avoid the next workaround
        if (self.isScopeAttribute === true || self.isScopeAttribute == "true") {
            self.nodeType = node.style.replace('ui-bizagi-type-scope-', '').replace(/(attribute)(-\w+)/g, "$1");
            self.nodeSubtype = node.style.replace('ui-bizagi-type-scope-', '').replace(/(attribute)(-)(\w+)/g, "$3");
        } else {
            self.nodeType = node.style.replace('ui-bizagi-type-', '').replace(/(attribute)(-\w+)/g, "$1");
            self.nodeSubtype = node.style.replace('ui-bizagi-type-', '').replace(/(attribute)(-)(\w+)/g, "$3");
        }

        /// This allow us to remove if from template
        if (self.isDraggable === "true")
            self.dragabbleClass = "ui-bizagi-draggable-item ui-bizagi-itemfordrag";
    },

    /*
    *   Gets the original data
    */
    getOriginalData: function () {
        var self = this;
        var data = self.data;

        // Skip children
        data.nodes = [];
        return data;
    },

    /*
    *   Set the children nodes
    */
    setChildren: function (children) {
        var self = this;
        self.nodes = children;
    },

    /*
    *   Retrieve the children nodes
    */
    getChildren: function () {
        var self = this;
        return self.nodes;
    },

    /*
    *   Set the parent node
    */
    setParent: function (parent) {
        this.parent = parent;
    },

    /*
    *   Retrieve the paent node
    */
    getParent: function () {
        return this.parent;
    },

    /*
    *   Retrieve the displayName node
    */
    getDisplayName: function () {
        return this.displayName;
    },

    /*
    *   Gets xpath context menu model for the node
    */
    getContextMenuModel: function () {
		var self = this;
        var key = self.nodeTypeContextMenuMappings[this.nodeType];
        var data = self.contextMenuModels[key];
        return new bizagi.editor.component.contextmenu.model(data);
    },

    /*
    * Changes caption for item viewAll in contextMenu 
    */
    setCaptionViewContextMenu: function () {
        var self = this;
        var model = self.getContextMenuModel();
        var viewItem = model.getItemByAction("viewall");

        if (viewItem) {

            viewItem.caption = (viewItem.caption == bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-viewall")) ?
                               bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-onlycontextentity") :
                               bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-viewall");
        }
    },

    /*
    * Return true if the dragg option is enabled
    */
    enableDrag: function (context) {
        var self = this;

        if (context === "grid" || context === "queryform") {
            return bizagi.util.parseBoolean(self.isDraggable);
        }

        return self.parentIsGrid() ? false : bizagi.util.parseBoolean(self.isDraggable);
    },

    /*
    * Returns true is the ancestor is a grid
    */
    parentIsGrid: function () {
        var self = this;
        var result = false;

        if (self.parent) {
            result = (self.parent.renderType === "grid") || self.parent.parentIsGrid();
        }

        return result;
    },

    /*
    * Return true if the parent is the process entity
    */
    parentIsProcessEntity: function (processEntityId) {
        var self = this;

        if (!processEntityId) { return false; }
        return (processEntityId === self.guidRelatedEntity);

    },

    /*
    * Gets maxLength attribute
    */
    getMaxLength: function () {
        var self = this;
        return self.maxLength;
    },

    /*
    * Gets render type
    */
    getRenderType: function () {
        return this.nodeSubtype;
    },

    /*
    * Sets of value of property scopeAttribute
    */
    setScopeAttribute: function (value) {
        var self = this;

        self.isScopeAttribute = value;
    },

    /*
    *  Returns true if node is an attribute of a parameter entity
    */
    parentIsParametricEntity: function () {
        var self = this;
        var result = false;

        if (self.parent) {
            result = (self.parent.style == "ui-bizagi-type-entity-parametric") || self.parent.parentIsParametricEntity();
        }

        return result;
    }

});

/*
* Define context menu models
*/

bizagi.editor.component.xpathnavigator.node.extend("bizagi.editor.component.xpathnavigator.node", {},
	{
		init: function(args) {
		var self = this;

		/*
		 *   Xpath context menu static models by node
		 */
		self.contextMenuModels = {
			applicationEntity: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-editentity"), style: "editentity", action: "editentity" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-viewall"), style: "viewall", action: "viewall" },
					{ "separator": true },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-refresh"), style: "refresh", action: "refresh" }
				]
			},

			masterEntity: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addelement"), style: "addelement", action: "addelement" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addchildelements"), style: "addchildelements", action: "addchildelements" },
					{ "separator": true },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-editentity"), style: "editentity", action: "editentity" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-refresh"), style: "refresh", action: "refresh" }
				]
			},

			entity: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addelement"), style: "addelement", action: "addelement" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addchildelements"), style: "addchildelements", action: "addchildelements" },
					{ "separator": true },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-editentity"), style: "editentity", action: "editentity" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-editvalues"), style: "editvalues", action: "editvalues" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-refresh"), style: "refresh", action: "refresh" }
				]
			},

			entitySystem: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addelement"), style: "addelement", action: "addelement" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addchildelements"), style: "addchildelements", action: "addchildelements" },
					{ "separator": true },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-refresh"), style: "refresh", action: "refresh" }
				]
			},

			attribute: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addelement"), style: "addelement", action: "addelement" }
				]
			},

			form: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-newform"), style: "newform", action: "newForm" }
				]
			},

			collection: {
				items: [
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-addelement"), style: "addelement", action: "addelement" },
					{ "separator": true },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-editentity"), style: "editentity", action: "editentity" },
					{ caption: bizagi.localization.getResource("formmodeler-component-xpathnavigator-context-menu-refresh"), style: "refresh", action: "refresh" }
				]
			}
		};

		/*
		 *   Define node type mappings for context menus
		 */
		self.nodeTypeContextMenuMappings = {
			"entity-application": "applicationEntity",
			"entity-master": "masterEntity",
			"entity-parametric": "entity",
			"entity-system": "entitySystem",
			"attribute": "attribute",
			"form": "form"
		};

		self._super(args);
	}
    
});

