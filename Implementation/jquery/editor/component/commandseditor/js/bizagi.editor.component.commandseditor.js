/*
@title: Commands Editor Component
@authors: Diego Parra
@date: 04-jul-12
*/
$.Controller(
    "bizagi.editor.component.commandseditor", {
        /*
        *   Initializes the class
        */
        init: function (canvas, model, controller) {
            this.canvas = canvas;
            this.model = model;
            this.controller = controller;
            this.tmpl = {};
        },


        /*
        *   Refresh the control
        */
        refresh: function () {
            var self = this;
            var element = self.element;

            // Retrieve current tab
            this.currentTab = $("#bz-fm-commandseditor-tabs", element).tabs("option", "selected");
            var parent = this.currentTab == 0 ? $(".bz-fm-commandseditor-actions").parent() : $(".bz-fm-commandseditor-validations").parent();
            this.scrollPosition = parent.scrollTop();

            // Render again
            this.render();
        },

        /*
        *   Renders the preview editor
        */
        render: function () {
            var self = this;
            var element = self.element;

            // Clear everything
            element.empty();

            // Wait for templates
            $.when(self.loadTemplates()).done(function () {

                // Render
                var mainTemplate = self.getTemplate("container");
                $.tmpl(mainTemplate, self.model.getReadOnlyModel()).appendTo(element);

                // Apply tabs plugin
                $("#bz-fm-commandseditor-tabs", element).tabs({
                    selected: self.currentTab
                });

                // Scroll to previous position
                if (self.scrollPosition) {
                    var parent = self.currentTab == 0 ? $(".bz-fm-commandseditor-actions").parent() : $(".bz-fm-commandseditor-validations").parent();
                    parent.scrollTop(self.scrollPosition);
                }
            });
        },

        // Selection handler
        ".bz-fm-commandseditor-action, .bz-fm-commandseditor-validation click": function (element) {
            var self = this;
            var container = self.element;
            $(".bz-fm-commandseditor-action, .bz-fm-commandseditor-validation", container).removeClass("bz-state-selected").removeClass("ui-state-active");
            element.addClass("bz-state-selected").addClass('ui-state-active');
        },

        // Action edition handler
        ".bz-fm-commandseditor-action .bz-fm-commandseditor-icons-edit click": function (element) {
            var self = this;
            var parent = element.closest(".bz-fm-commandseditor-action");
            var guid = parent.data("guid");
            self.controller.editAction(self.model, guid);
        },

        // Validation edition handler
        ".bz-fm-commandseditor-validation .bz-fm-commandseditor-icons-edit click": function (element) {
            var self = this;
            var parent = element.closest(".bz-fm-commandseditor-validation");
            var guid = parent.data("guid");
            self.controller.editValidation(self.model, guid);
        },

        // Action addition handler
        "#bz-fm-commandseditor-actions-editor .ui-widget-actions button click": function () {
            var self = this;
            self.controller.createAction(self.model);
        },

        // Validation addition handler
        "#bz-fm-commandseditor-validations-editor .ui-widget-actions button click": function () {
            var self = this;
            self.controller.createValidation(self.model);
        },

        // Action delete handler
        ".bz-fm-commandseditor-action .bz-fm-commandseditor-icons-delete click": function (element) {
            var self = this;
            $.when(bizagi.showConfirmationBox(bizagi.localization.getResource("formmodeler-component-delete-action-message"), "Bizagi", "warning"))
            .done(function () {
                var parent = element.closest(".bz-fm-commandseditor-action");
                var guid = parent.data("guid");

                // Delete the action in the model and refresh
                self.model.deleteAction(guid);
                self.refresh();
            });


        },

        // Validation delete handler
        ".bz-fm-commandseditor-validation .bz-fm-commandseditor-icons-delete click": function (element) {
            var self = this;
            $.when(bizagi.showConfirmationBox(bizagi.localization.getResource("formmodeler-component-delete-validation-message"), "Bizagi", "warning"))
            .done(function () {
                var parent = element.closest(".bz-fm-commandseditor-validation");
                var guid = parent.data("guid");

                // Delete the validation in the model and refresh
                self.model.deleteValidation(guid);
                self.refresh();
            });

        },

        /*
        *   Load all the templates needed
        */
        loadTemplates: function () {
            var self = this;
            var defer = new $.Deferred();

            $.when(
                self.loadTemplate("container", bizagi.getTemplate("bizagi.editor.component.commandseditor").concat("#commands-editor-container"))
            ).done(function () {
                defer.resolve();
            });

            return defer.promise();
        },

        /*
        *   Load a single template
        */
        loadTemplate: function (name, path) {
            var self = this;
            var defer = new $.Deferred();
            if (self.tmpl[name]) {
                defer.resolve(self.tmpl[name]);
            } else {
                return bizagi.templateService.getTemplate(
                    path
                ).done(function (tmpl) {
                    self.tmpl[name] = tmpl;
                });
            }
            return defer.promise();
        },

        /*
        *   Gets a template
        */
        getTemplate: function (name) {
            if (this.tmpl[name]) {
                return this.tmpl[name];
            } else {
                return null;
            }
        }
    }
);