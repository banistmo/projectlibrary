
/*
*   Name: BizAgi FormModeler Editor Load Widgets Command
*   Author: Diego Parra
*   Comments:
*   -   This script will define basic stuff for loadwidgetscommand
*/

bizagi.editor.notUndoableCommand.extend("bizagi.editor.loadWidgetsCommand", {}, {

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;
        var args = self.arguments;

        // Adds definition widsets to model
        self.model.addWidgets(args.controls.widgets);

        // Gets controls model
        var controlsModel = self.controller.componentModels["controlsNavigator"],
            controlsHash = self.controller.componentModels["controls"]; 
        
        // Loads widgets
        $.each(args.controls.widgets, function (index, val) {
            if (val.type && val.type == 'userfield') {
                self.controller.loadIconUserField(val);
                self.controller.loadUserfield(val);
            }
        });

        controlsModel.processData({ controls: args.controls.widgets });

        bizagi.log("Updating Controls", args.controls.widgets);
        controlsHash.processData({ controls: args.controls.widgets });
        
        return true;
    }
})
