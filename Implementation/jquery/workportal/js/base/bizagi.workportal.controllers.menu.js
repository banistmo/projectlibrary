/*
 *   Name: BizAgi Workportal Menu Controller
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will define a base class to handle manu layouts for any device
 */

bizagi.workportal.controllers.controller.extend("bizagi.workportal.controllers.menu", {}, {
    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("menu");
        var defer = new $.Deferred();

        // Get Current User
        $.when(self.dataService.getCurrentUser()).done(function (data) {
            $.when(self.dataService.getWPLogo()).done(function (logo) {
                // Define Global information to actual user
                bizagi.currentUser = data;
                // Render content                
                var content = self.content = $.tmpl(template, $.extend(data, {
                    environment: bizagi.loader.environment || "",
                    build: bizagi.loader.build,
                    base64image: logo.image || ""
                }));

                defer.resolve(content);
            });
        });

        return defer.promise();
    },
    /*
    *   Helper method to change the current widget in the workportal
    */
    changeWidget: function (params) {
        var self = this;
        self.publish("changeWidget", params);
    },
    /*
    *   Helper method to show a widget inside a dialog in the wokportal
    */
    showDialogWidget: function (params) {
        var self = this;
        self.publish("showDialogWidget", params);
    },

    performResizeLayout: function () {
        var self = this;
        if (typeof jQuery.browser != "undefined" && $.browser.msie && parseInt($.browser.version, 10) <= 8) {
            var idInterval = window.setInterval(function () {
                if (self.content != null) {
                    var labels = $("#ui-bizagi-wp-app-menu-bt-container .text", self.content);
                    var userprofile = $("#userprofile", self.content); 

                    var menuUsername = $("#ui-bizagi-wp-menu-username", userprofile);
                    var labelsUser = $(".text", menuUsername);

                    var logout = $("#logout", userprofile);
                    var about = $("#about", userprofile);

                    if ($(window).width() < 1400) {

                        labels.css({
                            "line-height": 0,
                            "display": "block"
                        });

                        if( $(window).width() < 1195) {

                            //Adjust the size to make room for the about button
                            logout.css({
                                "width": "20px"
                            });

                            //Minimize the about button
                            about.css({
                                "width": "24px"
                            });

                            $(".ui-button-text", about).hide();

                            //Readjust the logout icon
                            $(".ui-icon", logout).css({
                                "top" : "12px"
                            });

                            //hide the label
                            $(".ui-button-text",logout).hide();

                            //Hide the label user
                            labelsUser.css({
                                "display": "none"
                            });

                            //Set an specific height to the user name menu
                            menuUsername.css({
                                "height": "19px"
                            });

                        }
                        else
                        {

                            about.removeAttr("style");
                            $(".ui-icon", about).removeAttr("style");
                            $(".ui-button-text", about).show();
                            
                            logout.removeAttr("style");
                            $(".ui-icon",logout).removeAttr("style");
                            
                            //show the label
                            $(".ui-button-text",logout).show();
                        }

                    } else {
                        labels.css({
                            "line-height": "59px",
                            "display": "inline-block"
                        });

                        //Reset the user name menu
                        menuUsername.removeAttr("style");

                        //re-adjust the styles
                        menuUsername.css({
                            "padding-left": "25px",
                            "width": "auto"
                        });

                        labelsUser.removeAttr("style");
                        
                        //Reset the about styles
                        about.removeAttr("style");
                        $(".ui-icon", about).removeAttr("style");
                        $(".ui-button-text", about).show();

                        logout.removeAttr("style");
                        $(".ui-icon",logout).removeAttr("style");
                        
                        //show the label
                        $(".ui-button-text",logout).show();
                    }

                    window.clearInterval(idInterval);
                }
            }, 300);
        }
    }

});
