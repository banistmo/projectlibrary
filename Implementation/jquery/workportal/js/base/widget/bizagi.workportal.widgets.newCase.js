/*
 *   Name: BizAgi Workportal New Case Widget Controller
 *   Author: Edward Morales
 *   Comments:
 *   -   This script will define a base class to to define the new case widget
 */

bizagi.workportal.widgets.widget.extend("bizagi.workportal.widgets.newCase", {}, {
    /*
    *   Returns the widget name
    */
    getWidgetName: function () {
        return bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_NEWCASE;
    },

    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("newCase");
        var content = self.content = $.tmpl(template);

        return content;
    },

    /*
    *   Creates a new case based on the selected process
    */
    createNewCase: function (idWfClass, idOrganization,isOfflineForm) {
        var self = this;
        var def = new $.Deferred();
        var dialogTemplate = self.workportalFacade.getTemplate("error-message");
        var content = self.getContent();

        // Creates a new case
        $.when(self.dataService.createNewCase({
            idWfClass: idWfClass,
            idOrganization: idOrganization,
            isOfflineForm: isOfflineForm
        })).done(function (data) {
            def.resolve(data);
            // Then we call the routing action
            self.publish("executeAction", {
                action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
                idCase: data.idCase,
                radNumber: data.radNumber,
                formsRenderVersion: (typeof data.isOfflineForm != "undefined") ? data.formsRenderVersion : 0,
                isOfflineForm: (typeof data.isOfflineForm != "undefined") ?data.isOfflineForm : false
            });
        }).fail(function (msg) {
            var buttons = {};
            var errorContent = $.tmpl(dialogTemplate, {
                message: msg.responseText
            });


            if (bizagi.workportal.desktop)
            {
                // Also close the popup
                bizagi.workportal.desktop.popup.closePopupInstance();

                // Hide waiting modal box
                $("#modalNewCaseOverlay", content).addClass("modalNewCaseOverlay").removeClass("modalNewCaseOverlayShow");
                $("#modalNewCaseMessage", content).removeClass("show");

                buttons[bizagi.localization.getResource("workportal-case-dialog-box-close")] = function() {
                    $(this).dialog('close');
                    $(this).dialog('destroy');
                };

                errorContent.dialog({
                        modal: true,
                        title: "Error",
                        closeOnEscape: true,
                        width: 550,
                        height: 200,
                        buttons: buttons
                    });

            }


            def.reject(msg);
        });
        return def.promise();
    }
});