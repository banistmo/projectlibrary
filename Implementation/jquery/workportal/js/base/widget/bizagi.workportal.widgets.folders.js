/*
*   Name: BizAgi Workportal Folder Widget Controller
*   Author: Edward Morales
*   Comments:
*   -   This script will define a base class to to define the folders widget
*/

bizagi.workportal.widgets.widget.extend("bizagi.workportal.widgets.folders", {}, {
    /*
    *   Returns the widget name
    */
    getWidgetName: function(){  
        return bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_FOLDERS;
    },
    
    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("folders");
        var content = self.content = $.tmpl(template);
        
        return content;
    }
});
