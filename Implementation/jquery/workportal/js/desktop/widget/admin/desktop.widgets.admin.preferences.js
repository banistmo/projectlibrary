/**
 * Name: BizAgi Desktop Widget Administration preferences Implementation
 * 
 * @author Andres Fernando Muñoz
 */


bizagi.workportal.widgets.admin.preferences.extend("bizagi.workportal.widgets.admin.preferences", {}, {
    init: function (workportalFacade, dataService, params) {

        this.guidForm;
        this.isNewForm;

        // Call base
        this._super(workportalFacade, dataService, params);
    },

    loadtemplates: function () {
        var self = this;

        self.container = self.workportalFacade.getTemplate("admin.preferences.container");
        self.buttons = self.workportalFacade.getTemplate("admin.preferences.buttons");
        self.iframe = self.workportalFacade.getTemplate("admin.preferences.iframe");
        self.errorMessage = self.workportalFacade.getTemplate("admin.preferences.error");
    },

    postRender: function () {
        var self = this;

        $.when(self.dataService.getPreferenceFormParams()).done(function (data) {

            self.guidForm = (data.id !== "-1") ? data.id : "";

            if (data.errorMessage === true) {
                self.renderErrorMessage();
            } else if (data.isNewForm === true) {
                self.renderPreferences();
            } else {
                self.renderPreferencesOldForm();
            }
        });

    },

    renderErrorMessage: function () {
        var self = this;

        var resource = self.getResource("workportal-widget-admin-entities-message-migrate");
        resource = resource.replace("{build}", bizagi.loader.productBuildToAbout);

        var errorContent = $.tmpl(self.errorMessage, { errorMessage: resource });

        self.content.html(errorContent);

    },

    setupData: function () {
        var self = this;
        var content = self.getContent();
    },

    renderPreferencesOldForm: function () {

        var self = this;

        var $content = self.getContent();
        var url = self.dataService.getUrl({
            "endPoint": "PreferenceFormOld"
        }) + "?idForm=" + self.guidForm + "&referer=userPreferences";

        var $iframeContent = $.tmpl(self.iframe, { "url": url });
        var $iframe = $("iframe", $iframeContent);

        $iframeContent.addClass('loading-iframe');

        $iframe.hide();

        $iframe.load(function () {
            $(this).fadeIn();
        });

        $content.html($iframeContent)

    },

    renderPreferences: function () {
        var self = this;
        var content = self.getContent();
        self.currentUser = bizagi.currentUser.idUser;

        var preferencesParams = {
            h_action: "LOADUSERPREFERENCESFORM",
            h_contexttype: "entity",
            h_xpathContext: "@Context.Users[idUser == " + self.currentUser + "]",
            h_guidForm: self.guidForm
        };

        var renderContainer = $.tmpl(self.container);
        var rendering = new bizagi.rendering.facade();
        //request to preferences definition service
        self.dataService.getPreferencesForm(preferencesParams).done(function (data) {

            var renderingParams = { canvas: renderContainer, data: data, type: "" };
            rendering.execute(renderingParams).done(function (form) {
                self.preferenceForm = rendering.form;
                $("#preferences", content).html(renderContainer);
                self.handleEvents();
            });
        });
    },

    /*
    *  Buttons for preferences actions
    */
    handleEvents: function () {
        var self = this;
        var content = self.getContent();
        $("#preferences-buttons", content).html($.tmpl(self.buttons));

        $("#update-preferences", content).click(function (e) {
            self.updateData();
        });
    },

    updateData: function () {
        var self = this;

        if (self.preferenceForm.validateForm()) {
            var serviceRender = new bizagi.render.services.service();
            var data = self.preferenceForm.collectRenderValuesForSubmit();

            $.when(serviceRender.multiactionService.submitData({
                action: "SAVEUSER",
                contexttype: "entity",
                data: data,
                surrogatekey: self.currentUser,
                idPageCache: self.preferenceForm.idPageCache
            })).done(function (message) {
                self.renderPreferences();
            });
        }
    }

});
