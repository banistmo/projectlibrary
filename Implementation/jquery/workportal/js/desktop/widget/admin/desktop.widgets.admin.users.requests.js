﻿/**
* Name: BizAgi Desktop Widget Pending User Request
* 
* @author Liliana Fernandez, David Andrés Niño Villalobos
*/


bizagi.workportal.widgets.admin.users.requests.extend("bizagi.workportal.widgets.admin.users.requests", {}, {
    init: function (workportalFacade, dataService, params) {
        // Call base
        this._super(workportalFacade, dataService, params);
    },
    postRender: function () {
        var self = this,
            content = self.getContent();


        self.maxElemShow = 10;
        self.maxPageToShow = 5;
        self.currentPage = 1;

        //Template vars
        self.userRequest = self.workportalFacade.getTemplate("users.requests");
        self.userRequestList = self.workportalFacade.getTemplate("users.requests.list");
        self.userRequestDetail = self.workportalFacade.getTemplate("users.requests.detail");
        self.userRequestpaginationTmpl = self.workportalFacade.getTemplate("users.requests.pagination");
        self.userRequestemailTmpl = self.workportalFacade.getTemplate("users.requests.email");


        //DOM Variables
        self.userRequestListWrapper = $("#users-request-list-wrapper", content);
        self.userRequestDetailWrapper = $("#users-request-detail-wrapper", content);


        self.containtersID = {
            LIST: "list",
            DETAIL: "detail"
        };

        //load form data
        self.setupInitialData();

    },

    /*
    * Load form fields and combo's data
    */
    setupInitialData: function (selectedPage) {
        var self = this;

        //Switch content to detai
        self.switchContent(self.containtersID.LIST);

        if (selectedPage)
            self.currentPage = selectedPage;

        var params = { pag: self.currentPage, pagSize: self.maxElemShow };

        $.when(self.dataService.userPendingRequests(params)
        ).done(function (result) {
            self.renderUserRequestList(result);

        });
    },

    /* call authentication Info
    */
    authenticationInfo: function (userId) {
        var self = this,
            content = self.getContent();

        var params_1 = {};
        params_1 = { idUser: userId };

        var params_2 = {};
        params_2 = { idUser: userId, pag: 1, pagSize: 1 };

        $.when(self.dataService.userAuthenticationInfo(params_1),
            self.dataService.userPendingRequests(params_2)
        ).done(function (resultAuthentication, resultUser) {
            self.renderUserRequestDetails(resultAuthentication, resultUser);
        });
    },

    /*
    * generate Render Users Request
    */
    renderUserRequestList: function (data) {
        var self = this,
            content = self.getContent();

        self.userRequestListWrapper.empty();

        var requestList = $.tmpl(self.userRequestList, { userData: data.rows });

        self.userRequestListWrapper.append(requestList);

        var usersRequestsSummaryWrapper = $("#users-requests-pager-wrapper", content);

        //Keep in track the total Records
        self.totalRecords = data.records;

        //keep in track the total pages
        self.totalPages = data.total;

        if (data.rows.length > 0) {

            var pageToshow = (self.maxPageToShow > self.totalPages) ? self.totalPages : self.maxPageToShow;
            //var summaryWrapper = $("#authentication-log-pager-wrapper");
            var paginationHtml;

            var pagerData = {};
            // show or hide "load more" button
            pagerData.pagination = (self.totalPages > 1) ? true : false;
            pagerData.page = data.page;
            pagerData.pages = {};
            $("#biz-wp-userrequest-norequest").css("display","none");
            $("#users-request-list-wrapper .biz-wp-window ").css("display","block");
            $("#users-request-list-wrapper .biz-wp-table ").css("display","table");
        
           
            for (var i = 1; i <= pageToshow; i++) {
                pagerData["pages"][i] = {
                    "pageNumber": i
                };
            }

            //load and append the paginator to the result table
            paginationHtml = $.tmpl(self.userRequestpaginationTmpl, pagerData).html();
            usersRequestsSummaryWrapper.append(paginationHtml);

            //add data and behaviour to pager
            $("ul").bizagiPagination({
                maxElemShow: self.maxElemShow,
                totalPages: self.totalPages,
                actualPage: data.page,
                listElement: $("#users-requests-pager-wrapper"),
                clickCallBack: function (options) {
                    self.setupInitialData(options.page);
                }
            });

        } else {
           $("#biz-wp-userrequest-norequest").show("500");
           $("#users-request-list-wrapper .biz-wp-window ").css("display","none");
           $("#users-request-list-wrapper .biz-wp-table ").css("display","none");
        }
        /**/

        //Add ui events
        $(".selectLink", content).click(function (e) {
            self.authenticationInfo(e.currentTarget.id);
        });
    },

    /*
    * Load Info Users requests detail
    */
    renderUserRequestDetails: function (resultAuthentication, resultUser) {

        var self = this,
            content = self.getContent();

        //Switch content to detai
        self.switchContent(self.containtersID.DETAIL);


        self.userRequestDetailWrapper.empty();


        var requestDetail = $.tmpl(self.userRequestDetail, { resultUser: resultUser[0].rows[0], resultAuthentication: resultAuthentication[0] });

        self.userRequestDetailWrapper.append(requestDetail);

        self.idUser = resultAuthentication[0].idUser;
        self.password = resultAuthentication[0].password;
        self.enabled = resultAuthentication[0].Enabled;
        self.isExpired = resultAuthentication[0].expired;
        self.isLocked = resultAuthentication[0].locked;

        var noEmptyField;

        $("#btn-passwordRandom", content).click(function (e) {

            $.when(self.dataService.generateRandomPassword()).done(function (result) {
                $("#txtPasswordRequest", content).val(result.randomPassword);

            }).fail(function (error) {

            });
        });


        $("#btn-updateRequest", content).click(function () {

            noEmptyField = self.validateKey();

            var password = $("#txtPasswordRequest").val();
            self.password = password;

            if (noEmptyField) {
                self.dataService.generateDataToSendByEmail({
                    idUser: self.idUser,
                }).done(function (response) {
                    self.showEmailtPopup(response);
                    self.updateUser();
                });
            } else {
                if (!noEmptyField)
                    bizagi.showMessageBox(bizagi.localization.getResource("workportal-widget-admin-users-request-emptyfields-message"), "Bizagi", "warning");
            }
        });

        $("#btn-cancelRequest", content).click(function (e) {
            self.setupInitialData();
        });
    },

    /*
    * Switch Templates
    */
    switchContent: function (contentToDisplay) {
        var self = this,
            content = self.getContent();

        if (self.containtersID.DETAIL == contentToDisplay) {
            self.userRequestListWrapper.hide();

            self.userRequestDetailWrapper.show();
        } else {
            self.userRequestListWrapper.show();

            self.userRequestDetailWrapper.hide();
        }
    },

    /*
    * Update User
    */
    updateUser: function () {
        var self = this,
                content = self.getContent();

        var params = {};
        params = { idUser: self.idUser, password: self.password, enable: $("#chkActive").is(':checked'), expired:$("#chkExpired").is(':checked'), locked:$("#chkBlocked").is(':checked') };

        $.when(self.dataService.updateUserAuthenticationInfo(params)
            ).done(function (result) {
            //    console.log(result);
            }).fail(function (error) {
                bizagi.log(error);
            });
        },

    /*
    * generate Random Password
    */
    setupFooterButtons: function () {

        var self = this,
            content = self.getContent();

        var widgetButtonSet = $("#users-request-data-buttonset-Password", content);

        $("#btn-passwordRandom", widgetButtonSet).click(function (e) {
            $.when(self.dataService.generateRandomPassword()).done(function (result) {
                $("#txtPasswordRequest", content).val(result.randomPassword);

            }).fail(function (error) {

            });
        });
    },
    /*generate Windows PopupContent email
    *
    */
    showEmailtPopup: function (messageResponse) {

        var self = this;
        var doc = window.document;

        var template = self.workportalFacade.getTemplate("users.requests.email");

        // Opens a dialog in order to upload Email
        var dialogBoxEmail = self.dialogBoxEmail = $("<div class= users-requests-email/>");
        dialogBoxEmail.empty();
        dialogBoxEmail.appendTo("body", doc);

        // Define buttons
        var buttonsEmail = {};

        buttonsEmail[self.getResource("workportal-widget-admin-users-requests-button-label-send")] = function (e) {

            // Select button Send
            var params = {};

            params['eMailTo'] = $("#txtTextTo", dialogBoxEmail).val();
            params['subject'] = $("#txtSubject", dialogBoxEmail).val();
            var messgeBody = $("#txtBody", dialogBoxEmail).val();
            params['body'] = messgeBody;
            params['userSecret'] = self.getUserSecret(self.password);

            $.when(self.dataService.sendUserEmail(params)).done(function (result) {
                self.closeUploadDialogEmail();
            });
            bizagi.showMessageBox(bizagi.localization.getResource("workportal-widget-admin-users-request-Send-message"), "Bizagi", "warning");
        };

        // Cancel button Send
        buttonsEmail[self.getResource("workportal-widget-admin-users-requests-button-label-notSend")] = function () {
            self.closeUploadDialogEmail();
        };

        //Creates a dialog
        dialogBoxEmail.dialog({
            width: 500,
            height: 400,
            title: self.getResource("workportal-widget-admin-users-request-subtitle-email"),
            modal: true,
            buttons: buttonsEmail,
            close: function () {
                self.closeUploadDialogEmail();
            },
            resizeStop: function (event, ui) {
                if (self.form) {
                    self.form.resize(ui.size);
                }
            }
        });

        // Render template
        messageResponse.body = messageResponse.body.replace(/\\n/g, "\n").replace(/\\/g, "");

        $.tmpl(template, { messageResponse: messageResponse }).appendTo(dialogBoxEmail);

        $('.ui-bizagi-loading-message').remove();
    },

    closeUploadDialogEmail: function () {
        var self = this;
        self.dialogBoxEmail.dialog('destroy');
        self.dialogBoxEmail.remove();
        self.setupInitialData();
    },
    /*
    * Validate required fields
    */
    validateKey: function () {
        var self = this,
                content = self.getContent(),
                fieldAreEmpty = false;


        if ($("#txtPasswordRequest", content).val() != "") {
            fieldAreEmpty = true;
        }
        return fieldAreEmpty;
    },
    getUserSecret: function (inputValue) {
        var self = this;
        var C = CryptoJS;
        var userFormsSpanner = C.enc.Utf8.parse('pc30n84e15lvdD68');
        var userFormsCode = CryptoJS.enc.Latin1.parse('1v00628X62bJE2mi');
        var aes = C.algo.AES.createEncryptor(userFormsSpanner, {
            mode: C.mode.CBC,
            padding: C.pad.Pkcs7,
            iv: userFormsCode
        });
        var resultMain = aes.finalize(inputValue);
        var resultMainFinal = C.enc.Base64.stringify(resultMain);
        return resultMainFinal;
    }
});