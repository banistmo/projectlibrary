/*
*   Name: BizAgi Desktop Widget Dialog Implementation
*   Author: Diego Parra
*   Comments:
*   -   This script will shows a widget inside a modal dialog
*/

// Extends itself
$.Class.extend("bizagi.workportal.desktop.widgets.dialog", {
    DIALOG_WIDTH: 800,
    DIALOG_HEIGHT: 550

}, {

    /* 
    *   Constructor
    */
    init: function (dataService, workportalFacade) {
        this.dataService = dataService;
        this.workportalFacade = workportalFacade;
        this.triggerCloseCallback = true;
    },

    /*
    *   Render the Widget
    *   Returns a deferred
    */
    renderWidget: function (params) {
        var self = this;
        var doc = window.document;
        var defer = new $.Deferred();
        var widget;

        // Creates widget
        $.when(
			self.workportalFacade.getWidget(params.widgetName, params)

			).pipe(function (result) {
			    widget = result;
			    return widget.render(params);

			}).done(function () {
			    var content = widget.getContent();

			    // Append content into a dialog
			    self.dialogBox = $("<div />").append(content).appendTo("body", doc);

			    // Create dialog box
			    self.showDialogBox(self.dialogBox, params)
			.done(function (data) {
			    defer.resolve();
			});
			});

        // Return promise
        return defer.promise();
    },

    /*
    *   Shows the dialog box in the browser
    *   Returns a promise that the dialog will be closed
    */
    showDialogBox: function (dialogBox, params) {
        var self = this;

        // Define buttons
        var buttons = [];
        var defer = new $.Deferred();
        var dialogParameters = params["modalParameters"] || {};

        // TODO: Customize buttons?
        var closeButtonVisible = params.closeVisible == undefined ? true : params.closeVisible;
        var closeEscape = params.closeOnEscape == undefined ? true : params.closeOnEscape;
        var cssClass = params.dialogClass == undefined ? "" : params.dialogClass;

        if (closeButtonVisible) {
            buttons = [{
                text: bizagi.localization.getResource("workportal-widget-dialog-box-close"),
                click: function () {
                    dialogBox.dialog('close');
                }
            }];
        } else {
            buttons = [];
        }


        if (params.buttons) {
            if (params.buttons.length > 0) {
                // its object
                buttons = $.merge(buttons, params.buttons);
            } else {
                //its array
                $.extend(buttons, buttons, params.buttons);
            }
        }

        // Declare global instance
        //bizagi.workportal.desktop.widgets.dialog.instance = this;

        // Creates a dialog
        dialogBox.dialog({
            show: dialogParameters["show"] || 'fade',
            width: dialogParameters["width"] || (params.modalParameters && self.getDialogSize(params.modalParameters.id).width) || this.Class.DIALOG_WIDTH,
            height: dialogParameters["height"] || (params.modalParameters && self.getDialogSize(params.modalParameters.id).height) || this.Class.DIALOG_HEIGHT,
            modal: true,
            dialogClass: cssClass,
            closeOnEscape: closeEscape,
            draggable: false,
            title: dialogParameters["title"] || "",
            maximize: (typeof params.maximize !== "undefined")? params.maximize : true,
            maximized: (params.maximized !== undefined ? params.maximized : false),
            maximizeOnly: (params.maximizeOnly !== undefined ? params.maximizeOnly : false),
            buttons: buttons,
            resizable: (params.modalParameters && self.getResizableState(params.modalParameters.id)) || false,
            close: function (ev, ui) {
                dialogBox.dialog('destroy');
                dialogBox.remove();
                defer.resolve();

                // Execute callback
                if (params.onClose && self.triggerCloseCallback) params.onClose();
            },
            open: function (event, ui) {
                if (params.modalParameters && self.getDialogSize(params.modalParameters.id).maximize) {
                    if (self.getDialogSize(params.modalParameters.id).maximizeOnly) {
                        dialogBox.dialog("option", {
                            'maximizeOnly': true
                        });
                    }

                    dialogBox.dialog('maximize');
                }
            }
        });




        // set global dialog object


        // Return promise
        return defer.promise();
    },

    /*
    *   Close dialog
    */
    close: function () {
        // If the dialog is closed externally, the callback is not used
        this.triggerCloseCallback = false;
        this.dialogBox.dialog("close");
    },

    /*
    *   Get if the window is resizable or not
    */
    getResizableState: function (module) {
        module = module || "";
        var resizable;
        switch (module) {
            case "EntityAdmin":
                resizable = true;
                break;
            default:
                resizable = false;
                break;
        }
        return resizable;
    },

    /*
    *   Get the dialog size of the popup window
    */
    getDialogSize: function (module) {
        module = module || "";
        var size = {
            height: this.Class.DIALOG_HEIGHT,
            width: this.Class.DIALOG_WIDTH,
            maximize: false,
            maximizeOnly: false
        };
        switch (module) {
            case "BAMProcess":
            case "BAMTask":
            case "AnalyticsProcess":
            case "AnalyticsTask":
            case "AnalyticsSensor":
            case "GRDimensionAdmin":
                size.maximizeOnly = true;
                size.width = this.Class.DIALOG_WIDTH + 220;
            case "Queries":
                if(bizagi.util.isIE9()){
                    size.width = this.Class.DIALOG_WIDTH + 7;
                    size.height = this.Class.DIALOG_HEIGHT + 7;
                }
                break;
            case "BusinessPolicies":
            case "ResourceBAM":
                size.maximize = true;
                break;
            case "UserAdmin":
                size.height = this.Class.DIALOG_HEIGHT + 100;
                break;
            case "AsynchronousWorkitemRetries":
                size.width = this.Class.DIALOG_WIDTH + 200;
                break;

        }
        return size;
    }


});
