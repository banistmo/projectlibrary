﻿/**
* Name: BizAgi Desktop Widget - Tree
* 
* @author Christian Collazos
*/


bizagi.workportal.widgets.tree.extend("bizagi.workportal.widgets.tree", {}, {
    init: function (workportalFacade, dataService, params) {
        // Call base
        this._super(workportalFacade, dataService, params);
    },

    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("tree.wrapper");
        var content;

        content = self.content = $.tmpl(template, {});

        // Override canvas if it has been defined
        if (self.params.canvas) {
            content = $(self.params.canvas).append(content);
        }

        return content;
    },

    postRender: function () {
        var self = this;

        //load templates 
        self.loadtemplates();
    },

    loadtemplates: function () {
        var self = this;

        //Template vars 
        self.decisionTableContent = self.workportalFacade.getTemplate("tree.content");
    }
});