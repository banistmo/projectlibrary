/*
 *   Name: BizAgi Workportal Desktop New Case Widget Controller
 *   Author: Edward Morales
 *   Comments:
 *   -   This script will provide sub menu widget
 */

// Auto extend
bizagi.workportal.widgets.subMenu.extend("bizagi.workportal.widgets.subMenu", {}, {
    /** 
    *   To be overriden in each device to apply layouts
    */
    postRender: function () {
        var self = this;
        // render sub menu content
        self.renderSubMenuContent();

        self.scrollVertical({
            "autohide": false
        });
    },
    scrollVertical: function () {
        var self = this;
        var content = self.getContent();

        $("#categories", content).bizagiScrollbar({
            "autohide": false
        });
    },
    /*
    *   Show content for sub menu item
    */

    renderSubMenuContent: function () {
        var self = this;
        var content = self.getContent();
        var listTemplate = self.workportalFacade.getTemplate("menu.submenu.list");
        var listContainer = $("#categories", content);
        var data = {};

        var callBack = {
            "ThemeBuilder": function (args) {
                args = args || {};
                if (args.url) {
                    window.open(args.url, "ThemeBuilder");
                }
            },
            "asyncECMUpload": function (args) {
                args = args || {};
                self.showWidgetPopup(args.url, args.title, args.id);
            }
        };

        // TODO: SNAKE-1296 remove these lines
        try {
            if (bizagi.override.enableMultipleCasesReassigment) {
                self.params.options.menuData.push({
                    categoryKey: "CaseAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-adminReassignCases"),
                    categoryUrl: "adminReassignCases"
                });

                callBack.CaseAdmin = function (args) {
                    args = args || {};
                    self.showWidgetPopup(args.url, args.title, args.id);
                };
            }
        } catch (e) {
            // Nothing todo, just skip this option in menu
        }

        //DRAGON-152 Adidas - Async ECM
        try {
            if (bizagi.override.enableAsyncECMUploadJobs) {
                self.params.options.menuData.push({
                    categoryKey: "asyncECMUpload",
                    categoryName: self.getResource("workportal-menu-submenu-asyncECMUpload"),
                    categoryUrl: "asyncECMUpload"
                });
            }
        } catch (e) {
            // Nothing todo, just skip this option in menu
        }

       

        try {

            //delete frankenstein implementations
            // revisar el contenido de self.params.options.menuData para tener en cuenta los sub menus   autorizados
            var authorized = self.getSubItemsAuthorized();
            
            self.deleteMenuOption("UserAdmin");
            self.deleteMenuOption("AuthenticationLogQuery");
            self.deleteMenuOption("EncryptionAdmin");
            self.deleteMenuOption("UserPendingRequests");
            self.deleteMenuOption("CaseAdmin");
            self.deleteMenuOption("AsynchronousWorkitemRetries");
            self.deleteMenuOption("UserDefaultAssignation");
            self.deleteMenuOption("Profiles");
            self.deleteMenuOption("AlarmAdmin");
            self.deleteMenuOption("Licenses");
            self.deleteMenuOption("GRDimensionAdmin");
            self.deleteMenuOption("DocumentTemplates");
            self.deleteMenuOption("ProcessAdmin");
            self.deleteMenuOption("Multilanguage");
            self.deleteMenuOption("EntityAdmin");
            //Falta implementacion de Business Policies
            //self.deleteMenuOption("BusinessPolicies");

            
                
            if ($.inArray("EntityAdmin", authorized) != -1) {
                self.params.options.menuData.unshift({
                    categoryKey: "EntityAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-EntityAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_ENTITIES
                });
            }
            

            //create the new menu option using the old categoryKey
            if ($.inArray("AuthenticationLogQuery", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "AuthenticationLogQuery",
                    categoryName: self.getResource("workportal-menu-submenu-AuthenticationLogQuery"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_AUTHENTICATION_LOG
                });
            }

            if ($.inArray("EncryptionAdmin", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "EncryptionAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-EncryptionAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_ENCRYPT_PASSWORDS
                });
            }

            if ($.inArray("UserPendingRequests", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "UserPendingRequests",
                    categoryName: self.getResource("workportal-menu-submenu-UserPendingRequests"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_USERS_REQUESTS
                });
            }

            if ($.inArray("CaseAdmin", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "CaseAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-CaseAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_CASE_SEARCH
                });
            }

            if ($.inArray("AsynchronousWorkitemRetries", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "AsynchronousWorkitemRetries",
                    categoryName: self.getResource("workportal-menu-submenu-AsynchronousWorkitemRetries"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_ASYNC_ACTIVITIES
                });
            }

            if ($.inArray("UserDefaultAssignation", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "UserDefaultAssignation",
                    categoryName: self.getResource("workportal-menu-submenu-UserDefaultAssignation"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_DEFAULTS_ASSIGNATION_USER

                });
            }

            if ($.inArray("Profiles", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "Profiles",
                    categoryName: self.getResource("workportal-menu-submenu-Profiles"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_USER_PROFILES

                });
            }

            if ($.inArray("AlarmAdmin", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "AlarmAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-AlarmAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_ALARMS
                });
            }

            if ($.inArray("Licenses", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "Licenses",
                    categoryName: self.getResource("workportal-menu-submenu-Licenses"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_USER_LICENSES
                });
            }

            if ($.inArray("GRDimensionAdmin", authorized) != -1) {
                self.params.options.menuData.push({
                    categoryKey: "GRDimensionAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-GRDimensionAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_DIMENSIONS
                });
            }

            if ($.inArray("DocumentTemplates", authorized) != -1) {//No está en la lista de autorizados en ambiente de desarrollo
                self.params.options.menuData.push({
                    categoryKey: "DocumentTemplates",
                    categoryName: self.getResource("workportal-menu-submenu-DocumentTemplates"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_DOCUMENT_TEMPLATES
                });
            }

            if ($.inArray("ProcessAdmin", authorized) != -1) {//No está en la lista de autorizados en ambiente de desarrollo
                self.params.options.menuData.push({
                    categoryKey: "ProcessAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-ProcessAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_PROCESSES
                });
            }

            if ($.inArray("UserAdmin", authorized) !== -1) {
                self.params.options.menuData.unshift({
                    categoryKey: "UserAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-UserAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_USERS_ADMINISTRATION
                });
            }

            //Falta implementacion de Business Policies
            /*if ($.inArray("BusinessPolicies", authorized) != -1) {
            self.params.options.menuData.push({
            categoryKey: "BusinessPolicies",
            categoryName: self.getResource("workportal-menu-submenu-BusinessPolicies"),
            categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_BUSINESS_POLICIES
            });
            }*/

            if ($.inArray("Multilanguage", authorized) != -1 ) {//No está en la lista de autorizados en ambiente de desarrollo
                self.params.options.menuData.push({
                    categoryKey: "LanguageAdmin",
                    categoryName: self.getResource("workportal-menu-submenu-LanguageAdmin"),
                    categoryUrl: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_LANGUAGE
                });
            }


            //add the callback for each new widget
            callBack.UserAdmin =
            callBack.AuthenticationLogQuery =
            callBack.EncryptionAdmin =
            callBack.UserPendingRequests =
            callBack.CaseAdmin =
            callBack.AsynchronousWorkitemRetries =
            callBack.UserDefaultAssignation =
            callBack.Profiles =
            callBack.AlarmAdmin =
            callBack.Licenses =
            callBack.GRDimensionAdmin =
            callBack.DocumentTemplates =
            callBack.ProcessAdmin =
            callBack.LanguageAdmin =
            callBack.EntityAdmin =
            //callBack.BusinessPolicies =
                function (args) {
                    args = args || {};

                    $.extend(args, {
                        showCloseButton: false
                    });

                    self.showCustomWidgetPopup(args);
                };



        } catch (e) {
            // Nothing todo, just skip this option in menu
        }


        

        data["listmenu"] = self.params.options.menuData;
        var render = $.tmpl(listTemplate, data);

        listContainer.append(render);

        // Bind sub menu
        if (!self.params.options.customClickHandler) {
            $("li", listContainer).click(function () {
                var url = $(this).find("#categoryUrl").val();
                var title = $(this).find("h3").text();
                var id = $(this).data('id') || '';

                // Find call back
                if (callBack[id]) {
                    callBack[id]({
                        url: url,
                        title: title,
                        id: id
                    });
                } else {
                    self.showContentPopup(url, title, id);
                }
                bizagi.workportal.desktop.popup.closePopupInstance();
            });

        } else {
            $("li", listContainer).click(function () {
                var id = $(this).data("id");
                bizagi.workportal.desktop.popup.closePopupInstance();
                self.params.options.customClickHandler(id);
            });
        }

    },
    /*
    *   Determines if a window must be show always in quirks mode for IE
    */
    needsQuicksMode: function (url) {

        // If an old url is not working ok, put the url in the following filters
        if (url.toLowerCase().indexOf("businesspoliciesselector.aspx") >= 0)
            return true;
        if (url.toLowerCase().indexOf("bamprocess.aspx") >= 0)
            return true;
        if (url.toLowerCase().indexOf("bamtask.aspx") >= 0)
            return true;
        if (url.toLowerCase().indexOf("analyticsprocess.aspx") >= 0)
            return true;
        if (url.toLowerCase().indexOf("analyticssensor.aspx") >= 0)
            return true;

        return false;
    },
    /*
    *   Get the page hacks in order to work properly in each case
    */
    getPageHacks: function (url, title) {
        var self = this;
        var callback;
        var hackParams = {
            title: title
        };

        //if (url.toLowerCase().indexOf("casesearch.aspx") > 0 || url.toLowerCase().indexOf("AsynchDisabledWorkitems.aspx") > 0) {
        // Hack for  Admin / Processes
        callback = function (params) {
            // This script will execute inside the iframe context
            this.openBACase = function (idCase, sHref) {
                var workitem = null;
                if (sHref.indexOf("idWorkitem=") > -1) {
                    workitem = sHref.substring(sHref.indexOf("idWorkitem=") + 11);
                    workitem = workitem.substring(0, workitem.indexOf("&"));
                }
                // Executes the act on
                params.controller.publish("executeAction", {
                    action: params.routingAction,
                    idCase: idCase,
                    idWorkItem: workitem
                });

                // close dialog
                params.controller.publish("closeCurrentDialog");
            };

            this.changeToWidget = function (widget) {
                params.controller.publish("changeWidget", {
                    widgetName: widget || params.bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_INBOX_GRID
                });

                // close dialog
                params.controller.publish("closeCurrentDialog");
            };
        };

        // Set hack params
        hackParams = $.extend(hackParams, {
            routingAction: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
            bizagi: bizagi
        });
        // }

        return {
            callback: callback,
            params: hackParams
        };
    },
    /*
    *  Show modal window with static url
    **/
    showContentPopup: function (url, title, id) {
        var self = this;
        id = id || "";

        // Define hacks for each page
        var hack = self.getPageHacks(url, title);

        if (bizagi.util.isIE() && bizagi.util.getInternetExplorerVersion() < 10 && self.needsQuicksMode(url)) {
            // Shows a popup window
            self.publish("showOldWindow", {
                url: url,
                windowParameters: $.extend(hack.params, {
                    controller: self,
                    afterLoad: hack.callback
                })
            });

        } else {
            // Show a normal popup

            // If the same popup is opened close it
            if (self.currentPopup == "genericiframe") {
                bizagi.workportal.desktop.popup.closePopupInstance();
                return;
            }

            // Shows a popup widget
            self.currentPopup = "genericiframe";
            self.publish("showDialogWidget", {
                widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_GENERICIFRAME,
                widgetURL: url,
                injectCss: [".ui-bizagi-old-render*", "." + title, "@font-face"],
                modalParameters: {
                    title: title,
                    id: id
                },
                afterLoad: hack.callback,
                afterLoadParams: hack.params
            });
        }
    },
    showWidgetPopup: function (widget, title, id) {
        var self = this;

        self.publish("showDialogWidget", {
            widgetName: widget,
            modalParameters: {
                title: title,
                width: "1020px",
                showCloseButton: false
            }
        });
    },
    showCustomWidgetPopup: function (params) {
        var self = this;

        self.publish("showDialogWidget", {
            widgetName: params.url,
            data: { "idCase": params.id },
            closeVisible: params.showCloseButton,
            maximize: (typeof params.maximize !== "undefined") ? params.maximize : true,
            modalParameters: {
                title: params.title,
                width: "910px",
                id: params.id
            }
        });
    },
    deleteMenuOption: function (categoryKey) {
        var self = this;
        var menuData = self.params.options.menuData;

        for (var i = 0; i < menuData.length; i++) {
            if (menuData[i].categoryKey == categoryKey) {
                self.params.options.menuData.splice(i, 1);
            }
        }
    },

    /*
    * Regresa un arreglo con los submenu autorizados
    */
    getSubItemsAuthorized: function () {
        var self = this;
        var result = [];
        $.each(self.params.options.menuData, function (k, v) {
            result.push(v.categoryKey);
        });
        return result;
    }

});
