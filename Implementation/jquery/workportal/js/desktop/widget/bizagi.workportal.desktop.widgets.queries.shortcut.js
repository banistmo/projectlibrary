/*
 *   Name: BizAgi Workportal Desktop stored queries shortcut
 *   Author: Iván Ricardo Taimal Narváez
 *   Comments:
 *   -   This script will provide behavior to stored queries shortcut
 */

// Auto extend
bizagi.workportal.widgets.queries.definition.extend("bizagi.workportal.widgets.queries.shortcut", {}, {

    /**
     *   To be overriden in each device to apply layouts
     */
    loadtemplates: function () {
        var self = this;
        self.flatListTemplate = self.workportalFacade.getTemplate("queries-shortcut-flat-list");
        self.confirmTemplate = self.workportalFacade.getTemplate("queries-shortcut-query-confirm");
        self.messageErrorTemplate = self.workportalFacade.getTemplate("queries-shortcut-query-message-error");
    },
    postRender: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("queries-shortcut");
        var content = self.content = $.tmpl(template);
        self.handleEvents();
        self.unsubscribe("refresh-list-stored-query-form");
        self.subscribe("refresh-list-stored-query-form", function( event, params ) {
            self.renderStoredQueries( params );
        });
    },

    handleEvents: function() {
        var self = this;
        $("#queryFormShortcutContainer").show();
        $("#queryFormShortcutContainer").click(function () {
            var queryFormShortcut =  $("#queryFormShortcut");
            var arrow = $(this).find("span");
            var isHidden = (queryFormShortcut.is(":hidden")) ? true : false;
            if (isHidden) {
                queryFormShortcut.show();
                arrow.removeClass("Right");
                arrow.addClass("Down");
                self.renderStoredQueries();
            } else {
                queryFormShortcut.hide();
                arrow.removeClass("Down");
                arrow.addClass("Right");
            }
        });
    },

    /**
     * Bind tree navigation
     */
    configureTreeNavigation: function () {
        var self = this;
        var content = self.getContent();
        var categoryTree = $("#categoryTree", content);
        var queriesContainer = $("#queries-shortcut", content);
        $("li.storedQuery",queriesContainer).click(function () {
            var guid    =   $(this).data("guid");
            var id      =   $(this).data("id");
            var idNode =    $(this).data("idnode");
            var paramsStoredQuery;
            var result  =  $.grep(self.storedQueryForm, function(e){ return e.idNode == idNode; });
            if (result.length == 1) {
                paramsStoredQuery =   result[0];
                self.showQueryFormPopup({
                    "idStoredQuery": guid,
                    "idQuery":1,
                    "title": paramsStoredQuery.definition.name,
                    "paramsStoredQuery":paramsStoredQuery,
                    "queryFormAction":"execute",
                    "notMigratedUrl": paramsStoredQuery.notMigratedUrl ? paramsStoredQuery.notMigratedUrl.execute : ""
                });
            }
            self.currentPopup=undefined;
           // self.renderQueries();
        });

        // Bind for action elements (Edit and delete buttons)
        $(".editButton", "#queries-shortcut").click(function (e) {
            var queryId = "-1";
            var guid = $(this).parent().data("guid");
            var idStoredQuery = $(this).parent().data("id");
            var idNode =    $(this).parent().data("idnode");
            var paramsStoredQuery;
            var result = $.grep(self.storedQueryForm, function (e) {
                return e.idNode == idNode;
            });
            if (result.length == 1) {
                paramsStoredQuery = result[0];
                self.showQueryFormPopup({
                    "queryFormAction": "edit",
                    "idStoredQuery": (idStoredQuery == "-1") ? "" : idStoredQuery,
                    "paramsStoredQuery": paramsStoredQuery,
                    "title": paramsStoredQuery.definition.name,
                    "notMigratedUrl": paramsStoredQuery.notMigratedUrl ? paramsStoredQuery.notMigratedUrl.edit : ""
                });
                bizagi.workportal.desktop.popup.closePopupInstance();
            }
        });

        $(".deleteButton", "#queries-shortcut").click(function (e) {
            e.stopPropagation();
            var queryId = "-1";
            var guid = $(this).parent().data("guid");
            var idStoredQuery = $(this).parent().data("id");
            var idNode =    $(this).parent().data("idnode");
            var confirmContent, paramsStoredQuery, confirmContentParams;

            var messageErrorForm    = self.getResource("workportal-widget-admin-entities-message-migrate");
            messageErrorForm        = messageErrorForm.replace("{build}", bizagi.loader.productBuildToAbout);

            var result = $.grep(self.storedQueryForm, function (e) {
                return e.idNode == idNode;
            });
            if (result.length == 1) {
                paramsStoredQuery = result[0];

                confirmContentParams ={
                    resizable: true,
                    modal: true,
                    title: self.getResource("workportal-widget-queries-confirm-title"),
                    buttons: []
                };

                var p_sessionId = bizagi.cookie("JSESSIONID");
                if(p_sessionId!==null && paramsStoredQuery.notMigratedUrl){
                    confirmContent = $.tmpl(self.messageErrorTemplate, { messageError: messageErrorForm });
                }
                else{
                    confirmContent = $.tmpl(self.confirmTemplate);

                    confirmContentParams.buttons = [
                        {
                            text: self.getResource("workportal-widget-queries-confirm-delete"),
                            click: function () {
                                $(this).dialog("close");
                                if (paramsStoredQuery.notMigratedUrl && paramsStoredQuery.notMigratedUrl["delete"]) {
                                    $.get("App/" + paramsStoredQuery.notMigratedUrl["delete"]);
                                }else{
                                    $.when(self.dataService.deleteStoredQueryForm({idStoredQueryForm: idStoredQuery})).done(function () {
                                        bizagi.workportal.desktop.popup.closePopupInstance();
                                        self.renderStoredQueries();
                                    });
                                }
                                self.publish("refresh-list-stored-query-form");
                                bizagi.workportal.desktop.popup.closePopupInstance();
                            }
                        },
                        {
                            text: self.getResource("workportal-widget-queries-confirm-cancel"),
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }

                // Open dialog with confirm message
                $(confirmContent).dialog(confirmContentParams);
            }
        });
    },
    renderStoredQueries: function(params) {
        var self = this;
        var content = self.getContent();
        var flatListTemplate = self.flatListTemplate;
        var queriesContainer = $("#queries-shortcut", content);
        var storedQueryFormContent;

        //Update 11/11/2014 to !
        //if(bizagi.util.parseBoolean(bizagi.override.disableFrankensteinQueryForms)) {

            $.when(self.dataService.getStoredQueryFormList()).done(function (storedQueryForm) {
                self.storedQueryForm = storedQueryForm.storedQueryForms;
                storedQueryFormContent = $.tmpl(flatListTemplate, {storedQueryForm: self.storedQueryForm});
                storedQueryFormContent.appendTo(queriesContainer);
                self.configureTreeNavigation();
                if ( params && params.response ) {
                    $(".storedQuery[data-id=" + params.response + "]" + ( (params.fromExec) ? " .editButton": ""), "#queries-shortcut").click();
                }
            });

        //}

    }

});