/*
 *   Name: BizAgi Rendering Services
 *   Author: Diego Parra (based on Edward Morales version)
 *   Comments:
 *   -   This class will provide a facade to access to workportal REST services
 */

$.Class.extend("bizagi.workportal.services.service", {
    // Statics
    BA_CONTEXT_PARAMETER_PREFIX: "h_",
    ENTITIES_QUERY_PAGE_SIZE: 10
}, {
    /* 
     *   Constructor
     */
    init: function (params) {
        params = typeof (params) == "object" ? params : {};
        params.context = !bizagi.util.isEmpty(params.context) ? params.context : "workportal";

        params.proxyPrefix = !bizagi.util.isEmpty(params.proxyPrefix) ? params.proxyPrefix : "";
        params.proxyPrefix = this.normalizeUrl(params.proxyPrefix);
        bizagi.RPproxyPrefix = params.proxyPrefix;


        params.sharepointProxyPrefix = !bizagi.util.isEmpty(params.sharepointProxyPrefix) ? params.sharepointProxyPrefix : "";
        params.sharepointProxyPrefix = this.normalizeUrl(params.sharepointProxyPrefix);

        this.serviceLocator = new bizagi.workportal.services.context(params);

        // Define default value of number of records to show
        this.pageSize = 10;

        // Get override for pageSize, use BIZAGI_INBOX_ROWS_PER_PAGE constant within config file
        if (typeof BIZAGI_INBOX_ROWS_PER_PAGE != "undefined") {
            this.pageSize = ($.isNumeric(BIZAGI_INBOX_ROWS_PER_PAGE)) ? BIZAGI_INBOX_ROWS_PER_PAGE : this.pageSize;
        }
    },
    /**
     * This method will return the url defined as endPoint using the parameter "endPoint" name
     */
    getUrl: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl(params.endPoint);
        return url;
    },
    /**
     * This method will return the url normalized
     */
    normalizeUrl: function (url) {
        if (url != "") {
            if (url[url.length - 1] != "/") {
                url = url.concat("/");
            }

            if (url.indexOf("http://") == -1 && url.indexOf("https://") == -1) {
                url = "http://" + url;
            }
        }
        return url;
    },
    /**
     * This method return workportal logo
     */
    getWPLogo: function () {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl('theme-handler-getLogoImagePath'),
            dataType: "json",
            type: "GET"
        });
    },
    /**
     * This service get json of theme definition
     *@return {json} theme definition
     */
    getCurrentTheme: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("getCurrentTheme");

        return $.read(url);
    },

    /**
     * This service get all overrides keys within configuration project
     *
     * @return {json}
     */
    getOverrides: function () {
        var self = this;
        return $.read({
            url: self.serviceLocator.getUrl("overrides")
        });
    },

    /**
     * This method will return the response of the user information login is correct
     * this method is use for native smartphone
     **/
    authenticatedUser: function (params) {


        var self = this;

        var urlSend = self.serviceLocator.getUrl("login-handler");

        return $.read({
            url: urlSend,
            data: {
                "userName": BIZAGI_USER,
                "password": BIZAGI_PASSWORD,
                "domain": BIZAGI_DOMAIN
            }
        }).pipe(
            function (response) {
                return response;
            },
            function (data) {
                urlSend = self.serviceLocator.getUrl("login-handlerv10");
                urlSend = urlSend.replace("{0}", BIZAGI_USER || "");
                urlSend = urlSend.replace("{1}", BIZAGI_PASSWORD || "");
                urlSend = urlSend.replace("{2}", BIZAGI_DOMAIN || "");
                return $.ajax({
                    url: urlSend,
                    type: "GET",
                    dataType: "json"
                });
            }
        );
    },

    /**
     *
     * this method is use for native smartphone
     **/
    logoutUser: function (params) {
        var self = this;
        var urlSend = self.serviceLocator.getUrl("logoff-handlerv1");
        return $.read({
            url: urlSend
        });
    },
    /*
     *   Logoff the current user
     */
    logOffUser: function (params) {
        var self = this;
        var urlSend = self.serviceLocator.getUrl("logoff-handler");
        return $.read({
            url: urlSend
        });
    },
    /*
     *   Gets the current working user
     */
    getCurrentUser: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("user-handler-getCurrentUser"));
    },
    /*
     *   Gets the inbox summary for the current logged user
     *   Returns a promise of the data being retrieved
     */
    getInboxSummary: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("inbox-handler-getInboxSummary"))
            .pipe(function (response) {
                // Extract inboxSummary property
                return response["inboxSummary"];
            });
    },
    /*
     *   Return all the available processes in bizagi
     */
    getAllProcesses: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        var url = self.serviceLocator.getUrl("process-handler-getAllProcesses");

        if (params.taskState != undefined) {
            data["taskState"] = params.taskState;
        }

        if (params.onlyFavorites != undefined && params.onlyFavorites != "") {
            data["onlyFavorites"] = params.onlyFavorites;
        }

        if (params.smartfoldersParameters) {
            url = url + "?" + params.smartfoldersParameters;
        }

        if (bizagi.isMobile()) {
            data["mobileDevice"] = true;
        }

        // Call ajax and returns promise
        return $.read(url, data)
            .pipe(function (response) {
                // !! (double Not) (bang bang) <-- For Boolean Type Casting http://jibbering.com/faq/notes/type-conversion/#tcBool
                if (!!params.skipAggrupate)
                    return response;
                // Aggrupate workflows by categories
                var responseData = {};
                var categories = {};
                var allProcesses = bizagi.localization.getResource("workportal-widget-inbox-all-processes");
                var allCases = {
                    name: bizagi.localization.getResource("workportal-widget-inbox-all-cases"),
                    path: "",
                    category: allProcesses,
                    isFavorite: (params.onlyFavorites || false),
                    guidFavorite: "",
                    idworkflow: "",
                    guidWFClass: "",
                    count: 0
                };

                // Create a default category
                categories[allProcesses] = [];
                categories[allProcesses].workflows = [];
                categories[allProcesses].workflows.push(allCases);
                if (response.workflows) {
                    $.each(response.workflows.workflow, function (i, workflow) {
                        // Creates the category if it doesn't exist
                        if (!categories[workflow.category]) {
                            categories[workflow.category] = [];
                            categories[workflow.category].workflows = [];
                        }

                        // Push the workflow
                        categories[workflow.category].workflows.push(workflow);

                        // Sum the cases count
                        //  if (bizagi.util.isEmpty(params.onlyFavorites) || params.onlyFavorites == false) {
                        allCases.count += Number(workflow.count);
                        // }
                    });
                }

                // Build result array once grouped
                responseData.categories = [];
                for (key in categories) {
                    responseData.categories.push({
                        name: key,
                        workflows: categories[key].workflows
                    });
                }

                return responseData;
            });
    },
    /*
     *   Return all the available cases in bizagi and thinking for mobile response
     */
    getCasesList: function (params) {
        var self = this;
        params = params || {};
        var data = {};
        var url = self.serviceLocator.getUrl("case-handler-getCasesList");

        data["pageSize"] = params.pageSize || 10;
        data["page"] = params.page || 1;
        data["numberOfFields"] = params.numberOfFields || "-1"; //1;
        data["idWfClass"] = params.idWfClass || "-1";

        return $.read(url, data)
            .pipe(function (response) {
                var elementToHtml = {
                    overdue: [],
                    today: [],
                    tomorrow: [],
                    upcoming: [],
                    page: response.page,
                    totalPages: response.totalPages
                };
                var actualDate = new Date();
                var actualMonth = actualDate.getMonth();
                var actualDay = actualDate.getDate();
                var actualYear = actualDate.getYear();

                for (var counter = 0; response.elements.length > counter; counter++) {

                    var tmpElement = response.elements[counter];
                    var tmpdate = new Date(tmpElement[3]);
                    var tmpDay = tmpdate.getDate();
                    var tmpMonth = tmpdate.getMonth();
                    var tmpYear = tmpdate.getYear();

                    if (actualDay > tmpDay || (actualMonth > tmpMonth && actualYear >= tmpYear)) {
                        tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), "dd MMM"); // "dd MMM hh:mm"
                        elementToHtml.overdue.push(tmpElement);
                    } else if (actualDay == tmpDay) {
                        tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), "hh:mm");
                        elementToHtml.today.push(tmpElement);

                    } else if ((actualDay + 1) == tmpDay) {
                        tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), "hh:mm");
                        elementToHtml.tomorrow.push(tmpElement);

                    } else {
                        tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), "dd MMM");
                        elementToHtml.upcoming.push(tmpElement);
                    }
                }
                //console.info(elementToHtml);
                return elementToHtml;
            });

    },
    /*
     *   Return all the available processes in bizagi
     */
    getAllAdministrableEntities: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["action"] = "entitiesList";
        data["kind"] = "administrable";

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("entities-administration"),
            data: data,
            type: "POST",
            dataType: "json"
        }).pipe(function (data) {
            var result = {
                entities: []
            };
            $.each(data, function (i, item) {
                result.entities.push(item);
            });
            return result;
        });
    },
    /*
     *   Return all the available processes in bizagi
     */
    getEntitiesList: function (params) {
        var self = this;

        // Define data
        params = params || {};
        params["action"] = "entitiesList";
        params["kind"] = "entitiesData";

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("entities-administration"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     *   Returns all the cases for the current user filtered by a workflow
     */
    getCasesByWorkflow: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        var url = self.serviceLocator.getUrl("process-handler-getCustomizedColumnsDataInfo");


        // Required params
        if (!bizagi.util.isEmpty(params.taskState))
            data["taskState"] = params.taskState;
        if (!bizagi.util.isEmpty(params.idWorkflow))
            data["idWorkflow"] = params.idWorkflow;

        // Special case to search all favorite cases for all processes
        if (params.onlyFavorites == true) {
            data["onlyFavorites"] = true;
            data["taskState"] = "all";
        }

        // Optional params
        data["pageSize"] = params.pageSize || self.pageSize;
        data["page"] = params.page || 1;

        if (bizagi.isMobile()) {
            data["mobileDevice"] = true;
        }


        if (params.smartfoldersParameters) {
            var param = params.smartfoldersParameters.split('&');
            $.each(param, function (k, v) {
                var keyvalue = v.split('=');
                data[keyvalue[0]] = keyvalue[1];
            });
        }


        // Call ajax and returns promise
        return $.read(url, data)
            .pipe(function (response) {
                $.each(response.cases.rows, function (key, value) {
                    var newFieldsValue = [];

                    //  Set id of case to radnumber
                    response.cases.rows[key]["radnumber"] = value.id;
                    $.each(value.fields, function (fieldsKey, fieldsValue) {
                        // when delete element from fields array, each function lose key value
                        if (fieldsValue != undefined) {
                            // Radnumber
                            try {
                                if (fieldsValue.isRadNumber != undefined && fieldsValue.isRadNumber == "true") {
                                    response.cases.rows[key]["radnumber"] = fieldsValue.Value;
                                } else if (fieldsValue.workitems != undefined) {
                                    response.cases.rows[key]["workitems"] = fieldsValue.workitems;
                                } else {
                                    newFieldsValue.push(fieldsValue);
                                }
                            } catch (e) {}
                        }
                    });

                    // Update Fields 
                    response.cases.rows[key]['fields'] = newFieldsValue;
                });
                return response.cases;
            });
    },
    /*
     *   Returns the Organization List
     */
    getOrganizationsList: function () {
        var self = this;

        var url = self.serviceLocator.getUrl("process-handler-getOrganizations");
        var data = {};
        data["type"] = 12;
        data["name"] = "";

        return $.read(url, data);
    },
    /*
     *   Returns the data for the inbox grid view
     */
    getCustomizedColumnsData: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        var url = self.serviceLocator.getUrl("process-handler-getCustomizedColumnsData");
        data["pageSize"] = params.pageSize || self.pageSize;
        data["page"] = params.page || 1;

        // data for sort columns
        data["orderFieldName"] = params.orderFieldName || "";
        data["orderType"] = params.orderType || "0";
        data["order"] = params.order || "";

        if (bizagi.isMobile()) {
            data["mobileDevice"] = true;
        }

        // Fixes stuff
        // TODO: this must be fixed in the server
        params.taskState = params.taskState || "all";
        if (params.taskState.toString().toLowerCase() === "")
            params.taskState = "all";
        if (params.taskState.toString().toLowerCase() === "red")
            params.taskState = "Red";
        if (params.taskState.toString().toLowerCase() === "yellow")
            params.taskState = "Yellow";
        if (params.taskState.toString().toLowerCase() === "green")
            params.taskState = "Green";

        data.taskState = params.taskState;
        // Required params
        if (!bizagi.util.isEmpty(params.idWorkflow)) {
            data["idWorkflow"] = params.idWorkflow;
        }
        if (!bizagi.util.isEmpty(params.idTask)) {
            data["idTask"] = params.idTask;
        }

        if (!bizagi.util.isEmpty(params.radNumber)) {
            data["radNumber"] = params.radNumber;
        }

        // Special case to search all favorite cases for all processes
        if (params.onlyFavorites == true) {
            data["onlyFavorites"] = true;
            data["taskState"] = "all";
        }

        if (params.smartfoldersParameters) {
            var param = params.smartfoldersParameters.split('&');
            $.each(param, function (k, v) {
                var keyvalue = v.split('=');
                data[keyvalue[0]] = keyvalue[1];
            });
        }

        // Call ajax and returns promise
        return $.read(url, data);
    },
    /*
     *   Returns the available categories filtered by a parent category
     *   If not parent category is sent, it will return the base categories
     */
    getCategories: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};

        // Required params
        if (params.idCategory)
            data["idCategory"] = params.idCategory;
        if (params.idApp)
            data["idApp"] = params.idApp;

        if (bizagi.isMobile()) {
            data["mobileDevice"] = true;
        }


        data["groupByApp"] = params.groupByApp || false;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("process-handler-getCategory"), data);
    },
    /*
     *   Returns the recent process
     */
    getRecentProcesses: function (params) {
        var self = this;
        params = params || {};

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("process-handler-getRecentProcesses"), params);
    },
    /*
     *   Returns the summary for a case
     */
    getCaseSummary: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read({
                url: self.serviceLocator.getUrl("case-handler-getCaseSummary"),
                data: {
                    idCase: params.idCase,
                    eventAsTasks: params.eventAsTasks || "false",
                    onlyUserWorkItems: params.onlyUserWorkItems || "true",
                    mobileDevice: bizagi.isMobile() || "false"
                },
                serviceType: "GETSUMMARY"
            })
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the assigness for a case (all users assigned to any task of this case)
     */
    getCaseAssignees: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getCaseAssignees"), {
                    idCase: params.idCase
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns all the tasks belonging to a process, aditionally return if it has an active workitem or not
     */
    getCaseTasks: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
            self.serviceLocator.getUrl("case-handler-getCaseTasks"), {
                idCase: params.idCase
            }
        );
    },
    /*
     *   Returns all the events available for a case
     */
    getCaseEvents: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read({
                url: self.serviceLocator.getUrl("case-handler-getCaseEvents"),
                data: {
                    idCase: params.idCase
                },
                serviceType: "GETEVENTS"
            })
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns all the available subprocesses for a case
     */
    getCaseSubprocesses: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read({
                url: self.serviceLocator.getUrl("case-handler-getCaseSubprocesses"),
                data: {
                    idCase: params.idCase
                },
                serviceType: "GETSUBPROCESSES"
            })
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns forms render version for a case
     */
    getCaseFormsRenderVersion: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
            self.serviceLocator.getUrl("case-handler-getCaseFormsRenderVersion"), {
                idCase: params.idCase
            }
        );
    },
    /**
     *  Get json from forms render version service
     */
    getCaseFormsRenderVersionDataContent: function (options) {
        var self = this;
        var defer = new $.Deferred();

        $.when(self.getCaseFormsRenderVersion(options))
            .done(function (data) {
                // return json content                    
                defer.resolve(data);
            });
        return defer.promise();
    },
    /*
     *   Returns all the assignees belonging to a workitem (case - task combination)
     */
    getTaskAssignees: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase, idTask
        return $.read(
            self.serviceLocator.getUrl("case-handler-getTaskAssignees"), {
                idCase: params.idCase,
                idTask: params.idTask
            }
        );
    },
    /*
     *   Returns the available workitems for a case (Routing service)
     */
    getWorkitems: function (params) {
        var self = this;

        params = params || {};
        var data = {};

        data.idCase = params.idCase;
        data.fromTask = params.fromTask;
        data.fromWorkItemId = params.fromWorkItemId;
        data.mobileDevice = bizagi.isMobile();

        if (typeof params.onlyUserWorkItems !== "undefined") {
            data.onlyUserWorkItems = params.onlyUserWorkItems;
        }


        // Required params: idCase
        return $.read({
            url: self.serviceLocator.getUrl("case-handler-getWorkItems"),
            data: data,
            serviceType: "GETWORKITEMS"
        });
    },
    /*
     *   Returns the available workitems for a case (Routing service)
     */
    getQueriesDefinitions: function (params) {
        var self = this;
        if (self.hashQueriesDef && params.idNode) {
            return self.hashQueriesDef[params.idNode];
        } else {
            var defer = new $.Deferred();
            // Call ajax and returns promise
            $.read(self.serviceLocator.getUrl("query-handler-getqueries-definitions"))
                .done(function (response) {
                    self.hashQueriesDef = {};
                    self.hashQueriesDef[0] = {};
                    self.processQueriesDefinitions("-1", response.query);
                    defer.resolve(self.hashQueriesDef["-1"]);
                });
            return defer.promise();
        }
    },
    /*
     *   Creates a hash of queries to simulate each category request
     */
    processQueriesDefinitions: function (parent, queries) {
        var self = this;
        $.each(queries, function (i, query) {
            if (!self.hashQueriesDef[parent]) {
                self.hashQueriesDef[parent] = [];
            }
            self.hashQueriesDef[parent].push(query);
            if (query.nodes) {
                self.processQueriesDefinitions(query.idNode, query.nodes);
            } else {
                query.nodes = {};
            }
        });
    },
    /*
     *   Returns the available workitems for a case (Routing service)
     */
    getQueries: function (params) {
        var self = this;

        if (self.hashQueries && params.idElement) {
            return self.hashQueries[params.idElement];
        } else {
            var defer = new $.Deferred();

            // Call ajax and returns promise
            $.read(self.serviceLocator.getUrl("query-handler-getqueries"))
                .done(function (response) {
                    self.hashQueries = {};
                    self.hashQueries[0] = {};

                    // Process the full response converting it into small entries inside a hashtable
                    self.processQueries(response.query);
                    defer.resolve(self.hashQueries["-1"]);
                });
            return defer.promise();
        }
    },
    /*
     *   Creates a hash of queries to simulate each category request
     */
    processQueries: function (queries) {
        var self = this;
        $.each(queries, function (i, query) {
            if (!self.hashQueries[query.idParent]) {
                self.hashQueries[query.idParent] = [];
            }
            self.hashQueries[query.idParent].push(query);
            if (query.nodes) {
                self.processQueries(query.nodes);
            } else {
                query.nodes = {};
            }
        });
    },
    /*
     *   Creates a new case in BizAgi
     */
    createNewCase: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.create({
            url: self.serviceLocator.getUrl("case-handler-addNewCase"),
            data: {
                idWfClass: params.idWfClass,
                caseData: params.caseData ? JSON.stringify(params.caseData) : null,
                idOrganization: params.idOrganization ? params.idOrganization : null
            },
            serviceType: "NEWCASE"
        });
    },
    /*
     *   Search cases with radNumber
     */
    searchCases: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};

        data["h_action"] = "SEARCHCASES";
        data["onlyUserCases"] = false;
        data["active"] = false;
        data["page"] = params.page || 1;
        if (params.radNumber)
            data["radNumber"] = $.trim(params.radNumber);
        data["pageSize"] = params.pageSize || self.pageSize;

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     *   Search cases with radNumber
     */
    queryCases: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};

        data.h_action = "QUERYCASES";
        data.onlyUserCases = params.onlyUserCases || false;
        data.active = params.active && true;
        data.page = params.page || 1;
        if (params.radNumber)
            data.radNumber = $.trim(params.radNumber);
        data.pageSize = params.pageSize || self.pageSize;

        data.filter = params.filter;
        data.outputxpath = params.outputxpath;

        // data for sort columns
        data.orderFieldName = params.orderFieldName || "";
        data.orderType = (params.orderType == 'asc' || params.orderType == 1) ? 1 : 0;
        data.order = params.order || "";

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     *   Search entities service
     */
    queryEntities: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};

        data.h_action = "QUERYENTITIES";
        data.page = params.page || 1;
        data.pageSize = self.Class.ENTITIES_QUERY_PAGE_SIZE;

        // Query parameters
        data[self.Class.BA_CONTEXT_PARAMETER_PREFIX + "idEnt"] = params.idEntity;
        data.filter = params.filter;
        data.outputxpath = params.outputxpath;

        // Data to sort columns
        data.orderFieldName = params.orderFieldName || "";
        data.orderType = params.orderType || "";
        data.order = params.order || "";

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     *  Get Menu Authorization
     */
    getMenuAuthorization: function () {
        var self = this;

        return $.read(
            self.serviceLocator.getUrl("authorization-handler-getMenuAuthorization")
        );
    },
    /*
     *  The user have permissions to excecute this Case
     */
    isCaseCreationAuthorized: function (params) {
        var self = this;
        var urlSend = self.serviceLocator.getUrl("authorization-handler-isCaseCreationAuthorized");
        urlSend = urlSend.replace("{0}", params.idWfClass || "");

        return $.read({
            url: urlSend //,
            //data: { "idWfClass": params.idWfClass }
            //data: { "userName": BIZAGI_USER, "password": BIZAGI_PASSWORD, "domain": BIZAGI_DOMAIN }
        });
    },
    /*
     *   Returns the execution state of an async workitem
     */
    getAsynchExecutionState: function (params) {
        var self = this;
        var idAsynchWorkitem;
        params = params || {};

        if (params.idCase != -1) {
            return $.read({
                url: self.serviceLocator.getUrl("case-handler-getAsynchExecutionState"),
                data: {
                    idCase: params.idCase,
                    idAsynchWorkitem: "-1"
                },
                serviceType: "ASYNCHEXECUTION"
            });
        } else {
            return $.read({
                url: self.serviceLocator.getUrl("case-handler-getAsynchExecutionState"),
                data: {
                    idCase: params.idCase,
                    idAsynchWorkitem: params.idAsynchWorkitem
                },
                serviceType: "ASYNCHEXECUTION"
            });
        }

        // Required params: idCase idAsynchWorkitem

    },

    /*
     *  Returns the supported log types
     */
    supportedLogTypes: function () {
        var self = this;

        return $.read(self.serviceLocator.getUrl("case-handler-supportedLogTypes"));
    },
    /*
     *   Returns the Activity Log for a case
     */
    getActivityLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getActivityLog"), {
                    idCase: params.idCase,
                    orden: params.sort || 0,
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the Activity Detail Log for a case
     */
    getActivityDetailLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idWorkItemFrom
        return $.read(
                self.serviceLocator.getUrl("case-handler-getActivityDetailLog"), {
                    idCase: params.idCase,
                    idWorkItemFrom: params.idWorkItemFrom,
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the Entity Log for a case
     */
    getEntityLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getEntityLog"), {
                    idCase: params.idCase,
                    orden: params.sort || 0,
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the Entity Detail Log for a case
     */
    getEntityDetailLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getEntityDetailLog"), {
                    idCase: params.idCase,
                    idEntity: params.idEntity || -1,
                    userFullName: params.userFullName || "",
                    attribDisplayName: params.attribDisplayName || "",
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the User Log for a case
     */
    getUserLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getUserLog"), {
                    idCase: params.idCase,
                    orden: params.sort || 0,
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the User Detail Log for a case
     */
    getUserDetailLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase , idUser
        return $.read(
                self.serviceLocator.getUrl("case-handler-getUserDetailLog"), {
                    idCase: params.idCase,
                    idUser: params.idUser,
                    entDisplayName: params.entDisplayName || "",
                    attribDisplayName: params.attribDisplayName || "",
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Returns the Admin Log for a case
     */
    getAdminLog: function (params) {
        var self = this;

        params = params || {};

        // Required params: idCase
        return $.read(
                self.serviceLocator.getUrl("case-handler-getAdminLog"), {
                    idCase: params.idCase,
                    orden: params.sort || 0,
                    idActualPage: params.idActualPage || 1,
                    pageSize: params.pageSize || 10
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Adds a New Favorite
     */
    addFavorite: function (params) {
        var self = this;

        // Define data
        params = params || {};

        return $.create(
                self.serviceLocator.getUrl("favorites-handler-saveFavorite"), {
                    idObject: params.idObject,
                    favoriteType: params.favoriteType
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    /*
     *   Favorites service
     */
    delFavorite: function (params) {
        var self = this;

        // Define data
        params = params || {};

        // Required params: guidFavorite
        return $.destroy(
                self.serviceLocator.getUrl("favorites-handler-deleteFavorite"), {
                    guidFavorite: params.idObject
                }
            )
            .pipe(function (response) {
                return response;
            });
    },
    // Data Transformations for Summary Widget
    /*
     * Render Assigness
     */
    summaryAssigness: function (options) {
        var self = this;
        var def = $.Deferred();
        var idCase = options.idCase || "";

        $.when(
            self.getCaseAssignees({
                idCase: idCase
            })
        ).done(function (baseAssignees) {

            // ASING NEW VALUES FOR FINAL ARRAY assigneesFix
            var assignees = baseAssignees["assignees"];
            var assigneesFix = {};
            assigneesFix["showAssignees"] = (assignees.length >= 1) ? true : false;
            assigneesFix["assignees"] = {};
            assigneesFix["events"] = "";
            assigneesFix["activities"] = "";
            var assigneesEvents = "";
            var assigneesActivities = "";

            for (var i = 0; i < assignees.length; i++) {
                if (assignees[i]["isEvent"] == "true") {
                    assigneesEvents += (assigneesEvents.indexOf(assignees[i]["Name"]) == -1) ? assignees[i]["Name"] + ", " : "";
                } else {
                    assigneesActivities += (assigneesActivities.indexOf(assignees[i]["Name"]) == -1) ? assignees[i]["Name"] + ", " : "";
                }
            }
            //join assignees to event and activities
            assigneesFix["events"] = assigneesEvents.substring(0, assigneesEvents.length - 2);
            assigneesFix["activities"] = assigneesActivities.substring(0, assigneesActivities.length - 2);


            def.resolve(assigneesFix);
        });

        return def.promise();
    },
    /*
     * Render Assigness
     */
    summarySubProcess: function (options) {
        var self = this;
        var def = $.Deferred();
        var idCase = options.idCase || "";

        $.when(
            self.getCaseSubprocesses({
                idCase: idCase
            })
        ).done(function (subprocess) {
            subprocess["showSubProcess"] = (subprocess["subProcesses"].length >= 1) ? true : false;
            subprocess["showSubProcesColumns"] = (subprocess["CustFields"][0] !== undefined && subprocess["CustFields"][0][0].length >= 1) ? true : false;

            // Prepare json for subprocess
            if (subprocess["showSubProcesColumns"]) {
                subprocess["subProcPersonalized"] = {};
                var len;

                for (var n = 0; n < subprocess["CustFields"].length; n++) {
                    len = 0;
                    for (var i = 0; i < subprocess["subProcesses"].length; i++) {
                        if (subprocess["subProcesses"][i]["idCustData"] == n) {
                            subprocess["subProcPersonalized"][n] = subprocess["subProcPersonalized"][n] || {};
                            subprocess["subProcPersonalized"][n]["subProcesses"] = subprocess["subProcPersonalized"][n]["subProcesses"] || {};
                            subprocess["subProcPersonalized"][n]["subProcesses"][len++] = subprocess["subProcesses"][i];
                        }
                    }
                    if (subprocess["subProcesses"][n]["idCustData"] != -1 && subprocess["subProcPersonalized"][n] != undefined) {
                        subprocess["subProcPersonalized"][n]["CustFields"] = subprocess["CustFields"][n];
                        subprocess["subProcPersonalized"][n]["idCase"] = subprocess["idCase"];
                    }
                }
            }

            def.resolve(subprocess);
        });

        return def.promise();
    },
    /*
     * Render Case Events
     */
    summaryCaseEvents: function (options) {
        var self = this;
        var def = $.Deferred();
        var idCase = options.idCase || "";

        $.when(
            self.getCaseEvents({
                idCase: idCase
            })
        ).done(function (events) {
            events["showEvents"] = (events["events"].length >= 1) ? true : false;
            def.resolve(events);
        });
        return def.promise();
    },
    /*
     * Render Details
     * @param idWorkitem
     */
    summaryCaseDetails: function (options) {
        var self = this;
        var def = $.Deferred();
        var idWorkitem = options["idWorkitem"] || 0;

        $.when(
            self.getCaseSummaryDataContent(options)
        ).done(function (data) {
            // Show or hide tabs
            data["showEvents"] = (data["countEvents"] >= 1) ? true : false;
            data["showParentProcess"] = (data["idParentCase"] >= 1) ? true : false;
            data["parentProcess"] = {
                "displayName": data["parentDisplayName"],
                "idCase": data["idParentCase"]
            };
            data["isClosed"] = (data["isOpen"] == "true") ? false : true;
            data["showAssignees"] = (data["countAssigness"] >= 1) ? true : false;
            data["showSubProcess"] = (data["countSubProcesses"] >= 1) ? true : false;
            data["showForm"] = (data["hasGlobalForm"] == "true") ? true : false;
            data["allowsReassign"] = "false";


            for (i = 0; i < data.currentState.length; i++) {
                if (data["currentState"][i]["idWorkItem"] == idWorkitem) {
                    data["allowsReassign"] = data["currentState"][i]["allowsReassign"];
                }
            }

            // edit original data, no show events in activities tab
            var currentStateTypes = [];
            var m = 0; // counter for new activities array
            for (i = 0; i < data["currentState"].length; i++) {
                if (data["currentState"][i]["isEvent"] == "false" && data["currentState"][i]["assignToCurrentUser"] == "true" && data["currentState"][i]["idWorkItem"] != idWorkitem) {
                    currentStateTypes[m++] = data["currentState"][i];
                }
            }

            data["currentStateTypes"] = currentStateTypes;
            data["showActivities"] = (currentStateTypes.length >= 1) ? true : false;

            def.resolve(data);
        }).fail(function (error) {
            def.reject(error);
        });

        return def.promise();
    },
    releaseActivity: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("case-handler-releaseActivity");
        url = url.replace("{idCase}", params.idCase);
        var data = {};
        // Define data
        if (params) {
            data["idCase"] = params.idCase;
            data["idWorkItem"] = params.idWorkItem;
        }
        // Call ajax and returns promise
        return $.ajax({
            url: url,
            data: data,
            type: "POST",
            dataType: "json"
        });

    },
    /**
     *
     * Segment Activities for summary in render module
     * @param options {data,idWorkItem}
     */
    summaryActivities: function (options) {
        var activities = {};
        var data = options["data"] || {};
        var idWorkItem = idWorkItem || options["idWorkitem"] || 0;

        try {
            activities["showActivities"] = (data["currentStateTypes"].length >= 1) ? true : false;
            activities["currentState"] = data["currentStateTypes"];
            activities["globalIdWorkitem"] = idWorkItem;
            activities["creationDate"] = data["creationDate"];
            //    activities["idWorkItem"]=data["creationDate"];
        } catch (e) {}

        return activities;
    },
    /**
     *  Get json from cases service
     */
    getCaseSummaryDataContent: function (options) {
        var self = this;
        var promise = new $.Deferred();

        $.when(self.getCaseSummary(options))
            .done(function (data) {
                // Completes data
                data.taskState = self.icoTaskState,
                data.createdByName = data.createdBy.Name;
                data.createdByUserName = data.createdBy.userName;
                data.caseDescription = (data.caseDescription == "" ? "" : data.caseDescription);
                data.processPath = data.processPath.replace(/\//g, " > ") + data.process;
                data.showWorkOnIt = true;

                // return json content                    
                promise.resolve(data);
            }).fail(function (error) {
                promise.reject(error);
            });
        return promise.promise();
    },
    /**
     * Definition service for bizagi smart folders
     *
     * @param id optional with filter purpose
     */
    getSmartFolders: function (id) {
        var self = this;
        var data = {};

        data.idFolder = id || "";
        // Call read and returns promise
        return $.read(self.serviceLocator.getUrl("folders-handler-getUserQueries"), data).pipe(function (response) {
            if (id == -1 || id == "") {
                return response;
            } else {
                return self.searchFolders(id, response) || [];
            }
        });
    },
    /**
     * Delete smart folder
     * @param options {idSmartFolder,idUser}
     */
    deleteSmartFolder: function (options) {
        var self = this;
        var data = {};

        data.action = "6";
        options = options || {};

        data.idSmartFolder = options.idSmartFolder || "";
        data.idUser = options.idUser || "";

        return $.destroy(self.serviceLocator.getUrl("folders-associate-deleteSmartFolder"), data);
    },
    /**
     * Definition service for bizagi folders
     *
     * @param id optional with filter purpose
     */
    getFolders: function (id) {
        var self = this;
        var data = {};

        data.action = "getUserFolder";
        data.idFolder = id || "";
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("folders-handler"),
            type: "POST",
            data: data,
            dataType: "json"
        }).pipe(function (response) {
            if (id == "") {
                var folders = {
                    folders: [
                        {
                            name: bizagi.localization.getResource("workportal-widget-folders"),
                            id: "-1",
                            idParent: 0,
                            childs: response
                        }
                    ]
                };
                return folders;
            } else if (id == -1) {
                return response;
            } else {
                return self.searchFolders(id, response) || [];
            }
        });
    },
    searchFolders: function (idFolder, data) {
        for (var i = 0; i < data.folders.length; i++) {
            var value = data.folders[i];
            if (value.id == idFolder) {
                return data.folders[i]['childs'];
                break;
            }
        }
        return undefined;
    },
    getCasesByFolder: function (query) {
        query = query || "";

        return $.ajax({
            cache: true,
            url: "RestServices/" + query,
            type: "GET",
            dataType: "json"
        });
    },
    makeFolder: function (options) {
        var self = this;
        var data = {};

        data.action = "CreateUpdateFolder";
        data.folderName = options.folderName || "No Name";

        // Make folder under subcategory        
        if (options.idParentFolder != undefined && options.idParentFolder > 1) {
            data.idParentFolder = options.idParentFolder;
        }

        return $.ajax({
            url: self.serviceLocator.getUrl("folders-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    updateFolder: function (options) {
        var self = this;
        var data = {};
        options = options || {};

        data.action = "CreateUpdateFolder";
        // Define idFolder
        data.idFolder = options.idFolder || "";

        // Update name
        if (options.folderName) {
            data.folderName = options.folderName;
        }

        // Update path
        if (options.idParentFolder) {
            data.idParentFolder = options.idParentFolder;
        }

        return $.ajax({
            url: self.serviceLocator.getUrl("folders-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /**
     * Associate cases to folders
     *
     * @param options {idcase,idCustomFolder}
     */
    associateCaseToFolder: function (options) {
        var self = this;
        var data = {};

        data.action = "4";
        options = options || {};

        data.idCase = options.idCase || "";
        data.idCustomFolder = options.idCustomFolder;

        return $.ajax({
            url: self.serviceLocator.getUrl("folders-associate"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     * Dissasociate cases to folder
     *
     * @param options {idFolder, idCase}
     */
    dissasociateCaseFromFolder: function (options) {
        var self = this;
        var data = {};

        data.action = "DeleteCaseFromFolder";
        options = options || {};

        data.idFolder = options.idFolder;
        data.idCase = options.idCase;

        return $.ajax({
            url: self.serviceLocator.getUrl("folders-handler"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /**
     * Delete folder
     * @param options {idCustomFolder}
     */
    deleteFolder: function (options) {
        var self = this;
        var data = {};

        data.action = "5";
        options = options || {};

        data.idCustomFolder = options.idCustomFolder || "";

        return $.ajax({
            url: self.serviceLocator.getUrl("folders-associate"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /**
     * Gets a file binary from the server
     * @param options {fileId}
     */
    getFile: function (options) {
        var self = this;
        var data = {};
        var url = this.serviceLocator.getUrl("file-handler");
        options = options || {};

        data.action = "getFileContent";
        data.uploadId = options.uploadId || "";
        data.entityId = options.entityId || "";
        data.entityKey = options.entityKey || "";

        return bizagi.services.ajax.pathToBase + url + "?" + jQuery.param(data);
    },
    /**
     * Get Comments per case
     * @param params {idCase=int&idColorCategory=int&pag=int&pagSize=int}
     * @return json
     */
    getComments: function (params) {
        //params.idCase = params.idCase || 0;
        var self = this;

        params = params || {};

        return $.read(self.serviceLocator.getUrl("MessageHandler-GetComments"), {
                idCase: params.idCase || "",
                idColorCategory: (typeof params.idColorCategory == 'number') ? params.idColorCategory : "",
                pag: params.pag || 1,
                pagSize: params.pagSize || 10
            })
            .pipe(function (result) {
                var def = new $.Deferred();
                var picture = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";
                if (!result.comments) {
                    result.comments = [];
                } else {
                    // Add CategoryColor for each comment
                    $.each(result.comments, function (key, value) {
                        if (!result.comments[key].hasOwnProperty('CategoryColor')) {
                            result.comments[key]['CategoryColor'] = '';
                        }
                    });
                }
                if (!result.users) {
                    $.when(self.getCurrentUser())
                        .done(function (user) {
                            result.users = [];
                            result.users.push({
                                Id: user.idUser,
                                Name: user.user,
                                DisplayName: user.userName,
                                Picture: picture
                            });
                        });
                } else {
                    $.each(result.users, function (key, value) {
                        // Add picture to all users
                        result.users[key]['Picture'] = (result.users[key]['Picture'] === "") ? picture : result.users[key]['Picture'];
                    });
                }
                def.resolve(result);
                return def.promise();
            });
    },
    /**
     * This function allow create new comment into the activity
     *
     * @param options {idCase,comment}
     * @return json
     */
    makeNewComment: function (options) {
        var self = this;
        var picture = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";
        return $.create(self.serviceLocator.getUrl("MessageHandler-NewComment"), {
                idCase: options.idCase || '',
                comment: options.comment.replace(/\n/g, "<br>") || ''
            })
            .pipe(function (result) {
                if (result.comments) {
                    var comments = [result.comments];
                    $.each(comments, function (key, value) {
                        // Add CategoryColor for each comment
                        comments[key]['CategoryColor'] = '';
                    });
                    result.comments = comments;
                }

                if (result.users) {
                    var users = [result.users];
                    $.each(users, function (key, value) {
                        // Add picture to all users
                        users[key]['Picture'] = (users[key]['Picture'] == "") ? picture : users[key]['Picture'];
                    });
                    result.users = users;
                }

                return result;
            });
    },
    /**
     * This function allow create new replies into the comment
     *
     * @param options {idCase=int&idComment=int&comment=string}
     * @return json
     */
    makeNewReply: function (options) {
        var self = this;
        var picture = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";

        options = options || {};

        return $.create(self.serviceLocator.getUrl("MessageHandler-ReplyComment"), {
                idCase: options.idCase || '',
                idComment: options.idComment || '',
                comment: options.comment.replace(/\n/g, "<br>") || ''
            })
            .pipe(function (result) {
                if (result.comments) {
                    result.Replies = [result.comments];
                }

                if (result.users) {
                    var users = [result.users];
                    $.each(users, function (key, value) {
                        // Add picture to all users
                        users[key]['Picture'] = (users[key]['Picture'] == "") ? picture : users[key]['Picture'];
                    });
                    result.users = users;
                }

                return result;
            });
    },
    /**
     * Remove one comment from the list
     *
     * @param options {idCase=int&idComment=int}
     * @return json {action:true|false, message:'if error, explain why'}
     */
    removeComment: function (options) {
        var self = this;

        options = options || {};

        return $.destroy(self.serviceLocator.getUrl("MessageHandler-RemoveComment"), {
            idCase: options.idCase || '',
            idComment: options.idComment || ''
        });
    },
    /**
     * Remove one replies from the list
     *
     * @param options {idCase=int&idComment=int&idReply=int}
     * @return json {action:true|false, message:'if error, explain why'}
     */
    removeReply: function (options) {
        var self = this;

        options = options || {};

        return $.destroy(self.serviceLocator.getUrl("MessageHandler-RemoveReply"), {
            idCase: options.idCase || '',
            idComment: options.idComment || '',
            idReply: options.idReply || ''
        });

    },
    /**
     * Rename category
     *
     * @param options {idColorCategory=int&colorName=string}
     * @return json
     */
    renameCommentCategory: function (options) {
        var self = this;

        options = options || {};

        return $.update(self.serviceLocator.getUrl("MessageHandler-RenameCategoryColor"), {
            idColorCategory: (options.idColorCategory >= 0) ? options.idColorCategory : '',
            colorName: options.colorName || ''
        });
    },
    /**
     * @param options {idCase=int&idComment=int&idColorCategory={0-5}}
     * return json
     */
    setCommentCategory: function (options) {
        var self = this;

        options = options || {};

        return $.update(self.serviceLocator.getUrl("MessageHandler-SetCategoryToComment"), {
            idCase: options.idCase || '',
            idComment: options.idComment || '',
            idColorCategory: (typeof options.idColorCategory == 'number') ? options.idColorCategory : ""
        });
    },
    /**
     * @return json Definition of all categories of message
     */
    getCommentsCategories: function () {
        var self = this;

        return $.read(self.serviceLocator.getUrl("MessageHandler-GetCategoryColors"))
            .pipe(function (result) {
                if (result.length > 1 && result[0]['categories']) {
                    return result[0];
                } else {
                    return result;
                }
            });
    },
    /**
     * This service can be define number of new comments based on idComment
     * @param json  options {idCase=int, idLastComment=int}
     * @return int
     */
    getNewComments: function (options) {
        var self = this;

        options = options || {};

        return $.read(self.serviceLocator.getUrl("MessageHandler-CountNewComments"), {
            idCase: options.idCase || 0,
            idLastComment: options.idLastComment || 0
        });
    },
    /**
     * Get all analisys queries
     * @return json
     */
    getAnalisysQueries: function () {
        var self = this;

        return $.read(self.serviceLocator.getUrl("bamAnalytics-handler-getAnalisysQueries"));
    },
    /**
     * Update name and comment
     *
     * @param options json {queryName,queryDescription,idQuery }
     * @return json  {response:true}
     */
    updateQueries: function (options) {
        var self = this;

        return $.update(self.serviceLocator.getUrl("bamAnalytics-handler-updateQuery"), {
            idQuery: (options.idQuery >= 0) ? options.idQuery : "",
            queryName: options.queryName || "",
            queryDescription: options.queryDescription || ""
        });
    },
    /**
     * Delete query
     *
     * @param queryId integer
     * @return json  {response:true}
     */
    deleteQueries: function (queryId) {
        var self = this;
        var data = {};

        data.action = "1612";
        data.QueryId = queryId || '';

        return $.destroy(self.serviceLocator.getUrl("reports-handler-deleteQueries"), data);
    },
    /**
     * Get bizagi configuration
     * @param {Function}   type    callback function when the file load is succed
     * @return {deferred} ajax object with JSON content
     */
    getConfiguration: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("authenticationConfig");

        return $.read(url);
    },
    /**
     * Logout Service
     */
    logout: function () {
        var self = this;
        // Remove session storage of authentication data
        sessionStorage.removeItem("bizagiAuthentication");

        //Call to the restfull service for domain list
        try {

            $.when(self.getConfiguration()).done(function (data) {
                var authenticationType = data.authenticationType;
                switch (authenticationType) {
                case 'FederateAuthentication':
                    $.when($.create(self.serviceLocator.getUrl("logout"))).done(function () {
                        location.href = data.logOffURL;
                    }).fail(function () {
                        bizagi.log("Error logging out");
                    }).always(function () {
                        location.href = data.logOffURL;
                    });
                    break;
                case 'SAML':
                    loader.nativeAjax(loader.getPathUrl(self.serviceLocator.getUrl("logout-saml")), function (response) {
                        var jsonRedirect = JSON.parse(response.responseText);
                        if (typeof jsonRedirect.urlRedirect !== "undefined") {
                            window.location.href = jsonRedirect.urlRedirect;
                        } else {
                            location.reload();
                        }
                    })
                        .fail(function () {
                            bizagi.log("Error logging out");
                        });
                    break;
                default:
                    //Normal
                    $.when($.create(self.serviceLocator.getUrl("logout"))).done(function () {
                        window.location = bizagi.services.ajax.loginPage;

                        if (!bizagi.util.isIE()) {
                            //Redirect to home
                            window.location.href = location.origin + location.pathname;
                        }
                    }).fail(function () {
                        bizagi.log("Error logging out");
                    });
                    break;
                }
            }).fail(function (message) {
                console.log(message);
            });
        } catch (e) {
            console.log(e);
        }
    },

    /**
     * Logout Mobile Service
     */
    logoutMobile: function () {
        var self = this;

        // Remove session storage of authentication data
        sessionStorage.removeItem("bizagiAuthentication");

        var url = self.serviceLocator.getUrl("logout");

        return $.read(url);
    },

    /**
     * Logout  beforeunload Service
     */
    logoutBeforeUnload: function () {
        var self = this;
        // Remove session storage of authentication data
        sessionStorage.removeItem("bizagiAuthentication");

        //Call to the restfull service for domain list
        $.ajax({
            type: 'POST',
            async: false,
            url: self.serviceLocator.getUrl("logout")
        });
    },
    /**
     * Services for Massive Activity Assignment
     */

    /*
     *   Obtain the organization info, such as roles, skills or locations. One of this options must be specified as a parameter
     * @param object  {objectType=Roles|Skills|Locations}
     * @return json
     */
    getOrganizationInfo: function (params) {
        var self = this;

        // Define data
        var data = {};

        data["objectType"] = params;

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("massive-activity-assignments-getOrganizationInfo"),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },
    /*
     *   Get Cases By Organization, using parameters suchs as roles, skills and locations
     * @param object  {roles=(string),roles=(skills), locations=(string)}
     * @return json
     */
    getCasesByOrganization: function (params) {
        var self = this;

        // Define data
        var data = {};


        data["roles"] = (params.roles) ? '[' + params.roles.toString() + ']' : '[]';

        data["skills"] = (params.skills) ? '[' + params.skills.toString() + ']' : '[]';

        data["locations"] = (params.locations) ? '[' + params.locations.toString() + ']' : '[]';

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("massive-activity-assignments-getCasesByOrganization"),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },
    /*
     *   Get Cases By Organization, using parameters suchs as roles, skills and locations
     * @param object  {user=[array], roles=(string),roles=(skills), locations=(string)}
     */
    reassignCases: function (params) {
        var self = this;

        // Define data
        var data = {};

        data["users"] = (params.user) ? '[' + params.user.toString() + ']' : '[]';
        data["roles"] = (params.roles) ? '[' + params.roles.toString() + ']' : '[]';
        data["skills"] = (params.skills) ? '[' + params.skills.toString() + ']' : '[]';
        data["locations"] = (params.locations) ? '[' + params.locations.toString() + ']' : '[]';

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("massive-activity-assignments-reassignCases"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     * Search the users inside the Organization, using parameters suchs as userName, full name, domain, roles, skills and locations
     * All parameters are optional
     * @param object  {userName=(string), fullName=(string), domain=(string), roles=(string),roles=(skills), locations=(string)}
     * @return json
     */
    searchUsers: function (params) {
        var self = this;

        // Define data
        var data = {};

        data["userName"] = params.userName || "";

        data["fullName"] = params.fullName || "";

        data["domain"] = params.domain || "";

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("massive-activity-assignments-searchUsers"),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },

    /*
     * Search the users inside the Organization, using an id's array
     * @return json
     */
    searchUsersByID: function (ids) {
        var self = this;

        // Define data
        var data = {
            ids: "[" + ids.toString() + "]"
        };

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("massive-activity-assignments-searchUsersById"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     * Search users usign parameters suchs as userName, fullName, domain and organization
     * @params { domain: (string), userName: (string), fullName: (string), organization: (string), page: (int), pageSize: (int) }
     * @return {deferred} ajax object with JSON content
     */
    getUsersList: function (params) {
        var self = this;

        return $.read(self.serviceLocator.getUrl("admin-getUsersList"), params);
    },

    /**
     * Get json with bizagi domains list
     * @return {deferred} ajax object with JSON content
     */
    getDomainList: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("domains");

        return $.read(url);
    },
    /**
     * Services for Asynchronous ECM Upload
     */

    /* Get the ECM Pending Scheduled Jobs
     * @return {deferred} ajax object with JSON content
     */
    getEcmAllScheduledJobs: function () {
        var self = this;

        // Define data
        var data = {};
        data["action"] = "getEcmAllScheduledJobs";

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("async-ecm-upload-baseService"), data);
    },
    /* Retries an especific ECM Pending Scheduled Job
     * @param object  {jobId=(string)} //The id of the job wich is going to be retried
     * @return {deferred} ajax object with JSON content
     */
    retryECMPendingScheduledJob: function (params) {
        var self = this;

        // Define data
        var data = {};
        data["action"] = "retryECMPendingScheduledJob";
        data["idJob"] = params;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("async-ecm-upload-baseService"), data);
    },
    /*
     *   Returns the current workportal version
     */
    getWorkPortalVersion: function () {
        var self = this;

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("WorkPortalVersion"),
            type: "GET",
            dataType: "json"
        }).always(function (data) {
            // Compatibility with java version
            data = data || {};

            if (data.version == "") {
                // Take the build version
                data.version = bizagi.loader.productBuild || "";
            }
            return data;
        });
    },
    /*
     *   Returns the combo's data to authentication log depending of the params
     */
    getAuthenticationLogData: function (params) {
        var self = this;

        // Define data
        var data = {};
        var url = "";

        if (params["dataType"] == "domains") {
            url = "admin-getAuthenticationDomains";
        } else if (params["dataType"] == "events") {
            url = "admin-getAuthenticationEventsTypes";
        } else {
            url = "admin-getAuthenticationEventSubTypes";
        }

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl(url),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },
    /*
     *   Get the search result of authentication log
     */
    getAuthenticationLogResult: function (params) {
        var self = this;

        // Define data
        var data = {};

        data["action"] = params.action;
        data["domain"] = params.domain;
        data["userName"] = params.userName;
        data["dtFrom"] = params.dtFrom;
        data["dtTo"] = params.dtTo;
        data["eventSubType"] = params.eventSubType;
        data["eventType"] = params.eventType;
        data["pag"] = params.pag;
        data["pagSize"] = params.pagSize;

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-getAuthenticationLog"),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },
    /*
     *   Return the password encrypted
     */
    encryptString: function (params) {
        var self = this;

        var data = {};

        // Define data
        params = params || {};

        data.entry = params.entry;

        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-EncryptString"),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *   Return the users requests
     */
    userPendingRequests: function (params) {
        var self = this;

        // Define data
        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("admin-UserPendingRequests"), {
            pag: params.pag,
            pagSize: params.pagSize
        });
    },

    /*
     *   Return the User Authentication Info
     */
    userAuthenticationInfo: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("admin-UserAuthenticationInfo"), {
            idUser: params.idUser
        });
    },

    /*
     *   Return Update User Authentication Info
     */
    updateUserAuthenticationInfo: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.create(self.serviceLocator.getUrl("admin-updateUserAuthenticationInfo"), {
            idUser: params.idUser,
            password: params.password,
            enable: params.enable,
            expired: params.expired,
            locked: params.locked
        });
    },

    /*
     *   Return generate Data To Send By Email
     */
    generateRandomPassword: function (params) {
        var self = this;

        // Define data
        params = params || {};
        params["action"] = "generateRandomPassword";
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-generateRandomPassword"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *   Return generate Data To Send By Email
     */
    generateDataToSendByEmail: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["action"] = "GenerateDataToSendByEmail";
        data["idUser"] = params.idUser;
        data["password"] = params.password;
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-GenerateDataToSendByEmail"),
            data: params,
            type: "POST",
            dataType: "json"

        });
    },

    /*
     *   Return generate Send Mail
     */
    sendEmail: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["action"] = "sendEmail";
        data["emailTo"] = params.emailTo;
        data["subject"] = params.subject;
        data["body"] = params.body;
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-sendEmail"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *   Return generate Send user password Mail
     */
    sendUserEmail: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["action"] = "SendUserEmail";
        data["emailTo"] = params.emailTo;
        data["subject"] = params.subject;
        data["body"] = params.body;
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-sendUserEmail"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *   Return Applications, Categories or processes list
     */
    getApplicationCategoriesList: function (params) {
        var self = this;
        var data = {};
        var url = "";

        if (params.action == "Applications") {
            url = "admin-getApplicationList";
            return $.ajax({
                url: self.serviceLocator.getUrl(url),
                type: "GET",
                dataType: "json"
            });
        } else {
            url = "admin-getCategoriesList";
            data.idApp = params.idApp;
            data.idCategory = params.idCategory;
            if (data.idCategory == "") {
                data.idCategory = -1;
            }
            if( typeof ( params.filterStartEvent ) !== 'undefined' )  {
                data.filterStartEvent = params.filterStartEvent;
            }
            return $.ajax({
                url: self.serviceLocator.getUrl(url),
                data: data,
                type: "GET",
                dataType: "json"
            });
        }

        // Call ajax and returns promise

    },

    /*
     *   Return the list of all cases that matched the filters
     */
    getAdminCasesList: function (params) {
        var self = this;
        var data = params || {};

        // Call ajax and returns promise
        return $.ajax({
            //eliminar esto con el servicio restfull
            cache: true,
            url: self.serviceLocator.getUrl("admin-getCasesList"),
            data: data,
            type: "GET",
            dataType: "json"
        });

    },

    /*
     *   Return the application's processes
     */
    getApplicationProcesses: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-getApplicationProcesses");

        var data = {};

        data["idApp"] = params["idApp"] ? params["idApp"] : -1;


        return $.read(url, data);
    },

    /*
     *   Return the application's processes
     */
    getProcessVersion: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-getProcessVersion");

        var data = {};

        data["idWfClass"] = params["idWFClass"];

        return $.read(url, data);
    },

    /*
     *   Return the the workflow's versions task
     */
    getProcessTasks: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-getProcessTasks");

        var data = {};

        data["idWfClass"] = params["idWFClass"];
        data["version"] = params["version"] ? params["version"] : undefined;

        return $.read(url, data);
    },

    /*
     * Retrieves the alarms from an specific task
     */
    getTaskAlarms: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-getTaskAlarms");

        var data = {};

        data["idTask"] = params["idTask"];

        return $.read(url, data);
    },

    /*
     * Retrieves each kind of alarm lapse
     */
    getLapseMode: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-getLapseMode");

        return $.read(url);
    },

    /*
     * Retrieves each kind of alarm recurrence
     */
    getRecurrMode: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-getRecurrMode");

        return $.read(url);
    },


    /*
     * Retrieves each kind of alarm recurrence
     */
    getScheduleType: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-getScheduleType");

        return $.read(url);
    },

    /*
     *   Retrieves the boss list
     */
    getBossList: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-getBossList");

        return $.read(url);
    },

    /*
     *   Add a new alarm
     */
    addAlarm: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-addAlarm");

        var data = {};

        data["idTask"] = params["idTask"];
        data["idRecurrMode"] = params["idRecurrMode"];
        data["idLapseMode"] = params["idLapseMode"];
        data["scheduleType"] = params["scheduleType"];
        data["alarmTime"] = params["alarmTime"];
        data["alarmRecurrTime"] = params["alarmRecurrTime"];
        data["sendToCurrentAssignee"] = params["sendToCurrentAssignee"];

        return $.update(url, data);
        /*
        return $.ajax({
        url: self.serviceLocator.getUrl("admin-addAlarm"),
        data: data,
        type: "PUT",
        dataType: "json"
        });
        */
    },

    /*
     *   Edits an existin alarm
     */
    editAlarm: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-editAlarm");

        var data = {};

        data["idTask"] = params["idTask"];
        data["idAlarm"] = params["idAlarm"];
        data["idRecurrMode"] = params["idRecurrMode"];
        data["idLapseMode"] = params["idLapseMode"];
        data["scheduleType"] = params["scheduleType"];
        data["alarmTime"] = params["alarmTime"];
        data["alarmRecurrTime"] = params["alarmRecurrTime"];
        data["sendToCurrentAssignee"] = params["sendToCurrentAssignee"];


        return $.update(url, data);


    },

    /*
     *   Delete an alarm
     */
    deleteAlarm: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-deleteAlarm");

        var data = {};

        data["idAlarm"] = params["idAlarm"];

        return $.destroy(url, params);

    },

    /*
     * Returns the Alarm Recipients
     * @param param: idAlarm : the alarm id to get the related recipients
     */
    getAlarmRecipients: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-alarmRecipients");

        var data = {};

        data["idAlarm"] = params["idAlarm"];

        return $.read(url, data);
    },

    /*
     * Adds a new Alarm Recipient to the current alam
     * @param param: idAlarm : the alarm id to get the related recipients
     * @param param: idRecipient : the recipient id
     */
    addRecipientToAlarm: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-recipientToAlarm");

        var data = {};

        data["idAlarm"] = params["idAlarm"];
        data["idRecipient"] = params["idRecipient"];

        return $.update(url, data);


    },

    /*
     * Delete/s the alarm recipient/s from the current task
     * @param param: idRecipient : the recipient id/s who is going to be deleted
     */
    deleteRecipientsFromAlarm: function (params) {

        var self = this;
        var url = self.serviceLocator.getUrl("admin-deleteAlarmRecipients");

        var data = {};

        data["idRecipients"] = params["idRecipients"];

        return $.destroy(url, data);
    },

    /*
     * Send the action to toggle the alarms state: enable or disable
     * @param param: idTask : the task id who is going to be actvidated/deactivaded his alarms
     */
    enableAlarm: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-enableAlarm");

        var data = {};

        data["idTask"] = params["idTask"];

        return $.create(url, data);
    },

    /*
     * Invalidate or Reassign a set of cases
     */
    abortReassignItems: function (params) {
        var self = this;
        var data = params || {};
        var url = params.action == "abort" ? "admin-abortItems" : "admin-reassignItems";
        // Call ajax and returns promise

        return $.ajax({
            url: self.serviceLocator.getUrl(url),
            data: data,
            type: "POST",
            dataType: "json"
        });
    },
    /*
     * Services for Asynchronous Activities
     */
    asyncActivitiesServices: function (params) {
        var self = this;
        var data = params;
        var url = "";
        var type = "GET";
        if (params.action == "getActivities") {
            url = self.serviceLocator.getUrl("admin-async-activities-get-activities");
        } else if (params.action == "retryNow") {
            type = "POST";
            url = self.serviceLocator.getUrl("admin-async-activities-get-retry-now");
            url = url.replace("{idCase}", params.idCase);
            url = url.replace("{idworkItem}", params.idWorkitem);
            if (params.idCase == -1) {
                url = url.replace("{idAsynchWorkitem}", params.idAsynchWorkitem);
            } else {
                url = url.replace("{idAsynchWorkitem}", "-1");
            }
        } else if (params.action == "getActivitiesByTask") {
            url = self.serviceLocator.getUrl("admin-async-activities-get-activities-by-task");
        } else if (params.action == "enableExecution") {
            type = "POST";
            url = self.serviceLocator.getUrl("admin-async-activities-enable-execution");
        } else if (params.action == "enableMultiple") {
            url = self.serviceLocator.getUrl("admin-async-activities-enable-multiple");
            type = "POST";
        } else if (params.action == "asyncExecution") {
            url = self.serviceLocator.getUrl("admin-async-activities-async-execution");
        } else if (params.action == "asyncExecutionLog") {
            url = self.serviceLocator.getUrl("admin-async-activities-async-execution-log");

            url = url.replace("{idCase}", params.idCase);
            url = url.replace("{idworkItem}", params.idWorkItem);
            if (params.idCase == -1) {
                url = url.replace("{idAsynchWorkitem}", params.idAsynchWorkitem);
            } else {
                url = url.replace("{idAsynchWorkitem}", "-1");
            }
        } else if (params.action == "getAsyncExecution") {
            url = self.serviceLocator.getUrl("admin-async-activities-async-get-current-execution-log");
        }
        // Call ajax and returns promise
        var response = $.ajax({
            url: url,
            data: data,
            type: type,
            dataType: "json"
        });

        return response;
    },

    /*
     *  Return generate Default Assignation To all Process
     */
    getDefaultAssignationUserToAllProcess: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("admin-getDefaultAssignationUserToAllProcess"), {
            serviceAction: "getDefaultAssignationUserToAllProcess"
        });
    },

    /*
     *  Return generate Default Assignation To Process
     */
    getDefaultAssignationUserToProcess: function (params) {
        var self = this;
        var process;
        // Call ajax and returns promise
        if (params.idWFClass == "") {
            process = -1
        } else {
            process = params.idWfClass
        }
        return $.read(self.serviceLocator.getUrl("admin-getDefaultAssignationUserToProcess"), {
            serviceAction: "getDefaultAssignationUserToProcess",
            process: process

        });
    },

    /*
     * Return Assignation Process
     */
    setDefaultAssignationUserToProcess: function (params) {
        var self = this;
        if (params.idWFClass == "") {
            process = -1
        } else {
            process = params.idWfClass
        }

        // Call ajax and returns promise
        return $.create(self.serviceLocator.getUrl("admin-setDefaultAssignationUserToProcess"), {
            serviceAction: "setDefaultAssignationUserToProcess",
            process: process,
            idUser: params.idUser
        });
    },


    /*
     *   Returns the combo's data to Type Profiles depending of the params
     */
    getProfilesTypes: function (params) {
        var self = this;

        // Call ajax and returns promise
        return $.read(self.serviceLocator.getUrl("admin-getProfilesTypes"));
    },

    /*
     *  Return generate Search Profiles
     */
    searchProfiles: function (params) {
        var self = this;

        var url = self.serviceLocator.getUrl("admin-searchProfiles");
        var data = {};
        data["type"] = params["profileType"];
        data["name"] = params["profileName"];

        data["orgId"] = params["orgId"] ? params["orgId"] : null;

        return $.read(url, data);
    },

    /*
     *  Return Users By Profiles
     */
    getUsersByProfile: function (params) {
        var self = this;

        var url = self.serviceLocator.getUrl("admin-getUsersByProfile");
        var data = {};
        data["type"] = params["profileType"];
        data["id"] = params["idProfile"];

        return $.read(url, data);
    },

    /*
     *  Remove User Profiles
     */
    removeUserFromProfile: function (params) {
        var self = this;

        var url = self.serviceLocator.getUrl("admin-removeUserFromProfile");
        var data = {};
        data["type"] = params["profileType"];
        data["id"] = params["idProfile"];
        data["idUser"] = params["idUser"];

        return $.destroy(url, data);
    },

    /*
     *  Add Users Profiles
     */
    addUserToProfile: function (params) {
        var self = this;

        var url = self.serviceLocator.getUrl("admin-addUserToProfile");
        var data = {};
        data["type"] = params["profileType"];
        data["id"] = params["idProfile"];
        data["idUser"] = params["idUser"];

        return $.update(url, data);
    },
    /*
     *  Display Licenses
     */
    licenses: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-Licenses");
        return $.read(url);
    },

    /*
     *   Return Dimensions List
     */
    getDimensions: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-GetDimensions");
        return $.read(url);
    },

    /*
     *   Edit Dimensions
     */
    editDimension: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["id"] = params.id;
        data["displayName"] = params.displayName;
        data["name"] = params.name;
        data["idWfClass"] = params.idWfClass;
        data["entityPath"] = params.entityPath;
        data["description"] = params.description;
        var url = self.serviceLocator.getUrl("admin-EditDimension");
        // Call ajax and returns promise
        return $.create(url, data);
        /*return $.ajax({
        url: self.serviceLocator.getUrl("admin-EditDimension"),
        data: params,
        type: "POST",
        dataType: "json"
        });*/
    },

    /*
     *   Create Dimensions
     */
    createAdministrableDimension: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["displayName"] = params.displayName;
        data["name"] = params.name;
        data["idWfClass"] = params.idWfClass;
        data["entityPath"] = params.entityPath;
        data["Description"] = params.description;
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-CreateAdministrableDimension"),
            data: params,
            type: "PUT",
            dataType: "json"
        });
    },

    /*
     * Delete manageable dimensions
     */
    deleteDimension: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-DeleteDimension");
        // Define data
        params = params || {};
        var data = {};
        data["id"] = params.id;
        data["idDimension"] = params.id;
        data["administrable"] = params.administrable;
        // Call ajax and returns promise
        return $.destroy(url, data);
    },

    /*
     *   Dimensions Process Tree
     */
    entityPathChildNodesAction: function (params) {
        var self = this;

        // Define data
        params = params || {};
        var data = {};
        data["pathNodeType"] = params.nodeType;
        data["idNode"] = params.idNode;
        data["nodeDisplayPath"] = params.nodeDisplayPath;
        data["nodePath"] = params.nodePath;
        data["idWfClass"] = params.idWfClass;
        // Call ajax and returns promise
        return $.ajax({
            url: self.serviceLocator.getUrl("admin-EntityPathChildNodesAction"),
            data: params,
            type: "GET",
            dataType: "json"
        });
    },

    /*
     *   Return list of available processes
     */
    getActiveWFClasses: function (params) {
        var self = this;
        var data = params || {};

        // Call ajax and returns promise

        return $.ajax({
            //eliminar esto con el servicio restfull
            cache: true,
            url: self.serviceLocator.getUrl("admin-GetActiveWFClasses"),
            data: data,
            type: "GET",
            dataType: "json"
        });
    },

    /*
     *   Resturn the list of Stored Document Templates
     */
    storeDocumentTemplates: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-document-templates-storeDocumentTemplates");
        return $.read(url);
    },

    /*
     * Restore an selected document template using his Guid as parameter
     * @param params {Object} Guid: the associated Guid from the desired document template
     */
    restoreDocumentTemplates: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-document-templates-restoreDocumentTemplates");

        url += "?Guid=" + params.Guid;

        return $.read(url);
    },

    /*
     * Retreieves the workflow from the actual processes
     */
    getWorkFlowClasses: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-processes-workflowClasses");
        return $.read(url);
    },


    /*
     * Retreives the task from a desired workflow
     * @param params {Object} idWorkFlow: the desired workflow id
     */
    getTaskByWorkFlow: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-processes-tasksByWorkflow");

        var data = {};
        data["idWorkflow"] = params.idWorkflow;


        return $.read(url, data);
    },

    /*
     * Modifies the process durationworkflow
     * @param params {Object} idWorkflow: the desired process id to modify
     * @param params {Object} duration: the duracion, converted in minutes
     */
    modifyProcessDuration: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-processes-modifyProcessDuration");

        var data = {};
        data["idWorkflow"] = params.idWorkflow;
        data["duration"] = params.duration;

        return $.create(url, data);
    },

    /*
     * Modifies the task duration form a desired workflow
     * @param params {Object} idTask: the desired task id to modifie
     * @param params {Object} duration: the duracion, converted in minutes
     */
    modifyTaskDuration: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-processes-modifyTaskDuration");

        var data = {};
        data["idTask"] = params.idTask;
        data["duration"] = params.duration;

        return $.create(url, data);
    },

    /*
     * Retreives the process  from Hierarchy
     *
     */
    processesHierarchy: function () {
        var self = this;
        var data = {};
        data["removeOnlineItems"] = "true";
        return $.ajax({
            url: self.serviceLocator.getUrl("offline-getProcessTree"),
            data: data,
            dataType: "json",
            type: "GET"
        });


    },
    /*
     * this function send to the server the offline cases created
     */

    syncOfflineCases: function (params) {
        var self = this;

        return $.ajax({
            url: self.serviceLocator.getUrl("offline-sendForm"),
            data: {
                idCase: params.idCase,
                idWFClass: params.idWfClass,
                awCaseCreationContext: JSON.stringify(params.objToSend),
                idWorkflow: params.idWfClass
            },
            type: "POST",
            dataType: "json",
            serviceType: "LOAD"
        });
        /*.done(function (rta) {
        console.info(rta);
        var formData = decodeURIComponent(this.data).split('&');
        for (var i = formData.length - 1; i >= 0; i--) {
        var pivot = formData[i].split("=");
        if (pivot[0] == "idCase") {
        self.database.deleteCase({ idCase: pivot[1] });
        }
        };
        //cuando retorne que yes elimina el caso de la base de datos

        }).fail(function (fail) {
        console.info(fail);

        })*/
    },
    /*
     * Retreives Form structure + Form data
     *
     */
    processesHierarchyTofetchForms: function (params) {
        var self = this;
        var data = {};
        data["idChangeSet"] = params.changeSet;
        //        data["h_contexttype"] = "metadata";
        //        data["h_idprocess"] = idCat;

        //        return $.ajax({
        //            url: self.serviceLocator.getUrl("offline-getForms"),
        //            data: data,
        //            idprocess: idCat,
        //            type: "POST",
        //            dataType: "json",
        //            serviceType: "LOAD"
        //        });

        return $.ajax({
            url: self.serviceLocator.getUrl("offline-getForms"),
            data: data,
            dataType: "json"
        });

    },

    /*
     * this retrive data for the report my team
     */
    getDataForMyTeam: function () {

        var self = this;

        return $.ajax({
            url: self.serviceLocator.getUrl("bam-resourcemonitor-myteam"),
            type: "GET",
            dataType: "json"
        });
    },

    /*
     * Retrive data for reports
     */
    getReporstAnalysisQuery: function () {

        var self = this;

        return $.read(self.serviceLocator.getUrl("reports-analysisquery"));
    },

    /*
     * Update report data
     */
    updateReportData: function (params) {

        var self = this;

        return $.update(self.serviceLocator.getUrl("reports-analysisquery-update"), params);
    },

    /*
     * Delete report data
     */
    deleteReportData: function (params) {

        var self = this;

        return $.destroy(self.serviceLocator.getUrl("reports-analysisquery-delete") + "?" + params);
    },

    /*
     *Entrega la lista de entidades administrables
     */
    getAdminEntitiesList: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-entities-list");
        var data = {};
        return $.read(url, data);
    },

    /*
     *Entrega los registros de una entidad
     */
    getAdminEntitiesRowData: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-entities-row-data");
        var data = {};
        data["idEntity"] = params.idEntity;
        data["pag"] = params.pag || 1;
        data["pagSize"] = params.pagSize;
        if (typeof params.orderField !== "undefined") {
            data["orderField"] = params.orderField;
        }
        if (typeof params.orderType !== "undefined") {
            data["orderType"] = params.orderType;
        }
        return $.read(url, data);
    },

    /*
     *verify if entity was migrated return boolean value
     */
    getAdminEntityMigrated: function (guidEntity) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-entities-migrated-entity");
        var data = {};
        data["guidEntity"] = guidEntity;
        return $.read(url, data);
    },

    getAdminEntitiesForm: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-entities-get-form");
        var data = {};
        data["h_action"] = "LOADENTITYFORM";
        data["h_contexttype"] = "entity";


        if (typeof params.guid !== "undefined") {
            data["h_guidEntity"] = params.guid;
        }

        if (typeof params.guidForm !== "undefined") {
            data["h_guidForm"] = params.guidForm;
        }
        if (typeof params.idRow !== "undefined") {
            data["h_surrogateKey"] = params.idRow;
        }

        if (typeof params.idPageCache !== "undefined") {
            data["h_pageCacheId"] = params.idPageCache;
            data["h_isRefresh"] = true;
        }



        return $.ajax({
            url: url,
            data: data,
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *Entrega la lista de idiomas soportados
     */
    getBizagiObjects: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-language-bizagi-objects");
        var data = {};
        return $.read(url, data);
    },

    /*
     *Entrega la lista de entidades
     */
    getEntitiesList: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-language-entities");
        var data = {};
        return $.read(url, data);
    },


    /*
     *Entrega la lista de idiomas soportados
     */
    getStoreLanguageTemplates: function () {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-language-languages");
        var data = {};
        return $.read(url, data);
    },

    /*
     *Enable or disable languages
     */
    setLanguages: function (dataArray) {
        var self = this;
        var url = self.serviceLocator.getUrl("admin-language-languages");
        var data = dataArray;
        return $.ajax({
            url: url,
            data: {
                languages: data
            },
            type: "POST",
            dataType: "json"
        });
    },

    /*
     *Reset language personalization
     */

    resetPersonalization: function () {
        var self = this;

        return $.ajax({
            url: self.serviceLocator.getUrl("admin-language-reset"),
            data: {},
            type: "DELETE",
            dataType: "json"
        });
    },

    /*
     *Download template language list
     */
    getLanguageTemplate: function (params) {
        var self = this,
            urlEndPoint,
            formExcel;
        var parameter = "";
        //create temporal form
        if (params.elements) {
            urlEndPoint = "admin-language-resource-download";
            parameter = "&elements=" + params.elements;
        } else if (params.entities) {
            urlEndPoint = "admin-language-entities-download";
            parameter = "&entities=" + params.entities;
        } else {
            urlEndPoint = "admin-language-resource-download";
        }

        var winview = (self.device === "tablet_android") ? "_system" : "_self";
        var url = encodeURI(self.serviceLocator.getUrl(urlEndPoint) + "?cultureName=" + params.cultureName + parameter);

        window.open(url, winview, 'location=yes');
    },

    getAuditLicense: function (params) {
        var self = this;
        var defer = $.Deferred();
        var url = self.serviceLocator.getUrl("admin-audit-license");
        defer.resolve("OK");
        document.location = url;

        return defer.promise();
    },
    /*
     * Process definition service for process viewer
     */
    processDefinition: function (params) {
        var self = this;
        return $.read(self.serviceLocator.getUrl("processviewer-processdefinition"), params);
    },

    /*
     * Graphic info for process viewer
     */
    graphicInfo: function (params) {
        var self = this;
        return $.read(self.serviceLocator.getUrl("processviewer-processgraphicinfo"), params);
    },

    getQueryFormResponse: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler-getQueryFormResponse"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    getQueryFormExportExcel: function (params) {
        var self = this;
        var defer = $.Deferred();
        var paramsSend = "";
        var target = "_self";
        if (bizagi.util.isIE()) {
            target = "iframeExcel";
        }
        //create temporal form
        var formExcel = $("<form>", {
            "action": self.serviceLocator.getUrl("query-handler-getQueryFormExportExcel"),
            "target": target,
            "id": "formExportExcel",
            "method": "POST",
            "style": "display:none"
        });
        $(formExcel).attr("enctype","application/x-www-form-urlencoded");
        $(formExcel).attr("accept-charset","UTF-8");

        var iframeExcel = $("<iframe>", {
            "name": "iframeExcel",
            "id": "iframeExcel",
            "style": "display:none"
        });
        //add parameters
        $.each(params, function (k, v) {
            paramsSend = $("<input>", {
                "name": k,
                "value": v,
                "type": "hidden"
            });
            $(formExcel).append(paramsSend);
        });
        $("body").append(formExcel);
        $("body").append(iframeExcel);
        //submit and remove form
        formExcel.submit().remove();
        iframeExcel.submit().remove();
        defer.resolve("OK");
        return defer.promise();
    },

    getQueryForm: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler-getQueryForm"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    getPreferencesForm: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("preferences-handler-getPreferencesForm"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },

    saveStoredQueryForm: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler-storedQueryForm"),
            data: params,
            type: "POST",
            dataType: "json"
        });
    },
    updateStoredQueryForm: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler-storedQueryForm"),
            data: params,
            type: "PUT",
            dataType: "json"
        });
    },

    deleteStoredQueryForm: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("query-handler-storedQueryForm-id");
        return $.destroy(url, params);
    },

    getStoredQueryFormList: function (params) {
        var self = this;
        return $.ajax({
            url: self.serviceLocator.getUrl("query-handler-storedQueryForm"),
            data: params,
            type: "GET",
            dataType: "json"
        });
    },

    getStoredQueryFormResponse: function (params) {
        var self = this;
        var url = self.serviceLocator.getUrl("query-handler-storedQueryForm-id");
        var data = {};
        data["idStoredQueryForm"] = params;
        return $.read(url, data);
    },
    /*
     * Graphic Query Info
     */
    getGraphicQueryInfo: function (params) {
        var self = this;
        return $.read("Rest/Cases/Workitems", params);
    },

    /*
     * Get case path
     */
    getCasePath: function (params) {
        var self = this;
        return $.read("Rest/Cases/TransitionLog", params);
    },

    /*
     * Get Users Creation/Edition Form
     */
    getUsersForm: function (params) {

        var self = this;

        return $.create(self.serviceLocator.getUrl("admin-usersform"), params);
    },

    /**
     * This service get the last version of BizagiBPM
     *
     * @return {json}
     */
    getLastUpdateByMobile: function () {
        var self = this;
        return $.read({
            url: self.serviceLocator.getUrl("mobile-getLastUpdate")
        });
    },

    /*
     * Users Administration Log
     */
    getUsersAdministrationLog: function (params) {
        var self = this;
        return $.read({
            url: self.serviceLocator.getUrl("admin-userslog"),
            data: params
        });
    },

    /*
     * Query Users Licenses
     */
    queryUsersLicenses: function () {
        var self = this;
        return $.read({
            url: self.serviceLocator.getUrl("admin-userslicenses")
        });
    },

    /*
     * Create User Form
     */
    createUserAdministrationForm: function () {
        var self = this;

        return $.get(self.serviceLocator.getUrl("admin-createuserform"));
    },

    /*
     * Get User Preference Form Params
     */
    getPreferenceFormParams: function () {

        var self = this;

        return $.read(self.serviceLocator.getUrl("admin-userpreferenceform-isnew"));
    },

    /*
    * Get User Positions
    */
    getUserPositions: function () {

        var self = this;

        return $.read(self.serviceLocator.getUrl("admin-userGetPositions"));
    },

    /*
    * Get user info by task
    */
    getUserInfoByTask: function(idCase, guid) {

        var self = this;
        var params = {
            idCase: idCase,
            guidTask: guid
        };

        return $.read(self.serviceLocator.getUrl("graphicquery-trailusers"), params);
    }

});