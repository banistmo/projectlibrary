
/*
*   Name: BizAgi Workportal
*   Author: Diego Parra
*   Comments:
*   -   This script will process a workportal page using a device factory to use the adequate rendering
*       to create the layout
*/

// Define BizAgi Workportal namespace
bizagi.workportal = bizagi.workportal || {};

// Define global variables
bizagi.workportal.currentInboxView = "inbox";

// Define state variables
bizagi.workportal.state = bizagi.workportal.state || {};

        
/*
*   Renders the workportal
*/
$.Class.extend("bizagi.workportal.facade", {}, {

    /* 
    *   Constructor
    */
    init: function (params) {

        // Defines a device factory for all rendering
        this.deviceFactory = new bizagi.workportal.device.factory(this);

        // Creates a data service instance
        this.dataService = new bizagi.workportal.services.service(params);

        // Create instance of routing service
        this.dataService.routing = new bizagi.workportal.services.routing({ dataService: this.dataService });

        // Set default params
        this.defaultParams = params || {};

        // Set default params
        this.timeoutID;
    },

    /*
    *   Loads the default widget when executing the workportal
    */
    loadDefaultWidget: function () {
        if (!this.defaultWidget && !bizagi.util.isEmpty(window.location.hash)) {
            // Check in the hash
            var hashParams = bizagi.util.getHashParams();
            if (hashParams[0] != "/")
                this.defaultWidget = hashParams[0];
        }
        if (!this.defaultWidget && !bizagi.util.isEmpty(window.location.search)) {
            // Check in the query string
            var queryString = bizagi.util.getQueryString();
            this.defaultWidget = queryString["widget"];
        }
        if (!this.defaultWidget && !bizagi.util.isEmpty(this.defaultParams.widget)) {
            // Check in the default params
            this.defaultWidget = this.defaultParams.widget;
        }
        if (!this.defaultWidget && typeof (BIZAGI_DEFAULT_WIDGET) != "undefined" && !bizagi.util.isEmpty(BIZAGI_DEFAULT_WIDGET)) {
            // Check with overriding variable in html
            this.defaultWidget = BIZAGI_DEFAULT_WIDGET;
        }
        if (!this.defaultWidget && bizagi.cookie("bizagiDefaultWidget")) {
            // Check with cookie
            this.defaultWidget = bizagi.cookie("bizagiDefaultWidget");
        }
        if (!this.defaultWidget) {
            // Set a default widget based on the current device
            this.defaultWidget = (bizagi.util.detectDevice() == "smartphone_ios" || bizagi.util.detectDevice() == "smartphone_android") ? bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_INBOX : bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_INBOX_GRID;

        }

        // Save in cookie for next executions
        bizagi.cookie("bizagiDefaultWidget", this.defaultWidget, { expires: 30 });
    },

    /*
    *   Start point method to use in the main javascript pages
    *   This method will process everything and attach the html directly to the dom
    */
    execute: function (canvas) {
        var self = this;
        var doc = this.ownerDocument;
        var body = $("body", doc);
        canvas = canvas || $("<div/>").appendTo(body);

        // Loads the default Widget
        self.loadDefaultWidget();

        // Creates ready deferred
        self.executionDeferred = new $.Deferred();
        return self.process().done(function (content) {

            // Replace canvas with content
            bizagi.util.replaceSelector(body, canvas, content);

            self.executionDeferred.resolve();

        });
    },

    /*
    *   Creates a webpart inside a canvas but don't execute it
    */
    createWebpart: function (params) {
        return this.executeWebpart($.extend(params, {
            creating: true
        }));
    },
    /*
    * Verifies that the webpart configuration parameters are ok, before processing the webpart
    */
    testWebpartConfiguration: function (params) {

        var self = this;
        var webpartConfiguration = params.webpartConfiguration
        var doc = this.ownerDocument;
        var body = $("body", doc);
        var canvas = params.canvas || $("<div/>").appendTo(body);
        if (!webpartConfiguration.url || webpartConfiguration.url == "") {
            bizagi.webparts.addConfigurationMessage(params, "bizagi-sharepoint-configuration-error");
            return false;
        }
        return true;
    },
    /*
    *   Executes a webpart inside a canvas
    */
    executeWebpart: function (params) {
        var self = this;
        var doc = this.ownerDocument;
        var body = $("body", doc);

        // Creates ready deferred
        var canvas = params.canvas || $("<div/>").appendTo(body);
        var processWebpart = true;
        if (params.webpartConfiguration) {
            processWebpart = self.testWebpartConfiguration(params);
        }
        if (processWebpart) {
            // Process the webpart asynchonous
            return self.processWebpart(params).done(function (result) {

                // Append content to canvas
                canvas.append(result.content);
            });
        }
    },


    /*
    *   Returns the execution deferred to determine if the component is ready or not
    */
    ready: function () {

        return this.executionDeferred.promise();
    },

    /*
    *   Loads a workportal facade to delegate rendering based on device detection
    *   Returns a deferred to set callbacks when the process is done
    */
    process: function () {
        var self = this;
        var defer = new $.Deferred();

        // Create a workportal facade
        var facade = this.deviceFactory.getWorkportalFacade(self.dataService);
        //process offline data 
        self.processOfflineData();

        // Set callback when requests have been done
        $.when(facade)
        .pipe(function (workportalFacade) {
            // Creates workportal component
            return self.processWorkportal(workportalFacade);

        }).done(function (content) {
            // Resolve deferred
            defer.resolve(content);
        });

        return defer.promise();
    },

    processOfflineData: function () {
        var self = this;

        if (typeof self.dataService.fetchOfflineData != "undefined" && self.dataService.online == true) {
            $.when(self.dataService.pushOfflineData()).always(function () {
                self.dataService.fetchOfflineData();
            });
        }

    },

    /*
    *   Process a webpart
    */
    processWebpart: function (params) {
        var self = this;
        var defer = new $.Deferred();
        var webpartController;

        // Create a workportal facade
        var facade = this.deviceFactory.getWorkportalFacade(self.dataService, false); /* Do not initialize templates*/

        // Combine workportal params with call params
        $.extend(params, this.defaultParams);

        // Set callback when requests have been done
        $.when(facade)
        .pipe(function (workportalFacade) {

            var deferTemplates = new $.Deferred();

            // Load webpart templates
            $.when(workportalFacade.loadWebpart(params))
            .done(function () {
                deferTemplates.resolve(workportalFacade);
            });

            return deferTemplates.promise();
        })
        .pipe(function (workportalFacade) {
            webpartController = workportalFacade.getWebpart(params.webpart, params);

            // Render the full content
            return webpartController.render(params);
        }).done(function (content) {


            // Resolve deferred 
            defer.resolve({ webpart: webpartController, content: content });
        });
        return defer.promise();
    },


    /*
    *   Process the workportal layout 
    */
    processWorkportal: function (workportalFacade) {
        var self = this;
        var controller = this.mainController = workportalFacade.getMainController();
        var defer = new $.Deferred();
        // Render the full content
        return controller.render().pipe(function (result) {
            controller.setWidget($.extend(self.defaultParams, {
                widgetName: self.defaultWidget
            }));


            // Perform resize 
            if (controller.menu) {
                controller.menu.performResizeLayout();
            }

            return result;
        });



    },


    /*
    *   Returns the main controller
    */
    getMainController: function () {
        return this.mainController;
    }

});
