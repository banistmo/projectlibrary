/**
 * Define path of REST services
 */

$.Class.extend("bizagi.render.services.context", {}, {
    
    /**
     *  @argument {object} params Configurations params
     */
    init: function(params) {
        params = params || {};
        var proxyPrefix = params.proxyPrefix || "";
        this.endPoint = {            
            generalAuthentication: proxyPrefix + "Rest/Authentication",
            authenticationUser: proxyPrefix + "Rest/Authentication/User",
            quickLoginUsers: proxyPrefix + "Rest/Authentication/Users",
            authenticationConfig: proxyPrefix + "Rest/Authentication/BizagiConfig",
            authenticationDomains: proxyPrefix + "Rest/Authentication/Domains",
            forgotPassword: proxyPrefix + "Rest/Authentication/ForgottenPassword",
            secretQuestion: proxyPrefix + "Rest/Authentication/SecretQuestion",
            unlockAccount: proxyPrefix + "Rest/Authentication/Unlock",
            changePassword: proxyPrefix + "Rest/Authentication/ChangePassword",
            readUserCookies: proxyPrefix + "Rest/Authentication/ReadUserCookies",
            userHandlerGetCurrentUser : proxyPrefix+ "Rest/Users/CurrentUser",
            userSamlSession: proxyPrefix + "Rest/Users/SamlSession"       
        };
    },
    /**
     * This method will return the url defined as endPoint using the parameter "endPoint" name
     */
    getUrl: function(endpoint) {
        var self = this;
        var url = self.endPoint[endpoint] || "";
        return url;
    }
});
