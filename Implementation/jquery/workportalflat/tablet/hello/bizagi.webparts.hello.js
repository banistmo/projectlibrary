/**
 *   Name: BizAgi Test
 *   Author: Oscar Osorio
 *   *   Comments:
 *   -   This script will define a hello world test
 */
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.hello", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();
        var sharedVariables = {
            header: 'Prueba1 header',
            header2: 'Header2 test'
        };
        var template = kendo.template(self.getTemplate('hello-tmpl'), { useWithBlock: false });
        params.numberOfFields = 3;
        $.when(self.dataService.getCasesList(params)).done(function (list) {
            self.content = template($.extend({}, sharedVariables, list));
            bizagi.util.smartphone.stopLoading();
            deferred.resolve(self.content);
        });
        return deferred.promise();
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;
        self.getContent().kendoMobileView();
        self.configureHandlers();
        self.canvas.one('ondomincluded', function () {
            self.showView();
        });
    },
    /**
    custom method for webpart
    */
    configureHandlers: function () {
        var self = this;
        $('.km-list > li', self.getContent()).bind('click', function (e) {
            //self.publish('inbox-case-click', {idCase : this.id});
            //console.log('You click one item');
        });
    },
    /**
    custom method for webparts
    */
    showView: function () {
        bizagi.kendoMobileApplication.bizagiNavigate("hello");
    }
});
