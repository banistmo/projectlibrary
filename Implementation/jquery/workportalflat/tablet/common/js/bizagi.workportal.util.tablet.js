bizagi = (typeof (bizagi) !== 'undefined') ? bizagi : {};
bizagi.util = (typeof (bizagi.util) !== 'undefined') ? bizagi.util : {};

bizagi.environment = typeof (BIZAGI_ENVIRONMENT) !== "undefined" ? BIZAGI_ENVIRONMENT : (queryString["environment"] || "debug");

bizagi.log = function (params) {
    console.log(params);

};

//Change to common


bizagi.chrono = {};

bizagi.chrono.init = function (timer) {
    bizagi.enableChrono = true;
    if (!timer) bizagi.chronos = {};
    else bizagi.chronos[timer] = { millis: 0, timestamp: 0 };
};

bizagi.chrono.start = function (timer, profile) {
    if (!bizagi.enableChrono) return;
    profile = profile !== undefined ? profile : false;
    bizagi.chronos[timer] = bizagi.chronos[timer] || { millis: 0, timestamp: 0 };

    // If the timer is already running don't do anything
    if (bizagi.chronos[timer].running) return;

    // Start timers
    bizagi.chronos[timer].timestamp = new Date().getTime();
    bizagi.chronos[timer].profiling = (bizagi.enableProfiler && profile);
    bizagi.chronos[timer].running = true;

    // Use chrome profiling
    if (bizagi.enableProfiler && profile) {
        console.time(timer);
        console.timeStamp(timer);
        console.profile(timer);
    }
};

bizagi.chrono.stop = function (timer) {
    if (!bizagi.enableChrono) return;
    bizagi.chronos[timer] = bizagi.chronos[timer] || { millis: 0, timestamp: 0 };

    // If the timer is not running don't do anything
    if (!bizagi.chronos[timer].running) return;

    // Stop the timer
    var diff = new Date().getTime() - bizagi.chronos[timer].timestamp;
    bizagi.chronos[timer].millis = bizagi.chronos[timer].millis + diff;
    bizagi.chronos[timer].running = false;

    // Use chrome profiling
    if (bizagi.chronos[timer].profiling) {
        console.timeEnd(timer);
        console.timeStamp(timer + 'end');
        console.profileEnd(timer);
    }
};


bizagi.chrono.initAndStart = function (timer) {
    bizagi.chrono.init(timer);
    bizagi.chrono.start(timer);
};

bizagi.chrono.stopAndLog = function (timer) {
    // If the timer is not running don't do anything
    if (bizagi.chronos[timer] && bizagi.chronos[timer].running) bizagi.chrono.stop(timer);
    bizagi.chrono.log(timer);
};

bizagi.chrono.log = function (timer) {
    if (!bizagi.chronos[timer]) return;
    console.info('Timer ' + timer + ': ' + bizagi.chronos[timer].millis + 'ms');
    $('#debug-panel .content').append('<div>' + 'Timer ' + timer + ': ' + bizagi.chronos[timer].millis + 'ms' + '</div>');
};

bizagi.chrono.logTimers = function () {
    if (!bizagi.enableChrono) return;
    for (key in bizagi.chronos) {
        bizagi.chrono.log(key);
    }
};
bizagi.chrono.init();


bizagi.debug = function (params) {
    console.log('debug', params);
};


/*
*   Creates an error message
*/
bizagi.logError = function (message, data) {
    console.error(message, typeof (data) === 'object' ? JSON.stringify(data) : data);
};



$.fn.bizagi_notifications = function (params) {
    //this.css( 'color', 'green' );
};


/*
*   get time ago
*
bizagi.getTimeAgo = function (commentTime) {
    return $.timeago(new Date(commentTime));
};

/*
*   Data grouped
*
bizagi.getGroupedData = function (date) {
    var groupedData = {};

    var actualDate = new Date();
    var actualYear = actualDate.getFullYear();
    var actualMonth = actualDate.getMonth();
    var actualDay = actualDate.getDate();

    var tmpDate = new Date(date);
    var tmpMonth = tmpDate.getMonth();
    var tmpYear = tmpDate.getFullYear();
    var tmpDay = tmpDate.getDate();

    var dateDiff = ((new Date(tmpYear, tmpMonth, tmpDay, 0, 0, 0) - new Date(actualYear, actualMonth, actualDay, 0, 0, 0)));

    if (dateDiff < 0) {
        groupedData.date = bizagi.util.dateFormatter.formatDate(new Date(tmpDate), 'dd MMM');
        groupedData.type = 'overdue';
    } else {
        if (actualDay == tmpDay) {
            groupedData.date = bizagi.util.dateFormatter.formatDate(new Date(tmpDate), 'hh:mm');
            groupedData.type = 'today';

        } else if ((actualDay + 1) == tmpDay) {
            groupedData.date = bizagi.util.dateFormatter.formatDate(new Date(tmpDate), 'hh:mm');
            groupedData.type = 'tomorrow';

        } else {
            groupedData.date = bizagi.util.dateFormatter.formatDate(new Date(tmpDate), 'dd MMM');
            groupedData.type = 'upcoming';
        }
    }

    return groupedData;
};

*/