﻿/*
 *   Name: BizAgi Smartphone Workportal Facade
 *   Author: oscaro
 *   Comments:
 *   -   This script will define a workportal facade to access to all components
 */


$.Class.extend("bizagi.workportal.tablet.facade",
    {
        /*
         *   Returns the implementation class by widget
         */
        getWidgetImplementation: function (widget) {
            bizagi.log("getWidgetImplementation" + widget);
        }
    },
    {
        /*
         *   Constructor
         */
        init: function (dataService) {
            this.templates = {};
            this.dataService = dataService;
            this.setIPhoneMetaTags();

        },

        /* SETS iPhone META TAGS
         =====================================================*/
        setIPhoneMetaTags: function () {
            $('<meta>', {
                name: "apple-mobile-web-app-capable",
                content: "yes"
            }).appendTo('head');

            $('<meta>', {
                name: "viewport",
                content: "width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=no"
            }).appendTo('head');

            $('<link>', {
                rel: 'apple-touch-icon-precomposed',
                href: 'jquery/common/base/css/tablet/images/BizAgi_logo.png'
            }).appendTo('head');

            $('<link>', {
                rel: 'apple-touch-startup-image',
                href: 'jquery/common/base/css/tablet/images/splash.png'
            }).appendTo('head');
        },

        /*
         *   This function will load asynchronous stuff needed in the module
         */
        initAsyncStuff: function () {
            var self = this;
            // Load default templates
            // return $.when(self.loadTemplates


            return $.when(self.loadTemplate("base", bizagi.getTemplate("base.workportal.tablet"))).done(function () {
                $("body").append(self.getTemplate("base"));
                bizagi.util.smartphone.startkendo();
            });
        },

        /*
         *   Load one template and save it internally
         */
        loadTemplate: function (template, templateDestination) {
            var self = this;
            // Go fetch the template
            return bizagi.templateService.getTemplate(templateDestination, template)
                .done(function (resolvedTemplate) {
                    if (typeof resolvedTemplate === "string") {
                        self.templates[template] = $.trim(resolvedTemplate.replace(/\n/g, ""));
                    }
                });
        },

        /*
         *   Load one template and save it internally
         */
        loadTemplateWebpart: function (template, templateDestination) {
            var self = this;

            // Go fetch the template
            return bizagi.templateService.getTemplateWebpart(templateDestination, template)
                .done(function (resolvedRemplate) {
                    self.templates[template] = $.trim(resolvedRemplate.replace(/\n/g, ""));
                });
        },

        /*
         *   Method to fetch templates from a private dictionary
         */
        getTemplate: function (template) {
            var self = this;
            return self.templates[template];
        },
        /*
         *   Initializes a webpart
         */
        loadWebpart: function (params) {
            var self = this;
            var defer = new $.Deferred();
            var webpartName = params.webpart;
            var webpart = bizagi.getWebpart(webpartName, params); //self.getWebpart(webpartName, params);
            if (!webpart) {
                bizagi.log("webpart not found");
            }

            // Ensure the webpart is initialized
            $.when(bizagi.util.initWebpart(webpart))
                .done(function () {
                    // Load all templates asyncronously
                    $.when.apply(this, $.map(webpart.tmpl, function (tmpl) {
                        return self.loadTemplateWebpart(tmpl.originalAlias, bizagi.getTemplate(tmpl.alias, true));
                    })).done(function () {
                        defer.resolve(webpart);
                    });

                });
            return defer.promise();
        },

        /*
         *   Returns a webpart
         */
        getWebpart: function (webpartImplementation, params) {
            try {
                // Create a dynamic function to avoid problem with eval calls when minifying the code
                if (webpartImplementation.indexOf("bizagi") == "-1") {
                    webpartImplementation = "bizagi.workportal.webparts." + webpartImplementation

                }
                var dynamicFunction = "var baDynamicFn = function(facade, dataService, params){ \r\n";
                dynamicFunction += "return new " + webpartImplementation + "(facade, dataService, params);\r\n";
                dynamicFunction += "}\r\n";
                dynamicFunction += "baDynamicFn";
                dynamicFunction = eval(dynamicFunction);
                // Call the dynamic function
                return dynamicFunction(this, this.dataService, params);
            } catch (e) {
                bizagi.log(e);
            }
        },
        /*
         * call the render method for webparts and insert into canvas
         */
        executeWebpart: function (params) {
            var self = this;
            var defer = new $.Deferred();
            var doc = this.ownerDocument;

            $.when(self.loadWebpart(params)).done(function (webpart) {
                var webpartImplementation = webpart["class"];
                dynamic = self.getWebpart(webpartImplementation, params);

                $.when(dynamic.render({ creating: false })).done(function (result) {

                    // result.name = result.webpart.Class.fullName;
                    //bizagi.webparts.instances.push(result);

                    params.canvas.append(result);
                    params.canvas.triggerHandler("ondomincluded");
                    defer.resolve(dynamic);

                    /*

                     result.name = result.webpart.Class.fullName;
                     bizagi.webparts.instances.push(result);
                     // Append content to canvas
                     canvas.append(result.content);
                     canvas.triggerHandler("ondomincluded");

                     */

                });
            });
            return defer.promise();
        },

        executeWebparts: function (params) {
            bizagi.chrono.initAndStart("loadWebparts", true);
            var self = this;
            return $.when(
                self.executeWebpart({
                    webpart: "hello",
                    canvas: $("body")
                })
              
            ).then(function () {
                    console.log("Finalizo ejecución de webparts");
                    bizagi.chrono.stopAndLog("loadWebparts");
                    $("body").triggerHandler("load-modules");
                    window.addEventListener("orientationchange", function () {//desbindiar
                        if (bizagi.util.isIphoneHigherIOS5()) {
                                $('body').removeAttr('style');
                        }
                    }, false);
                });

        }
    });