var BIZAGI_RESPONSE_TIME = 0;
var BIZAGI_MOCKS_PATH = "jquery/workportalflat/testsuite/tablet/data/";

bizagi.loader.loadFile(
     { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false })
    .then(function () {

        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
            }

            if (settings.url.indexOf("Rest/Users/CurrentUser") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.currentUser.txt"
                };
            }
            if (settings.url.indexOf("Rest/Authorization/MenuAuthorization") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.authorization.txt"
                };
            }
            if (settings.url.indexOf("Rest/Inbox/Summary") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.render.summary.txt"
                };
            }
            if (settings.url.indexOf("Rest/Util/Version") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.render.version.txt"
                };
            }
            if (settings.url.indexOf("Rest/Processes") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.render.processes.txt"
                };
            }
            if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.render.handler.txt"
                };
            }
            if ((/Rest\/Cases\/\d+\/Summary/).test(settings.url)) {
                return {
                    mockjson: BIZAGI_MOCKS_PATH + "dummy.rendering.caseSummary.txt"
                };
            }
        });
    });