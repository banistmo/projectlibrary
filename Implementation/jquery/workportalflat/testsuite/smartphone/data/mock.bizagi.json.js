
bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
).then(function() {

    var fullDate = new Date();
    var tmpDay = fullDate.getDate();
    var tmpMonth = fullDate.getMonth()+1;
    var tmpYear = fullDate.getFullYear();
    $.mockJSON.data.CURRENT_DATE = [
            tmpMonth+'/'+tmpDay+'/'+tmpYear
    ];

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate()+1);
    $.mockJSON.data.TOMORROW_DATE = [
            (tomorrow.getMonth()+1)+'/'+tomorrow.getDate()+'/'+tomorrow.getFullYear()
    ];

    var nextMonth = new Date();
    nextMonth.setMonth(nextMonth.getMonth()+1);
    $.mockJSON.data.NEXT_MONTH_DATE = [
            (nextMonth.getMonth()+1)+'/'+nextMonth.getDate()+'/'+nextMonth.getFullYear()
    ];

    //$.mockjaxSettings.responseTime = 5000; //4000; //10; //2000;
    // DUMMIES
    $.mockjax(function(settings) {
        if (settings.dataType == "text") {
            if (!BIZAGI_ENABLE_MOCKS) return;
            if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                return "false";
            }
        }

        if (settings.dataType == "json") {
            if (!BIZAGI_ENABLE_MOCKS) return;
            if (settings.url.indexOf("Rest/Processes/SearchCases") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/taskFeed/test/data/dummy.txt"
                    ,transform: function(response, originalSettings) {
                        var elements = response.elements.sort(function(a, b) {
                            var dateA = new Date(a[3]), dateB = new Date(b[3]);
                            if (dateA < dateB) // Sort string ascending
                                return -1;
                            if (dateA > dateB)
                                return 1;
                            return 0; // Default return value (no sorting)
                        });

                        var iStart = (originalSettings.data.page * originalSettings.data.pageSize) - originalSettings.data.pageSize;
                        var iEnd = (originalSettings.data.page * originalSettings.data.pageSize);

                        response.page = originalSettings.data.page;

                        response.totalPages = Math.ceil(response.elements.length / originalSettings.data.pageSize);

                        var dateMock = new Date(2014, 04, 22, 0, 0, 0, 0);
                        var tmpDate = new Date();
                        var today = new Date(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate(), 0, 0, 0, 0);

                        var datediff = (today.getTime() - dateMock.getTime());

                        elements = elements.slice(iStart, iEnd);

                        for (var i = 0, len = elements.length; i < len; i++) {
                            var cdMilliseconds = new Date(elements[i][3]).getTime();
                            elements[i][3] = new Date((cdMilliseconds + datediff));
                        }
                        response.elements = elements;
                        return response;
                    }
                };
            }

            if (settings.url.indexOf("Rest/Processes/CustomizedColumnsData") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/menu/test/data/dummy2.txt"
                };
            }
            // New Case Webpart
            if (settings.url.indexOf("Rest/Processes/Categories") > -1) {
                if (settings.data.idCategory) {
                    if (settings.data.idCategory === "1000") {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.allprocess.txt"
                        };
                    }
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.txt"
                    };
                } else if (settings.data.idApp) {
                    if (settings.data.idApp === 1000) {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.app.allprocess.txt"
                        };
                    }
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.app.txt"
                    };
                } else {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.group.txt"
                    };
                }
            }
            
            // Summary Case - Details (Rest/Cases/{idCase}/Summary)
            if ((/Rest\/Cases\/\d+\/Summary/).test(settings.url)) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.txt",
                };
            }

            // Summary Case - Assignees (Rest/Cases/{idCase}/Assignees)              
            if ((/Rest\/Cases\/\d+\/Assignees/).test(settings.url)) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.assignees.txt",
                };
            }

            // Summary Case - Comments (Rest/Cases/{idCase}/Comments)              
            if ((/Rest\/Cases\/\d+\/Comments/).test(settings.url)) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.comments.txt",
                };
            }
            // Summary Case - Events (Rest/Cases/{idCase}/Events)              
            if ((/Rest\/Cases\/\d+\/Comments/).test(settings.url)) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.events.txt",
                };
            }

            if (settings.url.indexOf("Rest/Cases") > -1 && settings.url.indexOf("WorkItems") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/newCase/test/data/dummy.workitems.txt"
                };
            }

            if (settings.url.indexOf("Rest/Case") > -1) {
                self.idWfClass = null;
                if (settings.data.idWfClass === "1000") {
                    self.idWfClass = 1000;
                }
                return {
                    mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.createcase.txt"
                };
            }

            if (settings.url.indexOf("Rest/Processes/RecentProcesses") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.recentprocesses.txt"
                };
            }

            if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                if (self.idWfClass === 1000 || settings.data.h_idCase === 1000) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/render/test/data/dummy.rendering.all.txt"
                    };
                }
                if (typeof settings.data.h_idCase === "string") {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/dummy.rendering." + settings.data.h_idCase + ".txt"
                    };
                }
                return {
                    mockjson: "jquery/workportalflat/smartphone/render/test/data/dummy.txt"
                };
            }

            if (settings.url.indexOf("Rest/Handlers/MultiAction") > -1) {
                if (settings.data.h_action === "multiaction") {
                    var data = JSON.parse(settings.data.h_actions);
                    if (data[0].h_action == "PROCESSPROPERTYVALUE") {
                        if (data[0].h_idRender == "Departments" || data[0].h_idRender == "Jobs") {
                            return {
                                mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/dummy.data.departments.txt",
                                transform: function(response, originalSettings) {
                                    if (data[0].h_idRender == "Jobs") {
                                        var newResponse = [{}];
                                        newResponse[0].tag = data[0].h_tag;
                                        response[0].result.forEach(function(item, i) {
                                            if (item.id == data[0].p_parent) {
                                                newResponse[0].result = item.jobs;
                                            }
                                        }
                                        );
                                        return newResponse;
                                    } else {
                                        response[0].tag = data[0].h_tag;
                                        return response;
                                    }
                                }
                            };
                        }
                    }
                }
            }

            if (settings.url.indexOf("Rest/Users/CurrentUser") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/menu/test/data/dummy.currentUser.txt"
                };
            }

            if (settings.url.indexOf("Rest/Authorization/MenuAuthorization") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/menu/test/data/dummy.menuAuthorization.txt"
                };
            }

            if (settings.url.indexOf("Rest/Authentication/logout") > -1) {
                return {
                    mockjson: "jquery/workportalflat/smartphone/menu/test/data/dummy.logout.txt"
                };
            }
        }
    });
});