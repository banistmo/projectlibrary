/**
 * Created by RicardoPD on 5/15/2014.
 */

var BIZAGI_PATH_TO_BASE = '';
var BIZAGI_LANGUAGE = 'en';
var BIZAGI_ENVIRONMENT = 'debug';
var BIZAGI_DEFAULT_DEVICE = 'smartphone_ios';
var BIZAGI_ENABLE_LOG = false;
var BIZAGI_ENABLE_MOCKS = true;
var BIZAGI_ENABLE_COVERAGE = true;

//var BIZAGI_MOCKS_PATH = 'jquery/test/app/mocks/';
