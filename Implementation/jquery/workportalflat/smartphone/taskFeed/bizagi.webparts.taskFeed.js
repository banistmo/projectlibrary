/**
 *   Name: BizAgi Webpart taskFeed
 *   Author: RicharU
 *   Comments:
 *   -   This script will define a base class to task feed
 */

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.taskFeed", {
    PAGE_SIZE: 10
}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        self.properties = {};
        self.properties.page = 1;
        self.properties.pageSize = self.Class.PAGE_SIZE;
        self.properties.totalPages = 0;
        self.properties.elementPages = self.Class.PAGE_SIZE;
        self.properties.numberOfFields = 2;
        self.properties.idWfClass = -1;
        self.properties.data = [];
        self.routingCalled = true; //the first time is true to allow load data

        // Call base
        this._super(workportalFacade, dataService, initialParams);
    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        bizagi.chrono.initAndStart("taskFeed-all", true);

        var templateHeader = kendo.template(self.getTemplate("main-taskFeed-kendo-tmpl"), { useWithBlock: false });

        self.content = templateHeader({});
        deferred.resolve(self.content);

        return deferred.promise();
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;
        self.getContent().kendoMobileView({
            'show': function () {
                self.updateDataList();
            }
        });

        var templateContent = self.getTemplate('item-taskFeed-kendo-tmpl');

        self.dataSource = new kendo.data.DataSource({
            group: { field: "group" }
        });

        $("#items-content-taskFeed", self.content).kendoMobileListView({
            headerTemplate: '#=(value.split("|"))[1]#',
            fixedHeaders: true,
            template: templateContent,
            selectable: "single",
            pullToRefresh: true,
            pullParameters: function (item) {
                /* istanbul ignore next */
                return self.updateDataListByPullRefresh(self.options);
            }, click: function (e) {
                e.preventDefault();
                self.showSummary(e);
            }
        });

        // Configure handlers to click events
        self.configureHandlers();
    },

    updateDataListByPullRefresh: function (params) {
        var self = this;
        var deferred = new $.Deferred();
        var pullParemetersOptions = $.extend(params, { 'page': 1, 'pageSize': params.page * params.pageSize });

        bizagi.kendoMobileApplication.showLoading();
        $.when(
            self.dataService.getCasesListBeta(pullParemetersOptions)
        ).done(function (data) {
            deferred.resolve(data);
            bizagi.util.smartphone.stopLoading();
        });

        return deferred;
    },

    loadDataList: function (params) {
        var self = this;
        var deferred = new $.Deferred();
        params = params || {};
        self.options = self.options || {};
        bizagi.chrono.initAndStart("taskFeed-data", true);

        self.options.numberOfFields = self.properties.numberOfFields;
        self.options.page = params.page || self.properties.page;
        self.options.pageSize = params.pageSize || self.properties.pageSize;
        self.options.idWfClass = self.properties.idWfClass;

        $.when(
            self.dataService.getCasesListBeta(self.options)
        ).done(function (data) {
            if (params.more) {//load more
                self.dataSourceNew.pushCreate(data.elements);
                deferred.resolve();
            } else {//refresh data
                self.dataSourceNew = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            $.when(options.data).done(function (dataDeferred) {
                                var listData = (dataDeferred.elements) ? dataDeferred : data;
                                options.success(listData.elements);
                                bizagi.chrono.stopAndLog("taskFeed-data");
                                bizagi.chrono.stopAndLog("taskFeed-all");
                            });
                        }
                    },
                    group: { field: "group" }
                });
                deferred.resolve(self.dataSourceNew);
            }
            if (data.page >= data.totalPages) {
                self.disableLoadMore();
            }
        });
        return deferred.promise();
    },

    /**
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        $("#loadMoreTaskFeed", self.content).bind("click", function (e, callback) {
            self.properties.page += 1;
            bizagi.util.smartphone.startLoading();
            $.when(self.loadDataList({ 'more': true })).done(function (data) {
                if (callback) {
                    callback();
                }
                bizagi.util.smartphone.stopLoading();
            });
        });

        self.getContent().find(".km-rightitem").click(function () {
            bizagi.webpart.publish("bz-new-case");
        });

        self.getContent().find(".km-leftitem").click(function () {
            bizagi.webpart.publish("bz-menu");
        });

        $("#modalview-summary", self.canvas).delegate("#closeModalView", "click", function (e) {
            $("#modalview-summary", self.canvas).data("kendoMobileModalView").close();
        });

        $("#modalview-summary", self.canvas).delegate("#WorkOnItModalView", "click", function (e) {
            var item = $(this).data("item");
            bizagi.webpart.publish("changeCase", {
                idCase: item[0],
                idWorkitem: item[1],
                idTask: item[2],
                displayName: item[5]
            });
        });

        self.canvas.one("ondomincluded", function () {
            self.showView();
        });

        self.subscribe("bz-inbox", function () {
            self.showView();
        });

        self.subscribe("bz-update-list-cases", function (params) {
            params = params || {};
            self.routingCalled = true;
            /* istanbul ignore next */
            if (params.updateDataList) {
                self.updateDataList();
            }
        });

    },

    updateDataList: function () {
        var self = this;
        if (self.routingCalled) {
            var params = {
                page: 1,
                pageSize: (self.options) ? (self.options.page * self.options.pageSize) : (self.properties.page * self.properties.pageSize)
            };
            bizagi.kendoMobileApplication.showLoading();
            $.when(self.loadDataList(params)).done(function (data) {
                var dataSource = data;
                if (dataSource) {
                    $("#items-content-taskFeed", self.content).data("kendoMobileListView").setDataSource(dataSource);
                }
                bizagi.util.smartphone.stopLoading();
            });
            self.routingCalled = false;
        }
    },

    /**
    * Disable Button Load More
    */
    disableLoadMore: function () {
        var self = this;
        $('#loadMoreTaskFeed', self.content).hide();
    },
    /**
    * Custom method for webpart
    */
    showView: function () {
        bizagi.kendoMobileApplication.navigate('taskFeed');
    },
    /**
    Show Summary task feed
    */
    showSummary: function (params) {
        var self = this;
        var data = params.dataItem;

        var templateSummary = kendo.template(self.getTemplate('summary-taskFeed-kendo-tmpl'), { useWithBlock: false });
        var summary = templateSummary(data);

        // footer 48 | header  48 | autosize
        $('#modalview-text').html($(bizagi.util.trim(summary)));
        $('#WorkOnItModalView').data('item', data);
        var modalView = $('#modalview-summary', self.canvas).data('kendoMobileModalView');
        modalView.open();
        /* istanbul ignore next: untestable */
        modalView.adjustSize = function () {//adjust summary popup to window size
            var listHeight = $('ul.bz-wp-modal-list', this.element).height();
            var maxHeight = 0;
            var windowHeightSize = window.innerHeight;
            var popupHeight = $('.km-header',this.wrapper).height() + $('.km-footer',this.wrapper).height();
            if (listHeight + popupHeight >= windowHeightSize) {
                maxHeight = windowHeightSize - 20;
            } else {
                maxHeight = listHeight + popupHeight+10;
            }
            var top = ((windowHeightSize - maxHeight) / 2);
            this.wrapper.parent().css('top', top);
            this.wrapper.parent().css('height', maxHeight);
        };

        modalView.adjustSize();
        /* istanbul ignore next: untestable */
        window.addEventListener("orientationchange", function () {//desbindiar
            if(bizagi.util.isAndroid()){
                setTimeout(function(){
                    modalView.adjustSize();
                }, 500);
            }else{
                modalView.adjustSize();
            }
        }, false);
    }
});
