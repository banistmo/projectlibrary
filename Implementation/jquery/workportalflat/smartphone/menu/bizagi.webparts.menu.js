/*
*   Name: BizAgi Test
*   Author: oscaro
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.menu", {
    authCurrentUserAdministration: false,
    authAnalysisReports: false,
    authAdmin: false,
    enableLogOut: true,
    enableDebug: false,
    authInbox: false,
    enableSettings: false

}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        // return self.content = "";
        //bizagi.util.smartphone.stopLoading();

        var defer = new $.Deferred();
        $.when(self.dataService.getCurrentUser(), self.getSecurityMenu()).done(function (data, dataSecurity) {
            bizagi.currentUser = data;
            self.currentUser = data;
            self.security = dataSecurity;
            defer.resolve("");
        });

        return defer.promise();
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        /*overrides for default menu*/
        self.security.authCurrentUserAdministration = self.Class.authCurrentUserAdministration;
        self.security.authAnalysisReports = self.Class.authAnalysisReports;
        self.security.authAdmin = self.Class.authAdmin;
        self.security.enableLogOut = self.Class.enableLogOut;
        self.security.enableDebug = BIZAGI_ENABLE_LOG || self.Class.enableDebug;

        // Enable native preferences for smartphone Android
        var enableSettings = bizagi.detectDevice() == "smartphone_android" && typeof (cordova) != "undefined";                
        self.security.enableSettings = enableSettings || self.Class.enableSettings;

        /* disable inbox btn*/
        self.security.authInbox = self.Class.authInbox;

        if (typeof BIZAGI_ENABLE_LOG != "undefined" && BIZAGI_ENABLE_LOG == true) {
            self.initDebug();
        }

        var template = kendo.template(self.getTemplate('menu-tmpl'), { useWithBlock: false });

        self.contentInternal = template($.extend(self.security, self.currentUser));
        $("#content-left-drawer .km-scroll-container", self.canvas).append(self.contentInternal);
        $(".component-menu, .menu-items", self.canvas).kendoMobileListView({ appendOnRefresh: false, pullToRefresh: false });

        // Search
        $(".bz-wp-mu-button-search").hide();

        /* istanbul ignore next: untestable */
        $(".component-menu", self.canvas).delegate("[data-icon='new-smallsize']", "click", function (e) {
            e.preventDefault();
            self.openNewCaseAction();

        }).delegate("[data-icon='log-out-small']", "click", function (e) {
            e.preventDefault();
            self.logOutAction();

        }).delegate(".debug", "click", function () {
            //debug only without context
            $("#debug-panel").show();
        }).delegate("[data-icon='settings-small']", "click", function (e) {
            e.preventDefault();

            if (bizagi.detectDevice() == "smartphone_android" && typeof (cordova) != "undefined") {
                console.log("Show Android preferences");
                window.Bizagi.showSettingsActivity();
            }
        });

        //         this option not available on smartphone 
        //        "[data-icon='analitics']"   bizagi.webpart.publish("bz-analitics");
        //        "[data-icon='admin']"       bizagi.webpart.publish("bz-admin");
        //        "[data-icon='preferences']" bizagi.webpart.publish("bz-preferences");

        self.canvas.one("ondomincluded", function () {

        });

        /* istanbul ignore next: untestable */
        bizagi.webpart.subscribe("bz-menu", function () {
            $("#left-drawer").data("kendoMobileDrawer").show();
        });
    },

    openNewCaseAction: function () {
        var defer = new $.Deferred();
        $("#left-drawer").data("kendoMobileDrawer").hide();

        setTimeout(function () {
            bizagi.webpart.publish("bz-new-case");
            defer.resolve();
        }, 500);
        return defer.promise();
    },

    logOutAction: function () {
        var self = this;
        var defer = new $.Deferred();
        bizagi.webpart.publish("bz-logout");

        $.when(self.dataService.logoutMobile())
            .always(function (response) {
                self.defaultLogout();
                defer.resolve();
            });

        return defer.promise();
    },
    /**
    *   Customize the web part in each device
    */
    defaultLogout: function () {
        /* istanbul ignore next: untestable */
        if (typeof (cordova) !== "undefined") {
            window.location = bizagi.services.ajax.logoutPage;
        } else {
            window.location.replace('app/Inicio/LogOff.aspx');
        }
    },

    /**
    *   Customize the web part in each device
    */
    initDebug: function () {
        var self = this;
        $("body").append(self.getTemplate('debug-tmpl'));
        /* istanbul ignore next: untestable */
        $("#debug-panel .close-debug").click(function (e) {
            $("#debug-panel").hide();
        });
    },

    /**
    *   Customize the web part in each device
    */

    getSecurityMenu: function () {
        var self = this;
        var authMenu = {};
        var df = new $.Deferred();
        self.security = {};
        self.jsonSecurityList = {};

        $.when(self.dataService.getMenuAuthorization()).done(function (data) {
            self.convertSecurityData(data);
            self.jsonSecurityList = data;
            authMenu.authNewCase = self.security.NewCase || false;
            authMenu.authAnalysisReports = self.checkRootCategory(data, "AnalysisReports");
            authMenu.authAdmin = self.checkRootCategory(data, "Admin");
            authMenu.authCurrentUserAdministration = self.security.CurrentUser;
            $.each(self.security, function (key, value) {
                authMenu[key] = value;
            });
            df.resolve(authMenu);
        });
        return df.promise();
    },
    /**
    *   Customize the web part in each device
    */
    convertSecurityData: function (data) {
        var self = this;
        data = data || {};
        for (var i in data) {
            if (typeof data[i] == 'object') {
                self.convertSecurityData(data[i]);
            }
            else if (data[i] != undefined) {
                self.security[data[i]] = true;
            }
        }
    },
    /**
    *   Customize the web part in each device
    */
    checkRootCategory: function (data, key) {
        if (data.permissions != undefined) {
            for (var i = 0; i < data.permissions.length; i++) {
                if (data.permissions[i][key] != undefined) {
                    return true;
                }
            }
        }
        return false;
    }

});
