/*
*   Name: BizAgi Menu Search
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.menu.extend("bizagi.workportal.webparts.menu", {
    PAGE_SIZE: 30,
    SEARCH_DELAY: 700
}, {
    postRender: function (params) {
        var self = this;
        self._super(params);

        var templateContent = self.getTemplate('item-search-menu-tmpl');

        var listview = $('.bz-menu-search', self.canvas).kendoMobileListView({
            dataSource: new kendo.data.DataSource(),
            template: templateContent,
            selectable: 'single',
            click: function (element) {
                /* istanbul ignore next */
                self.renderCase(element);
            }
        });

        self.listViewSearch = listview.data('kendoMobileListView');

        self.configureHandlers();
    },

    /*
    * Method Search data
    */
    search: function (params) {
        var self = this;

        if (params.query === self.lastProcessedValue && !params.force) {
            return;
        }

        self.lastProcessedValue = params.query;

        bizagi.util.smartphone.startLoading();
        if (typeof (BIZAGI_ENABLE_QUICK_SEARCH) != 'undefined' && eval(BIZAGI_ENABLE_QUICK_SEARCH)) {
            self.loadDataListBeta(params).done(function () {
                bizagi.util.smartphone.stopLoading();
            });
        } else {
            self.loadDataList(params).done(function () {
                bizagi.util.smartphone.stopLoading();
            });
        }

    },

    /**
    *  Render the case
    */
    renderCase: function (params) {
        var self = this;
        var dataItem = params.dataItem;

        if (typeof (dataItem.noResults) === 'undefined') {
            var data = {};
            if (dataItem[1] !== -1 && dataItem[2] !== -1) {
                data.idCase = dataItem[0];
                data.idWorkitem = dataItem[1];
                data.idTask = dataItem[2];
                data.displayName = dataItem[5] || dataItem[6];
            }
            else {
                data.idCase = dataItem[0];
                data.displayName = dataItem[4] || dataItem[0];
            }
            self.publish('changeCase', data);
            $('.bz-wp-mu-button-search', self.canvas).click();
        }
    },

    /*
    *   Load data by server
    */
    loadDataList: function (params) {
        var self = this;

        var referrerParams = {
            radNumber: $.trim(params.query),
            idWorkflow: '',
            taskState: 'all',
            onlyFavorites: false,
            order: '',
            numberOfFields: 2,
            orderFieldName: '',
            orderType: '0',
            page: params.page || 1,
            pageSize: self.Class.PAGE_SIZE
        };

        return self.dataService.getCustomizedColumnsData(referrerParams).done(function (data) {
            data.elements = [];
            var elements = [];
            if (data.cases.totalPages === 0) {
                elements.push({ noResults: true, message: bizagi.localization.getResource('workportal-menu-search-found-no-cases') });
            }

            var actualDate = new Date();
            var actualMonth = actualDate.getMonth();
            var actualDay = actualDate.getDate();
            var actualYear = actualDate.getFullYear();

            for (var counter = 0; data.cases.rows.length > counter; counter++) {

                var tmpElement = data.cases.rows[counter]['fields'];
                var element = {};

                //Ignore unexpected first new element
                if (tmpElement.length == 5) {
                    tmpElement.splice(0, 1);
                }

                if (typeof (tmpElement[3]) != "string") {
                    var datesField = Object.keys(tmpElement[3]);
                    var dateValueArray = tmpElement[3][datesField];
                    tmpElement[3] = "";


                    for (var i = 0; i < dateValueArray.length; i++) {
                        var dateValue = dateValueArray[i];
                        if (dateValue != "" && typeof (dateValue) == "string") {
                            tmpElement[3] = dateValue;
                        }
                    }

                }

                //var tmpdate = new Date(tmpElement[4]['Task due date'][0]);
                if (tmpElement[3] != "") {
                    var tmpdate = new Date(tmpElement[3]);
                    var tmpDay = tmpdate.getDate();
                    var tmpMonth = tmpdate.getMonth();
                    var tmpYear = tmpdate.getFullYear();
                    var dateDiff = ((new Date(tmpYear, tmpMonth, tmpDay, 0, 0, 0) - new Date(actualYear, actualMonth, actualDay, 0, 0, 0)));

                    if (dateDiff < 0) {
                        tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), 'dd MMM');
                    } else {
                        if (actualMonth === tmpMonth && actualDay == tmpDay) {
                            tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), 'hh:mm');

                        } else if (actualMonth === tmpMonth && (actualDay + 1) == tmpDay) {
                            tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), 'hh:mm');
                        } else {
                            tmpElement[3] = bizagi.util.dateFormatter.formatDate(new Date(tmpElement[3]), 'dd MMM');
                        }
                    }
                }

                if (data.cases.rows[counter]['fields'][2]['workitems'] != undefined) {

                    for (var counterCase = 0; data.cases.rows[counter]['fields'][2]['workitems'].length > counterCase; counterCase++) {
                        element[0] = data.cases.rows[counter]['id'];
                        element[1] = tmpElement[2]['workitems'][counterCase]['idWorkItem'];
                        element[2] = tmpElement[2]['workitems'][counterCase]['idTask'];
                        element['state'] = tmpElement[2]['workitems'][counterCase]['State'].toLowerCase();
                        element[5] = tmpElement[2]['workitems'][counterCase]['TaskName'];
                        element[6] = []; element[6][0] = [];
                        element[6][0] = [0, tmpElement[0]];

                        if (typeof (tmpElement[1]) === "string") {
                            element[4] = tmpElement[1];
                        } else {
                            var usernameKey = Object.keys(tmpElement[1]);
                            element[4] = tmpElement[1][usernameKey][0];
                        }

                        element[3] = tmpElement[3];
                        elements.push(element);
                    }

                } else {

                    element[0] = data.cases.rows[counter]['id'];
                    element[1] = element[2] = element['state'] = element[5] = "";
                    element[6] = []; element[6][0] = [];
                    element[6][0] = [0, tmpElement[0]];
                    element[4] = tmpElement[1];
                    element[3] = tmpElement[3];
                    elements.push(element);
                }
            }
            data.elements = elements;
            self.listViewSearch.dataSource.data(data.elements);
        }).fail(function (msg) {
            var data = { 'elements': [], 'page': 1, 'totalPages': 1 };
            data.elements.push({ noResults: true, message: bizagi.localization.getResource('workportal-menu-search-found-no-cases') });
            self.listViewSearch.dataSource.data(data.elements);
            bizagi.util.smartphone.stopLoading();
        });
    },

    /*
    *   Load data by server
    */
    loadDataListBeta: function (params) {
        var self = this;

        var referrerParams = {
            radNumber: $.trim(params.query),
            idWorkflow: '',
            taskState: 'all',
            onlyFavorites: false,
            order: '',
            numberOfFields: 2,
            orderFieldName: '',
            orderType: '0',
            page: params.page || 1,
            pageSize: self.Class.PAGE_SIZE
        };

        return self.dataService.getCasesListBeta(referrerParams).done(function (data) {
            if (data.elements.length === 0) {
                data.elements.push({ noResults: true, message: bizagi.localization.getResource('workportal-menu-search-found-no-cases') });
            }
            self.listViewSearch.dataSource.data(data.elements);
        }).fail(function (msg) {
            var data = { 'elements': [], 'page': 1, 'totalPages': 1 };
            data.elements.push({ noResults: true, message: bizagi.localization.getResource('workportal-menu-search-found-no-cases') });
            self.listViewSearch.dataSource.data(data.elements);
            bizagi.util.smartphone.stopLoading();
        });
    },

    /*
    * Custom method for webpart
    */
    configureHandlers: function () {
        var self = this;

        var drawer = $('#left-drawer', self.canvas);
        var drawerData = drawer.data('kendoMobileDrawer');

        // Event Search
        $('.bz-wp-mu-input-search-menu', self.canvas).bind('click', function (e) {

            if (typeof (self.originalWidth) === 'undefined') {
                self.originalWidth = drawerData.getSize().width;
            }

            if (typeof (self.searchWidth) == 'undefined') {
                self.searchWidth = $('.bz-wp-mu-input-search-menu').width();
            }

            drawerData.element.width(window.innerWidth);
            drawerData.show();

            $('.bz-wp-mu-input-search-menu', self.canvas).animate({ width: '75%' });
            $('.bz-menu', self.canvas).fadeOut(undefined, undefined, function () {
                $('.bz-menu-search', self.canvas).fadeIn();
                $('.bz-wp-mu-button-search').show();
            });

        }).keyup(function (e) {
            if (e.which == 13) {
                self.search({ query: this.value, force: true });
                return;
            }
            if (e.which !== 13 && this.value.toString().length >= 3) {
                var that = this;
                setTimeout(function (e) {
                    self.search({ query: that.value });
                }, self.Class.SEARCH_DELAY);
                return;
            }
        });



        // Event cancel button        
        $('.bz-wp-mu-button-search', self.canvas).bind('click', function (e) {

            self.listViewSearch.dataSource.data([]);

            $('.bz-wp-mu-input-search-menu', self.canvas).val('');
            drawerData.element.width(self.originalWidth);
            drawerData.hide();

            setTimeout(function () {
                $('.bz-menu', self.canvas).show();
                $('.bz-menu-search', self.canvas).hide();
                $('.bz-wp-mu-button-search').hide();
                $('.bz-wp-mu-input-search-menu', self.canvas).animate({ width: self.searchWidth });
            }, 500);
        });
    }
});
