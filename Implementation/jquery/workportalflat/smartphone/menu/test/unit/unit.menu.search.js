﻿describe("Test webpart Menu", function () {
    BIZAGI_ENABLE_LOG = true;

    var webpartMenu;
    var canvas = $("body");

    it("Executing webpart", function (done) {
        var workportal = bizagi.collection.get("workportal");

        $(document).one("bizagi-webpart-created", function (e, wp) {
            var webpart = wp.webpart;
            spyOn(webpart, 'renderContent').and.callThrough();
            spyOn(webpart, 'postRender').and.callThrough();
            spyOn(webpart, 'configureHandlers').and.callThrough();
            console.log('Webpart Menu search Created');
        });

        workportal.executeWebpart({
            webpart: "menu",
            canvas: canvas
        }).done(function (wp) {
            webpartMenu = wp.webpart;

            //tracks that the spy was called
            expect(webpartMenu.renderContent).toHaveBeenCalled();
            expect(webpartMenu.postRender).toHaveBeenCalled();
            expect(webpartMenu.configureHandlers).toHaveBeenCalled();

            //tracks that the spy was called one time
            expect(webpartMenu.renderContent.calls.count()).toEqual(1);
            expect(webpartMenu.postRender.calls.count()).toEqual(1);
            expect(webpartMenu.configureHandlers.calls.count()).toEqual(1);

            done();
        });

    });

    it("Instance correct webpart", function () {
        expect(typeof webpartMenu).toEqual('object');
    });

    it("Call to click search on right drawer", function () {

        $('.bz-wp-mu-input-search-menu', canvas).val(61103);
        var event = jQuery.Event('keyup', { which: 13 });
        $('.bz-wp-mu-input-search-menu', canvas).trigger(event, function () {
            expect($('div.bz-wp-item-container', canvas).length).toEqual(10);
            done();
        });
    });

    it("Call to click search on right drawer with data", function () {

        $('.bz-wp-mu-input-search-menu', canvas).val(61103);
        var event = jQuery.Event('keyup', { which: 76 });
        $('.bz-wp-mu-input-search-menu', canvas).trigger(event, function () {
            expect($('div.bz-wp-item-container', canvas).length).toEqual(10);
            done();
        });
    });

    it('Call to click case in list view control', function (done) {
        setTimeout(function () {

            webpartMenu.renderCase({
                dataItem: {
                    id: 61101,
                    taskState: "Red",
                    isFavorite: "false",
                    guidFavorite: "",
                    isOpen: "true",
                    isAborted: "false",
                    fields: [
                        "61101",
                        "actionsOnGrid",
                        {
                            "workitems": [
                                {
                                    "TaskName": "Task 2",
                                    "State": "Red",
                                    "idTask": 411,
                                    "idWorkItem": 66403,
                                    "estimatedSolutionDate": "07/17/2013 08:43"
                                }
                            ]
                        },
                        "17 Jul",
                        {
                            "Actividad vence en": [
                                "7/17/2013 8:43:20 AM"
                            ]
                        },
                        "07/17/2013 08:43"
                    ],
                    group: "overdue"
                }
            });

            $('div[data-case="61101"]', canvas).trigger('click');
            done();
        }, 500);
    });

    it('Call to click case in list view control no data', function (done) {
        setTimeout(function () {

            webpartMenu.renderCase({
                dataItem: {
                    id: -1,
                    taskState:-1,
                    isFavorite: -1,
                    guidFavorite:-1,
                    isOpen: "",
                    isAborted: "",
                    fields: [],
                    group: ""
                }
            });
            $('div[data-case="61101"]', canvas).trigger('click');
            done();
        }, 500);
    });

    it('Call to click case in list view control without data', function () {
        setTimeout(function () {
            webpartMenu.renderCase({
                dataItem: { noResults: true, message: "Search returned no results" }
            });

        }, 500);
    });

    it('Call item from ListWiew', function () {
        $('.bz-wp-mu-input-search-menu', self.canvas).trigger('click');
    });

    it('Load search cases from service', function () {
        var listview = $('.bz-menu-search', self.canvas).data('kendoMobileListView');
        $('.bz-wp-mu-input-search-menu', canvas).val(6);
        expect(listview.items().length).toEqual(0);
        $('.bz-wp-mu-input-search-menu', canvas).val(61);
        expect(listview.items().length).toEqual(0);
    });

    it('Validate search', function (done) {

        $.when(webpartMenu.search({ query: '', force: true })).then(function (data) {
            console.log('Search');
            done();
        });
    });

});

