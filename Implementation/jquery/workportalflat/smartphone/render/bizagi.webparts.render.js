/*
 *   Name: BizAgi Test
 *   Author: oscaro
 *   Comments:
 *   -   This script will define a base class to all widgets
 */

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.render", {
}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        self.renderingStarted = new $.Deferred();
        self.navigation = initialParams.navigation;
        // Call base
        self._super(workportalFacade, dataService, initialParams);

        self.routing = new bizagi.workportal.action.routing(self);
        self.async = new bizagi.workportal.widget.async(self);
        self.navigation = {};

        /* istanbul ignore next: untestable */
        this.subscribe("showDialog", function (e, params) {
            // Shows a dialog
            self.showDialog(params);
        });

        /* istanbul ignore next: untestable */
        this.subscribe("createEditionView", function (e, params) {
            // Create a RenderEdition View
            self.createEditionView(params);
        });

    },


    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {

        bizagi.services.ajax.loginPage = function () {
        };
        var self = this;
        var deferred = $.Deferred();
        deferred.resolve('');

        return deferred.promise();
    },

    initNavigation: function () {
        var self = this;
        self.navigation = new bizagi.workportal.action.navigation({ 'content': self.getContent() });
        return self.navigation;
    },


    /**
    *   Renders the form component of the widget
    */
    renderForm: function (params) {
        var self = this;
        var originalParams = params;
        var drawerSummary = $('#bz-render-drawer', 'body').data('kendoMobileDrawer');
        if(self.view){//if view exists
            self.view.destroy();
            drawerSummary.options.views.pop();//removes the current view from the summary drawer
            kendo.history.locations.pop();//Remove ths last navigated view from history
        }
        var template = kendo.template(self.getTemplate('render-tmpl'), { useWithBlock: false });
        self.content = $(template({'title': params.idCase }));
        $('body').append(self.content);
        self.initNavigation();

        self.content.kendoMobileView({
            'useNativeScrolling': true,
            'show': function(){
                self.navigation.setVisibleIdView(this.id.substring(1, this.id.length));
            },
            'destroy': function (args, e) {
                this.element.remove();
                if (typeof ($("#actionsheetDynamic" + this.id.substring(1, this.id.length)).data('kendoMobileActionSheet')) !== 'undefined') {
                    /* istanbul ignore next: untestable */
                    $("#actionsheetDynamic" + this.id.substring(1, this.id.length)).data('kendoMobileActionSheet').destroy();
                }
                if (typeof ($("#modalview-async-message")).data('kendoMobileModalView') !== 'undefined') {
                    $("#modalview-async-message").data('kendoMobileModalView').destroy();
                }
            }
        });

        var renderContainer = $("#render-content", self.content);

        self.view = self.content.data('kendoMobileView');
        self.idRender = self.view.id;
        
        //add drawer view
        drawerSummary.options.views.push(this.idRender);

        bizagi.kendoMobileApplication.navigate(self.idRender);

        if (params.withOutGlobalForm) {
            var errorTemplate = kendo.template(self.workportalFacade.getTemplate("info-message"), { useWithBlock: false });
            var message = params.messageForm || self.resources.getResource("render-without-globalform");
            var errorHtml = errorTemplate({'message': message});
            self.navigation.initContextualButtons();
            $(self.navigation.buttonsContextual.back).bind("click", function (e) {
                /* istanbul ignore next: untestable */
                bizagi.kendoMobileApplication.navigate('taskFeed');
            });
            $(renderContainer).append(errorHtml);
            return renderContainer;
        }

        


        // Load render page
        /* istanbul ignore if: rendering is declared on unit test */
        if (typeof self.rendering === "undefined") {
            var proxyPrefix = params.proxyPrefix || self.dataService.serviceLocator.proxyPrefix;
            self.rendering = new bizagi.rendering.facade({ "proxyPrefix": proxyPrefix });
        }

        //Executes rendering into render container
        var resultRender = self.rendering.execute({
            canvas: renderContainer,
            navigation: self.navigation,
            idCase: params.idCase,
            displayName: params.displayName || params.taskName,
            idWorkitem: params.idWorkitem,
            idTask: params.idTask,
            postRenderEdit: self.postRenderEdit.bind(self),
            originalParams: originalParams,
            getTemplate: self.getTemplate,
            notification_box: $('#notification-box', self.content)
        }).done(function(e){
            console.log("Termino ejecucion");
        });

        bizagi.util.setContext({
            idCase: params.idCase || null,
            displayName: params.displayName || params.taskName,
            caseNumber: params.caseNumber || null,
            idWorkitem: params.idWorkitem || null,
            idTask: params.idTask || null,
            isOfflineForm: params.isOfflineForm || false,
            formsRenderVersion: params.formsRenderVersion || null,
            fromTask: params.fromTask || null
        });


        // Attach handler to render container to subscribe for routing events
        renderContainer.bind("routing", function (context, triggerparams) {
            // Executes routing action
            var deferred = $.Deferred();
            var params = {
                'action': 'routing',//bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING
                'idCase': bizagi.context.idCase,
                'displayName': bizagi.context.displayName,
                'idTask': bizagi.context.idTask,
//                'idWorkitem': bizagi.context.idWorkitem,//was revomed because was not neccesary and was causing errors
                'fromTask': bizagi.context.fromTask,
                'isOfflineForm': bizagi.context.isOfflineForm,
                'formsRenderVersion': bizagi.context.formsRenderVersion
            };
            params = $.extend(params, triggerparams);
            $.when(self.routing.executeRouting(params)).done(function(){
                self.publish("bz-update-list-cases");
                deferred.resolve();
            });
            return deferred.promise();
        });

        renderContainer.one("refresh", function (context, triggerparams) {
            var params = {
                'idCase': bizagi.context.idCase,
                'idTask': bizagi.context.idTask,
                'idWorkitem': bizagi.context.idWorkitem,
                'isOfflineForm': bizagi.context.isOfflineForm,
                'formsRenderVersion': bizagi.context.formsRenderVersion
            };
            self.refresh($.extend(params, triggerparams));
        });

        return resultRender;
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;
        self.configureHandlers();
    },
    /**
    custom method for webpart
    */
    configureHandlers: function () {
        var self = this;

        //Load modules after everything is loaded
        self.canvas.one("load-modules", function () {
            bizagi.loader.start("renderingflat").then(function () {
                self.renderingStarted.resolve();
            });
        });

        // show the render form to case
        self.subscribe("render-case", function (e, params) {
            bizagi.util.smartphone.startLoading();
            var deferred = $.Deferred();
            $.when(self.renderingStarted).done(function () {
                $.when(self.renderCase(params)).done(function(){
                    deferred.resolve();
                }).fail(function(){
                    deferred.reject();
                });
            });
            return deferred.promise();
        });
    },

    renderCase: function(params){
        var self = this;
        var deferred = $.Deferred();
        bizagi.webpart.publish("bz-summary-case", params);
        if (typeof params.idWorkitem === "undefined") {
            self.dataService.getWorkitems(params).done(function (resp) {
                var idWorkItem = resp.workItems[0] ? resp.workItems[0].idWorkItem : resp.idWorkitem;
                var idTask = resp.workItems[0] ? resp.workItems[0].idTask : resp.idTask;
                var data = {
                    'idCase': resp.idCase || resp.workItems[0].idCase,
                    'idWorkitem': idWorkItem || "",
                    'idTask': idTask || ""

                };
                $.when(self.renderForm($.extend(params, data, { refresh: true }))).done(function () {
                    bizagi.kendoMobileApplication.view().scroller.reset();
                    bizagi.util.smartphone.stopLoading();
                    deferred.resolve();
                })./* istanbul ignore next: untestable */fail(function (e) {
                    bizagi.util.smartphone.stopLoading();
                    deferred.fail();
                });
            });
            bizagi.util.smartphone.stopLoading();
            return deferred.promise();
        }

        $.when(self.renderForm($.extend(params, { refresh: true }))).done(function () {
            bizagi.util.smartphone.stopLoading();
            bizagi.kendoMobileApplication.view().scroller.reset();
            deferred.resolve();
        }).fail(function (e) {
            self.navigation.initContextualButtons();
            $(self.navigation.buttonsContextual.back).bind("click", function (e) {
                /* istanbul ignore next: untestable */
                bizagi.kendoMobileApplication.navigate('taskFeed');
            });
            bizagi.util.smartphone.stopLoading();
            deferred.reject();
        });
        return deferred.promise();
    },

    //edit Items on render
    postRenderEdit: function (renderControl) {
        var self = this;
        var render = renderControl;
        var properties = render.properties;

        var viewParams = (typeof(render.titleLable) !== undefined && render.titleLable !== false ) ? { "label": properties.displayName } : {}
        var editRender = $.tmpl(render.renderFactory.getTemplate("editRender"), viewParams);
        self.createEditionView({
            'idCase': self.params.idCase || '',
            'displayName': bizagi.context.idCase,
            'content': editRender,
            'callback': function(params){

                render.kendoView = $(params.id, 'body');
                render.kendoView.find(".ui-bizagi-container-inputs").html(render.inputEdition);

                bizagi.kendoMobileApplication.navigate(params.id);
                render.getParams().navigation.setNavigationButtons(render, render.renderEditionType || 'edition');
            },
            'kendoViewOptions': render.kendoViewOptions,
            'kendoViewFinish': render.kendoViewFinish
        });
    },

    /*
    *   Shows a component inside a modal dialog
    */
    showDialog: function (data) {
        var self = this;
        var dialog = new bizagi.workportal.widget.dialog(self);
        dialog.renderDialog(data);
        return dialog;
    },

    createEditionView: function (params) {
        var self = this;
        var properties = self.properties;
        var now = $.now();
        var renderEdition = kendo.template(self.getTemplate("renderEdition-tmpl"), { useWithBlock: false });
        renderEdition = renderEdition({ 'now': now, 'displayName': params.displayName || params.idCase, 'content': params.content });
        $('body').append(renderEdition);

        var viewContainer = $('#inputEdition_' + now);
        viewContainer.kendoMobileView($.extend({
            'show': function(){
                self.navigation.setVisibleIdView(this.id.substring(1, this.id.length));
                if(typeof(params.kendoViewFinish) !== 'undefined')
                    params.kendoViewFinish();
            },/* istanbul ignore next: untestable */
            'hide': function () {
                console.info("hide----> destroy");
                // this.destroy();
            },/* istanbul ignore next: untestable */'destroy': function (args, e) {
                this.element.remove();
                if (typeof ($("#actionsheetDynamic" + this.id.substring(1, this.id.length)).data('kendoMobileActionSheet')) !== 'undefined') {
                    $("#actionsheetDynamic" + this.id.substring(1, this.id.length)).data('kendoMobileActionSheet').destroy();
                }
            }
        }, params.kendoViewOptions));

        if(params.callback)
        {
            var callbackParams = (typeof(params.getNavigation) !== 'undefined' && params.getNavigation === true) ? {'viewContainer':viewContainer,'id':'#inputEdition_' + now, 'navigation': self.navigation} : {'viewContainer':viewContainer,'id':'#inputEdition_' + now};
            params.callback( callbackParams );
        }

    }

});
