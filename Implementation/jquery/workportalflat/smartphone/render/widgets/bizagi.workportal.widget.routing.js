/**
 * Created by LuisCE on 12/06/2014.
 */
/*
 *   Name: BizAgi Smartphone routing widget Implementation
 *   Author: luisce
 *   Comments:
 *   -   This script will shows routing widget
 */

// Extends itself
$.Class.extend("bizagi.workportal.widget.routing", {
}, {

    /*
     *   Constructor
     */
    init: function (params) {
        this.content = "";
    },

    renderContent: function (params) {
        var self = this;
        var dialogTemplate = kendo.template(bizagi.templates.services.service.cachedTemplates["routing-tmpl"], { useWithBlock: false });
        //TODO internationalization
        dialogTemplate = dialogTemplate(params);
        self.setContent(dialogTemplate);
        return self.getContent();
    },

    /*
     *   To be overriden in each device to apply layouts
     */
    postRender: function (params) {
        var self = this;
        var content = self.getContent();


        var workOnITActivitySelector = $("#ui-bizagi-wp-app-routing-activity-wf tbody tr", content);

        /*var onlyUserWorkItems = (self.params.data.fromSearchWidget) ? "false" : "true";
        var eventAsTasks = (self.params.data.fromSearchWidget) ? "true" : "false";*/

        // Assing even style
        $("tr:nth-child(even)", content).addClass("event");

        $(workOnITActivitySelector).bind('click', function () {
            self.showWorkitem({
                idCase: $(this).children(":first").children("#idCase").val(),
                idWorkitem: $(this).children(":first").children("#idWorkItem").val(),
                idTask: $(this).children(":first").children("#idTask").val(),
                taskName: $(this).children(":first").text()
            });

            // Delete activity selector
            $('#ui-bizagi-wp-app-inbox-activities-routing-wrapper').closest('div').remove();

            // close dialog
            params.close();
        });

        $(".wp-routing-container-fother button", content).one('click', function () {
            // remove dialog and close dialog
            params.close();
            bizagi.kendoMobileApplication.navigate("taskFeed");
        });

    },

    /*
     *   Shows the rendering widget
     */
    showWorkitem: function (params) {
        var self = this;

        if (params.taskName)
            self.notifiesNavigation(params.taskName);

        // Shows render widget

        bizagi.webpart.publish("render-case", {
            idCase: params.idCase,
            idWorkitem: params.idWorkitem,
            idTask: params.idTask,
            taskName: params.taskName
        });
    },


    /*
     *notify to suscribe the message in the header
     */
    notifiesNavigation: function (message) {
        var self = this;
        bizagi.webpart.publish("notifiesNavigation", { message: message });
    },

    getContent: function(){
        return this.content;
    },

    setContent: function(content) {
        this.content = content;
    }

});
/**
 * Created by LuisCE on 12/06/2014.
 */
