/**
 * Created by LuisCE on 12/06/2014.
 */
/*
 *   Name: BizAgi Smartphone Dialog Implementation
 *   Author: luisce
 *   Comments:
 *   -   This script will shows a component inside a modal dialog
 */

// Extends itself
$.Class.extend("bizagi.workportal.widget.dialog", {

}, {

    /*
     *   Constructor
     */
    init: function (params) {
        this.dataService = params.dataService;
        this.workportalFacade = params.workportalFacade;
    },

    /*
     *   Render the Dialog
     *   Returns a deferred
     */
    renderDialog: function (params) {
        var self = this;
        if($('#dialog').length > 0){
            self.close();
        }
        var defer = new $.Deferred();
        var routingWidget = new bizagi.workportal.widget.routing();
        routingWidget.renderContent(params);
        var routingTemplate = routingWidget.getContent();
        var dialogTemplate = kendo.template(bizagi.templates.services.service.cachedTemplates["dialog-tmpl"], { useWithBlock: false });
        //TODO internationalization
        dialogTemplate = dialogTemplate({'content': routingTemplate});
        $('body').append(dialogTemplate);
        routingWidget.setContent($('#dialog'));
        routingWidget.postRender({'close': self.close});
        var dialog = $("#dialog").kendoMobileModalView();
        dialog.data("kendoMobileModalView").open();
    },

    register : function(widget) {


    },

    /*
     *   Shows the dialog box in the browser
     *   Returns a promise that the dialog will be closed
     */
    showDialogBox: function (dialogBox,params) {


    },

    /*
     *   Close dialog
     */
    close: function(){
        var dialog = $("#dialog");
        dialog.data("kendoMobileModalView").close();
        dialog.data("kendoMobileModalView").destroy();
        $(dialog).remove();
    }
});
