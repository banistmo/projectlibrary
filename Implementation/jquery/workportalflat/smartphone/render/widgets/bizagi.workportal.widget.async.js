/**
 * Created by RicardoPD on 6/20/2014.
 */
$.Class.extend("bizagi.workportal.widget.async", {
    ASYNC_CHECK_TIMER: 3000
}, {
    /*
     *   Constructor
     */
    init: function (params) {
        var self = this;
        self.webpart = params;
        self.configureHandlers();
        self.modal = $('#modalview-async-message');
    },

    configureHandlers: function(){
        var async = this;
        var self = this.webpart;//set as if it was the webpart
        self.subscribe('render-async', function (e, params) {
            async.executeAsync(params).done(function(){
                self.publish('changeCase', {
                    action: bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ROUTING,
                    idCase: params.idCase
                });
            });
        });
    },

    executeAsync: function(params){
        var self = this;
        var defer = new $.Deferred();
        self.modal =  $('#modalview-async-message');
        self.modal.data('kendoMobileModalView').open();
        self.checkAsycnExecutionState(defer, params);
        return defer.promise();
    },

    checkAsycnExecutionState: function(defer, params){
        var self = this.webpart;
        var async = this;
        $.when(self.dataService.getAsynchExecutionState({
            idCase: params.idCase
        })).done(function(response) {
            var template = kendo.template(self.getTemplate('async-tmpl'), { useWithBlock: false });

            // verify errors in response
            if(response.state == "Error" && response.errorMessage !== undefined){
                // Change default error
                response.errorMessage = bizagi.localization.getResource('render-async-error');
            }

            var html= template(response);
            // Render base template
            $('.bz-rn-wg-modal-async', self.modal).empty();
            $('.bz-rn-wg-modal-async', async.modal).append(html);
            $('#render-async-close', async.modal).bind('click', function(){
                async.modal.data('kendoMobileModalView').close();
            });

            // Check for finish signal
            if (response.state == "Processing") {
                // Check for non-error message
                if (response.errorMessage.length === 0) {
                    setTimeout(function() {
                        // Refresh widget after a timer
                        async.checkAsycnExecutionState(defer, params);

                    }, self.Class.ASYNC_CHECK_TIMER);
                }

            } else if (response.state == "Finished") {
                // Resolve main deferred
                defer.resolve();
                async.modal.data('kendoMobileModalView').close();
            }
        });
    }
});