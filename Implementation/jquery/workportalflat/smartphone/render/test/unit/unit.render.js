﻿//Unit test for webpart render
describe("Testing webpart Render", function () {
    var webpartRender;
    var canvas = $('body');

    it("Executing webpart", function (done) {
        var workportal = bizagi.collection.get("workportal");

        $(document).one('bizagi-webpart-created', function (e, wp) {
            webpartRender = wp.webpart;
            spyOn(webpartRender, 'renderContent').and.callThrough();
            spyOn(webpartRender, 'postRender').and.callThrough();
            spyOn(webpartRender, 'configureHandlers').and.callThrough();
        });

        $.when(
            workportal.executeWebpart({
                webpart: "render",
                canvas: canvas
            }).done(function (wp) {
                webpartRender = wp.webpart;
                bizagi.util.smartphone.stopLoading();
                expect(webpartRender.renderContent.calls.count()).toEqual(1);
                expect(webpartRender.postRender.calls.count()).toEqual(1);
                expect(webpartRender.configureHandlers.calls.count()).toEqual(1);
                console.log('Webpart Render Created but no data is loaded');
                done();
            })
        ).then(function(){
            $('body').triggerHandler("load-modules");
        });

    });

    it(" Test instance correct webpart", function () {
        expect(typeof webpartRender).toEqual('object');
    });

    it(" test of renderContent", function () {
        expect(bizagi.webparts.instances.filter(function(el){
            return el.name == "bizagi.workportal.webparts.render";
        }).length).toBeGreaterThan(0);
    });

    describe("renders a case", function(){
        beforeEach(function(){
            var deferred = $.Deferred();
            webpartRender.rendering = {
                execute: function(params){
                    setTimeout(function(){
                        deferred.resolve();
                    }, 50);
                    return deferred.promise();
                }
            };
            spyOn(webpartRender, 'renderCase').and.callThrough();
            spyOn(webpartRender, 'renderForm').and.callThrough();
            spyOn(webpartRender.rendering, 'execute').and.callThrough();
            spyOn(webpartRender, 'createEditionView').and.callThrough();
            spyOn(webpartRender.dataService, 'getWorkitems').and.callThrough();
            spyOn(webpartRender.workportalFacade, 'getTemplate').and.callThrough();
            spyOn(webpartRender, 'refresh').and.callThrough();
        });
        it("with workitem", function(done){
            var params = {
                displayName: "test render",
                eventAsTasks: false,
                formsRenderVersion: 2,
                idCase: 1,
                idTaks: 10,
                idWorkitem: 252,
                isComplex: false,
                isOfflineForm: false,
                onClose: "",
                onlyUserWorkItems: "true",
                referrer: "",
                widgetName: "activityform"
            };
            $.when(webpartRender.publish("render-case", params)).done(function(){
                expect(webpartRender.renderCase.calls.count()).toEqual(1);
                expect(webpartRender.renderForm.calls.count()).toEqual(1);
                expect(webpartRender.rendering.execute.calls.count()).toEqual(1);

                var executeParams = webpartRender.rendering.execute.calls.argsFor(0)[0];
                expect(executeParams.idCase).toEqual(params.idCase);
                expect(executeParams.displayName).toEqual(params.displayName);
                expect(executeParams.idWorkitem).toEqual(params.idWorkitem);
                expect(executeParams.idTask).toBeUndefined();
                expect(executeParams.originalParams).toEqual(params);
                done();
            });
        });


        it("with workitem fails", function(done){
            var params = {
                displayName: "test render",
                eventAsTasks: false,
                formsRenderVersion: 2,
                idCase: 1,
                idTaks: 10,
                idWorkitem: 252,
                isComplex: false,
                isOfflineForm: false,
                onClose: "",
                onlyUserWorkItems: "true",
                referrer: "",
                widgetName: "activityform"
            };

            var deferred = $.Deferred();
            $.extend(webpartRender.rendering, {
                execute: function(){
                    setTimeout(function(){
                        deferred.reject();
                    }, 50);
                    return deferred.promise();
                }
            });
            $.when(webpartRender.publish("render-case", params)).fail(function(){
                expect(webpartRender.renderCase.calls.count()).toEqual(1);
                expect(webpartRender.renderForm.calls.count()).toEqual(1);
                done();
            });
        });


        it("refresh form", function(){
            $.when($("#render-content").triggerHandler("refresh", {})).done(function(){
                expect(webpartRender.refresh.calls.count()).toEqual(1);
                expect(webpartRender.refresh.calls.argsFor(0)[0].idCase).toEqual(bizagi.context.idCase);
                expect(webpartRender.refresh.calls.argsFor(0)[0].idTask).toEqual(bizagi.context.idTask);
                expect(webpartRender.refresh.calls.argsFor(0)[0].idWorkitem).toEqual(bizagi.context.idWorkitem);
                expect(webpartRender.refresh.calls.argsFor(0)[0].isOfflineForm).toEqual(bizagi.context.isOfflineForm);
                expect(webpartRender.refresh.calls.argsFor(0)[0].formsRenderVersion).toEqual(bizagi.context.formsRenderVersion);
            });
        });

        it("without global form", function(done){
            var params = {
                displayName: "test render",
                eventAsTasks: false,
                formsRenderVersion: 2,
                idCase: 1,
                idTaks: 10,
                idWorkitem: 252,
                withOutGlobalForm: true,
                isComplex: false,
                isOfflineForm: false,
                onClose: "",
                onlyUserWorkItems: "true",
                referrer: "",
                widgetName: "activityform"
            };
            $.when(webpartRender.publish("render-case", params)).done(function(){
                expect(webpartRender.renderCase.calls.count()).toEqual(1);
                expect(webpartRender.renderForm.calls.count()).toEqual(1);
                expect(webpartRender.workportalFacade.getTemplate.calls.count()).toEqual(2);
                expect(webpartRender.workportalFacade.getTemplate.calls.argsFor(1)[0]).toEqual("info-message");
                expect(webpartRender.rendering.execute.calls.count()).toEqual(0);
                done();
            });
        });

        it("without workitem", function(done){
            var params = {
                displayName: "test render",
                eventAsTasks: false,
                formsRenderVersion: 2,
                idCase: 1,
                idTaks: 10,
                isComplex: false,
                isOfflineForm: false,
                onClose: "",
                onlyUserWorkItems: "true",
                referrer: "",
                widgetName: "activityform"
            };
            $.when(webpartRender.publish("render-case", params)).done(function(){
                expect(webpartRender.dataService.getWorkitems.calls.count()).toEqual(1);
                expect(webpartRender.renderCase.calls.count()).toEqual(1);
                expect(webpartRender.renderForm.calls.count()).toEqual(1);
                expect(webpartRender.rendering.execute.calls.count()).toEqual(1);

                var executeParams = webpartRender.rendering.execute.calls.argsFor(0)[0];
                expect(executeParams.idCase).toEqual(params.idCase);
                expect(executeParams.displayName).toEqual(params.displayName);
                expect(executeParams.idWorkitem).toEqual(params.idWorkitem);
                expect(executeParams.idTask).toBeDefined();
                expect(executeParams.originalParams).toEqual(params);
                done();
            });
        });
    });

    describe("Post Render Edition", function(){
        var renderControl;
        beforeEach(function(){
            renderControl = {
                properties: {displayName: "title"},
                titleLable: "title",
                inputEdition: $("<div>"),
                kendoViewOptions: {},
                kendoViewFinish: function(){},
                getParams: function(){
                    var navigation = {
                        setNavigationButtons: function(render, type){ }
                    };
                    return {navigation: navigation};
                }
            };

            renderControl.renderFactory = {
                getTemplate: function(template){
                    return "<div></div>";
                }
            };
        });
        it("works", function(){
            webpartRender.postRenderEdit.bind({params: {idCase: 2}});
            webpartRender.postRenderEdit(renderControl);
            expect(true).toBe(true);
        });
    });


    describe("and runs navigation", function(){
        describe("init navigation buttons", function(){

            it("for render with buttons", function(){
                webpartRender.navigation.changeContextualButtons();//no contextual buttons
                var getactualButtons = webpartRender.navigation.getActualStateContextualButtons();//no contextual buttons
                expect(getactualButtons).toEqual(false);
                var buttons = [
                    {
                        "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
                        "caption": "Save",
                        "actions": [
                            "submitData",
                            "refresh"
                        ],
                        "submitData": true,
                        "refresh": true,
                        "ordinal": 0,
                        "action": "save",
                        "save": true,
                        "style": ""
                    },
                    {
                        "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
                        "caption": "Next",
                        "actions": [
                            "validate",
                            "submitData",
                            "next",
                            "routing"
                        ],
                        "validate": true,
                        "submitData": true,
                        "next": true,
                        "routing": true,
                        "ordinal": 1,
                        "action": "next",
                        "save": false,
                        "style": ""
                    }
                ];
                var context = {
                    buttons: buttons,
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: buttons
                    },
                    params: {},
                    processButton: function(){}
                };
                expect(webpartRender.navigation.observerList.length).toEqual(0);
                webpartRender.navigation.setNavigationButtons(context);
                expect(webpartRender.navigation.observerList.length).toEqual(1);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(true);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);


                //webpartRender.navigation.notifyLastObserver("back");

            });

            it("for render without buttons", function(){
                var context = {
                    buttons: [],
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: []
                    },
                    params: {},
                    processButton: function(){}
                };
                webpartRender.navigation.setNavigationButtons(context);
                expect(webpartRender.navigation.observerList.length).toEqual(2);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);


                //webpartRender.navigation.notifyLastObserver("back");

            });

            it("for render without buttons and refresh", function(){
                var context = {
                    buttons: [],
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: []
                    },
                    params: {
                        isRefresh: true,
                        originalParams: {}
                    },
                    nextButton: {
                        validate: false
                    },
                    validationController: {
                        performValidations: function(){ return false}
                    },
                    processButton: function(){}
                };
                webpartRender.navigation.setNavigationButtons(context);
                expect(webpartRender.navigation.observerList.length).toEqual(3);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);


                //webpartRender.navigation.notifyLastObserver("back");

            });

            it("for render preview", function(){
                var context = {
                    buttons: [],
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: []
                    },
                    params: {}
                };
                webpartRender.navigation.setNavigationButtons(context, "preview");
                expect(webpartRender.navigation.observerList.length).toEqual(3);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);


                //webpartRender.navigation.notifyLastObserver("back");

            });


            it("for render edition without buttons", function(){
                var context = {
                    buttons: [],
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: []
                    },
                    params: {}
                };
                webpartRender.navigation.setNavigationButtons(context, "edition");
                expect(webpartRender.navigation.observerList.length).toEqual(4);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(true);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);


                //webpartRender.navigation.notifyLastObserver("back");

            });

            it("for render edition with form buttons", function(){
                var buttons = [
                    {
                        "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
                        "caption": "Save",
                        "actions": [
                            "submitData",
                            "refresh"
                        ],
                        "submitData": true,
                        "refresh": true,
                        "ordinal": 0,
                        "action": "save",
                        "save": true,
                        "style": ""
                    },
                    {
                        "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
                        "caption": "Next",
                        "actions": [
                            "validate",
                            "submitData",
                            "next",
                            "routing"
                        ],
                        "validate": true,
                        "submitData": true,
                        "next": true,
                        "routing": true,
                        "ordinal": 1,
                        "action": "next",
                        "save": false,
                        "style": ""
                    }
                ];
                var context = {
                    buttons: [],
                    properties: {
                        id: Math.floor((Math.random() * 1000) + 1),
                        buttons: buttons
                    },
                    form: {
                        buttons: buttons,
                        properties: {
                            buttons: buttons
                        }
                    },actionSave: function(){}
                };
                webpartRender.navigation.setNavigationButtons(context, "edition");
                expect(webpartRender.navigation.observerList.length).toEqual(5);
                expect($(webpartRender.navigation.buttonsContextual.button).is(":visible")).toEqual(false);
                expect($(webpartRender.navigation.buttonsContextual.dynamic).is(":visible")).toEqual(true);
                expect($(webpartRender.navigation.buttonsContextual.back).is(":visible")).toEqual(true);

                context.properties.id = Math.floor((Math.random() * 1000) + 1);
                webpartRender.navigation.setNavigationButtons(context, "edition");//set's up aditional susbscribers
                expect(webpartRender.navigation.observerList.length).toEqual(6);

                context.properties.id = Math.floor((Math.random() * 1000) + 1);
                jQuery.extend(context, {
                    actionCancel: function(){}
                });
                webpartRender.navigation.setNavigationButtons(context, "edition");//set's up aditional susbscribers
                expect(webpartRender.navigation.observerList.length).toEqual(7);
            });
        });


        it("Changes navigation buttons", function(){
            expect(webpartRender.navigation.buttonsContextual).toBeDefined();
            expect(webpartRender.navigation.buttonsContextual.button).toBeDefined();
            expect(webpartRender.navigation.buttonsContextual.back).toBeDefined();
            expect(webpartRender.navigation.buttonsContextual.dynamic).toBeDefined();
            webpartRender.navigation.changeContextualButtons("button");
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(true);
            expect(getActualButtons.dynamic).toEqual(false);
            webpartRender.navigation.changeContextualButtons("button", {visible: false});
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(false);


            webpartRender.navigation.changeContextualButtons("dynamic");
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(false);
            expect(getActualButtons.dynamic).toEqual(true);
            webpartRender.navigation.changeContextualButtons("dynamic", {visible: false});
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.dynamic).toEqual(false);

            webpartRender.navigation.changeContextualButtons("back");
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.back).toEqual(true);
            webpartRender.navigation.changeContextualButtons("back", {visible: false});
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.back).toEqual(false);

            webpartRender.navigation.changeContextualButtons("refresh", {button: true, back: true});
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(true);
            expect(getActualButtons.back).toEqual(true);
            expect(getActualButtons.dynamic).toEqual(false);

            webpartRender.navigation.changeContextualButtons("refresh", {dynamic: true});
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(false);
            expect(getActualButtons.back).toEqual(true);
            expect(getActualButtons.dynamic).toEqual(true);

            webpartRender.navigation.changeContextualButtons("disable");
            var getActualButtons = webpartRender.navigation.getActualStateContextualButtons();
            expect(getActualButtons.button).toEqual(false);
            expect(getActualButtons.back).toEqual(false);
            expect(getActualButtons.dynamic).toEqual(false);
        });


        describe("navigates", function(){
            beforeEach(function(){
                spyOn(webpartRender.navigation, 'navigationActions').and.callThrough();
                spyOn(webpartRender.navigation, 'removeSuscriber').and.callThrough();
                spyOn(bizagi.kendoMobileApplication, 'navigate').and.callFake(function(){ });
            });

            it("back from edition with action save", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("back")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith("#:back");
                    expect(webpartRender.navigation.observerList.length).toEqual(6);
                    done();
                });
            });

            it("forward from edition", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("buttom")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith("#:back");
                    expect(webpartRender.navigation.observerList.length).toEqual(5);
                    done();
                });
            });

            it("dynamic from edition", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("dynamic")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith("#:back");
                    expect(webpartRender.navigation.observerList.length).toEqual(4);
                    done();
                });
            });

            it("back from edition", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("back")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith("#:back");
                    expect(webpartRender.navigation.observerList.length).toEqual(3);
                    done();
                });
            });

            it("forward from render", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("buttom")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(0);
                    expect(webpartRender.navigation.observerList.length).toEqual(2);
                    done();
                });
            });

            it("dynamic from render", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("dynamic")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.observerList.length).toEqual(2);
                    done();
                });
            });

            it("back from render", function(done){
                $.when(webpartRender.navigation.notifyLastObserver("back")).done(function(){
                    expect(webpartRender.navigation.navigationActions.calls.count()).toEqual(1);
                    expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate.calls.count()).toEqual(1);
                    expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith("taskFeed");
                    expect(webpartRender.navigation.observerList.length).toEqual(1);
                    done()
                });
            });
        });

        describe("test subscribers", function(){
            beforeEach(function(){
                spyOn(webpartRender.navigation, 'changeContextualButtons').and.callThrough();
                spyOn(webpartRender.navigation, 'removeSuscriber').and.callThrough();
            });

            it("undo last subscriber removed", function(){
                webpartRender.navigation.undoRemoveLastSuscriber();
                webpartRender.navigation.undoRemoveLastSuscriber();
                expect(webpartRender.navigation.observerList.length).toEqual(2);
                expect(webpartRender.navigation.changeContextualButtons.calls.count()).toEqual(1);
                expect(webpartRender.navigation.changeContextualButtons).toHaveBeenCalledWith("back", { visible: true });
                expect(webpartRender.navigation.lastElementRemoveHistory).toEqual(null);
            });

            it("removes last subscriber", function(){
                var lastSubscriber = webpartRender.navigation.getLastSuscriber();
                webpartRender.navigation.removeLastSuscriber();
                expect(webpartRender.navigation.removeSuscriber.calls.count()).toEqual(1);
                expect(webpartRender.navigation.removeSuscriber.calls.argsFor(0)[0]).toEqual(lastSubscriber.key);
                expect(webpartRender.navigation.observerList.length).toEqual(1);
            });

            it("notify observers", function(){
                webpartRender.navigation.setTypeObserver(false);
                webpartRender.navigation.notifyObservers("dynamic", {});//do nothing
                webpartRender.navigation.setTypeObserver(true);
                webpartRender.navigation.notifyObservers("dynamic", {});
            });

            it("empty all subscribers", function(){
                webpartRender.navigation.emptySuscribers();
                expect(webpartRender.navigation.observerList.length).toEqual(0);
                expect(webpartRender.navigation.changeContextualButtons).toHaveBeenCalledWith("back", { visible: false });
                webpartRender.navigation.removeLastSuscriber();//do nothing
                webpartRender.navigation.removeSuscriber();//do nothing
                webpartRender.navigation.notifyLastObserver();//do nothing
                webpartRender.navigation.notifyObservers();//do nothing
                var lastSubscriber = webpartRender.navigation.getLastSuscriber();
                expect(lastSubscriber).toEqual(null);
            });

            it("notifies multiple callbacks", function(){
                var numberOfCalls = 0;
                var callbacks = [function(){numberOfCalls++;},function(){numberOfCalls++},function(){numberOfCalls++}];
                webpartRender.navigation.notify(callbacks);
                expect(numberOfCalls).toEqual(3);
            });

        });
    });

    describe("and executing routing", function(){
        beforeEach(function(){
            webpartRender.rendering = {
                execute: function(params){
                    var deferred = $.Deferred();
                    setTimeout(function(){
                        deferred.resolve();
                    }, 50);
                    return deferred.promise();
                }
            };

            spyOn(webpartRender.dataService.routing, 'getRoute').and.callThrough();
            spyOn(webpartRender.async, 'executeAsync').and.callThrough();
            spyOn(webpartRender, 'publish').and.callThrough();
            spyOn(bizagi.webpart, 'publish').and.callThrough();
        });

        it("render a case", function(done){
            $.when($("#render-content").triggerHandler("routing")).done(function(){
                expect(webpartRender.dataService.routing.getRoute.calls.count()).toEqual(1);
                expect(bizagi.webpart.publish.calls.count()).toEqual(2);
                expect(bizagi.webpart.publish.calls.argsFor(0)[0]).toEqual("render-case");
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].displayName).toEqual(bizagi.context.displayName);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].idCase).toEqual(bizagi.context.idCase);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].idTask).toEqual(bizagi.context.idTask);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].fromTask).toEqual(bizagi.context.fromTask);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].isOfflineForm).toEqual(bizagi.context.isOfflineForm);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].formsRenderVersion).toEqual(bizagi.context.formsRenderVersion);
                expect(bizagi.webpart.publish.calls.argsFor(1)[0]).toEqual("bz-summary-case");
                expect(webpartRender.publish.calls.argsFor(0)[0]).toEqual("bz-update-list-cases");
                done();
            });
        });

        it("render a case offline", function(done){
            bizagi.util.setContext({
                isOfflineForm: true
            });
            $.when($("#render-content").triggerHandler("routing")).done(function(){
                expect(webpartRender.dataService.routing.getRoute.calls.count()).toEqual(1);
                expect(bizagi.webpart.publish.calls.count()).toEqual(2);
                expect(bizagi.webpart.publish.calls.argsFor(0)[0]).toEqual("render-case");
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].displayName).toEqual(bizagi.context.displayName);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].idCase).toEqual(bizagi.context.idCase);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].idTask).toEqual(bizagi.context.idTask);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].fromTask).toEqual(bizagi.context.fromTask);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].isOfflineForm).toEqual(bizagi.context.isOfflineForm);
                expect(bizagi.webpart.publish.calls.argsFor(0)[1].formsRenderVersion).toEqual(bizagi.context.formsRenderVersion);
                expect(bizagi.webpart.publish.calls.argsFor(1)[0]).toEqual("bz-summary-case");
                expect(webpartRender.publish.calls.argsFor(0)[0]).toEqual("bz-update-list-cases");
                bizagi.util.setContext({
                    isOfflineForm: false
                });
                done();
            });
        });

        it("renders async case", function(done){
            var getWorkItems = webpartRender.dataService.getWorkitems;
            var getAsynchExecutionState = webpartRender.dataService.getAsynchExecutionState;
            jQuery.extend(webpartRender.dataService, {
                getWorkitems: function () {
                    return {
                        "idCase": 651,
                        "idWorkFlow": 2,
                        "radNumber": "651",
                        "hasGlobalForm": "false",
                        "formsRenderVersion": 2,
                        "idCaseForGlobalForm": 651,
                        "msgTask": "",
                        "workItems": [
                            {
                                "idWorkItem": 963,
                                "idCase": 651,
                                "idTask": 8,
                                "displayName": "Task 1",
                                "estimatedSolutionDate": "08/26/2014 15:28",
                                "isAsynch": "true",
                                "taskType": "UserInteraction"
                            }
                        ],
                        "subProcesses": []
                    };
                }, getAsynchExecutionState: function(){
                    return {
                        'radNumber': 12,
                        'state': "Finished",
                        'errorMessage': ""
                    };
                }
            });

            $.when($("#render-content").triggerHandler("routing")).done(function(){
                expect(bizagi.webpart.publish.calls.count()).toEqual(1);
                expect(bizagi.webpart.publish.calls.argsFor(0)[0]).toEqual("render-async");
                expect(webpartRender.async.executeAsync.calls.count()).toEqual(1);

                //restores method
                jQuery.extend(webpartRender.dataService, {
                    getWorkitems: getWorkItems,
                    getAsynchExecutionState: getAsynchExecutionState
                });
                done();
            });
        });

        it("pops up routing widget", function(done){
            var getWorkItems = webpartRender.dataService.getWorkitems;
            jQuery.extend(webpartRender.dataService, {
                getWorkitems: function () {
                    return {
                        "idCase": 651,
                        "idWorkFlow": 2,
                        "radNumber": "651",
                        "hasGlobalForm": "false",
                        "formsRenderVersion": 2,
                        "idCaseForGlobalForm": 651,
                        "msgTask": "",
                        "workItems": [
                            {
                                "idWorkItem": 963,
                                "idCase": 651,
                                "idTask": 8,
                                "displayName": "Task 1",
                                "estimatedSolutionDate": "08/26/2014 15:28",
                                "isAsynch": "false",
                                "taskType": "UserInteraction"
                            },
                            {
                                "idWorkItem": 965,
                                "idCase": 651,
                                "idTask": 12,
                                "displayName": "Task 2",
                                "estimatedSolutionDate": "08/26/2014 15:29",
                                "isAsynch": "false",
                                "taskType": "UserInteraction"
                            }
                        ],
                        "subProcesses": []
                    };
                }
            });

            $.when($("#render-content").triggerHandler("routing")).done(function(){
                expect(bizagi.webpart.publish.calls.count()).toEqual(1);
                expect(bizagi.webpart.publish.calls.argsFor(0)[0]).toEqual("showDialog");

                //restores method
                jQuery.extend(webpartRender.dataService, {
                    getWorkitems: getWorkItems
                });
                done();
            });
        });
    });

});
