﻿{
  "form": {
    "properties": {
      "id": "F6A47A99-DF2A-4460-B3AF-53AF443C2BE7",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "render": {
          "properties": {
            "id": "ca531720-20f1-430e-8a6f-aa5ae4062fdb",
            "type": "date",
            "xpath": "asimpleprocess.dateField",
            "displayName": "dateField",
            "dataType": 12,
            "helpText": ""
          }
        }
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "wemj2tqr41ylvxlior3zeco2",
    "pageCacheId": 1519089037
  },
  "type": "form"
}