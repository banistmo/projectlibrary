{
  "form": {
    "properties": {
      "id": "22B33C56-518E-44F3-B36E-69F4D173C6C2",
      "helpText": "help text...",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "render": {
          "properties": {
            "id": "0095c7e8-b658-4509-8757-1399e87bdc8c",
            "type": "grid",
            "xpath": "AsociationControl.CollectGuarentees",
            "displayName": "CollectGuarentees",
            "helpText": "",
            "submitOnChange": true,
            "data": {
              "records": 0,
              "total": 0,
              "rows": [],
              "editable": [],
              "visible": [],
              "page": 1
            }
          },
          "elements": [
            {
              "render": {
                "properties": {
                  "id": "4b84afb8-73c1-4877-9bc2-4ce2e6a0bd6a",
                  "type": "columnText",
                  "xpath": "GuaranteeName",
                  "xpath-column": "GuaranteeName",
                  "displayName": "GuaranteeName",
                  "maxLength": 50,
                  "dataType": 15,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true,
                  "autoExtend": false
                }
              }
            }
          ]
        }
      },
      {
        "render": {
          "properties": {
            "id": "f3024f3f-0046-4ee3-8697-3591a7a3a88d",
            "type": "grid",
            "xpath": "AsociationControl.CollectProducts",
            "displayName": "CollectProducts",
            "helpText": "",
            "submitOnChange": true,
            "data": {
              "records": 0,
              "total": 0,
              "rows": [],
              "editable": [],
              "visible": [],
              "page": 1
            },
            "textFormat": {}
          },
          "elements": [
            {
              "render": {
                "properties": {
                  "id": "40fb4de5-567d-4249-a16d-1b04cf40e393",
                  "type": "columnText",
                  "xpath": "ProductName",
                  "xpath-column": "ProductName",
                  "displayName": "ProductName",
                  "maxLength": 50,
                  "dataType": 15,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true,
                  "autoExtend": false
                }
              }
            },
            {
              "render": {
                "properties": {
                  "id": "6809429a-2070-4b34-8b3c-a7e15312769d",
                  "type": "columnMoney",
                  "xpath": "ProductValue",
                  "xpath-column": "ProductValue",
                  "displayName": "ProductValue",
                  "dataType": 8,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true
                }
              }
            },
            {
              "render": {
                "properties": {
                  "id": "66989d64-6447-4cbf-98b2-f0d5a58849e4",
                  "type": "columnNumber",
                  "xpath": "Iva",
                  "xpath-column": "Iva",
                  "displayName": "Iva",
                  "allowDecimals": true,
                  "dataType": 10,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true
                }
              }
            },
            {
              "render": {
                "properties": {
                  "id": "e5fb5621-a1b7-438f-80bb-96e89fe68219",
                  "type": "columnMoney",
                  "xpath": "Total",
                  "xpath-column": "Total",
                  "displayName": "Total",
                  "dataType": 8,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true
                }
              }
            },
            {
              "render": {
                "properties": {
                  "id": "eb831f25-063e-4c26-9c6f-150796179057",
                  "type": "columnButton",
                  "xpath-column": "constant(\"\")",
                  "displayName": "columnbutton",
                  "caption": "Clip",
                  "submitOnChange": false,
                  "totalize": {
                    "format": {}
                  },
                  "hasTotalizer": true,
                  "preventDefault": true
                }
              }
            }
          ]
        }
      },
      {
        "render": {
          "properties": {
            "id": "0310d3b8-6a7c-4840-a088-6adc0c2c9878",
            "type": "association",
            "xpath": "AsociationControl.CollectGuarentees.GuaranteeToProduct",
            "displayName": "GuaranteeToProduct",
            "leftData": [],
            "leftFactName": "GuaranteeToProduct",
            "rightData": [],
            "leftLabel": "Ricardo    ",
            "rightLabel": "OscarO",
            "leftXpath": "AsociationControl.CollectGuarentees",
            "rightXpath": "AsociationControl.CollectProducts",
            "rightFactName": "ProductToGuarantee",
            "value": [],
            "editable": false,
            "dataType": 24
          }
        }
      }
    ],
    "actions": [
      {
        "conditions": {
          "expressions": [
            {
              "simple": {
                "propertyToUpdate": "renderId",
                "renderId": "AsociationControl.CollectProducts[].eb831f25-063e-4c26-9c6f-150796179057",
                "operator": "on-press",
                "argumentType": "const"
              }
            }
          ]
        },
        "commands": [
          {
            "command": "submit-data",
            "argumentType": "const"
          },
          {
            "command": "execute-rule",
            "argument": "{\"guid\":\"86cc49d8-9f6a-415e-a3ad-f118e9ef8cec\",\"label\":\"Producto\"}",
            "argumentType": "const"
          },
          {
            "xpath": "AsociationControl.CollectProducts[].ProductName",
            "command": "background-color",
            "argument": "#d24848",
            "argumentType": "const"
          },
          {
            "xpath": "AsociationControl.CollectProducts[].Total",
            "command": "refresh",
            "argumentType": "const"
          }
        ],
        "dependencies": [
          "AsociationControl.CollectProducts[].eb831f25-063e-4c26-9c6f-150796179057"
        ],
        "elseCommands": []
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "ybaqfjpgkyxl5m5q2akscwyz",
    "pageCacheId": 752571860
  },
  "type": "form"
}