
bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
)
    .then(function () {

        //$.mockjaxSettings.responseTime = 5000; //4000; //10; //2000;
        // DUMMIES
        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    if(CONTROL_EDITABLE) {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/cascadingCombo/dummy.rendering.cascadingcombo.txt",
                            transform: function (response, originalSettings) {
                                return response;
                            }
                        };
                    }
                    else{
                        return {
                            mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/cascadingCombo/dummy.rendering.cascadingcombo.noeditable.txt",
                            transform: function (response, originalSettings) {
                                return response;
                            }
                        };
                    }
                }
            }

            //Multiaction Continente - Pais - Ciudades

            if (settings.url.indexOf("Rest/Handlers/MultiAction") > -1) {
                if (settings.data.h_action === "multiaction") {
                    var data = JSON.parse(settings.data.h_actions);
                    if (data[0].h_action == "PROCESSPROPERTYVALUE") {
                        if (data[0].h_idRender == "Continentes"){
                            return {
                                mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/dummy.data.continents.txt",
                                transform: function(response, originalSettings){
                                    var arrayResponse = [];
                                    response.tag = data[0].h_tag;
                                    arrayResponse.push(response);
                                    return arrayResponse;
                                }
                            };
                        }

                        if (data[0].h_idRender == "Paises") {
                            return {
                                mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/dummy.data.countries.txt",
                                transform: function(response, originalSettings){
                                    var arrayResponse = [];
                                    function isInContinent(element) {
                                        return element.continent == data[0].p_parent;
                                    }
                                    response.tag = data[0].h_tag;
                                    response.result = response.result.filter(isInContinent);
                                    arrayResponse.push(response);
                                    return arrayResponse;
                                }
                            };
                        }

                        if (data[0].h_idRender == "Ciudades") {
                            return {
                                mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/dummy.data.cities.txt",
                                transform: function(response, originalSettings){
                                    var arrayResponse = [];
                                    function isInContinent(element) {
                                        return element.country == data[0].p_parent;
                                    }
                                    response.tag = data[0].h_tag;
                                    response.result = response.result.filter(isInContinent);
                                    arrayResponse.push(response);
                                    return arrayResponse;
                                }
                            };
                        }
                    }
                }
            }
        });
    });