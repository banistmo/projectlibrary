{
  "form": {
    "properties": {
      "id": "67752B4A-F178-4A30-8E7E-8B656C39D217",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "render": {
          "properties": {
            "id": "a6937498-b4d9-42c3-96cd-cd12a55cdfd5",
            "type": "boolean",
            "xpath": "yesnoprocess.yesno1",
            "displayName": "yesno1",
            "helpText": "help test",
            "dataType": 5,
            "required": true
          }
        }
      },
      {
        "render": {
          "properties": {
            "id": "f9fc0e40-f713-49da-a460-6b3db86c4090",
            "type": "boolean",
            "xpath": "yesnoprocess.yesno2",
            "displayName": "yesno2",
            "helpText": "",
            "dataType": 5,
            "value": "False"
          }
        }
      },
      {
        "render": {
          "properties": {
            "id": "ecfa9290-b69c-4877-a7d8-4fc5f231ac74",
            "type": "boolean",
            "display": "check",
            "xpath": "yesnoprocess.yesno3",
            "displayName": "yesno3",
            "helpText": "",
            "dataType": 5,
            "required": true
          }
        }
      },
      {
        "render": {
          "properties": {
            "id": "4d282c65-728e-40c9-81af-4be272a4ad1f",
            "type": "boolean",
            "display": "check",
            "xpath": "yesnoprocess.yesno4",
            "displayName": "yesno4",
            "helpText": "",
            "dataType": 5,
            "value": "False"
          }
        }
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "siedmt51oz3g3xosy5lvpvv4",
    "pageCacheId": 1612105794
  },
  "type": "form"
}