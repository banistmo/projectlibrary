﻿{
  "form": {
    "properties": {
      "id": "0C95D93B-8E69-4D02-943B-5259603CEB39",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "container": {
          "properties": {
            "id": "f92ed095-e659-4217-9fc7-d2d6d0a98cbe",
            "type": "horizontal"
          },
          "elements": [
            {
              "container": {
                "properties": {
                  "id": "0c6b4dfe-da6b-4e0d-8217-a1b76e496edb",
                  "type": "panel",
                  "width": "50%"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "0b6e5e3f-27de-43e4-82a7-3d6cd62040c1",
                        "type": "combo",
                        "xpath": "actionOnGrid2.combo",
                        "value": [],
                        "displayName": "Moneda",
                        "dataType": 2,
                        "data": [
                          {
                            "id": 1,
                            "value": "US"
                          },
                          {
                            "id": 2,
                            "value": "EUR"
                          },
                          {
                            "id": 3,
                            "value": "GBP"
                          },
                          {
                            "id": 4,
                            "value": "COP"
                          }
                        ],
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    ],
    "actions": [
      {
        "conditions": {
          "operator": "or",
          "expressions": [
            {
              "simple": {
                "xpath": "actionOnGrid2.grid[].intA",
                "operator": "changes",
                "argumentType": "const"
              }
            },
            {
              "simple": {
                "xpath": "actionOnGrid2.grid[].intB",
                "operator": "changes",
                "argumentType": "const"
              }
            },
            {
              "simple": {
                "xpath": "actionOnGrid2.combo",
                "operator": "changes",
                "argumentType": "const"
              }
            }
          ]
        },
        "commands": [
          {
            "command": "submit-data",
            "argumentType": "const"
          },
          {
            "command": "execute-rule",
            "argument": "{\"guid\":\"8a868f08-725e-4127-849f-e405742f8a3b\",\"label\":\"set total value\"}",
            "argumentType": "const"
          },
          {
            "xpath": "actionOnGrid2.grid[].intC",
            "command": "refresh",
            "argumentType": "const"
          }
        ],
        "dependencies": [
          "actionOnGrid2.grid[].intA",
          "actionOnGrid2.grid[].intB",
          "actionOnGrid2.combo"
        ],
        "elseCommands": []
      },
      {
        "conditions": {
          "expressions": [
            {
              "simple": {
                "xpath": "actionOnGrid2.boolean1",
                "operator": "changes",
                "argumentType": "const"
              }
            }
          ]
        },
        "commands": [
          {
            "container": "c46704d1-c911-451c-918d-d1456aff7000",
            "command": "refresh",
            "argumentType": "const"
          }
        ],
        "dependencies": [
          "actionOnGrid2.boolean1"
        ],
        "elseCommands": []
      },
      {
        "conditions": {
          "expressions": [
            {
              "simple": {
                "xpath": "actionOnGrid2.boolean2",
                "operator": "changes",
                "argumentType": "const"
              }
            }
          ]
        },
        "commands": [
          {
            "xpath": "actionOnGrid2.string2",
            "command": "refresh",
            "argumentType": "const"
          }
        ],
        "dependencies": [
          "actionOnGrid2.boolean2"
        ],
        "elseCommands": []
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "wemj2tqr41ylvxlior3zeco2",
    "pageCacheId": 1891780353
  },
  "type": "form"
}