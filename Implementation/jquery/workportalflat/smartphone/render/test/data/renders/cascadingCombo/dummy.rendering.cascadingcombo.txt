﻿{
  "form": {
    "properties": {
      "id": "491496F9-3E41-4AF3-ACA2-66F53C2DB667",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "render": {
          "properties": {
            "id": "Continentes",
            "type": "cascadingCombo",
            "xpath": "combosoc.Ciudades.Pais.Continente",
            "value": [],
            "displayName": "Continente",
            "dataType": 2,
            "helpText": "Continentes del mundo"
          }
        }
      },
      {
        "render": {
          "properties": {
            "id": "Paises",
            "type": "cascadingCombo",
            "xpath": "combosoc.Ciudades.Pais",
            "value": [],
            "displayName": "Pais",
            "dataType": 2,
            "helpText": "Paises del continente",
            "parentCombo": "Continentes"
          }
        }
      },
      {
        "render": {
          "properties": {
            "required": true,
            "id": "Ciudades",
            "type": "cascadingCombo",
            "xpath": "combosoc.Ciudades",
            "value": [],
            "displayName": "Ciudades",
            "dataType": 2,
            "helpText": "Ciudades del pais",
            "parentCombo": "Paises"
          }
        }
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "wemj2tqr41ylvxlior3zeco2",
    "pageCacheId": 1625327523
  },
  "type": "form"
}