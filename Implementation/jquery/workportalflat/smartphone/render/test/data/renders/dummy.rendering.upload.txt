﻿{
  "form": {
    "properties": {
      "id": "C6626026-6B71-4508-95A5-36B700B3E23E",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "render": {
          "properties": {
            "id": "ec85436a-14c2-4db4-9e29-a121ae8dcdf0",
            "type": "upload",
            "xpath": "ImageTest.Image",
            "value": [],
            "displayName": "Image",
            "required": true,
            "dataType": 4,
            "labelAlign": "right",
            "valueAlign": "right",
            "helpText": ""
          }
        }
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "wemj2tqr41ylvxlior3zeco2",
    "pageCacheId": 274191323
  },
  "type": "form"
}