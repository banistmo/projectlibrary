/**
 * Created by LuisCE on 12/06/2014.
 */
/*
 *   Name: BizAgi Workportal Smarthpone Routing controller
 *   Author: luisce
 *   Comments:
 *   -   This script will provide action routing
 */

// Auto extend
$.Class.extend("bizagi.workportal.action.routing", {

}, {
    /*
    *   Constructor
    */
    init: function (params) {
        var self = this;
        // Set data service
        this.dataService = params.dataService;

        // Set l10n service
        this.resources = params.resources;
        this.getResource = params.getResource;
        /* istanbul ignore next: untestable */
        bizagi.webpart.subscribe('changeCase', function (e, params) {
            bizagi.util.smartphone.startLoading();
            self.executeRouting(params);
        });
    },
    /*
    *   Executes the action
    *   Could return a deferred
    */
    executeRouting: function (params) {
        var self = this;
        self.params = params || {};
        var deferred = $.Deferred();
        /* New implementation of routing*/

        $.when(self.dataService.routing.getRoute(params)).done(function (route) {
            route = route || {};

            if (self.params.isOfflineForm != "undefined" && self.params.isOfflineForm === true) {
                route.moduleParams = $.extend(route.moduleParams, { formsRenderVersion: self.params.formsRenderVersion, isOfflineForm: self.params.isOfflineForm, idCase: self.params.idCase, idWorkitem: self.params.idWorkitem }) || {};
            }
            else {
                route.moduleParams = route.moduleParams || {};
            }

            if ((route.moduleParams.displayName === "" || typeof route.moduleParams.displayName === 'undefined' ) && ((self.params.workItems && self.params.workItems.length > 0) || self.params.displayName)) {
                route.moduleParams = $.extend(route.moduleParams, { displayName: self.params.displayName || self.params.workItems[0].displayName });
            }


            switch (route.module) {
                case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_RENDER:
                    $.when(bizagi.webpart.publish("render-case", route.moduleParams)).done(function(){
                        deferred.resolve();
                    });
                    break;
                case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ASYNC:
                    $.when(bizagi.webpart.publish("render-async", route.moduleParams)).done(function(){
                        deferred.resolve();
                    });
                    break;
                case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ROUTING:

                    var parameters = {
                        title: self.getResource("workportal-widget-routing-window-selector"),
                        width: 670,
                        height: 400,
                        onClose: route.moduleParams.onClose
                    };
                    bizagi.util.smartphone.stopLoading();
                    bizagi.webpart.publish("showDialog", {
                        widgetName: bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ROUTING,
                        data: route.moduleParams.data,
                        modalParameters: parameters,
                        onClose: parameters.onClose
                    });
                    deferred.resolve();
                    break;
            }

        });
        return deferred.promise();
    }
});
