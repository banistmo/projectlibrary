/**
 * Created by LuisCE on 13/06/2014.
 */
/**
 * Created by LuisCE on 04/06/2014.
 */
/*
 *   Name: BizAgi Workportal Smarthpone Observer controller
 *   Author: luisce 
 *   Comments:
 *   -
 */

// Auto extend
$.Class.extend("bizagi.workportal.action.navigation", {

}, {

    /*
    *   Constructor
    */
    init: function (params) {
        var self = this;
        self.observerList = [];
        self.notifyObserverAll = false;
        self.content = $('header', params.content)
        self.id = params.content.attr("id");
    },

    getContent: function () {
        var self = this;
        return self.content;
    },

    initContextualButtons: function () {
        var self = this;
        self.currentView = bizagi.kendoMobileApplication.view().element;
        self.buttonsContextual = {};
        self.buttonsContextual.button = $(".bz-render-custom", self.currentView);
        self.buttonsContextual.back = $(".bz-render-back", self.currentView);
        self.buttonsContextual.dynamic = $(".bz-render-dynamic", self.currentView);
    },

    /*
    *Enable or disable elments include in the menu
    * and the application is hibryd it hides the log-out btn
    */
    renderNavigationItems: function () {
        var self = this;

        self.buttonsContextual.back.unbind();
        self.buttonsContextual.button.unbind();
        self.buttonsContextual.dynamic.unbind();

        /* istanbul ignore next: untestable */
        $(self.buttonsContextual.back).bind("click", function (e) {
            e.preventDefault();
            $.when(bizagi.util.autoSave()).done(function () {
                self.notifyObservers("back");
            });
        });
        /* istanbul ignore next: untestable */
        $(self.buttonsContextual.button).bind("click", function (e) {
            e.preventDefault();
            self.notifyObservers("buttom");
        });

        /* istanbul ignore next: untestable */
        $(self.buttonsContextual.dynamic).bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("#actionsheetDynamic" + self.id).data("kendoMobileActionSheet").open();
        });
    },

    dynamicButtons: function (dbuttons) {
        var self = this;
        var actionSheet = kendo.template(bizagi.templates.services.service.cachedTemplates["actionsheetDynamic-tmpl"], { useWithBlock: false });
        $(self.getContent()).append(actionSheet({ buttons: dbuttons, id: self.id }));
        return actionSheet;
    },

    setVisibleIdView: function (id) {
        this.id = id;
    },

    changeContextualButtons: function (active, args) {
        var self = this;
        if (self.buttonsContextual === undefined) {
            return;
        }

        console.info("changeContextualButtons: " + active);

        switch (active) {
            case "button":
                self.buttonsContextual.button.show();
                self.buttonsContextual.dynamic.hide();
                if (args && args.visible === false) {
                    self.buttonsContextual.button.hide();
                }
                break;
            case "dynamic":
                self.buttonsContextual.button.hide();
                self.buttonsContextual.dynamic.show();
                if (args && args.visible === false) {
                    self.buttonsContextual.dynamic.hide();
                }
                break;
            case "back":
                self.buttonsContextual.back.show();
                if (args && args.visible === false) {
                    self.buttonsContextual.back.hide();
                }
                break;
            case "refresh":
                if (args.button) {
                    self.changeContextualButtons("button"); //, { value: args.textButton });
                }
                if (args.back) {
                    self.changeContextualButtons("back");
                }
                if (args.dynamic) {
                    self.changeContextualButtons("dynamic");
                }
                break;
            case "disable":
                self.buttonsContextual.button.hide();
                self.buttonsContextual.dynamic.hide();
                self.buttonsContextual.back.hide();
                break;
        }

    },

    //Sets display and action buttons ///---------Must be set after the view was showed
    setNavigationButtons: function (context, view) {
        var self = this;
        self.initContextualButtons();
        self.renderNavigationItems();

        self = $.extend(self, context);
        var buttons, properties, params;
        var hasButtons = true;
        if (view === 'edition') {
            if ((self.form && self.form.buttons) && (context.form && context.form.buttons)) {
                buttons = self.form.buttons;
                properties = self.form.properties;
                params = {};
            } else {
                hasButtons = false;
                buttons = [];
                params = {};
            }
        } else if (view === 'preview') {
            hasButtons = false;
        } else {
            buttons = context.buttons;
            properties = context.properties;
            params = context.params;
        }

        if (!hasButtons) {
            if (view === 'preview') {
                self.changeContextualButtons("back");
            } else {
                self.changeContextualButtons("button");
                self.changeContextualButtons("back");
            }
        } else {
            if (!buttons || buttons.length === 0 || (view !== 'edition' && properties.buttons.length === 0)) {
                self.changeContextualButtons("button", { visible: false });
                self.changeContextualButtons("back");
            }

            if (params.isRefresh) {
                self.removeSuscriber(self.properties.id, false);
            }

            /*if (properties.buttons && properties.buttons.length > 2) {*/
            if (typeof ($("#actionsheetDynamic" + self.id).data('kendoMobileActionSheet')) !== 'undefined') {
                $("#actionsheetDynamic" + self.id).data('kendoMobileActionSheet').destroy();
            }

            if (buttons.length > 0) {
                var dynamic = self.dynamicButtons(properties.buttons || buttons);
                $("#actionsheetDynamic" + self.id, self.getContent()).kendoMobileActionSheet({
                    /* istanbul ignore next: untestable */'destroy': function () {
                        this.element.remove();
                    }
                });
                //The action sheet link was changed to a kendo button because the action click wasn't working
                //Kendo should release a click event eventually
                $("a[data-bz-ordinal]", "#actionsheetDynamic" + self.id).kendoMobileButton({
                    /* istanbul ignore next: untestable */click: function (e) {
                        if (e.button.data("bz-ordinal") < 0) {//default back/close if not ordinal
                            self.notifyObservers("back");
                        } else {
                            self.notifyObservers("dynamic", { ordinal: e.button.data("bz-ordinal") });
                        }
                    }
                });
                self.changeContextualButtons("dynamic");
            }
            /*  } else if (self.nextButton !== null && self.saveButton !== null && (view === 'edition' || properties.buttons.length <= 2) ) {
            */
            /*if (view === 'edition' || properties.buttons.length <= 2) {
            self.changeContextualButtons("button", { value: self.nextButton.caption });
            self.changeContextualButtons("back");
            }*/
            /*
            self.changeContextualButtons("button", { value: self.nextButton.caption });
            self.changeContextualButtons("back");
            }*/

        }

        self.setNavigationSubcribers(context, view, properties, params);

    },

    setNavigationSubcribers: function(context, view, properties, params){
        var self = this;
        if (view === 'edition') {
            $.when(self.nextSuscribe(context.properties.id || context.properties.idRender,
                function (args) {
                    return self.navigationActions(context, args, view);
                }
            )).done(function () {
                //self.changeContextualButtons("button", { value: bizagi.localization.getResource("confirmation-box-ok") });
                console.log('Process the buttoms, done...');
            });
        } else if (view === 'preview') {
            $(self.buttonsContextual.back).unbind('click');
            /* istanbul ignore next: untestable */
            $(self.buttonsContextual.back).bind("click", function (e) {
                e.preventDefault();
                bizagi.kendoMobileApplication.navigate("#:back");
            });
        } else {
            $.when(self.nextSuscribe(properties.id,
                function (args) {
                    return self.navigationActions(context, args, view, params);
                }
            )).done(function () {
                console.log('Process the buttoms, done...');
            });
        }
    },

    navigationActions: function(context, args, view, params){
        var self = this;
        var defer = new $.Deferred();
        if(view === 'edition'){
            var actualView = bizagi.kendoMobileApplication.view();

            if (args.action === "back") {
                if (context.actionCancel) {
                    $.when(context.actionCancel()).then(function (response) {
                        self.removeSuscriber(args.key);
                        defer.resolve();
                    });
                } else {
                    self.removeSuscriber(args.key);
                    defer.resolve();
                }

                bizagi.kendoMobileApplication.navigate("#:back");
                actualView.destroy();
            }

            if (args.action === "buttom") {
                $.when(context.actionSave()).then(function (response) {
                        self.removeSuscriber(args.key);
                        //TODO FIX SUBMITONCHANGE
                        /*if (context.properties.submitOnChange) {
                         context.submitOnChange();
                         }*/
                        bizagi.kendoMobileApplication.navigate("#:back");
                        actualView.destroy();
                        defer.resolve();
                    });
            }

            if (args.action === "dynamic") {
                $.when(context.actionSave()).then(function (response) {
                        self.removeSuscriber(args.key);
                        //TODO FIX SUBMITONCHANGE
                        /*if (context.properties.submitOnChange) {
                         context.submitOnChange();
                         }*/
                        bizagi.kendoMobileApplication.navigate("#:back");
                        actualView.destroy();
                        defer.resolve();
                    });
            }
        }else{
            switch (args.action) {
                case 'back':
                    self.removeSuscriber(args.key);
                    if (self.observerList.length > 1) {
                        bizagi.kendoMobileApplication.navigate("#:back");
                    }
                    else {
                        bizagi.kendoMobileApplication.navigate('taskFeed');
                    }
                    defer.resolve();
                    break;
                case 'buttom':
                    var valid = bizagi.util.parseBoolean(context.nextButton.validate) ? context.validationController.performValidations() : true;
                    context.processButton(context.nextButton);
                    // Se podria usar un trigger para notificar cuando no sea necesario remover el evento
                    // fix for complexgateway next
                    var properties = params.originalParams;
                    if (typeof (properties.dataComplexGateway) === 'undefined' && valid) {
                        self.removeSuscriber(args.key);
                    }
                    defer.resolve();
                    break;
                case 'dynamic':
                    context.processButton(context.properties.buttons[args.ordinal]);
                    defer.resolve();
                    break;
            }
        }
        return defer.promise();
    },

    getActualStateContextualButtons: function () {
        var self = this;
        if (self.buttonsContextual === undefined) {
            return false;
        }

        return {
            button: self.buttonsContextual.button.is(':visible'),
            back: (self.buttonsContextual.back.is(':visible')),
            dynamic: self.buttonsContextual.dynamic.is(':visible')
        };
    },
    setTypeObserver: function (notifyAll) {
        this.notifyObserverAll = notifyAll;
    },
    nextSuscribe: function (key, callback) {
        var self = this;
        self.changeContextualButtons("back", { visible: true });
        console.info("next Suscribe: " + (self.observerList.length + 1));
        return self.observerList.push({ "key": key, "callback": callback, "beforeButtons": self.getActualStateContextualButtons() });
    },
    emptySuscribers: function () {
        this.observerList = [];
        this.changeContextualButtons("back", { visible: false });

    },
    undoRemoveLastSuscriber: function () {
        var self = this;
        if (typeof self.lastElementRemoveHistory === 'undefined' || self.lastElementRemoveHistory === null) {
            return;
        }
        self.changeContextualButtons("back", { visible: true });
        self.observerList.push(self.lastElementRemoveHistory);
        self.lastElementRemoveHistory = null;
    },
    removeSuscriber: function (key, activeBeforeButtons) {
        var self = this;
        var tmpbefore;

        if (key === undefined) {
            return;
        }

        this.observerList = jQuery.grep(self.observerList, function (value) {
            if (value.key == key) {
                tmpbefore = value.beforeButtons;
                self.lastElementRemoveHistory = value;
            }
            return value.key != key;
        });
        var acbuttons = (activeBeforeButtons !== undefined) ? activeBeforeButtons : true;
        if (acbuttons) {
            self.changeContextualButtons("refresh", tmpbefore);
        }
    },

    removeLastSuscriber: function (activeBeforeButtons) {
        var self = this;
        if (typeof (self.observerList[self.observerList.length - 1]) != "object") {
            return;
        }
        var observer = self.observerList[self.observerList.length - 1];
        var key = observer.key;
        self.removeSuscriber(key, activeBeforeButtons);

    },

    getLastSuscriber: function () {

        var self = this;
        if (typeof (self.observerList[self.observerList.length - 1]) != "object") {
            return null;
        }
        return self.observerList[self.observerList.length - 1];
    },

    notifyObservers: function (action, argsExtend) {
        var self = this;
        if (!this.notifyObserverAll) {
            this.notifyLastObserver(action, argsExtend);
            return;
        }
        for (var itobserver in this.observerList) {
            var oblist = this.observerList;
            var observer = oblist[itobserver];
            var args = {
                "index": itobserver,
                "action": action,
                "key": observer.key,
                "lastKey": oblist[oblist.length - 1].key
            };
            if (argsExtend) {
                args = jQuery.extend(args, argsExtend);
            }

            self.notify(observer.callback, args);
        }
    },

    notifyLastObserver: function (action, argsExtend) {
        var self = this;
        var defer = new $.Deferred();
        if (typeof (this.observerList[this.observerList.length - 1]) != "object") {
            return;
        }
        var observer = this.observerList[this.observerList.length - 1];
        var index = this.observerList.length - 1;
        var args = {
            "index": index,
            "action": action,
            "key": observer.key,
            beforeButtons: observer.beforeButtons
        };

        if (argsExtend) {
            args = jQuery.extend(args, argsExtend);
        }


        $.when(self.notify(observer.callback, args)).done(function(){
            defer.resolve();
        });
        return defer.promise();
    },

    notify: function (callbacks, args) {
        if (jQuery.isArray(callbacks)) {
            for (var identifier in callbacks)
                callbacks[identifier](args);
            return;
        }
        return callbacks(args);
    }
});