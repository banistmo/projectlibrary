﻿/*
 *   Name: Bizagi newcase webpart
 *   Author: luisce
 *   Comments:
 *   -   This script will define the newcase webpart
 */

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.newcase", {}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        
        self._super(workportalFacade, dataService, initialParams);
        bizagi.webpart.subscribe("bz-new-case", function () {
            /* istanbul ignore next: untestable */
            $("#bz-right-drawer").data("kendoMobileDrawer").show();
        });
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();
        var l10n = {
            "l10n":
            {
                newcase: bizagi.localization.getResource("workportal-widget-newcase-title")
            }
        };
        var newcaseTemplate = kendo.template(self.getTemplate('newcase'), { useWithBlock: false });
        self.content = newcaseTemplate();
        $("#content-right-drawer", self.canvas).empty();
        $("#content-right-drawer", self.canvas).html(self.content);
        $('.km-content:visible').data('kendoMobileScroller').reset();
        var buttongroup = $('#select-process-type', self.canvas).kendoMobileButtonGroup();
        var breadCrumb = params.breadCrumb || [{ name: l10n.l10n.newcase}];
        self.changeBreadcrumb(breadCrumb);
        var status = params.status || "recent";

        if (status === "recent") {
            buttongroup.data("kendoMobileButtonGroup").select(0);
        } else {/* istanbul ignore next: untestable */
            buttongroup.data("kendoMobileButtonGroup").select(1);
        }

        var templateParams = { status: status, idApp: params.idApp, idCategory: params.idCategory };
        $(".bz-newcase-list", self.canvas).kendoMobileListView({
            filterable: false,
            template: self.getListTemplate(templateParams),
            click: function (element) {
                /* istanbul ignore next: untestable */
                var renderJson = self.selectListElement(element, breadCrumb);

                /* istanbul ignore next: untestable */
                if (renderJson.idWfClass) {
                    self.createNewCase(renderJson);
                } else {
                    self.renderContent(renderJson);
                }
            }
        });


        /*var dataSource = self.getData(templateParams);*/
        var dataSource = {};
        bizagi.kendoMobileApplication.showLoading();
        $.when(self.getData(templateParams)).done(function (data) {
            dataSource = data;
            $(".bz-newcase-list", self.canvas).data("kendoMobileListView").setDataSource(dataSource);
            bizagi.util.smartphone.stopLoading();
            deferred.resolve();
        });

        /* istanbul ignore next: untestable */
        $('.bz-grouped-buttons-all, .bz-grouped-buttons-recent', self.canvas).click(function () {
            //remove search field
            $('.km-filter-form', self.canvas).remove();
            $(".bz-newcase-list", self.canvas).data("kendoMobileListView").destroy();
            if ($(this).hasClass("bz-grouped-buttons-all")) {
                self.renderContent({ "status": "all" });
            } else {
                self.renderContent({ "status": "recent" });
            }
        });

        //sets the search
        /* istanbul ignore next: untestable */
        $('.bz-newcase-search', self.canvas).keyup(function () {
            dataSource._filter = {
                logic: "or",
                filters: [
                    { ignoreCase: true,
                        field: 'appName',
                        operator: "contains",
                        value: this.value
                    },
                    { ignoreCase: true,
                        field: "categoryName",
                        operator: "contains",
                        value: this.value
                    },
                    { ignoreCase: true,
                        field: "displayName",
                        operator: "contains",
                        value: this.value
                    }
                ]
            };
            $(".bz-newcase-list", self.canvas).data("kendoMobileListView").setDataSource(dataSource);
        });

        return deferred.promise();
    },

    selectListElement: function (element, breadCrumb) {
        var renderJson = {};
        if ($("a", element.item).data("appid")) {
            $(".bz-newcase-list").data("kendoMobileListView").destroy();
            $('.km-filter-form').remove();
            breadCrumb.push({ name: $("#categoryName", element.item).val(), value: $("a", element.item).data("appid") });
            renderJson = { "status": "all", idApp: $("a", element.item).data("appid"), breadCrumb: breadCrumb };
        } else if ($("#isProcess", element.item).val() === "false") {
            $(".bz-newcase-list").data("kendoMobileListView").destroy();
            $('.km-filter-form').remove();
            breadCrumb.push({ name: $("#categoryName", element.item).val(), value: $("#idCategory", element.item).val() });
            renderJson = { "status": "all", idCategory: $("#idCategory", element.item).val(), breadCrumb: breadCrumb };
        } else {
            var idClass = $("#idWFClass", element.item).val() ? $("#idWFClass", element.item).val() : $("#idCategory", element.item).val();
            var newCaseParams = {};
            newCaseParams.idWfClass = idClass;
            $("#bz-right-drawer").data("kendoMobileDrawer").hide();
            bizagi.util.smartphone.startLoading();
            renderJson = newCaseParams;
        }
        return renderJson;
    },

    getData: function (params) {
        var self = this;
        var getParams = {};
        var deferred = new $.Deferred();
        if (params.idCategory) {
            getParams = { idCategory: params.idCategory };
        } else if (params.idApp) {
            getParams = { idApp: params.idApp };
        }

        if (params.status === "recent") {
            $.when(
                self.dataService.getRecentProcesses()
            ).done(function (data) {
                self.dataSource = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            options.success(data.processes);
                        }
                    }
                });
                deferred.resolve(self.dataSource);
            });
        } else {
            $.when(
                self.dataService.getCategories(getParams)
            ).done(function (data) {
                self.dataSource = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            options.success(data.category);
                        }
                    }
                });
                deferred.resolve(self.dataSource);
            });
        }
        return deferred.promise();
    },

    getListTemplate: function (params) {
        var self = this;
        var template = '';
        if (params.status === "recent") {
            template = self.getTemplate('newCase-categories-recent-process');
        } else {
            template = self.getTemplate('newCase-categories-tree');
        }

        return template;
    },

    changeBreadcrumb: function (params) {
        var self = this;
        var breadCrumb = kendo.template(self.getTemplate('newcase-breadcrum'), { useWithBlock: false });
        $('.bz-newcase-title', self.canvas).html(breadCrumb(params));
        $('.bz-newcase-title', self.canvas).click(function () {
            /* istanbul ignore next: untestable */
            self.renderContent(self.clickBreadcrumb(params));
        });
    },

    clickBreadcrumb: function (params) {
        var breadCrumb = {};
        if (params.length >= 3) {
            params.pop();
            breadCrumb = { "status": "all", idCategory: params[params.length - 1].value, breadCrumb: params };
        }
        else if (params.length === 2) {
            params.pop();
            breadCrumb = { "status": "all", idApp: params[params.length - 1].value, breadCrumb: params };
        }
        else if (params.length == 1) {
            breadCrumb = { "status": "all" };
        }
        return breadCrumb;
    },

    createNewCase: function (newCaseParams) {
        var self = this;
        var deferred = new $.Deferred();
        self.dataService.createNewCase(newCaseParams)
            .done(function (resp) {
                self.publish("changeCase", resp);

                //Activa flag para actualizar lista de casos en la proxima visualización
                self.publish("bz-update-list-cases");
                deferred.resolve();
            })
            ./* istanbul ignore next: untestable fail */fail(function (msg) {
                console.log("error cargando la forma");
                bizagi.util.smartphone.stopLoading();
                var response = $.parseJSON(msg.responseText);
                alert(response.message);
                deferred.fail();
            });
        return deferred;
    }
});
