﻿describe("Testing webpart New Case", function () {
    var webpartNewCase;
    var canvas = $('body');

    it("Executing webpart", function (done){
        var workportal = bizagi.collection.get("workportal");

        $(document).one('bizagi-webpart-created', function(e, wp){
            webpartNewCase = wp.webpart;
            spyOn(webpartNewCase, 'renderContent').and.callThrough();
            spyOn(webpartNewCase, 'getData').and.callThrough();
            spyOn(webpartNewCase, 'getListTemplate').and.callThrough();
            spyOn(webpartNewCase, 'changeBreadcrumb').and.callThrough();
        });

        workportal.executeWebpart({
            webpart: "newcase",
            canvas: canvas
        }).done(function (wp) {
            webpartNewCase = wp.webpart;
            $("#bz-right-drawer").data("kendoMobileDrawer").show();
            bizagi.util.smartphone.stopLoading();
            expect(webpartNewCase.renderContent.calls.count()).toEqual(1);
            expect(webpartNewCase.getData.calls.count()).toEqual(1);
            expect(webpartNewCase.getListTemplate.calls.count()).toEqual(1);
            expect(webpartNewCase.changeBreadcrumb.calls.count()).toEqual(1);

            done();
            //tracks that the spy was called
        });
    });

    it("Test instance correct webpart", function () {
        expect(typeof webpartNewCase).toEqual('object');
    });

    it("Test of renderContent", function () {
        expect(bizagi.webparts.instances.filter(function(el){
            return el.name == "bizagi.workportal.webparts.newcase";
        }).length).toBeGreaterThan(0);
    });

    describe("Ensure passed data", function(){
        describe("as recent", function(){
            var dataSourceRecent;
            it("loads data", function(done){
                $.when( webpartNewCase.getData({status: "recent"})).done(function (data) {
                    data.fetch();
                    dataSourceRecent = data.view();
                    done();
                });
            });

            it("has the same number of new cases as expectations", function(){
                expect(dataSourceRecent.length).toBeGreaterThan(4);
                expect(dataSourceRecent.length).toBeLessThan(11);
            });

            it("contains the right json definition and format", function(){
                for(i = 0; i < dataSourceRecent.length; i++){
                    expect(dataSourceRecent[i].idWFClass).toBeDefined();
                    expect(dataSourceRecent[i].displayName).toBeDefined();
                    expect(dataSourceRecent[i].renderVersion).toBeDefined();
                }
            });
        });

        describe("as all aplications", function(){
            var dataSourceAllGroup;
            it("loads data", function(done){
                $.when(webpartNewCase.getData({status: "all"}) ).done(function (data) {
                    data.fetch();
                    dataSourceAllGroup = data.view();
                    done();
                });
            });

            it("has the same number of new cases as expectations", function(){
                expect(dataSourceAllGroup.length).toEqual(5);
            });

            it("contains the right json definition and format", function(){
                for(i = 0; i < dataSourceAllGroup.length; i++){
                    expect(dataSourceAllGroup[i].appId).toBeDefined();
                    expect(dataSourceAllGroup[i].appName).toBeDefined();
                    expect(dataSourceAllGroup[i].spritePosition).toBeDefined();
                }
            });
        });

        describe("as all processes", function(){
            var dataSourceAllApp;
            it("loads data", function(done){
                $.when(webpartNewCase.getData({status: "all", idApp: 1 })).done(function (data) {
                    data.fetch();
                    dataSourceAllApp = data.view();
                    done();
                });
            });

            it("has the same number of new cases as expectations", function(){
                expect(dataSourceAllApp.length).toBeGreaterThan(0);
                expect(dataSourceAllApp.length).toBeLessThan(101);
            });

            it("contains the right json definition and format", function() {
                for (i = 0; i < dataSourceAllApp.length; i++) {
                    expect(dataSourceAllApp[i].categoryName).toBeDefined();
                    expect(dataSourceAllApp[i].description).toBeDefined();
                    expect(dataSourceAllApp[i].hasOfflineForm).toBeDefined();
                    expect(dataSourceAllApp[i].idCategory).toBeDefined();
                    expect(dataSourceAllApp[i].isProcess).toBeDefined();
                    expect(dataSourceAllApp[i].renderVersion).toBeDefined();
                }
            });
        });

        describe("as all process taks", function(){
            var dataSourceAllProccess;
            it("loads data", function(done){
                $.when(webpartNewCase.getData({status: "all", idCategory: 1})).done(function (data) {
                    data.fetch();
                    dataSourceAllProccess = data.view();
                    done();
                });
            });

            it("has the same number of new cases as expectations", function(){
                expect(dataSourceAllProccess.length).toBeGreaterThan(1);
                expect(dataSourceAllProccess.length).toBeLessThan(51);
            });

            it("contains the right json definition and format", function() {
                for (i = 0; i < dataSourceAllProccess.length; i++) {
                    expect(dataSourceAllProccess[i].categoryName).toBeDefined();
                    expect(dataSourceAllProccess[i].description).toBeDefined();
                    expect(dataSourceAllProccess[i].hasOfflineForm).toBeDefined();
                    expect(dataSourceAllProccess[i].idCategory).toBeDefined();
                    expect(dataSourceAllProccess[i].isProcess).toBeDefined();
                    expect(dataSourceAllProccess[i].renderVersion).toBeDefined();
                }
            });
        });
    });

    describe("Get the right template", function(){
        beforeEach(function(){
            spyOn(webpartNewCase, 'getTemplate').and.callThrough();
        });

        it("passing recent process", function(){
            var template = webpartNewCase.getListTemplate({status: 'recent'});
            expect(webpartNewCase.getTemplate).toHaveBeenCalledWith('newCase-categories-recent-process');
            expect(template).toBeDefined();
        });

        it("passing all process", function(){
            var template = webpartNewCase.getListTemplate({status: ''});
            expect(webpartNewCase.getTemplate).toHaveBeenCalledWith('newCase-categories-tree');
            expect(template).toBeDefined();
        });
    });

    describe("Click on breadcrumb", function(){
        var param = [{ 'name': 'breacrum1', 'value': Math.floor(Math.random()*100) +1 },
            { 'name': 'breacrum2', 'value':  Math.floor(Math.random()*100) +1 },
            { 'name': 'breacrum3', 'value':  Math.floor(Math.random()*100) +1 }
        ];

        it("contain 3 crumbs", function(){
            var breadCrumb = webpartNewCase.clickBreadcrumb(param);
            expect(breadCrumb.status).toBeDefined();
            expect(breadCrumb.idCategory).toBeDefined();
            expect(breadCrumb.idApp).toBeUndefined();
            expect(breadCrumb.breadCrumb).toBeDefined();
            expect(breadCrumb.status).toBe("all");
            expect(breadCrumb.idCategory).toBe(param[param.length - 1].value);
            expect(breadCrumb.breadCrumb).toEqual(param);

        });

        it("contain 2 crumbs", function(){
            var breadCrumb = webpartNewCase.clickBreadcrumb(param);
            expect(breadCrumb.status).toBeDefined();
            expect(breadCrumb.idApp).toBeDefined();
            expect(breadCrumb.idCategory).toBeUndefined();
            expect(breadCrumb.breadCrumb).toBeDefined();
            expect(breadCrumb.status).toBe("all");
            expect(breadCrumb.idApp).toBe(param[param.length - 1].value);
            expect(breadCrumb.breadCrumb).toEqual(param);
        });

        it("contain 1 crumb", function(){
            var breadCrumb = webpartNewCase.clickBreadcrumb(param);
            expect(breadCrumb.status).toBeDefined();
            expect(breadCrumb.idApp).toBeUndefined();
            expect(breadCrumb.idCategory).toBeUndefined();
            expect(breadCrumb.breadCrumb).toBeUndefined();
            expect(breadCrumb.status).toBe("all");
        });
    });

    describe("Select an item from newcase list", function(){
        var breadcrumb = [{ 'name': 'breacrum1', 'value': Math.floor(Math.random()*100) +1 },
            { 'name': 'breacrum2', 'value':  Math.floor(Math.random()*100) +1 },
            { 'name': 'breacrum3', 'value':  Math.floor(Math.random()*100) +1 }
        ];
        var kendoListViewDestroy;
        beforeEach(function(){
            kendoListViewDestroy = $(".bz-newcase-list").data("kendoMobileListView").destroy;
            $(".bz-newcase-list").data("kendoMobileListView").destroy = function(){};
        });

        afterEach(function(){
            $(".bz-newcase-list").data("kendoMobileListView").destroy = kendoListViewDestroy;
        });

        it("with app id", function(){
            var element = {'item': $('<li data-uid="712727dd-42d8-4ce6-afc0-fe02eb67dbc0"><span class="km-icon km-process-full"></span>    <a class="bz-newprocess-link" data-appid="2"><span class="bz-newprocess"></span>app2</a>    <input type="hidden" id="hasOfflineForm" value="">    <input type="hidden" id="idCategory" value="">    <input type="hidden" id="isProcess" value="false">    <input type="hidden" id="categoryName" value="app2"></li>')};
            var renderJson = webpartNewCase.selectListElement(element, breadcrumb);
            expect(renderJson.status).toBeDefined();
            expect(renderJson.idApp).toBeDefined();
            expect(renderJson.idCategory).toBeUndefined();
            expect(renderJson.breadCrumb).toBeDefined();
            expect(renderJson.status).toBe("all");
        });

        it("with category id", function(){
            var element = {'item': $('<li data-uid="9bd4d973-ed7c-47b7-b21f-5f79e25dc042"><span class="km-icon km-process-full"></span>    <a class="bz-newprocess-link"><span class="bz-newprocess"></span>Processes</a>    <input type="hidden" id="hasOfflineForm" value="false">    <input type="hidden" id="idCategory" value="1">    <input type="hidden" id="isProcess" value="false">    <input type="hidden" id="categoryName" value="Processes"></li>')};
            var renderJson = webpartNewCase.selectListElement(element, breadcrumb);
            expect(renderJson.status).toBeDefined();
            expect(renderJson.idApp).toBeUndefined();
            expect(renderJson.idCategory).toBeDefined();
            expect(renderJson.breadCrumb).toBeDefined();
            expect(renderJson.status).toBe("all");
        });

        it("with WF id", function(){
            var element = {'item': $('<li data-uid="2de3b738-170b-45fe-9cd4-cee078ea1961"><span class="km-icon km-process-full"></span>    <a class="bz-newprocess-link"><span class="bz-newprocess"></span>acombogrilla</a>    <input type="hidden" id="hasOfflineForm" value="false">    <input type="hidden" id="idCategory" value="48">    <input type="hidden" id="isProcess" value="true">    <input type="hidden" id="categoryName" value="acombogrilla"></li>')};
            var renderJson = webpartNewCase.selectListElement(element, breadcrumb);
            expect(renderJson.idWfClass).toBeDefined();
            expect(renderJson.status).toBeUndefined();
            expect(renderJson.idApp).toBeUndefined();
            expect(renderJson.idCategory).toBeUndefined();
            expect(renderJson.breadCrumb).toBeUndefined();
        });
    });

    describe("Create new case", function(){
        beforeEach(function(){
            spyOn(webpartNewCase, 'publish').and.callThrough();
        });

        it("works", function(done){
            $.when( webpartNewCase.createNewCase({'idWfClass': 1}) ).done(function(resp){
                expect(webpartNewCase.publish.calls.count()).toEqual(2);
                expect(webpartNewCase.publish.calls.argsFor(0)).toEqual(["changeCase", jasmine.any(Object)]);
                expect(webpartNewCase.publish.calls.argsFor(1)).toEqual(["bz-update-list-cases"]);
                done();
            });
        });
    });
});

