﻿/*
*   Name:
*   Author: Oscar Osorio
*   Comments:
*

Structure to applicate plugin
<div class="isTab"> //container
    <div class="cell bz-container-items-tabs"> // represent item of tab
        <span >${displayName}</span>// the first span always is the title                                    
        <span> ........</span> // after the span can place the items you want
        <div> .........</div> // etc
    </div> <!-- end cell-->
</div><!-- end isTab-->


*/

(function ($) {
    $.fn.bztabs = function (params) {

        var self = $(this);
        params = params || {};
        params.activeTab = params.activeTab || 0;

        var activate = (params.activate && typeof params.activate === 'function') ? params.activate : function () { };

        //oculta el pop-up de tabs
        self.find(">div.isTab").hide();

        //base para construir el header de tabs
        var tabHeader = $("<div class='bz-header_select'></div>");
        //PopUp
        var popUp = self.find(">div.isTab");

        //replica los tabs para crear el header
        popUp.children().each(function (i, item) {
            tabHeader.append("<div class='bz-wp-tabs-ui' data-reference-tab='" + item.getAttribute("data-reference-tab") + "'><span>" +
                item.textContent + "</span></div>");
        });

        self.append("<div class='bz-wp-tabs-arrow-container'><div class='bz-cm-icon bz-wp-tabs-arrow'></div></div>");
        self.prepend(tabHeader);

        var tabCurrent = self.find("> .isTab > .bz-container-items-tabs").eq(params.activeTab);
        var itemCurrent = $($(tabCurrent).data("reference-tab"), self);

        itemCurrent.addClass("childrenActive");
        tabCurrent.addClass("tabSelected");
        //marcar la pestaña seleccionada en la cabecera
        var activeTab = $(".bz-wp-tabs-ui:nth-child(" + (params.activeTab + 1) + ")", tabHeader).addClass("tabSelected");

        if (typeof params.domIncluded !== 'undefined') {
            //hasta que el control sea adicionado al DOM podemos saber los anchos para agregar el icono del popup
            $.when(params.domIncluded).done(function () {
                updateTabSize();
            });
        }
        else {
            updateTabSize();
        }

        function changeTab(e, params) {
            params = params || {};
            params.origin = params.origin || "header";

            e.stopPropagation();
            e.preventDefault();

            if ($(this).hasClass("tabSelected") && params.origin !== "popUp") {
                return;
            }
            var dataRef = e.currentTarget.getAttribute("data-reference-tab");

            activeTab = $(e.currentTarget);

            //cambia la visibilidad del contenido de los tabs
            self.parent().find("> .ui-bizagi-container-tab >div.childrenActive").removeClass("childrenActive");
            var tabChildren = $(dataRef, self);
            tabChildren.addClass("childrenActive");

            tabHeader.find(">div.tabSelected").removeClass("tabSelected");

            if (popUp.css("display") === "block") {
                popUp.toggle();
            }

            if (params && params.origin === "popUp") {
                activeTab = $("[data-reference-tab='" + dataRef + "']", tabHeader);
                reorderTabs(dataRef);
            }

            activeTab.addClass("tabSelected");
            activate(e, params);
        }

        function reorderTabs(dataRef) {
            var tempTabs = tabHeader.find('.bz-wp-tabs-ui');
            var moveAtTheEnd = true;
            tempTabs.each(function (i, item) {
                if (dataRef !== item.getAttribute("data-reference-tab") && moveAtTheEnd) {
                    tabHeader.remove("[data-reference-tab='" + dataRef + "']");
                    tabHeader.append(item);
                }
                else {
                    moveAtTheEnd = false;
                }
            });
        }

        //Attach events
        self.find(".bz-wp-tabs-arrow-container").bind('click', function (event) {
            //para determinar si un elemento esta visible o no
            var headerWidth = (tabHeader.innerWidth() - 30);
            var totalWidth = 0;

            //Removemos los elementos visibles
            $('.bz-container-items-tabs', popUp).removeClass("show");

            tabHeader.find('.bz-wp-tabs-ui').each(function (i, item) {
                totalWidth += $(":first-child", item).outerWidth();
                if (totalWidth > headerWidth) {//si "true" entonces es un tab para mostrar en el popup
                    popUp.children().each(function (j, pop_item) {
                        if (pop_item.textContent === item.textContent) {
                            $(pop_item).addClass("show");
                            $('.cell.bz-container-items-tabs').css('border-bottom', '');
                            $('.cell.bz-container-items-tabs.show').last().css('border-bottom', 'none');
                            return false;
                        }
                    });
                }
            });

            event.stopPropagation();
            event.preventDefault();
            popUp.toggle();
        });

        $(">div.cell", popUp).bind("click", function (e) {
            changeTab(e, { origin: "popUp" });
        });
        $(".bz-wp-tabs-ui", tabHeader).bind("click", changeTab);

        tabHeader.bind("resizeHeader", function () {
            updateTabSize();
        });

        function updateTabSize() {
            if (tabHeader.get(0).scrollHeight > tabHeader.get(0).clientHeight) {
                $('.bz-wp-tabs-arrow-container', self).addClass('show-pluss');
                tabHeader.addClass('show-pluss');
                activeTab.trigger('click', { origin: 'popUp' });
            } else {
                $('.bz-wp-tabs-arrow-container', self).removeClass('show-pluss');
                tabHeader.removeClass('show-pluss');
            }
        }

    };
})(jQuery);