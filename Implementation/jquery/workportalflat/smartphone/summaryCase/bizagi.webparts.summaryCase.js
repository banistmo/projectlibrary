/**
*   Name: BizAgi Webpart summaryCase
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to render case summary
*/

bizagi.workportal.webparts.webpart.extend('bizagi.workportal.webparts.summaryCase', {
    COMMENT_SIZE: 5
}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        this._super(workportalFacade, dataService, initialParams);

        self.summaryCaseTemplate = kendo.template(self.getTemplate('summary-case-main'), { useWithBlock: false });
        self.summaryCaseTemplateSubprocess = kendo.template(self.getTemplate('summary-case-subprocess'), { useWithBlock: false });
        self.summaryCaseTemplateComments = kendo.template(self.getTemplate('summary-case-comments'), { useWithBlock: false });
        self.summaryCaseTemplateAssigness = kendo.template(self.getTemplate('summary-case-assignees'), { useWithBlock: false });
        self.summaryCaseTemplateActivities = kendo.template(self.getTemplate('summary-case-activities'), { useWithBlock: false });
        self.summaryCaseTemplateEvents = kendo.template(self.getTemplate('summary-case-events'), { useWithBlock: false });
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var summaryDeferred = $.Deferred();

        summaryDeferred.resolve('');

        return summaryDeferred.promise();
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        // Configure handlers to click events
        self.configureHandlers();
    },

    /*
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        bizagi.webpart.subscribe('bz-summary-case', function (e, params) {
            return self.renderSummary(params);
        });

        $('#bz-render-drawer', self.canvas).data('kendoMobileDrawer').bind('show', function () {
            $('.bz-header_select', self.canvas).trigger('resizeHeader');
        });

        /* istanbul ignore next */
        $('#bz-render-drawer', self.canvas).delegate('.summaryLink', 'click', function (e) {

            e.preventDefault();
            self.routingExecute($(this));

            $($(this), self.canvas).remove();
            self.cache = {};
        });
    },
    /*
    *   Renders the summary component of the webpart
    */
    renderSummary: function (params) {
        var self = this;
        self.params = params || {};
        self.cache = {};
        var defer = new $.Deferred();

        self.dataService.summaryCaseDetails({
            idCase: params.idCase,
            eventAsTasks: params.eventAsTasks || false,
            onlyUserWorkItems: params.onlyUserWorkItems || false,
            idWorkitem: params.idWorkitem,
            isOfflineForm: params.isOfflineForm || false
        }).done(function (data) {
            var date = typeof (data.estimatedSolutionDate) !== 'undefined' ? data.estimatedSolutionDate : data.solutionDate;
            data.params = self.params;

            data.group = bizagi.getGroupedData(date);
            self.data = data;

            var content = self.summaryCaseTemplate(self.data);

            $('#content-render-drawer .km-scroll-container', self.canvas).empty();
            $('#content-render-drawer .km-scroll-container', self.canvas).html(content);
            self.content = $('#content-render-drawer .km-scroll-container', self.canvas);

            /* istanbul ignore next */
            $('.ui-bizagi-container-tab', self.canvas).bztabs({
                activate: function (e) {
                    bizagi.util.smartphone.startLoading();

                    data.currentTab = $(e.currentTarget).data('reference-tab');

                    self.renderCurrentTab(data);
                }
            });

            if (data.currentState && data.currentState[0].allowReleaseActivity) {
                /* istanbul ignore next */
                $('.bz-wp-summary-release', self.content).bind('click', function () {
                    self.release(params);
                });
            }
            defer.resolve();
        });
        return defer.promise();
    },

    /*
     * Action release
     */
    release: function(params){
        var self = this;
        var defered = new $.Deferred();
        var buttons = [{ 'label': self.getResource("workportal-widget-dialog-box-release-ok"), 'action': 'resolve' }, { 'label': self.getResource("workportal-widget-dialog-box-release-cancel")}];
        $.when(bizagi.showConfirmationBox(self.getResource("workportal-widget-dialog-box-release"), self.getResource("render-actions-release"), '', buttons)).done(function () {
            bizagi.util.smartphone.startLoading();
            $.when(self.dataService.releaseActivity({
                idCase: params.idCase,
                idWorkItem: params.idWorkitem})).done(function (data) {
                var status = (data && data.status) ? data.status : 'Error';
                switch (status) {
                    case "Success":
                        bizagi.kendoMobileApplication.navigate('taskFeed');
                        break;
                    case "ConfigurationError":
                        bizagi.showMessageBox(self.getResource("workportal-widget-dialog-box-release-configuration-error-message").replace("{0}", params.idWorkitem), self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                        break;
                    case "Error":
                    default:
                        bizagi.showMessageBox(self.getResource("workportal-widget-dialog-box-release-error-message").replace("{0}", params.idWorkitem), self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                        break;
                }
                bizagi.util.smartphone.stopLoading();
                defered.resolve();
            }).fail(function () {
                var message = self.getResource("workportal-widget-dialog-box-release-error-message").replace("{0}", params.idWorkitem);
                bizagi.showMessageBox(message, self.getResource("workportal-widget-dialog-box-release-error"), 'error', false);
                bizagi.util.smartphone.stopLoading();
                defered.reject();
            });
        });
        return defered.promise();
    },

    /*
    *   Renders the tabs component of the webpart
    */
    renderCurrentTab: function (args) {
        var self = this;
        var htmlContent = "";
        var defer = new $.Deferred();
        var params = args.params || self.params || {};

        self.cache = self.cache || {};
        self.data = self.data || {};

        switch (args.currentTab) {
            case '#ui-bizagi-tab-assignees':
                if (typeof (self.cache['assignees']) === 'undefined') {
                    $.when(
                        self.dataService.summaryAssigness({
                            idCase: params.idCase
                        })).done(function (assignees) {
                            htmlContent = $(bizagi.util.trim(self.summaryCaseTemplateAssigness(assignees)));
                            self.cache['assignees'] = true;

                            $(args.currentTab, self.content).empty();
                            $(args.currentTab, self.canvas).html(htmlContent);

                            bizagi.util.smartphone.stopLoading();
                            defer.resolve();
                        });
                } else {
                    bizagi.util.smartphone.stopLoading();
                    defer.resolve();
                }
                break;
            case '#ui-bizagi-tab-comments':
                if (typeof (self.cache['comments']) === 'undefined') {
                    $.when(
                        self.dataService.getComments({
                            idCase: params.idCase,
                            idLastComment: self.Class.COMMENT_SIZE
                        })).done(function (comments) {
                            comments['showComments'] = comments.totalRecords > 0 ? true : false;
                            comments['timeAgo'] = bizagi.getTimeAgo;
                            comments['getUserName'] = function (idUser) {
                                var users = comments.users || [];
                                var name = "";
                                $.each(users, function (key, value) {
                                    if (users[key]['Id'] == idUser) {
                                        name = value.DisplayName;
                                    }
                                });

                                return name;
                            };

                            htmlContent = $(bizagi.util.trim(self.summaryCaseTemplateComments(comments)));
                            self.cache['comments'] = true;

                            $(args.currentTab, self.content).empty();
                            $(args.currentTab, self.canvas).html(htmlContent);

                            bizagi.util.smartphone.stopLoading();
                            defer.resolve();
                        });
                } else {
                    bizagi.util.smartphone.stopLoading();
                    defer.resolve();
                }

                break;
            case '#ui-bizagi-tab-subprocess':
                if (typeof (self.cache['subprocess']) === 'undefined') {
                    $.when(
                        self.dataService.summarySubProcess({
                            idCase: params.idCase
                        })).done(function (subprocess) {
                            htmlContent = $(bizagi.util.trim(self.summaryCaseTemplateSubprocess(subprocess)));
                            self.cache['subprocess'] = true;

                            $(args.currentTab, self.content).empty();
                            $(args.currentTab, self.canvas).html(htmlContent);

                            bizagi.util.smartphone.stopLoading();
                            defer.resolve();
                        });
                } else {
                    bizagi.util.smartphone.stopLoading();
                    defer.resolve();
                }
                break;
            case '#ui-bizagi-tab-events':
                if (typeof (self.cache['events']) === 'undefined') {
                    $.when(
                        self.dataService.summaryCaseEvents({
                            idCase: params.idCase
                        })).done(function (events) {
                            htmlContent = $(bizagi.util.trim(self.summaryCaseTemplateEvents(events)));
                            self.cache['events'] = true;

                            $(args.currentTab, self.content).empty();
                            $(args.currentTab, self.canvas).html(htmlContent);

                            bizagi.util.smartphone.stopLoading();
                            defer.resolve();
                        });
                } else {
                    bizagi.util.smartphone.stopLoading();
                    defer.resolve();
                }
                break;
            case '#ui-bizagi-tab-activities':
                if (typeof (self.cache['activities']) === 'undefined') {
                    $.when(
                        self.dataService.summaryActivities({
                            data: self.data,
                            idWorkitem: params.idWorkitem
                        })).done(function (activities) {
                            activities['idCase'] = params.idCase;

                            htmlContent = $(bizagi.util.trim(self.summaryCaseTemplateActivities(activities)));
                            self.cache['activities'] = true;

                            $(args.currentTab, self.content).empty();
                            $(args.currentTab, self.canvas).html(htmlContent);

                            bizagi.util.smartphone.stopLoading();
                            defer.resolve();
                        });
                } else {
                    bizagi.util.smartphone.stopLoading();
                    defer.resolve();
                }
                break;
            default:
                bizagi.util.smartphone.stopLoading();
                defer.resolve();
                break;
        }
        return defer.promise();
    },

    /*
    *   Executes routing action
    */
    routingExecute: function (element) {
        var self = this;
        var defer = new $.Deferred();

        if (element) {
            var idCase = element.data('case') || self.params.idCase;
            var idWorkItem = element.data('workitem');
            var idTask = element.data('task');
            var displayName = element.data('displayname') || self.params.displayName;

            $('#bz-render-drawer').data('kendoMobileDrawer').hide();

            setTimeout(function () {
                self.publish('changeCase', {
                    idCase: idCase,
                    idWorkitem: idWorkItem,
                    idTask: idTask,
                    displayName: displayName
                });
                defer.resolve(true);
            }, 500);
        } else {
            defer.resolve(false);
        }
        return defer.promise();
    }
});
