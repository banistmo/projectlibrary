﻿
describe('Testing webpart render case summary.', function () {
    'use strict';

    var webpartSummaryCase;
    var webpartRender;
    var canvas = $('body');

    it('Executing webpart', function (done) {
        var workportal = bizagi.collection.get('workportal');

        /**
         * Se dispara una sola vez cuando se crea el webpart de render
         */
        $(document).one('bizagi-webpart-created', function (e, wp) {
            webpartRender = wp.webpart;
            spyOn(webpartRender, 'renderContent').and.callThrough();
            spyOn(webpartRender, 'configureHandlers').and.callThrough();
        });

        workportal.executeWebpart({
            webpart: "render",
            canvas: canvas
        }).done(function (wp) {
            console.log("Finalizo ejecución de webpart render");
            webpartRender = wp.webpart;
            wp.webpart.canvas.triggerHandler("load-modules");

            /**
             * Se dispara una sola vez cuando se crea el webpart de summaryCase
             */
            $(document).one('bizagi-webpart-created', function (e, wp) {
                webpartSummaryCase = wp.webpart;
                spyOn(webpartSummaryCase, 'renderContent').and.callThrough();
                spyOn(webpartSummaryCase, 'configureHandlers').and.callThrough();
            });

            // Load SummaryCase
            workportal.executeWebpart({
                webpart: 'summaryCase',
                canvas: canvas
            }).done(function (wp) {
                webpartSummaryCase = wp.webpart;

                // Tracks that the spy was called
                expect(webpartSummaryCase.renderContent).toHaveBeenCalled();

                // Tracks that the spy was called one time
                expect(webpartSummaryCase.renderContent.calls.count()).toEqual(1);
                expect(webpartSummaryCase.configureHandlers.calls.count()).toEqual(1);

                console.log('Webpart SummaryCase Created but no data is loaded');
                done();
            });

            //wp.webpart.publish("render-case", params);
        });
    });

    it("Test instance correct webpart", function () {
        expect(typeof webpartSummaryCase).toEqual('object');
    });

    it("Load correct webpart", function () {
        $("#bz-render-drawer").data("kendoMobileDrawer").show();
    });

    it('Call to click events that make a publish', function () {
        $('.summaryLink', canvas).trigger('click');
    });

    describe("render summary", function(){
        it("calls subscriber", function(done){
            webpartRender.rendering = {
                execute: function(params){
                    var deferred = $.Deferred();
                    setTimeout(function(){
                        deferred.resolve();
                    }, 50);
                    return deferred.promise();
                }
            };

            spyOn(webpartSummaryCase, 'renderSummary').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplate').and.callThrough();

            var params = {
                idCase: 7,
                idWorkitem: 10
            };

            $.when(bizagi.webpart.publish("bz-summary-case", params)).done(function(){
                expect(webpartSummaryCase.renderSummary.calls.count()).toEqual(1);
                expect(webpartSummaryCase.summaryCaseTemplate.calls.count()).toEqual(1);
                done();
            });
        });
    });

    describe("Events on list works", function () {
        beforeEach(function () {
            spyOn(webpartSummaryCase, 'renderContent').and.callThrough();
            spyOn(webpartSummaryCase, 'configureHandlers').and.callThrough();
            spyOn(webpartSummaryCase, 'renderSummary').and.callThrough();
            spyOn(webpartSummaryCase, 'renderCurrentTab').and.callThrough();
            spyOn(webpartSummaryCase, 'routingExecute').and.callThrough();
            spyOn(webpartSummaryCase.dataService, 'summaryAssigness').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateAssigness').and.callThrough();
            jasmine.clock().install();
        });

        afterEach(function () {
            jasmine.clock().uninstall();
        });
    });

    describe("changes tab", function(){
        var args = {};
        beforeEach(function () {
            args.params = { idCase: 7, caseNumber: 7, idWorkitem: 48 };
            spyOn(webpartSummaryCase.dataService, 'summaryAssigness').and.callThrough();
            spyOn(webpartSummaryCase.dataService, 'getComments').and.callThrough();
            spyOn(webpartSummaryCase.dataService, 'summarySubProcess').and.callFake(function(){
                return {"idCase":"7","subProcesses":[],"CustFields":[],"showSubProcess":false,"showSubProcesColumns":false};
            });
            spyOn(webpartSummaryCase.dataService, 'summaryCaseEvents').and.callFake(function(){
                return {"idCase":7,"idWorkFlow":-1,"events":[],"showEvents":false};
            });
            spyOn(webpartSummaryCase.dataService, 'summaryActivities').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateAssigness').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateComments').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateSubprocess').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateEvents').and.callThrough();
            spyOn(webpartSummaryCase, 'summaryCaseTemplateActivities').and.callThrough();
        });

        it('assignees', function (done) {
            args.currentTab = "#ui-bizagi-tab-assignees";
            webpartSummaryCase.cache = {};

            // Tab #ui-bizagi-tab-assignees
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab assignees');
                expect(webpartSummaryCase.dataService.summaryAssigness.calls.count()).toEqual(1);
                expect(webpartSummaryCase.dataService.summaryAssigness).toHaveBeenCalledWith({ 'idCase': args.params.idCase });
                expect(webpartSummaryCase.cache['assignees']).toBeDefined();
                expect(webpartSummaryCase.cache['assignees']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateAssigness.calls.count()).toEqual(1);
                done();
            });
        });

        it('assignees when it is already loaded', function(done){
            args.currentTab = "#ui-bizagi-tab-assignees";

            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                expect(webpartSummaryCase.dataService.summaryAssigness.calls.count()).toEqual(0);
                expect(webpartSummaryCase.summaryCaseTemplateAssigness.calls.count()).toEqual(0);
                done();
            });
        });

        it('comments', function (done) {
            args.currentTab = "#ui-bizagi-tab-comments";
            webpartSummaryCase.Class.COMMENT_SIZE = 5;

            (function ($) {
                $.timeago = function (timestamp) {
                    return "2 months ago";
                };

                $.extend($.timeago, {
                    settings: {
                    }
                });

            })(jQuery);

            // Tab #ui-bizagi-tab-assignees
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                expect(webpartSummaryCase.dataService.getComments.calls.count()).toEqual(1);
                expect(webpartSummaryCase.dataService.getComments).toHaveBeenCalledWith({ 'idCase': args.params.idCase, 'idLastComment': webpartSummaryCase.Class.COMMENT_SIZE });
                expect(webpartSummaryCase.cache['comments']).toBeDefined();
                expect(webpartSummaryCase.cache['comments']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateComments.calls.count()).toEqual(1);
                done();
            });
        });

        it('comments when it is already loaded', function (done) {
            args.currentTab = "#ui-bizagi-tab-comments";

            // Tab #ui-bizagi-tab-assignees
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                expect(webpartSummaryCase.dataService.getComments.calls.count()).toEqual(0);
                expect(webpartSummaryCase.cache['comments']).toBeDefined();
                expect(webpartSummaryCase.cache['comments']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateComments.calls.count()).toEqual(0);
                done();
            });
        });

        it('subprocess', function (done) {
            args.currentTab = "#ui-bizagi-tab-subprocess";

            // Tab #ui-bizagi-tab-assignees
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab subprocess');
                expect(webpartSummaryCase.dataService.summarySubProcess.calls.count()).toEqual(1);
                expect(webpartSummaryCase.dataService.summarySubProcess).toHaveBeenCalledWith({ 'idCase': args.params.idCase });
                expect(webpartSummaryCase.cache['subprocess']).toBeDefined();
                expect(webpartSummaryCase.cache['subprocess']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateSubprocess.calls.count()).toEqual(1);
                done();
            });
        });

        it('subprocess when it is already loaded', function (done) {
            args.currentTab = "#ui-bizagi-tab-subprocess";

            // Tab #ui-bizagi-tab-assignees
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab subprocess');
                expect(webpartSummaryCase.dataService.summarySubProcess.calls.count()).toEqual(0);
                expect(webpartSummaryCase.cache['subprocess']).toBeDefined();
                expect(webpartSummaryCase.cache['subprocess']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateSubprocess.calls.count()).toEqual(0);
                done();
            });
        });

        it('events', function (done) {
            args.currentTab = "#ui-bizagi-tab-events";
            // Tab #ui-bizagi-tab-activities
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab events');
                expect(webpartSummaryCase.dataService.summaryCaseEvents.calls.count()).toEqual(1);
                expect(webpartSummaryCase.dataService.summaryCaseEvents).toHaveBeenCalledWith({ 'idCase': args.params.idCase });
                expect(webpartSummaryCase.cache['events']).toBeDefined();
                expect(webpartSummaryCase.cache['events']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateEvents.calls.count()).toEqual(1);
                done();
            });
        });

        it('events when it is already loaded', function (done) {
            args.currentTab = "#ui-bizagi-tab-events";
            // Tab #ui-bizagi-tab-activities
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab events');
                expect(webpartSummaryCase.dataService.summaryCaseEvents.calls.count()).toEqual(0);
                expect(webpartSummaryCase.cache['events']).toBeDefined();
                expect(webpartSummaryCase.cache['events']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateEvents.calls.count()).toEqual(0);
                done();
            });
        });

        it('activities', function (done) {
            args.currentTab = "#ui-bizagi-tab-activities";
            // Tab #ui-bizagi-tab-activities
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab activities');
                expect(webpartSummaryCase.dataService.summaryActivities.calls.count()).toEqual(1);
                //expect(newWebpart.dataService.summaryActivities).toHaveBeenCalledWith({ 'data':{}, 'idWorkitem': args.params.idWorkitem });
                expect(webpartSummaryCase.cache['activities']).toBeDefined();
                expect(webpartSummaryCase.cache['activities']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateActivities.calls.count()).toEqual(1);
                done();
            });
        });

        it('activities when it is already loaded', function (done) {
            args.currentTab = "#ui-bizagi-tab-activities";
            // Tab #ui-bizagi-tab-activities
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab activities');
                expect(webpartSummaryCase.dataService.summaryActivities.calls.count()).toEqual(0);
                expect(webpartSummaryCase.cache['activities']).toBeDefined();
                expect(webpartSummaryCase.cache['activities']).toEqual(true);
                expect(webpartSummaryCase.summaryCaseTemplateActivities.calls.count()).toEqual(0);
                done();
            });
        });

        it('default', function (done) {
            args.currentTab = "#ui-bizagi-tab-default";
            // Tab #ui-bizagi-tab-activities
            $.when(webpartSummaryCase.renderCurrentTab(args)).done(function (data) {
                console.log('Tab default');
                expect(webpartSummaryCase.dataService.summaryActivities.calls.count()).toEqual(0);
                expect(webpartSummaryCase.dataService.summaryCaseEvents.calls.count()).toEqual(0);
                expect(webpartSummaryCase.dataService.summarySubProcess.calls.count()).toEqual(0);
                expect(webpartSummaryCase.dataService.getComments.calls.count()).toEqual(0);
                expect(webpartSummaryCase.dataService.summaryAssigness.calls.count()).toEqual(0);
                done();
            });
        });
    });

    describe("release an activity", function(){
        beforeEach(function(){
            $.extend(bizagi, {
                showMessageBox: function(){
                    return;
                },
                showConfirmationBox: function(params){
                    var deferred = $.Deferred();
                    setTimeout(function(){
                        deferred.resolve();
                    }, 50);
                    return deferred.promise();
                }
            });
            //spyOn(bizagi.kendoMobileApplication, 'navigate').and.callThrough();
            spyOn(bizagi.kendoMobileApplication, 'navigate').and.callFake(function(){
                console.log("Called Navigation");
            });

            spyOn(bizagi, 'showMessageBox').and.callThrough();
        });

        it("success", function(done){
            spyOn(webpartSummaryCase.dataService, 'releaseActivity').and.callFake(function(){
                return {"status": "Success"};
            });

            var params = {'idCase': 1, 'idWorkItem': 10};
            $.when(webpartSummaryCase.release(params)).done(function(){
                expect(bizagi.kendoMobileApplication.navigate).toHaveBeenCalledWith('taskFeed');
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalledWith({
                    idCase: params.idCase,
                    idWorkItem: params.idWorkitem});
                done();
            });
        });

        it("configuration error", function(done){

            spyOn(webpartSummaryCase.dataService, 'releaseActivity').and.callFake(function(){
                return {"status": "ConfigurationError"};
            });

            var params = {'idCase': 1, 'idWorkitem': 10};
            $.when(webpartSummaryCase.release(params)).done(function(){
                expect(bizagi.kendoMobileApplication.navigate).not.toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalledWith({
                    idCase: params.idCase,
                    idWorkItem: params.idWorkitem});
                expect(bizagi.showMessageBox).toHaveBeenCalled();
                done();
            });
        });

        it("error", function(done){


            spyOn(webpartSummaryCase.dataService, 'releaseActivity').and.callFake(function(){
                return {"status": "Error"};
            });


            var params = {'idCase': 1, 'idWorkitem': 10};
            $.when(webpartSummaryCase.release(params)).done(function(){
                expect(bizagi.kendoMobileApplication.navigate).not.toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalledWith({
                    idCase: params.idCase,
                    idWorkItem: params.idWorkitem});
                expect(bizagi.showMessageBox).toHaveBeenCalled();
                done();
            });
        });

        it("fail", function(done){
            var deferred2 = $.Deferred();

            spyOn(webpartSummaryCase.dataService, 'releaseActivity').and.callFake(function(){
                setTimeout(function(){
                    deferred2.reject();
                }, 50);
                return deferred2.promise();
            });

            var params = {'idCase': 1, 'idWorkitem': 10};
            $.when(webpartSummaryCase.release(params)).fail(function(){
                expect(bizagi.kendoMobileApplication.navigate).not.toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalled();
                expect(webpartSummaryCase.dataService.releaseActivity).toHaveBeenCalledWith({
                    idCase: params.idCase,
                    idWorkItem: params.idWorkitem});
                expect(bizagi.showMessageBox).toHaveBeenCalled();
                done();
            });
        });
    });

    describe("executes routing", function(){
        beforeEach(function(){

            webpartRender.rendering = {
                execute: function(params){
                    var deferred = $.Deferred();
                    setTimeout(function(){
                        deferred.resolve();
                    }, 50);
                    return deferred.promise();
                }
            };

            spyOn(webpartSummaryCase, "publish").and.callFake(function(){
                return;
            });
        });

        it('works', function (done) {
            var element = $('<div class="bz-wp-summary-value summaryLink" data-case="11" data-displayname="Purchase Request"><a href="#">11 - Purchase Request</a></div>');
            // Data
            $.when(webpartSummaryCase.routingExecute(element)).done(function (data) {
                console.log('Data');
                expect(webpartSummaryCase.publish).toHaveBeenCalledWith('changeCase', {idCase: 11,
                    idWorkitem: undefined,
                    idTask: undefined,
                    displayName: 'Purchase Request'});
                expect(data).toEqual(true);
                done();
            });
        });


        it('doesnt work', function (done) {
            // Data
            $.when(webpartSummaryCase.routingExecute()).done(function (data) {
                expect(webpartSummaryCase.publish.calls.count()).toEqual(0);
                expect(data).toEqual(false);
                done();
            });
        });

    });

});
