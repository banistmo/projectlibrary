/**
 * Created by LuisCE on 9/24/14.
 */


bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
).then(function() {

        var fullDate = new Date();
        var tmpDay = fullDate.getDate();
        var tmpMonth = fullDate.getMonth()+1;
        var tmpYear = fullDate.getFullYear();
        $.mockJSON.data.CURRENT_DATE = [
                tmpMonth+'/'+tmpDay+'/'+tmpYear
        ];

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate()+1);
        $.mockJSON.data.TOMORROW_DATE = [
                (tomorrow.getMonth()+1)+'/'+tomorrow.getDate()+'/'+tomorrow.getFullYear()
        ];

        var nextMonth = new Date();
        nextMonth.setMonth(nextMonth.getMonth()+1);
        $.mockJSON.data.NEXT_MONTH_DATE = [
                (nextMonth.getMonth()+1)+'/'+nextMonth.getDate()+'/'+nextMonth.getFullYear()
        ];

        //$.mockjaxSettings.responseTime = 5000; //4000; //10; //2000;
        // DUMMIES
        $.mockjax(function(settings) {
            if (settings.dataType == "text") {
                if (!BIZAGI_ENABLE_MOCKS) return;

            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;

                // Summary Case - Details (Rest/Cases/{idCase}/Summary)
                if ((/Rest\/Cases\/\d+\/Summary/).test(settings.url)) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.txt",
                    };
                }

                // Summary Case - Assignees (Rest/Cases/{idCase}/Assignees)
                if ((/Rest\/Cases\/\d+\/Assignees/).test(settings.url)) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.assignees.txt",
                    };
                }

                // Summary Case - Comments (Rest/Cases/{idCase}/Comments)
                if ((/Rest\/Cases\/\d+\/Comments/).test(settings.url)) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.comments.txt",
                    };
                }
                // Summary Case - Events (Rest/Cases/{idCase}/Events)
                if ((/Rest\/Cases\/\d+\/Comments/).test(settings.url)) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/summaryCase/test/data/dummy.summarycase.events.txt",
                    };
                }


            }
        });
    });
