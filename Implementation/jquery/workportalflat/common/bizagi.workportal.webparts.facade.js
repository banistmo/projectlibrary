﻿
/*
 *   Name: BizAgi Workportal
 *   Author:oo
 *   Comments:
 *   -   This script will process a workportal page using a device factory to use the adequate rendering
 *       to create the layout
 */

// Define BizAgi Workportal namespace
bizagi.workportal = bizagi.workportal || {};

// Define global variables
bizagi.workportal.currentInboxView = "inbox";

// Define state variables
bizagi.workportal.state = bizagi.workportal.state || {};

bizagi.webparts = bizagi.webparts || {};

bizagi.webparts.instances = bizagi.webparts.instances || [];


//bizagi.workportal.instances = {};
/*
 *   Renders the workportal
 */
$.Class.extend("bizagi.workportal.facade", {}, {

    /* 
    *   Constructor
    */
    init: function (params) {

        // Defines a device factory for all rendering
        this.deviceFactory = new bizagi.workportal.device.factory(this);

        // Creates a data service instance
        this.dataService = new bizagi.workportal.services.service(params);

        // Create instance of routing service
        this.dataService.routing = new bizagi.workportal.services.routing({ dataService: this.dataService });

        // Set default params
        this.defaultParams = params || {};


    },




    /*
    *   Creates a webpart inside a canvas but don't execute it
    */
    createWebpart: function (params) {
        return this.executeWebpart($.extend(params, {
            creating: true
        }));
    },

    /*
    *   Executes a webpart inside a canvas
    */
    executeWebpart: function (params) {
        var self = this;
        var doc = this.ownerDocument;
        var body = $("body", doc);

        // Creates ready deferred
        var canvas = params.canvas; //|| $("<div/>").appendTo(body);

        var processWebpart = true;
        if (params.webpartConfiguration) {
            processWebpart = self.testWebpartConfiguration(params);
        }
        if (processWebpart) {
            // Process the webpart asynchonous
            return self.processWebpart(params).done(function (result) {

                result.name = result.webpart.Class.fullName;
                bizagi.webparts.instances.push(result);
                // Append content to canvas
                canvas.append(result.content);
                canvas.triggerHandler("ondomincluded");

            });
        }
    },




    /*
    *   Returns the execution deferred to determine if the component is ready or not
    */
    ready: function () {

        return this.executionDeferred.promise();
    },


    /*
    *   Process a webpart
    */
    processWebpart: function (params) {
        var self = this;
        var defer = new $.Deferred();
        var webpartController;

        // Create a workportal facade
        var facade = this.deviceFactory.getWorkportalFacade(self.dataService, false);

        // Combine workportal params with call params
        $.extend(params, this.defaultParams);

        // Set callback when requests have been done
        $.when(facade)
            .pipe(function (workportalFacade) {

                var deferTemplates = new $.Deferred();

                // Load webpart templates
                $.when(workportalFacade.loadWebpart(params))
                    .done(function () {
                        deferTemplates.resolve(workportalFacade);
                    });

                return deferTemplates.promise();
            })
            .pipe(function (workportalFacade) {
                webpartController = workportalFacade.getWebpart(params.webpart, params);

                // Render the full content
                return webpartController.render(params);
            }).done(function (content) {


                // Resolve deferred
                defer.resolve({ webpart: webpartController, content: content });
            });
        return defer.promise();
    },


    /*
    *   Returns the main controller
    */
    getMainController: function () {
        return this.mainController;
    },

    execute: function () {
        var self = this;
        var doc = this.ownerDocument;
        var body = $("body", doc);
        canvas = body;
        var facade = this.deviceFactory.getWorkportalFacade(self.dataService);

        return $.when(facade).then(function (deviceFacade) {
            return deviceFacade.executeWebparts();
        });

    }




});
