/**
 * Created by LuisCE on 09/09/2014.
 */

describe('Testing Common Services', function () {
    'use strict';

    var workportal = bizagi.collection.get('workportal');
    var dataService = new bizagi.workportal.services.service({proxyPrefix: ""});
    describe("getCasesListBeta",function(){
        beforeEach(function(){
            spyOn(dataService.serviceLocator, 'getUrl').and.callThrough();
            spyOn(bizagi.localization, 'getResource').and.callThrough();
        });
        it("load mock yellow semaphore",function(){
            $.mockjaxClear();
            $.mockjax(function (settings) {
                if (settings.dataType == "json") {
                    if (settings.url.indexOf("Rest/Processes/SearchCases") > -1) {
                        return {
                            mockjson: "jquery/workportalflat/common/services/test/data/unit/dummy.searchCases.yellowsemaphore.txt"
                        };
                    }
                }
            });
        });
        it("test yellow semaphore", function(done){
            $.when(dataService.getCasesListBeta({numberOfFields: 2, page: 1, pageSize: 10, idWfClass: -1, radNumber: 1})).done(function(groupdata){
                expect(dataService.serviceLocator.getUrl.calls.count()).toEqual(1);
                expect(dataService.serviceLocator.getUrl).toHaveBeenCalledWith("case-handler-getCasesList");
                for(var i = 0; i < groupdata.elements.length; i++){
                    var flag = groupdata.elements[i][7];
                    expect(groupdata.elements[i].state).toEqual(flag);
                    switch(flag){
                        case 'red':
                            expect(groupdata.elements[i].group).toMatch("1|");
                            break;
                        case 'yellow':
                            expect(groupdata.elements[i].group).toMatch("2|");
                            break;
                        case 'green':
                            expect(groupdata.elements[i].group).toMatch("3|");
                            break;
                        default:
                            expect(groupdata.elements[i].group).toMatch("1|");
                            break;
                    }
                }
                done();
            });
        });

        it("load mock",function(){
            $.mockjaxClear();
            $.mockjax(function (settings) {
                if (settings.dataType == "json") {
                    if (settings.url.indexOf("Rest/Processes/SearchCases") > -1) {
                        return {
                            mockjson: "jquery/workportalflat/common/services/test/data/unit/dummy.searchCases.txt"
                        };
                    }
                }
            });
        });
        it("test no yellow semaphore", function(done){
            $.when(dataService.getCasesListBeta({numberOfFields: 2, page: 1, pageSize: 10, idWfClass: -1})).done(function(groupdata){
                expect(dataService.serviceLocator.getUrl.calls.count()).toEqual(1);
                expect(dataService.serviceLocator.getUrl).toHaveBeenCalledWith("case-handler-getCasesList");
                expect(bizagi.localization.getResource).toHaveBeenCalled();

                //overdue
                expect(bizagi.localization.getResource.calls.argsFor(0)).toEqual(["workportal-taskfeed-overdue"]);
                expect(groupdata.elements[0].state).toMatch("red");

                //today
                expect(bizagi.localization.getResource.calls.argsFor(1)).toEqual(["workportal-taskfeed-today"]);
                expect(groupdata.elements[1].state).toMatch("yellow");

                //tomorrow
                expect(bizagi.localization.getResource.calls.argsFor(2)).toEqual(["workportal-taskfeed-tomorrow"]);
                expect(groupdata.elements[2].state).toMatch("yellow");

                //upcomming
                expect(bizagi.localization.getResource.calls.argsFor(3)).toEqual(["workportal-taskfeed-upcomming"]);
                expect(groupdata.elements[3].state).toMatch("green");
                done();
            });
        });
    });

});

