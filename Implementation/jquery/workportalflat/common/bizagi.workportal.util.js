
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.util = (typeof (bizagi.util) !== "undefined") ? bizagi.util : {};
bizagi.context = (typeof (bizagi.context) !== "undefined") ? bizagi.context : {};
bizagi.util.smartphone = (typeof (bizagi.util.smartphone) !== "undefined") ? bizagi.util.smartphone : {};

/*
* Method to detect iPad visitors
*/
bizagi.util.isIPad = function () {
    return (navigator.platform.indexOf("iPad") != -1);
};
/*
* Method to detect iPhone visitors
*/
bizagi.util.isIPhone = function () {
    return (navigator.userAgent.toLowerCase().indexOf("iphone") > -1);
};
/*
* Method to detect iPod visitors
*/
bizagi.util.isIPod = function () {
    return (navigator.userAgent.toLowerCase().indexOf("ipod") > -1);
};
/*
* Method to detect IE7 visitors
*/
bizagi.util.isIE7 = function () {
    return bizagi.util.isIE() && document.documentMode == 7;
};
/*
* Method to detect IE10 visitors
*/
bizagi.util.isIE10 = function () {
    return bizagi.util.isIE() && document.documentMode == 10;
};
/*
* Method to detect IE8 visitors
*/
bizagi.util.isIE8 = function () {
    return bizagi.util.isIE() && document.documentMode == 8;
};
/*
* Method to detect IE9 visitors
*/
bizagi.util.isIE9 = function () {
    return bizagi.util.isIE() && document.documentMode == 9;
};
bizagi.util.isIE11 = function () {
    return !!navigator.userAgent.match(/Trident\/7.0/) && !navigator.userAgent.match(/MSIE/i);
};
/*
* Method to detect IE visitors
*/
bizagi.util.isIE = function () {
    return (navigator.appName.indexOf("Internet Explorer") > 0);
};
/*
 * Method to detect Android visitors
 */
bizagi.util.isAndroid = function(){
    return (navigator.userAgent.toLowerCase().indexOf("android") > -1);
};
/**
* Method to detect iOS version Higher than 5
*/
bizagi.util.isIphoneHigherIOS5 = function () {

    if (this.value != undefined)
        return this.value;
    return this.value = RegExp("OS\\s*(5|6|7|8)_*\\d").test(navigator.userAgent) && RegExp(" AppleWebKit/").test(navigator.userAgent);
};
/**
* Method to detect iOS version 5
*/
bizagi.util.isLessThanIOS5 = function () {
    if (navigator.userAgent.match(new RegExp(/CPU OS (1|2|3|4)/i))) {
        return true;
    } else {
        return false;
    }
};

bizagi.util.isIphoneAndLessIOS6 = function () {
    if (this.value != undefined)
        return this.value;
    return this.value = RegExp("OS\\s*(4|5|6)_*\\d").test(navigator.userAgent) && RegExp(" AppleWebKit/").test(navigator.userAgent);
};
/*
*   Method to detect IE version
*/
bizagi.util.getInternetExplorerVersion = function () {
    if (!bizagi.util.isIE())
        return -1;
    return Number(document.documentMode);
};
/*  
*   Detect a device based on the width
*/
bizagi.util.detectDevice = function () {
    // Call the method located in bizagi.loader
    return bizagi.detectDevice();
};
bizagi.util.isTablet = function () {
    return bizagi.util.isIPad();
};
// Check if a string is number
bizagi.util.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

/*
*   Checks if a given string is empty
*/
bizagi.util.isEmpty = function (value) {
    if (value === 0)
        return false;
    if (value === undefined ||
            value === null ||
            value === "") {

        return true;
    }

    if (typeof (value) === "Array") {
        return value.length == 0;
    }

    if (Object.prototype.toString.apply(value) === "[object Array]") {
        return value.length === 0;
    }

    if (Object.prototype.toString.apply(value) === "[object Object]") {
        return $.isEmptyObject(value);
    }

    return false;
};

/**
 * Decode data from encodeURI charset
 */
bizagi.util.decodeURI = function (value) {
    value = value || "";
    var finishDecoded = false;
    var decodedValue = value;
    var infinityControl;

    // Try to decode data value
    while (!finishDecoded) {
        try {
            infinityControl = decodeURI(decodedValue);
            if (infinityControl == decodedValue) {
                finishDecoded = true;
            } else {
                decodedValue = infinityControl;
            }
        } catch (e) {
            finishDecoded = true;
        }
    }

    return decodedValue;
};

/*
*   Initializes a webpart on-demand
*/
bizagi.util.initWebpart = function (webpart) {
    var defer = new $.Deferred();
    if (webpart.initialized) {
        // If already initialized resolve the deferred
        defer.resolve();
    } else {

        if (webpart.initializing) {
            $.when(webpart.loadingDeferred.promise())
                    .done(function () {
                        defer.resolve();
                    });
        } else {

            webpart.loadingDeferred = new $.Deferred();
            webpart.initializing = true;
            bizagi.loader.initWebpart(webpart, function () {

                // Resolve deferreds
                webpart.loadingDeferred.resolve();
                webpart.initializing = false;
                defer.resolve();
            });
        }
    }

    return defer.promise();
};

/**
* Set and retrieve Cookie
*/
bizagi.cookie = function (key, value, options) {
    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = $.extend({}, options);
        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);
        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) {
        return s;
    } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};


/*
* Format html with invariant date
*/
bizagi.util.formatInvariantDate = function (htmlContent, dateFormat) {
    htmlContent = htmlContent || "";
    dateFormat = dateFormat || "MM/dd/yyyy hh:mm:ss";
    $(".formatDate", htmlContent).not(".formated").each(function (index, elem) {
        var content = $(elem).html();
        $(elem).addClass("formated");
        try {
            var value = new Date(content);
            var formatDate = bizagi.util.dateFormatter.formatDate(value, dateFormat /*self.getResource("dateFormat")*/);
            $(elem).html(formatDate);
        } catch (e) {
            // Restore the original content
            $(elem).html(content);
        }
    });
};
/*
*   Defines a date time formatter that will be available globally
*/
bizagi.util.monthNames = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
bizagi.util.dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
bizagi.util.dateFormatter = new function () {
    this.LZ = function (x) {
        return (x < 0 || x > 9 ? "" : "0") + x;
    };
    /*
    *   isDate ( date_string, format_string )
    *   Returns true if date string matches format of format string and
    *   is a valid date. Else returns false.
    *   It is recommended that you trim whitespace around the value before
    *   passing it to this function, as whitespace is NOT ignored!
    */
    this.isDate = function (val, format) {
        var date = this.getDateFromFormat(val, format);
        if (date == 0) {
            return false;
        }
        return true;
    };
    /*
    *   compareDates(date1,date1format,date2,date2format)
    *   Compare two date strings to see which is greater.
    *   Returns:
    *   1 if date1 is greater than date2
    *   0 if date2 is greater than date1 of if they are the same
    *  -1 if either of the dates is in an invalid format
    */
    this.compareDates = function (date1, dateformat1, date2, dateformat2) {
        var d1 = getDateFromFormat(date1, dateformat1);
        var d2 = getDateFromFormat(date2, dateformat2);
        if (d1 == 0 || d2 == 0) {
            return -1;
        }
        else if (d1 > d2) {
            return 1;
        }
        return 0;
    };
    /*
    *   formatDate (date_object, format)
    *   Returns a date in the output format specified.
    *   The format string uses the same abbreviations as in getDateFromFormat()
    */
    this.formatDate = function (date, format) {
        var monthNames = bizagi.util.monthNames;
        var dayNames = bizagi.util.dayNames;
        format = format + "";
        var result = "";
        var i_format = 0;
        var c;
        var token;
        var y = date.getYear() + "";
        var M = date.getMonth() + 1;
        var d = date.getDate();
        var E = date.getDay();
        var H = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        // Convert real date parts into formatted versions
        var value = new Object();
        if (y.length < 4) {
            y = "" + (y - 0 + 1900);
        }
        value["y"] = "" + y;
        value["yyyy"] = y;
        value["yy"] = y.substring(2, 4);
        value["M"] = M;
        value["MM"] = this.LZ(M);
        value["MMM"] = monthNames[M + 11];
        value["MMMM"] = monthNames[M - 1];
        value["NNN"] = monthNames[M + 11];
        value["d"] = d;
        value["dd"] = this.LZ(d);
        value["ddd"] = dayNames[E + 7];
        value["dddd"] = dayNames[E];
        value["E"] = dayNames[E + 7];
        value["EE"] = dayNames[E];
        value["H"] = H;
        value["HH"] = this.LZ(H);
        value["tt"] = H < 12 ? "am" : "pm";
        value["TT"] = H < 12 ? "AM" : "PM";
        if (H == 0) {
            value["h"] = 12;
        }
        else if (H > 12) {
            value["h"] = H - 12;
        }
        else {
            value["h"] = H;
        }
        value["hh"] = this.LZ(value["h"]);
        if (H > 11) {
            value["K"] = H - 12;
        } else {
            value["K"] = H;
        }
        value["k"] = H + 1;
        value["KK"] = this.LZ(value["K"]);
        value["kk"] = this.LZ(value["k"]);
        if (H > 11) {
            value["a"] = "PM";
        }
        else {
            value["a"] = "AM";
        }
        value["m"] = m;
        value["mm"] = this.LZ(m);
        value["s"] = s;
        value["ss"] = this.LZ(s);
        while (i_format < format.length) {
            c = format.charAt(i_format);
            token = "";
            while ((format.charAt(i_format) == c) && (i_format < format.length)) {
                token += format.charAt(i_format++);
            }
            if (value[token] != null) {
                result = result + value[token];
            }
            else {
                result = result + token;
            }
        }
        return result;
    };
    /*
    *   getDateFromFormat( date_string , format_string )
    *
    *   This function takes a date string and a format string. It matches
    *   If the date string matches the format string, it returns the 
    *   getTime() of the date. If it does not match, it returns 0.
    */
    this.getDateFromFormat = function (val, format) {
        var monthNames = bizagi.util.monthNames;
        var dayNames = bizagi.util.dayNames;
        val = val + "";
        format = format + "";
        var i_val = 0;
        var i_format = 0;
        var c;
        var token;
        var x = 0, y = 0;
        var now = new Date();
        var year = now.getYear();
        var month = now.getMonth() + 1;
        var date = 1;
        var hh = now.getHours();
        var mm = now.getMinutes();
        var ss = now.getSeconds();
        var ampm = "";
        while (i_format < format.length) {
            // Get next token from format string
            c = format.charAt(i_format);
            token = "";
            while ((format.charAt(i_format) == c) && (i_format < format.length)) {
                token += format.charAt(i_format++);
            }
            // Extract contents of value based on format token
            if (token == "yyyy" || token == "yy" || token == "y") {
                if (token == "yyyy") {
                    x = 4;
                    y = 4;
                }
                if (token == "yy") {
                    x = 2;
                    y = 2;
                }
                if (token == "y") {
                    x = 2;
                    y = 4;
                }
                year = _getInt(val, i_val, x, y);
                if (year == null) {
                    return 0;
                }
                i_val += year.length;
                if (year.length == 2) {
                    if (year > 70) {
                        year = 1900 + (year - 0);
                    }
                    else {
                        year = 2000 + (year - 0);
                    }
                }
            }
            else if (token == "MMMM" || token == "MMM" || token == "NNN") {
                month = 0;
                for (var i = 0; i < monthNames.length; i++) {
                    var month_name = monthNames[i];
                    if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                        if (token == "MMMM" || token == "MMM" || (token == "NNN" && i > 11)) {
                            month = i + 1;
                            if (month > 12) {
                                month -= 12;
                            }
                            i_val += month_name.length;
                            break;
                        }
                    }
                }
                if ((month < 1) || (month > 12)) {
                    return 0;
                }
            }
            else if (token == "dddd" || token == "EE" || token == "E") {
                for (var j = 0; j < dayNames.length; j++) {
                    var day_name = dayNames[j];
                    if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                        i_val += day_name.length;
                        break;
                    }
                }
            }
            else if (token == "MM" || token == "M") {
                month = _getInt(val, i_val, 1, 2);
                if (month == null || (month < 1) || (month > 12)) {
                    return 0;
                }
                i_val += month.length;
            }
            else if (token == "dd" || token == "d") {
                date = _getInt(val, i_val, 1, 2);
                if (date == null || (date < 1) || (date > 31)) {
                    return 0;
                }
                i_val += date.length;
            }
            else if (token == "hh" || token == "h") {
                hh = _getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 1) || (hh > 12)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "HH" || token == "H") {
                hh = _getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 0) || (hh > 23)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "KK" || token == "K") {
                hh = _getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 0) || (hh > 11)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "kk" || token == "k") {
                hh = _getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 1) || (hh > 24)) {
                    return 0;
                }
                i_val += hh.length;
                hh--;
            }
            else if (token == "mm" || token == "m") {
                mm = _getInt(val, i_val, token.length, 2);
                if (mm == null || (mm < 0) || (mm > 59)) {
                    return 0;
                }
                i_val += mm.length;
            }
            else if (token == "ss" || token == "s") {
                ss = _getInt(val, i_val, token.length, 2);
                if (ss == null || (ss < 0) || (ss > 59)) {
                    return 0;
                }
                i_val += ss.length;
            }
            else if (token == "a") {
                if (val.substring(i_val, i_val + 2).toLowerCase() == "am") {
                    ampm = "AM";
                }
                else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") {
                    ampm = "PM";
                }
                else {
                    return 0;
                }
                i_val += 2;
            }
            else {
                if (val.substring(i_val, i_val + token.length) != token) {
                    return 0;
                }
                else {
                    i_val += token.length;
                }
            }
        }

        // Is date valid for month?
        if (month == 2) {
            // Check for leap year
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) { // leap year
                if (date > 29) {
                    return 0;
                }
            }
            else {
                if (date > 28) {
                    return 0;
                }
            }
        }
        if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
            if (date > 30) {
                return 0;
            }
        }
        // Correct hours value
        if (hh < 12 && ampm == "PM") {
            hh = hh - 0 + 12;
        }
        else if (hh > 11 && ampm == "AM") {
            hh -= 12;
        }
        var newdate = new Date(year, month - 1, date, hh, mm, ss);
        return newdate;
        /*
        *   Utility method for parsing in getDateFromFormat()
        */
        function _isInteger(_val) {
            var digits = "1234567890";
            for (var k = 0; k < _val.length; k++) {
                if (digits.indexOf(_val.charAt(k)) == -1) {
                    return false;
                }
            }
            return true;
        }
        ;
        /*
        *   Utility method for parsing in getDateFromFormat()
        */
        function _getInt(str, l, minlength, maxlength) {
            for (var m = maxlength; m >= minlength; m--) {
                var _token = str.substring(l, l + m);
                if (_token.length < minlength) {
                    return null;
                }
                if (_isInteger(_token)) {
                    return _token;
                }
            }
            return null;
        }
        ;
    };
    /*
    * parseDate( date_string [, prefer_euro_format] )
    *
    * This function takes a date string and tries to match it to a
    * number of possible date formats to get the value. It will try to
    * match against the following international formats, in this order:
    * y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
    * M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
    * d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
    * A second argument may be passed to instruct the method to search
    * for formats like d/M/y (european format) before M/d/y (American).
    * Returns a Date object or null if no patterns match.
    */
    this.parseDate = function (val) {
        var preferEuro = (arguments.length == 2) ? arguments[1] : false;
        var generalFormats = new Array('y-M-d', 'MMM d, y', 'MMM d,y', 'y-MMM-d', 'd-MMM-y', 'MMM d');
        var monthFirst = new Array('M/d/y', 'M-d-y', 'M.d.y', 'MMM-d', 'M/d', 'M-d');
        var dateFirst = new Array('d/M/y', 'd-M-y', 'd.M.y', 'd-MMM', 'd/M', 'd-M');
        var checkList = new Array(generalFormats, preferEuro ? dateFirst : monthFirst, preferEuro ? monthFirst : dateFirst);
        var d;
        for (var i = 0; i < checkList.length; i++) {
            var l = window[checkList[i]];
            for (var j = 0; j < l.length; j++) {
                d = getDateFromFormat(val, l[j]);
                if (d != 0) {
                    return new Date(d);
                }
            }
        }
        return null;
    };
    /*
    *   Method to analyze a time format and checks separator, and hour format, and seconds
    */
    this.analyzeTimeFormat = function (timeFormat) {
        var i = 0;
        var c;
        // Define return object
        var returnObj = {
            show24Hours: false,
            showSeconds: false,
            separator: ":"
        };
        // Analize format
        var token, lastToken = "";
        while (i < timeFormat.length) {
            // Get next token from format string
            c = timeFormat.charAt(i);
            token = "";
            while ((timeFormat.charAt(i) == c) && (i < timeFormat.length)) {
                token += timeFormat.charAt(i++);
            }

            // Extract contents of value based on format token
            if (token == "hh" || token == "h") {
                lastToken = token;
                returnObj.show24Hours = false;
            }
            else if (token == "HH" || token == "H") {
                lastToken = token;
                returnObj.show24Hours = true;
            }
            else if (token == "mm" || token == "m") {
                lastToken = token;
            }
            else if (token == "ss" || token == "s") {
                lastToken = token;
                returnObj.showSeconds = true;
            }
            else if (token == "a") {
                lastToken = token;
            }
            else {
                if (lastToken.toUpperCase() == "H" || lastToken.toUpperCase() == "HH") {
                    returnObj.separator = token;
                }
            }
        }

        return returnObj;
    };
    this.getDateFromInvariant = function (value, showTime) {
        value = (value == null) ? "" : value;
        value = (typeof value != "string") ? value.toString() : value;
        var INVARIANT_FORMAT = "MM/dd/yyyy" + (showTime ? " H:mm:ss" : "");
        if (showTime) {
            if (value && (value.toLowerCase().indexOf("am") > 0 || value.toLowerCase().indexOf("pm") > 0)) {
                INVARIANT_FORMAT = "MM/dd/yyyy h:mm:ss a";
            }
        }
        var date = bizagi.util.dateFormatter.getDateFromFormat(value, INVARIANT_FORMAT);
        // Also try to read the date with full format if the last instruction didn't success
        if (date == 0 && !showTime) {
            INVARIANT_FORMAT = "MM/dd/yyyy H:mm:ss";
            date = bizagi.util.dateFormatter.getDateFromFormat(value, INVARIANT_FORMAT);
        }

        return date;
    };
    this.formatInvariant = function (date, showTime) {
        var INVARIANT_FORMAT = "MM/dd/yyyy" + (showTime ? " HH:mm:ss" : "");
        var formattedDate = bizagi.util.dateFormatter.formatDate(date, INVARIANT_FORMAT);
        return formattedDate;
    };
    this.getDateFromISO = function (value, showTime) {
        var ISO_FORMAT = "yyyy-MM-dd" + (showTime ? " HH:mm" : "");
        var date = bizagi.util.dateFormatter.getDateFromFormat(value, ISO_FORMAT);
        // If the date could not be parsed, try it out with seconds
        if (date == 0 && showTime) {
            ISO_FORMAT = "yyyy-MM-dd" + (showTime ? " HH:mm:ss" : "");
            date = bizagi.util.dateFormatter.getDateFromFormat(value, ISO_FORMAT);
        }

        // Also try to read the date with full format if the last instruction didn't success
        if (date == 0 && !showTime) {
            ISO_FORMAT = "yyyy-MM-dd HH:mm";
            date = bizagi.util.dateFormatter.getDateFromFormat(value, ISO_FORMAT);
            // If the date could not be parsed, try it out with seconds
            if (date == 0) {
                ISO_FORMAT = "yyyy-MM-dd HH:mm:ss";
                date = bizagi.util.dateFormatter.getDateFromFormat(value, ISO_FORMAT);
            }
        }

        return date;
    };
    this.formatISO = function (date, showTime) {
        var ISO_FORMAT = "yyyy-MM-dd" + (showTime ? " HH:mm" : "");
        var formattedDate = bizagi.util.dateFormatter.formatDate(date, ISO_FORMAT);
        return formattedDate;
    };
    this.sleep = function (delay) {
        // Delay in miliseconds
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay) {
            // wait
        }
    };
};



bizagi.util.smartphone.startLoading = function (container) {
    bizagi.kendoMobileApplication.showLoading();
};
bizagi.util.smartphone.stopLoading = function (container) {

    bizagi.kendoMobileApplication.hideLoading();
};

bizagi.util.smartphone.startkendo = function () {
    var self = this;
    if (typeof bizagi.kendoMobileApplication === "undefined") {
        bizagi.kendoMobileApplication = new kendo.mobile.Application($("body"), {
            transition: "slide",
            skin: "flat",
            initial: "initKendo",
            //webAppCapable: false ,
            // serverNavigation : true,
            init: function () {
                this.showLoading();
                //  bizagi.kendoMobileApplication.showLoading();
            }

        });

        // bizagi.kendoMobileApplication.showLoading();
        bizagi.kendoMobileApplication.bizagiNavigate = function (e, t) {
            //this.hideLoading();
            this.navigate(e, t);
        };

        // bizagi.kendoMobileApplication.showLoading();
        /*bizagi.kendoMobileApplication.tmpl = function (tmpl, data) {
        bizagi.chrono.start("kendotemplate");
        var result = tmpl(data);
        bizagi.chrono.stop("kendotemplate");
        return result;

        };*/



    }
};


/**
* determines whether the element in the browser supports the event we are looking for, eg 'paste', 'input', 'blur', etc. More info check modernizer.js.
* @param  {string|*}           eventName  is the name of an event to test for (e.g. "resize")
* @param  {(Object|string|*)=} element    is the element|document|window|tagName to test on
* @return {boolean}
*/
bizagi.util.isEventSupported = function (eventName, element) {
    var needsFallback = !('onblur' in document.documentElement);
    var isSupported;

    if (!eventName) {
        return false;
    }
    if (!element || typeof element === 'string') {
        element = document.createElement(element || 'div');
    }

    // Testing via the `in` operator is sufficient for modern browsers and IE.
    // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and
    // "resize", whereas `in` "catches" those.
    eventName = 'on' + eventName;
    isSupported = eventName in element;

    // Fallback technique for old Firefox - bit.ly/event-detection
    if (!isSupported && needsFallback) {
        if (!element.setAttribute) {
            // Switch to generic element if it lacks `setAttribute`.
            // It could be the `document`, `window`, or something else.
            element = document.createElement('div');
        }
        if (element.setAttribute && element.removeAttribute) {
            element.setAttribute(eventName, '');
            isSupported = typeof element[eventName] === 'function';

            if (element[eventName] !== undefined) {
                // If property was created, "remove it" by setting value to `undefined`.
                element[eventName] = undefined;
            }
            element.removeAttribute(eventName);
        }
    }
    return isSupported;
};



/**
* Calculate circular dependencies 
* 
* @example var a = new bizagi.circularDependencies(); a.addNode('name',obj);
* a.resolve(node);
* 
*/
//TODO: DEPL - Check this because we agreed to a much simpler solution
bizagi.circularDependencies = function () {
    var circularDependencies = new Function();
    var self = circularDependencies.prototype;
    self.list = []; //[{ name: "root", obj: {}, nodeObj: {}, childs: {} }];
    self.lastNodeAdded; // Undefined reference

    self.addNode = function (name, obj) {
        var OBJ = JSON.encode(obj);
        var foundObjChildNode = _search(name, OBJ);
        if (foundObjChildNode) {
            var lastNode = self.lastNodeAdded || self.list[self.list.length - 1];
            lastNode.nodeObj.addChild(foundObjChildNode.nodeObj);
            self.lastNodeAdded = foundObjChildNode;
        } else {
            var node = new _addNode(name, OBJ);
            self.list.push({
                name: name,
                obj: OBJ,
                nodeObj: node,
                timeStamp: new Date().getTime()
            });
            return node;
        }
    };

    self.resolve = function (node, resolved, seen) {
        // if dont setted node variable, take the first one -> root
        node = node || _getFirstNodeWithChilds() || self.list[0].nodeObj;
        var resolveDependencies = new _resolve(node, resolved, seen);
        return resolveDependencies;
    };

    var _resetList = function () {
        self.list = [];
    };

    var _getFirstNodeWithChilds = function () {
        for (var i = 0; i < self.list.length; i++) {
            if (self.list[i].nodeObj.childs.length > 0) {
                return self.list[i].nodeObj;
            }
        }
        return (self.list.length > 0) ? self.list[0].nodeObj : {};
    };

    var _search = function (name, obj) {
        for (var i = 0; i < self.list.length; i++) {
            if (self.list[i].name == name) {
                // remove tag element
                var now = new Date().getTime();
                var timeToKeep = 2000; //milliseconds
                if ((self.list[i].timeStamp + timeToKeep) < now) {
                    //remove element from the list
                    self.list.splice(i, 1);
                } else if (self.list[i].obj == obj) {
                    return self.list[i];
                }
            }
        }
        return false;
    };

    var _addNode = (function (name, obj) {
        name = name || "";
        obj = obj || {};
        var node = function (name, obj) {
            this.name = name;
            this.obj = obj;
            this.childs = [];
        };
        var self = node.prototype;

        self.addChild = function (node) {
            this.childs.push(node);
        };

        return node;
    })();

    var _resolve = (function (node, resolved, seen) {
        var resolve = function (node, resolved, seen) {
            this.node = node || {};
            this.node.childs = this.node.childs || [];
            this.resolved = resolved || [];
            this.seen = seen || [];

            this.seen.push(this.node);

            for (var i = 0; i < this.node.childs.length; i++) {
                if (!self.hasObject(this.node.childs[i], this.resolved)) {
                    if (self.hasObject(this.node.childs[i], this.seen)) {
                        //throw new _error("error node:" + node.name + " -> " + node.childs[i].name);
                        _resetList();
                        var error = {
                            dependencyFrom: this.node.name,
                            dependencyFromObj: this.node.obj,
                            dependencyTo: this.node.childs[i].name,
                            dependencyToObj: this.node.childs[i].obj,
                            path: this.seen
                        };

                        return {
                            error: $.extend({
                                standardError: {
                                    status: "error",
                                    responseText: JSON.encode({ message: self.makeErrorMessage(error), type: "alert" })
                                },
                                multiactionError: {
                                    message: self.makeErrorMessage(error),
                                    type: "alert"
                                }
                            }, error)
                        };
                    } else {
                        resolve(this.node.childs[i], this.resolved, this.seen);
                    }
                }
            }
            if (this.resolved) {
                this.resolved.push(this.node);
            }
        };

        var self = resolve.prototype;

        self.hasObject = function (toSearch, collection) {

            for (var i = 0; i < collection.length; i++) {
                if (collection[i].name == toSearch.name) {
                    return true;
                }
            }
            return false;
        };

        self.makeErrorMessage = function (error) {
            var messageTmpl = bizagi.localization.getResource("render-actions-loop-validation");

            return printf(messageTmpl, error.dependencyFrom, error.dependencyTo);

            return {
                responseText: parsedMessage,
                message: parsedMessage,
                extraInfo: error
            };
        };

        return resolve;
    })();

    return self;
};

//TODO: DEPL - Move this to rendering
bizagi.util.autoSave = function () {

    var deferredSave = $.Deferred();

    //if attr data-event exist trigger event auto-save or resolve the deferred
    if ($(document).data('auto-save')) {

        $(document).trigger('save-form', [deferredSave]);
    } else {
        deferredSave.resolve();
    }

    return deferredSave.promise();

};

/*
*   Creates a replace all method that is left from the String Class
*/
bizagi.util.replaceAll = function (text, pcFrom, pcTo) {
    // Call the method located in bizagi.loader
    return bizagi.replaceAll(text, pcFrom, pcTo);
};
// Also append it to the string class
String.prototype.replaceAll = function (pcFrom, pcTo) {
    return bizagi.util.replaceAll(this, pcFrom, pcTo);
};

/*
*   Creates a trim method 
*/
bizagi.util.trim = function (text) {
    if (typeof (text) === "undefined" || text == null)
        return text;
    // Call the method located in bizagi.loader
    return text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
};


/* 
*   Converts a percent into a number
*/
bizagi.util.percent2Number = function (value) {
    return Number(String(value).replace("%", ""));
};
/*
*   Check if a object is empty
*/
bizagi.util.isObjectEmpty = function (obj) {
    if (obj.length > 0)
        return false;
    return true;
};


// Parses a value to return the correct boolean value
bizagi.util.parseBoolean = function (value) {
    if (value === undefined) {
        return null;
    }

    if (value === null) {
        return null;
    }

    if (value === '') {
        return null;
    }

    // Parse true values
    if (value === true || value === 1 || value.toString() === "true") {
        return true;
    }
    if (value.toString().toLowerCase() === "true") {
        return true;
    }

    // Parse false values
    if ((value !== null && value === false) || value === 0 || value.toString() === "false") {
        return false;
    }
    if (value.toString().toLowerCase() === "false") {
        return false;
    }

    return null;
};

bizagi.util.setContext = function (params, reset) {
    params = params || {};
    reset = reset || false;
    if (reset) {
        bizagi.context = {};
    }

    $.each(params, function (key, value) {
        bizagi.context[key] = value;
    });
}

/**
* Decode data from encodeURI charset
*/
bizagi.util.decodeURI = function (value) {
    value = value || "";
    var finishDecoded = false;
    var decodedValue = value;
    var infinityControl;

    // Try to decode data value
    while (!finishDecoded) {
        try {
            infinityControl = decodeURI(decodedValue);
            if (infinityControl == decodedValue) {
                finishDecoded = true;
            } else {
                decodedValue = infinityControl;
            }
        } catch (e) {
            finishDecoded = true;
        }
    }

    return decodedValue;
};


(function ($) {
    jQuery.nl2br = function (value) {
        return value.replace(/(\r\n|\n\r|\r|\n|\\r\\n|\\n\\r|\\r|\\n)/g, "<br>");
    };
})(jQuery);

bizagi.util.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

//remove all the character except the +, - and decimal point
bizagi.util.getStandardNotation = function(number, decimalSymbol){
    var plusCharacter = '+';
    var minusCharacter = '-';
    var decimalSymbol = decimalSymbol || '.';
    var expSymbol = 'e';
    var tmp = '';
    //remove all the character except the +, -, decimal point and the numbers
    var decimalNumber = false;
    var exponentialNumber = false;
    number = (number.length>0?number.toString().toLowerCase():'').replace(/ /g,'');
    for(var i = 0; i < number.length; i++){
        if((number[i] === plusCharacter || number[i] === minusCharacter) && (i === 0 || number[i-1] == expSymbol)){
            tmp += number[i];
        }else if(!isNaN(Number(number[i]))){
            tmp += number[i];
        }
        else if((number[i] === decimalSymbol) && !decimalNumber){
            tmp += number[i];
            decimalNumber = true;
        }
        else if((number[i] === expSymbol) && !exponentialNumber){
            tmp += number[i];
            exponentialNumber = true;
        }
    }
    return tmp;
};

//Return it in normalized scientific notation
bizagi.util.scientificNotationFormat = function(number, decimalSymbol, sdLimit, expMinLimit, expMaxLimit){
    decimalSymbol = decimalSymbol || '.';
    number = bizagi.util.getStandardNotation(number, decimalSymbol);
    sdLimit = ( sdLimit > 0 ) ? sdLimit : 38;
    expMinLimit = ( expMinLimit > 0 ) ? expMinLimit : -125;
    expMaxLimit = ( expMaxLimit > 0 ) ? expMaxLimit : 125;
    var expSimbol = 'e';
    var sNregex = new RegExp('[0-9]+(' + decimalSymbol + '[0-9]+)?(e[+-]?[0-9]+)?');
    var sNFormat = number.match(sNregex);

    //get que prefix
    var exp = 0;

    var prefix = number[0] === '-'? number[0]: '';
    var originalPointIndex = 0;

    number = number.replace(prefix, '');

    //if the number is not in scientific notation
    if(sNFormat && sNFormat[0] && sNFormat[2]){
        var tempParts = sNFormat[0]?(sNFormat[0].replace(sNFormat[2], '').split(decimalSymbol)):'';

        var significantDigits = '';
        var intPart = '';

        //calculate significant digits
        //remove zeros at the left
        tempParts[0] = tempParts[0].replace(/^0+/g,'');

        if (tempParts[0].length > 1){
            significantDigits = tempParts[0].slice(1);
            originalPointIndex = significantDigits.length;
            intPart = tempParts[0][0] || '';
            significantDigits = significantDigits.concat(sNFormat[1]||'');
        }
        else if (tempParts[0].length === 1){
            intPart = tempParts[0][0] || '';
            significantDigits = significantDigits.concat(tempParts[1]||'');
        }
        else{
            originalPointIndex = tempParts[1].length;
            tempParts[1] = tempParts[1].replace(/^0+/g,'');
            originalPointIndex = (tempParts[1].length - 1) - originalPointIndex;
            intPart = tempParts[1][0];
            significantDigits = tempParts[1].slice(1);
        }
        //remove . and zeros from significant digits
        significantDigits = significantDigits.replace(decimalSymbol, '').replace(/0+$/g,'');

        if(significantDigits.length >= sdLimit){
            significantDigits = significantDigits.slice(0, sdLimit-1);
        }
        exp = Number(sNFormat[2].replace(expSimbol, '')) + originalPointIndex;
        exp = (exp>expMaxLimit)?expMaxLimit:(exp<expMinLimit)?expMinLimit:exp;
        return prefix + intPart + (significantDigits?(decimalSymbol + significantDigits):'') + expSimbol + (exp>0?'+'+exp:exp);
    }
    else if(number){

        originalPointIndex = number.indexOf(decimalSymbol);
        var from = 0;
        var to = number.length-1;

        //remove decimal point
        number = number.replace(decimalSymbol, '');

        //from
        var i = from;
        while(!significantFlag){
            if(number[i]!== '0'&& number[i]!== decimalSymbol ){significantFlag = true;from = i;}
            else {i++;}
        }
        //to
        var significantFlag = false;
        var j = to;
        while(!significantFlag){
            if(number[j]!== '0'&& number[j]!== decimalSymbol && '-'){significantFlag = true; to = j + 1;}
            else {j--;}
        }
        to = to>sdLimit+from?sdLimit+from:to;
        if(originalPointIndex !== -1){
            //find significant digits
            var significantFlag = false;
            var significantDigits = number.slice(from, to);
            var pointIndex = from + 1;
            exp = originalPointIndex - pointIndex;
            if(significantDigits.length > 0 && !isNaN(Number(significantDigits[0]))){
                return prefix + significantDigits[0] + (significantDigits.length > 1? decimalSymbol + significantDigits.slice(1):'') + expSimbol + (exp>0?'+'+exp:exp);
            }
            else{ return '0';}

        }
        else{
            //Int numbers
            var firstDigit =  number.slice(from, from + 1);
            var restDigits =  number.slice(from + 1, number.length);
            exp = restDigits.length;
            restDigits = restDigits.replace(/0+$/g,'');
            if(restDigits.length > sdLimit){
                restDigits =  number.slice(from + 1, sdLimit + from);
            }
            if(Number(firstDigit)){
                return prefix + firstDigit + (restDigits.length > 0? decimalSymbol + restDigits: '') + expSimbol + (exp>0?'+'+exp:exp);
            }else{ return '0';}
        }
    }
    else{ return ''; }
};