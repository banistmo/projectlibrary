
/*
*   Name: BizAgi Workportal Webpart helper
*   Author: oscaro
*   Comments:
*   -   This script will define helper functions that the webparts could use to reuse some logic
*/
/*
*   Static Methods to configure webparts
*/
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.webparts = (typeof (bizagi.webparts) !== "undefined") ? bizagi.webparts : {};


$.Class.extend("bizagi.workportal.webpart.helper", {}, {
    /* 
    *   Constructor
    */
    init: function (webpart) {
        // Set data service
        this.dataService = webpart.dataService;

        // Set l10n service
        this.resources = webpart.resources;

        // Set workportal facade
        this.workportalFacade = webpart.workportalFacade;

        //Load Aditional templates
        this.templates = {};
    },
    /*  
    *   Returns the file url
    */
    getFileImage: function (files) {
        var self = this;

        if (!files || files.length == 0) return self.getEmptyFile();
        return files;
    },
    getFile: function (files) {
        var self = this;

        if (!files || files.length == 0) return self.getEmptyFile();
        return self.dataService.getFile(files[0]);
    },

    loadTemplate: function (template, templateDestination) {
        var self = this;

        // Go fetch the template
        return bizagi.templateService.getTemplate(templateDestination)
                .done(function (resolvedRemplate) {
                    self.templates[template] = resolvedRemplate;
                });
    },

    /*
    publishShowRenderEvent: function (event, webpart, params) {
        // Check if the current render form can be changed
        if (!!!event) {
            alert("Event param cannot be undefined");
        }
        //The webparts that not visible no receive events  
        var deferRet = webpart.publish("ui-bizagi-can-change-activityform");
        if (deferRet) {
            deferRet.done(function () {
                // When the "ui-bizagi-can-change-activityform" event deferred is resolved, it means that we can change the activityform instance
                // otherwise the deferred fails and nothing happens because we don't have fail implementation

                // Publish the event so any other webpart could react to that
                webpart.publish(event, params);
            });
        }
        else {
            //Error autosave verification send error
            alert("autosave verification send error");
            //webpart.publish(event, params);
        }
    },*/

    //beta Iframe
    isWebpartInIFrame: function () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    },
    //pending implement
    getFailServerErrorDeferred: function (a,b,c) { }

    /*,
    //This method send a message by postMessage to parent, and parent create a popUp,
    displayWebPartPopUpExternalIframe: function (webpartName, params, callback, postMessageSocket) {


    params.workportal = null;
    params.canvas = null;
    params.postMessageEventName = "postmessage-bizagi-openParentPopUp";
    params.webPartToDraw = webpartName;
    postMessageSocket.send(JSON.stringify(params));


    },
    displayWebPartPopUpinIFrame: function (webpartName, params, callback) {

    var webpartObject;
    // Publish the event so any other webpart could react to that
    var mask = document.createElement("div");
    mask.className = "containerPopUp";
    mask.style.height = "100%";

    var popUpParams = params;

    if (!mask.addEventListener) {
    mask.attachEvent("onclick", preClosePopUp);
    }
    else {
    mask.addEventListener("click", preClosePopUp, false);
    }

    function preClosePopUp(ev, params) {
    if (ev.eventPhase === 2) {
    closePopUp(ev, params);
    }
    }

    var canvas = document.createElement("div");
    canvas.className = "activitiFormContainer";
    var closeCanvas = document.createElement("div");
    closeCanvas.className = "closeCanvas";

    if (!closeCanvas.addEventListener) {
    closeCanvas.attachEvent("onclick", function (e, params) {
    closePopUp(e, params);
    });
    }
    else {
    closeCanvas.addEventListener("click", function (e, params) {
    closePopUp(e, params);
    }, false);
    }

    canvas.appendChild(closeCanvas);
    mask.appendChild(canvas);

    var webpartCanvasDiv = document.createElement("div");
    webpartCanvasDiv.className = "webpartCanvasDiv";
    webpartCanvasDiv.id = "webpartCanvasPopUp";
    canvas.appendChild(webpartCanvasDiv);

    var pathiFrame = "";
    if (params.pathiFrame) {
    pathiFrame = params.pathiFrame;
    }
    else {
    pathiFrame = window.location.href.substring(0, window.location.href.indexOf("jquery"));
    }

    var queryStringParams = window.location.toString().split('?')[1];


    pathiFrame = pathiFrame + "jquery/webparts/desktop/portal/pages/webpart.htm"
    pathiFrame = pathiFrame + "?type=" + webpartName;
    if (params.idCase) { pathiFrame = pathiFrame + "&idCase=" + params.idCase; }
    if (params.idWorkitem) { pathiFrame = pathiFrame + "&idWorkitem=" + params.idWorkitem; }
    if (params.idWfClass) { pathiFrame = pathiFrame + "&idWfClass=" + params.idWfClass; }
    if (queryStringParams) { pathiFrame = pathiFrame + "&" + queryStringParams; }

    pathiFrame = pathiFrame + "&remoteServer=" + window.location.toString();


    //Aqui se agrega el iframe
    var iframe = document.createElement("iframe");
    iframe.className = "csswebpartIframePopUp";
    iframe.id = "iframePopUp";
    iframe.src = pathiFrame;
    iframe.resizeInPopUp = function (params) {
    var self = this;
    var heightActivitiFormContainer = $(".activitiFormContainer").height() || 0;
    $(iframe).height(heightActivitiFormContainer);
    }

    webpartCanvasDiv.appendChild(iframe);
    document.body.appendChild(mask);

    iframe.resizeInPopUp(params);
    $(window).resize(function () {
    iframe.resizeInPopUp(params);
    });

    function closePopUp(ev, params) {
    //if webpart has canHide event, call event canHide before

    var postmessageSocket = new bizagi.postmessage({ remoteServer: pathiFrame, destination: iframe.contentWindow, origin: window });
    postmessageSocket.receive = function (msg) {
    //the canHide process is made in iframe
    var params = eval("(" + msg.data + ")");
    if (params.postMessageEventName == "postmessage-bizagi-closeIFramePopUp") {
    if (mask.parentNode) {
    mask.parentNode.removeChild(mask);
    if (callback) {
    callback(this, popUpParams);
    }
    }
    }
    };
    var iframeclosemsg = {};
    iframeclosemsg.postMessageEventName = "postmessage-bizagi-canCloseIFramePopUp";
    postmessageSocket.send(JSON.stringify(iframeclosemsg));
    }

    },
    displayWebPartPopUp: function (webpartName, params, callback) {
    var webpartObject;
    // Publish the event so any other webpart could react to that
    var mask = document.createElement("div");
    mask.className = "containerPopUp";
    mask.style.height = "100%";

    var popUpParams = params;

    if (!mask.addEventListener) {
    mask.attachEvent("onclick", preClosePopUp);
    }
    else {
    mask.addEventListener("click", preClosePopUp, false);
    }

    function preClosePopUp(ev, params) {
    if (ev.eventPhase === 2) {
    closePopUp(ev, params);
    }
    }

    function closePopUp(ev, params) {
    //if webpart has canHide event, call event canHide before
    if (webpartObject.canHide) {
    $.when(webpartObject.canHide())
    .done(function () {
    mask.parentNode.removeChild(mask);
    if (callback) {
    callback(this, popUpParams);
    }
    webpartObject.destroy();
    }).fail(function () {
    mask.parentNode.removeChild(mask);
    if (callback) {
    callback(this, popUpParams);
    }
    webpartObject.destroy();
    });
    }
    else {
    mask.parentNode.removeChild(mask);
    if (callback) {
    callback(this, popUpParams);
    }
    webpartObject.destroy();
    }
    }

    var canvas = document.createElement("div");
    canvas.className = "activitiFormContainer";
    var closeCanvas = document.createElement("div");
    closeCanvas.className = "closeCanvas";

    if (!closeCanvas.addEventListener) {
    closeCanvas.attachEvent("onclick", function (e, params) {
    closePopUp(e, params);
    });
    }
    else {
    closeCanvas.addEventListener("click", function (e, params) {
    closePopUp(e, params);
    }, false);
    }

    canvas.appendChild(closeCanvas);
    mask.appendChild(canvas);

    var webpartCanvasDiv = document.createElement("div");
    webpartCanvasDiv.className = "webpartCanvasDiv";
    webpartCanvasDiv.id = "webpartCanvasPopUp";
    canvas.appendChild(webpartCanvasDiv);

    var webpartParams = $.extend({}, params, { webpart: webpartName, canvas: $(webpartCanvasDiv), namespace: "PopUp", data: null, adjustButtonsToContent: true });

    //add popUp in document
    document.body.appendChild(mask);
    //The element webpartCanvasDiv must be already added to document, for can call  bizagi.starWaiting
    bizagi.startWaiting(webpartCanvasDiv);

    params.workportal.executeWebpart(webpartParams).done(function (result) {
    webpartObject = result.webpart;
    closeCanvas.onclick = function (e, params) {
    // callback(params);
    }
    });
    }*/
});


