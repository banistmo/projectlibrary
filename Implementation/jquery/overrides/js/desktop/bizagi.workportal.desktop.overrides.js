/**
* File reserved for clients
*/

/*----- CUSTOM MENU OPTIONS ---- */

// Add a button at the Menu

function ProjectUtilService(Options) {
    
    return $.ajax({
        type: "POST",
        url: "App/LocalAspx/ProjectUtilService.asmx/" + Options.method,
        data: "{options:" + JSON.stringify(Options.parameters) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

bizagi.override.addCustomMenuButton({
    id: "Test",
    className: "icon Test",
    text: "test Service",
    //EXAMPLE: SHOWING A FRAME WITH INFORMATION 
    click: function () {
        ProjectUtilService({
            "method": "GetGDFCategories",
            "parameters": { "userId": "1","categoryId": "1"}
        }).done(function (data) {
            $("#menuQuery").text(data.d);
        });
    }
});

bizagi.override.addCustomMenuButton({
    id: "customMenuReports",
    className: "css_customMenuReports",
    text: "Informes",
    click: function (menu) {
        var mainDiv = $("<div>");
        var myFrame = $("<iframe>");
        var width1 = $(window).width();
        var width2 = width1 * 0.96;
        var height1 = $(window).height();
        var height2 = height1 * 0.97;
        var sTokenA = window.btoa(bizagi.currentUser.language);
        var sTokenB = window.btoa(bizagi.currentUser.idUser);
        var sUrlReport = "App/LocalAspx/GeneralReportsParameters.aspx?tokena=" + sTokenA + "&tokenb=" + sTokenB;
        myFrame.attr("src", sUrlReport);
        myFrame.addClass("css_myFrame");
        mainDiv.append(myFrame);
        mainDiv.dialog({
            height: 600,
            width: 800,
            modal: true,
            title: "Informes"
        });
        mainDiv.dialog("option", "width", width2);
        mainDiv.dialog("option", "height", height2);
    }
});

bizagi.override.addCustomMenuButton({
    id: "basicQuery",
    className: "icon css_basicQuery queries",
    text: "Consulta b�sica",
    //EXAMPLE: SHOWING A FRAME WITH INFORMATION 
    click: function () {
        bizagiWorkportal.mainController.publish("showDialogWidget", 
                    {
                    "widgetName":"queryform",
                    "queryFormAction":"renderQueryForm",
                    "queryType":"1",
                    "paramsStoredQuery":{},
                    "modalParameters":{"title":"Consulta b�sica de casos","id":"Queries"},
                    "closeVisible":false,
                    "guidForm":"04195867-0c6b-4262-abc5-3cfbbb36765b",
                    "guidEntity":"a850b64f-d5f6-4c11-9c05-dc260a3ab7c2",
                    "notMigratedUrl":"",
                    "maximizeOnly":true
                    }
                );
            }
        });

bizagi.override.customizeWorkportalWidget("queriesDefinition",
	function (obj, widget) {
	    var divInformes = $('<div id="modalMenuBtList" class="clearfix"></div>');
	    var divInterno = $('<div id="queries2"></div>');
	    var ulInformes = $('<ul class="queriesListDisplay"><li><div class="processIco"></div><h3>Informes</h3></li></ul>');
	    ulInformes.click(function (menu) {
	        var mainDiv = $("<div>");
	        var myFrame = $("<iframe>");
	        var width1 = $(window).width();
	        var width2 = width1 * 0.96;
	        var height1 = $(window).height();
	        var height2 = height1 * 0.97;
	        var sTokenA = window.btoa(bizagi.currentUser.language);
	        var sTokenB = window.btoa(bizagi.currentUser.idUser);
	        var sUrlReport = "App/LocalAspx/GeneralReportsParameters.aspx?tokena=" + sTokenA + "&tokenb=" + sTokenB;
	        myFrame.attr("src", sUrlReport);
	        myFrame.addClass("css_myFrame");
	        mainDiv.append(myFrame);
	        mainDiv.dialog({
	            height: 600,
	            width: 800,
	            modal: true,
	            title: "Informes"
	        });
	        mainDiv.dialog("option", "width", width2);
	        mainDiv.dialog("option", "height", height2);
	    });
	    ulInformes.appendTo(divInterno);
	    divInterno.appendTo(divInformes);
	    widget.append(divInformes);
	}
); 