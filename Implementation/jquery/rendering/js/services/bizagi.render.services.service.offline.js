/*
*   Name: BizAgi Rendering Services
*   Author: oscaro
*   Comments:
*   -   This class will provide a facade to access to workportal REST services
*/

bizagi.render.services.service.extend("bizagi.render.services.service",
 {
     // Statics

 },
{

    /* 
    *   Constructor
    */
    init: function (params) {
        // Call base
        var self = this;
        self._super(params);

        //todo pasar la referencia de services workportal usar menos memoria
        self.database = params.database; //(typeof bizagi.workportal.services.db != "undefined") ? new bizagi.workportal.services.db() : null;
    },

    getFormData: function (params) {
        var self = this;

        params = params || {};

        if (!params.isOfflineForm) {
            return self._super(params);
        } else {
            // alert("cargar caso id");
            return $.when(self.database.getCase(params.idCase)).then(function (response) {
                //response.idWfClass
                return self.database.mergeForm(response, self.database.getFormData(response.idWfClass.toString()));
            });
        }

    },

    submitData: function (params) {
        var self = this;

        params = params || {};

        if (!params.isOfflineForm) {
            return self._super(params);
        } else {

            data = self.resolveData(params.data || {}, params.xpathContext);
            data[self.Class.BA_CONTEXT_PARAMETER_PREFIX + "action"] = params.action;
            return self.database.saveFormInfo(params, data);
        }
    }
});
