﻿/*
*   Name: BizAgi smartphone Render Label Extension
*   Author: oscaro
*   Comments:
*   -   This script will redefine the label render class to adjust to smartphones devices
*/

// Extends itself
bizagi.rendering.upload.extend("bizagi.rendering.upload", {
    BA_ACTION_PARAMETER_PREFIX: bizagi.render.services.service.BA_ACTION_PARAMETER_PREFIX,
    BA_CONTEXT_PARAMETER_PREFIX: bizagi.render.services.service.BA_CONTEXT_PARAMETER_PREFIX,
    QUALITY_PICTURE: 80,
    LIMIT: 1, //limit: The maximum number of audio clips,video clips,etc in the device user can record in a single capture operation.
    EXTENSIONSIMG: ["image/jpeg", "jpeg", "image", "png", "jpg"],
    EXTENSIONSVIDEO: ["video/quicktime", "quicktime", "qt", "mov"],
    EXTENSIONSAUDIO: ["audio/wav", "audio", "wav"]
}, {

    postRenderSingle: function () {
        var self = this;
        var properties = self.properties;
        var container = self.getControl();
        var containerRender = self.getContainerRender();
        var body = $("body");
        self.configureHelpText();
        self.getArrowContainer().css("visibility", "hidden");

        if (!properties.editable) {
            containerRender.addClass("bz-command-not-edit");
        } else {
            containerRender.addClass("bz-command-edit-inline");
        }

        self.itemAddfile = $(".bz-rn-upload-show-menu", container);

        if (typeof (cordova) === "undefined" || properties.editable == false) {
            self.itemAddfile.hide();
            return;
        }

        self.itemsUpload = {
            "file": $(".file", container),
            "image": $(".image", container),
            "cimage": $(".cimage", container),
            "caudio": $(".caudio", container),
            "cvideo": $(".cvideo", container)
        };

        self.itemAddfile.bind("click", function (e) {
            e.stopPropagation();
            var containerUploadItems = $(".bz_rn_upload_container_upload_items", container);
            //mejorar la implementacion cuando abro varios casos especiales al tiempo
            //bug IOS : z-inded for first item
            if (self.hideUploadItems()) {
                return;
            }

            if (containerUploadItems.is(':visible')) {
                containerUploadItems.hide();
                self.removeListener();

                return;
            }

            //this try/catch is use for close other upload controls in the form
            try {
                var formContainer = self.getContainerRender().parent();
                if (formContainer) {
                    formContainer.find(".bz_rn_upload_container_upload_items:visible").hide();
                }
            } catch (e) { }

            containerUploadItems.show();
            containerUploadItems.css("z-index", 20);

            //verify the visivility for container and puts the class for arrow
            var heigthContainer = parseInt(containerUploadItems.css("height"));
            if (((self.itemAddfile.offset().top - 60) - heigthContainer) > 0) {

                containerUploadItems.addClass("bottomArrow");

                containerUploadItems.position({
                    of: self.itemAddfile,
                    my: "center bottom",
                    at: "center top",
                    offset: "-10px",
                    collision: "flipfit flipfit"
                });

            }

            else {
                //bug IOS : z-inded for first item
                containerUploadItems.hide();
                containerUploadItems.addClass("bz_clone_active");
                var cloneUpload = containerUploadItems.clone();
                cloneUpload.attr("id", "bz_active_clone_Upload");
                cloneUpload.addClass("upArrow");
                cloneUpload.appendTo(body);
                cloneUpload.css('display', 'inline-block');

                cloneUpload.position({
                    of: self.itemAddfile,
                    my: "center top",
                    at: "center bottom",
                    offset: "5px",
                    collision: "flipfit flipfit"
                });

            }

            self.itemsUpload = {
                "file": $(".file", cloneUpload),
                "image": $(".image", cloneUpload),
                "cimage": $(".cimage", cloneUpload),
                "caudio": $(".caudio", cloneUpload),
                "cvideo": $(".cvideo", cloneUpload)
            };

            self.addListener();

            // if click out the upload (click in document), the upload menu will be removed
            $(document).bind("click", function () {
                self.hideUploadItems();
                $(document).unbind("click");
            });
        });

        self.checkExtensions();
        self.checkMaxFiles();

    },

    hideUploadItems: function () {
        var self = this;
        var container = self.getControl();
        var body = $("body");
        var containerUploadItems = $(".bz_rn_upload_container_upload_items", container);

        if (containerUploadItems.hasClass("bz_clone_active")) {

            containerUploadItems.removeClass("bz_clone_active");
            var divClone = $("#bz_active_clone_Upload", body);

            divClone.hide();
            divClone.remove();

            containerUploadItems.hide();

            self.removeListener();

            return true;
        }
        return false;
    },

    removeListener: function () {
        var self = this;
        self.itemsUpload.image.unbind("click");
        self.itemsUpload.file.unbind("click");
        self.itemsUpload.cimage.unbind("click");
        self.itemsUpload.caudio.unbind("click");
        self.itemsUpload.cvideo.unbind("click");
    },

    addListener: function () {
        var self = this;
        self.itemsUpload.image.bind("click", function () {

            navigator.camera.getPicture(function (dataImage) {

                //A hack that you should include to catch bug on Android 4.4 (bug < Cordova 3.5):
                if (dataImage.substring(0, 21) == "content://com.android") {
                    var photo_split = dataImage.split("%3A");
                    dataImage = "content://media/external/images/media/" + photo_split[1];
                }

                $.when(self.checkMaxSize(dataImage)).done(function (responseInternal) {
                    if (responseInternal)
                        self.saveImage(self, dataImage);
                });

                self.itemAddfile.click();

            }, self.onFail, {
                quality: self.Class.QUALITY_PICTURE,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            });

        });

        //capture file
        self.itemsUpload.file.bind("click", function () { alert("in development"); });
        //capture image from camera
        self.itemsUpload.cimage.bind("click", function () {
            navigator.camera.getPicture(function (dataImage) {

                $.when(self.checkMaxSize(dataImage)).done(function (responseInternal) {
                    if (responseInternal)
                        self.saveImage(self, dataImage);
                });

                self.itemAddfile.click();
            },
           self.onFail,
            { quality: self.Class.QUALITY_PICTURE,
                sourceType: navigator.camera.PictureSourceType.CAMERA,
                mediaType: navigator.camera.MediaType.PICTURE,
                destinationType: Camera.DestinationType.FILE_URI,
                correctOrientation: true,
                saveToPhotoAlbum: true

            });

        });



        //capture audio
        self.itemsUpload.caudio.bind("click", function () {



            navigator.device.capture.captureAudio(function (dataImage) {


                $.when(self.checkMaxSize(dataImage)).done(function (responseInternal) {
                    if (responseInternal)
                        self.saveAudio(self, dataImage);
                });

                self.itemAddfile.click();

            }, self.onFail, { limit: self.Class.LIMIT });



        });

        //capture video 
        self.itemsUpload.cvideo.bind("click", function () {
            navigator.device.capture.captureVideo(function (dataImage) {
                $.when(self.checkMaxSizeVideo(dataImage)).done(function (responseInternal) {
                    if (responseInternal)
                        self.saveVideo(self, dataImage);
                });

                self.itemAddfile.click();
            }, self.onFail, { limit: self.Class.LIMIT });

        });

    },

    renderUploadItem: function (file) {
        var self = this;
        var properties = self.properties;
        var html = self._super(file);
        return html;
    },

    getTemplateName: function () {
        return "upload";
    },

    getTemplateItemName: function () {
        return "uploadItem";
    },

    getTemplateEditionName: function () {
        return "edition.upload";
    },

    getTemplateEditionMenu: function () {
        return "edition.upload.menu";
    },

    saveImage: function (context, dataImage) {
        var self = context;

        var properties = self.properties;

        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());

        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileKey = "file";

        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);


        window.resolveLocalFileSystemURI(dataImage, function (fileEntry) {

            fileEntry.file(function (file) {

                data.filename = options.fileName;

                if (!self.validateFileExtensions(options.fileName)) {
                    var extension = file.type.substr(file.type.lastIndexOf('/') + 1);
                    options.fileName += "." + extension;
                }

                options.mimeType = "image/jpeg";

                options.params = data;

                var ft = new FileTransfer();

                ft.upload(dataImage, properties.addUrl,

                 function (r) {

                     self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));

                 },

                 function (error) {

                     bizagi.log("An error has occurred: Code = " + error.code);

                 }

                 , options);
            }, function () { //error
            });

        }, function () { // error 
        });



    },

    saveAudio: function (context, dataAudio) {

        var self = context;

        var properties = self.properties;

        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());

        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataAudio[0].name;

        //options.mimeType = "audio/wav";

        options.params = data;

        var ft = new FileTransfer();

        ft.upload(dataAudio[0].fullPath, properties.addUrl,
                 function (r) {
                     self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
                 },

                 function (error) {
                     bizagi.log("An error has occurred: Code = " + error.code);
                 }

                 , options);

    },

    saveVideo: function (context, dataVideo) {

        var self = context;

        var properties = self.properties;

        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());

        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataVideo[0].name;

        // options.mimeType = "video/quicktime";

        options.params = data;

        var ft = new FileTransfer();

        ft.upload(dataVideo[0].fullPath, properties.addUrl,

                 function (r) {

                     self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));

                 },

                 function (error) {

                     bizagi.log("An error has occurred: Code = " + error.code);

                 }

                 , options);

    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);
    },

    onUploadFileCompleted: function (context, response) {
        var self = context,

         control = self.getControl();

        var uploadWrapper = $(".bz-rn-upload-show-menu", control);

        var result = response;

        if (result.id && result.fileName) {
            var newItem = self.renderUploadItem(result);

            self.files.push([result.id, result.fileName]);

            // Locate it before the upload wrapper

            $(newItem).insertBefore(uploadWrapper);

            // Increment counter

            self.filesCount = self.filesCount + 1;

            self.triggerRenderChange();

            // Check maxFiles

            self.checkMaxFiles();
        } else {
            bizagi.log("E:" + result.message);

        }

        self.hideUploadItems();
    },

    checkMaxFiles: function () {
        var self = this;
        var properties = self.properties;
        var maxFiles = properties.maxfiles;
        var actualFiles = properties.value.length;

        if (maxFiles != 0 && (actualFiles >= maxFiles)) {
            self.itemAddfile.hide();
            self.removeListener();
        }
    },

    checkExtensions: function () {
        var self = this;
        var properties = self.properties;
        var validExtensions = properties.validExtensions;
        if (typeof validExtensions === "undefined" || validExtensions == "") {
            return;
        }

        validExtensions = validExtensions.split(";");

        if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
            return;
        }

        var enableVideo = false;
        var enableAudio = false;
        var enableImage = false;

        for (var i = 0; i < validExtensions.length; i++) {
            validExtensions[i] = validExtensions[i].replace("*.", "");
            var image = self.Class.EXTENSIONSIMG.toString().indexOf(validExtensions[i]);
            var audio = self.Class.EXTENSIONSAUDIO.toString().indexOf(validExtensions[i]);
            var video = self.Class.EXTENSIONSVIDEO.toString().indexOf(validExtensions[i]);

            if (image >= 0) {
                enableImage = true;
            }
            if (audio >= 0) {
                enableAudio = true;
            }

            if (video >= 0) {
                enableVideo = true;
            }

        }

        if (!enableAudio)
            self.itemsUpload.caudio.hide();
        if (!enableImage) {
            self.itemsUpload.image.hide();
            self.itemsUpload.cimage.hide();
        }
        if (!enableVideo)
            self.itemsUpload.cvideo.hide();

        if (!enableAudio && !enableImage && !enableVideo) {
            self.itemAddfile.hide();
            self.removeListener();
        }

    },
    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURI(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {

                if (fileObj.size >= properties.maxSize) {
                    navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    defer.resolve(false);
                }
                else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },

    checkMaxSizeVideo: function (objectVideo) {

        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        var size = objectVideo[0].size;

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        if (size >= properties.maxSize) {
            navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
            defer.resolve(false);
        }
        else {
            defer.resolve(true);
        }



        return defer.promise();

    },

    validateFileExtensions: function (nameArchivo) {
        var validExtensions; var mensaje;
        validExtensions = /(.jpg|.gif|.png|.bmp|.jpeg|.mov|.avi|.mpg|.mpeg|.3gp|.asf|.wmv|.flv|.mp4)$/i;

        if (validExtensions.test(nameArchivo)) {
            return true;
        }
        return false;

    }
    //TODO:android no arroja la extencion solo permite por el tipo imagen o no
    /* checkExtensionsInFile: function(extFile){
    var self = this;
    var properties = self.properties;
    var defer = new $.Deferred();
        
    if (typeof validExtensions === "undefined" || validExtensions == "") {
    defer.resolve(true);
    }
        
    validExtensions = validExtensions.split(";");
        
    if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
    defer.resolve(true);
    }   
        
    for (element in validExtensions) {
    if(extFile.indexOf(element) >= 0 )
    {
    defer.resolve(true);
    }
    }
    alert("not posible in cordova")
    defer.resolve(false);     
    return defer.promise();
    	
    }*/



});
