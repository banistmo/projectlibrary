/*
*   Name: BizAgi smartphone Render Link Extension
*   Author: Oscar O
*   Comments:
*   -   This script will redefine the link render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.link.extend("bizagi.rendering.link", {}, {

    renderSingle: function () {

        var self = this;
        var properties = self.properties;
        var container = self.getContainerRender();
        var control = self.getControl();
        var link = $(".ui-bizagi-render-link", control);

        /* var textTmpl = self.renderFactory.getTemplate("link");
        self.input = $.tmpl(textTmpl, { displayName: properties.value }).appendTo(control);
        self.input.bind('click', function (e) {
        e.stopPropagation();
        });*/

        self.input = control.find("a");

        $(self.input).on("click", function (e) {
            e.preventDefault();
            var elem = $(this);
            var url = elem.attr('href');
            if (url.indexOf('http://') !== -1) {
                console.log("bien");
                window.open(url, '_system');
            }
        });
        
        //if link is not editable, gives a visual feedback
        if (!properties.editable) {
            link.addClass("ui-state-disabled");
            container.addClass("bz-command-not-edit");
        }

    },

    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();
        self.input.attr("href", properties.value);
        self.input.attr("target", "_blank");
    },

    renderEdition: function () {
        var self = this;
        var properties = self.properties;
        var container = self.getContainerRender();
        var control = self.getControl();

        var textTmpl = self.renderFactory.getTemplate("edition.link");
        self.inputEdition = $.tmpl(textTmpl);


    },
    setDisplayValueEdit: function (value) {
        var self = this;
        var properties = self.properties;
        self.inputEdition.val(value);
        //self.inputEdition.attr("href", properties.value);
        // self.inputEdition.attr("target", "_blank");
    },

    actionSave: function () {
        var self = this;
        var value = self.inputEdition.val();
        var properties = self.properties;
        properties.value = value;
        self.setValue(value, false);
        self.input.html(value);
        self.input.attr("href", properties.value);
    }




});
