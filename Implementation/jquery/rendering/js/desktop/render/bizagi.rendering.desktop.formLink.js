/*
 *   Name: BizAgi Desktop Render Form Link Extension
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will redefine the link render class to adjust to desktop devices
 */

// Extends itself
bizagi.rendering.formLink.extend("bizagi.rendering.formLink", {}, {
    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        var control = self.getControl();
        var link = $(".ui-bizagi-render-link", control);

        // Call base
        self._super();

        // Define dialog
        self.dialog = null;

        // Bind click event
        link.click(function () {
            // Open the link inside a dialog
            self.openLink();
        });
    },
    /*
    *   Method to render non editable values
    */
    postRenderReadOnly: function () {
        var self = this,
            mode = self.getMode();
        
        if (mode == "execution") {
            self.configureHandlers();    
        }
    },
    /*
    *   Opens the link display form inside a dialog
    */
    openLink: function () {
        var self = this;
        var properties = self.properties;

        // Send edit request
        $.when(self.submitEditRequest()).done(function () {
            // Show dialog with new form after that
            self.dialog = new bizagi.rendering.dialog.form(self.dataService, self.renderFactory, {
                showSaveButton: properties.editable,
                maximized: properties.maximized,
                title: properties.displayName,
                onSave: function (data) {
                    return self.submitSaveRequest(data);
                }
            });

            self.dialog.render({
                idRender: properties.id,
                xpathContext: self.getFormLinkXpathContext(),
                idPageCache: properties.idPageCache,
                recordXPath: self.getFormLinkXpath(),
                requestedForm: "linkform",
                editable: properties.editable,
                url: properties.editPage
            }).fail(function () {
                // Sends a rollback request to delete checkpoints
                self.submitRollbackRequest();
            });
        });
    },
    getFormLinkXpathContext: function () {
        var self = this;
        var xpathContext = self.properties.xpathContext;
        if (self.getXpathContext() && xpathContext) {
            return self.getXpathContext() + "." + xpathContext;
        } else {
            return xpathContext;
        }
    }
});
