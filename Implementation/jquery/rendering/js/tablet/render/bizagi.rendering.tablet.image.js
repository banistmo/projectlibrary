/*
*   Name: BizAgi Render Image Class
*   Author: Edward J Morales
*   Comments:
*   -   This script will redefine the image render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.image.extend("bizagi.rendering.image", {}, {

    /*
    *   Template method to implement in each device to customize each render after processed
    */
    postRender: function () {
        var self = this;
        var control = self.getControl();
        // Call base 
        this._super();


        /*  if (self.getMode() == "execution") {
        $(control).css({
        height: (self.properties.height + 30) + "px"
        });
        }*/
        //esta clase solo esta presente si no llega a existir una imagen asociada
        /*   $(".image-file", control).css({
        width: self.properties.width + "px",
        height: self.properties.height + "px"
        });
        //si desea que las imagenes salgan del tamano original(ajustado al ancho del dispositivo) comente este codigo
        /*  $("IMG", control).css({
        width: self.properties.width + "px",
        height: self.properties.height + "px"
        });*/
        //control.find("IMG").length == 0 &&
        if (typeof (cordova) != "undefined")
            self.activeUploadNative();
    },
    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        // Call base
        self._super();
    },
    /*
    *   Template method to implement in each device to customize the render's behaviour when rendering in design mode
    */
    configureDesignView: function () {
        var self = this;
        // Call base
        self._super();
    },
    /*
    *   Template method to implement in each device to customize each render after processed in read-only mode
    */
    postRenderReadOnly: function () {
        var self = this;
        var control = self.getControl();
        var template = self.renderFactory.getTemplate("image");
        var mode = self.getMode();

        $.when(self.buildItemUrl()).done(function (dataUrl) {
            var url = (dataUrl != "") ? dataUrl : url;
            // Render template
            var html = $.fasttmpl(template, {
                xpath: bizagi.util.encodeXpath(self.getUploadXpath()),
                editable: false,
                url: url,
                allowDelete: false,
                mode: mode
            });
            control.append(html);
        });
    },

    activeUploadNative: function () {
        var self = this;
        var container = self.getControl();
        var body = $("body");
        self.itemAddfile = $(".image-file", container);
        if (self.itemAddfile.length == 0)
            self.itemAddfile = $("img", container);
        self.itemsUpload = {
            "image": $(".image", container),
            "cimage": $(".cimage", container)
        };

        self.itemAddfile.bind("click", function (e) {

            var containerUploadItems = $(".bz_rn_upload_container_upload_items", container);
            //mejorar la implementacion cuando abro varios casos especiales al tiempo
            //bug IOS : z-inded for first item
            if (containerUploadItems.hasClass("bz_clone_active")) {
                containerUploadItems.removeClass("bz_clone_active");
                var divClone = $("#bz_active_clone_Upload", body);
                divClone.hide();
                divClone.remove();
                containerUploadItems.hide();
                self.removeListener();
                return;
            }

            if (containerUploadItems.is(':visible')) {
                containerUploadItems.hide();
                self.removeListener();
                return;
            }

            //this try/catch is use for close other upload controls in the form
            try {
                var formContainer = self.getContainerRender().parent();
                if (formContainer) {
                    formContainer.find(".bz_rn_upload_container_upload_items:visible").hide();
                }

            } catch (e) { }

            containerUploadItems.show();

            //verify the visivility for container and puts the class for arrow
            var heigthContainer = parseInt(containerUploadItems.css("height"));
            if (((self.itemAddfile.offset().top - 60) - heigthContainer) > 0) {

                containerUploadItems.addClass("bottomArrow");
                containerUploadItems.position({
                    of: self.itemAddfile,
                    my: "center bottom",
                    at: "center top",
                    offset: "-10px",
                    collision: "flipfit flipfit"
                });
            }
            else {
                //bug IOS : z-inded for first item
                containerUploadItems.hide();
                containerUploadItems.addClass("bz_clone_active");
                var cloneUpload = containerUploadItems.clone();
                cloneUpload.attr("id", "bz_active_clone_Upload");
                cloneUpload.addClass("upArrow");
                cloneUpload.appendTo(body);
                cloneUpload.css('display', 'inline-block');
                cloneUpload.position({
                    of: self.itemAddfile,
                    my: "center top",
                    at: "center bottom",
                    offset: "5px",
                    collision: "flipfit flipfit"
                });

                self.itemsUpload = {
                    "image": $(".image", cloneUpload),
                    "cimage": $(".cimage", cloneUpload)
                };

            }
            self.addListener();
        });

    },


    removeListener: function () {

        var self = this;
        self.itemsUpload.image.unbind("click");
        self.itemsUpload.cimage.unbind("click");
    },

    addListener: function () {

        var self = this;

        self.itemsUpload.image.bind("click", function () {

            navigator.camera.getPicture(function (dataImage) {

                $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                    if (resp) {
                        self.saveImage(self, dataImage);
                    }
                    self.itemAddfile.click();
                });

            }, self.onFail, {
                quality: self.Class.QUALITY_PICTURE,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            });

        });

        //capture image from camera
        self.itemsUpload.cimage.bind("click", function () {

            navigator.camera.getPicture(function (dataImage) {
                $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                    if (resp) {
                        self.saveImage(self, dataImage);
                    }
                    self.itemAddfile.click();
                });
            },
            self.onFail,
            { quality: self.Class.QUALITY_PICTURE });

        });
    },

    saveImage: function (context, dataImage) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = data;


        if (bizagi.context.isOfflineForm == false) {
            var ft = new FileTransfer();
            ft.upload(dataImage, properties.addUrl,
         function (r) {
             self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
         },
         function (error) {
             bizagi.log("An error has occurred: Code = " + error.code);
         }
         , options);

        } else {
            var c = document.createElement('canvas');
            var ctx = c.getContext("2d");
            var img = new Image();
            img.onload = function () {
                c.width = this.width;
                c.height = this.height;
                ctx.drawImage(img, 0, 0);
            };
            img.src = dataImage;
            var dataURL = c.toDataURL("image/jpeg");

            self.getControl().find("img").replaceWith(img);
            self.getControl().find(".bz-cm-icon.image-file").replaceWith(img);

            self.removeListener();
            self.activeUploadNative();

            dataURL = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
            self.setValue(dataURL);
            bizagi.log(dataURL);

            //enables canbesent on offline mode
            self.canBeSent = function () { return true; };
        }

    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);
    },

    onUploadFileCompleted: function (context, response) {
        var self = context;
        try {
            self.submitOnChange();
        } catch (e) {

            self.getFormContainer().refreshForm();
        }
    },

    /*
    *
    */
    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURI(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {
                if (fileObj.size >= properties.maxSize) {
                    navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    defer.resolve(false);
                }
                else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    }
});
