/*
*   Name: BizAgi Tablet Link Column Decorator Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the link column decorator class to adjust to tablet devices
*/

// Extends from column
bizagi.rendering.columns.column.extend("bizagi.rendering.columns.link", {}, {
	
});
