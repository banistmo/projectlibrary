bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.util = (typeof (bizagi.util) !== "undefined") ? bizagi.util : {};
bizagi.context = (typeof (bizagi.context) !== "undefined") ? bizagi.context : {};


/**
 * Method to detect iPad visitors
 */
bizagi.util.randomNumber = function (min, max) {
    min = min || 1;
    max = max || 10000000;
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
 *   Returns the resolved result from a promise when the promise has been executed already
 */
bizagi.resolveResult = function (promise) {
    var result;
    promise.done(function (data) {
        result = data;
    });
    return result;
};

/**
 *   Returns a boolean if there are two equal objects
 */
bizagi.util.identicalObjects = function (obj1, obj2) {
    var self = this;

    if (typeof (obj1) !== typeof (obj2)) {
        return false;
    }

    if (typeof (obj1) === "function") {
        return obj1.toString() === obj2.toString();
    }

    if (obj1 instanceof Object && obj2 instanceof Object) {

        // Count properties
        if (bizagi.util.countProps(obj1) !== bizagi.util.countProps(obj2)) {
            return false;
        }

        var r = true;
        for (k in obj1) {
            r = bizagi.util.identicalObjects(obj1[k], obj2[k]);
            if (!r) {
                return false;
            }
        }
        return true;
    } else {
        return obj1 === obj2;
    }
};


bizagi.util.countProps = function (obj) {
    var count = 0;
    for (k in obj) {
        if (obj.hasOwnProperty(k)) {
            count++;
        }
    }
    return count;
};

/**
 *   Encode a html string
 */
bizagi.util.encodeHtml = function (text) {
    if (text === undefined || text === null) {
        return "";
    }
    if (typeof (text) == "string") {
        return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }
    return text;
};

/**
 *   Encodes the xpath
 */
bizagi.util.encodeXpath = function (xpath) {
    if (bizagi.util.isEmpty(xpath))
        return "";
    return xpath.replaceAll(".", "_").replaceAll("=", "_").replaceAll("[", "_").replaceAll("]", "_");
};

/**
 *   Add a clone to the object prototype
 */
bizagi.clone = function (obj) {
    return JSON.parse(JSON.encode(obj));
};

/**
 *   Generates a random guid
 */
Math.guid = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

/**
 * Execute deferreds in order
 * @example var a = new bizagiQueue(); a.add(function);
 *
 */
function bizagiQueue() {
    var self = this;
    var queue = [];

    self.add = function (callback) {
        queue.push({ callbacks: [callback] });
    };

    self.addParallel = function (callback) {
        if (queue.length == 0)
            return self.add(callback);

        // Put a parallel on last item
        var last = queue[queue.length - 1];
        last.callbacks.push(callback);
    };

    self.execute = function () {
        var starter = new $.Deferred();
        var promise = starter.promise();
        var queueLength = queue.length;
        for (var i = 0; i < queueLength; i++) {
            promise = promise.pipe(function () {
                var element = queue.shift();
                return $.when.apply($, $.map(element.callbacks, function (callback) {
                    // Function support
                    if (typeof (callback) == "function")
                        return callback();
                    // Deferred support
                    return callback;
                }))
            });
        };

        starter.resolve();
        promise = promise.done(function () {
            // Reset queue
            queue = [];
        });

        return promise;
    };
};

/**
 *   Check if a map object is empty
 */
bizagi.util.isMapEmpty = function (map) {
    // First we validate a special case
    if (map.length == 1 && map[0].length == 0)
        return true;
    // Then we do normal validation
    for (var key in map) {
        if (map.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

/**
 * Find element to make scroll top
 */
bizagi.util.scrollTop = function (canvas) {
    if (canvas && canvas[0]) {
        while (canvas[0].tagName !== "HTML" && canvas.css("overflow-y") != "scroll" && canvas.css("overflow-y") != "auto" && canvas.parent().length > 0) {
            canvas = canvas.parent();
        }
    }
    return canvas;
};

/*
*   Read the query string parameteres
*/
bizagi.util.getQueryString = function (url) {
    var params = {};
    url = url || window.location.href;
    var query = url.indexOf("?") > 0 ? url.substring(url.indexOf("?") + 1) : "";
    var pairs = query.split("&");
    for (var i = 0; i < pairs.length; i++) {
        var pos = pairs[i].indexOf('=');
        if (pos == -1)
            continue;
        var argname = pairs[i].substring(0, pos);
        var value = pairs[i].substring(pos + 1);
        params[argname] = unescape(value);
    }
    return params;
};


/*
*   Info scenarios
*/
var scenarioInfo = (function(params) {
    var self = this;
    var _state = {};

    /**
    * Return url where is located specific scenario
    *
    * @param params
    * @returns {string}
    */
    self.getFileScenario = function(params) {
        params = params || {};
        var url = "";
        var scenario = params.scenario.split(".") || [];
        var category = scenario[0];

        url = BIZAGI_SMARTPHONE_PATH + "scenarios/" + category + "/" + params.scenario + ".js";

        return url;
    };

    return {
        getFileScenario: self.getFileScenario
    };
});

/**
 *   Determines if parameter 'n' is numeric
 */
bizagi.util.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

/**
 *   Determines if a element has visibility
 */
bizagi.util.isScrolledIntoView = function (elem, layout) {
    if (elem == undefined) {
        return false;
    }

    layout = layout || $(window);
    var scrollTop = layout.scrollTop();
    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    var docViewBottom = scrollTop + $(layout).height();
    return ((elemBottom <= docViewBottom) && (elemTop >= scrollTop));
};

/**
 * determines whether the element in the browser supports the event we are looking for, eg 'paste', 'input', 'blur', etc. More info check modernizer.js.
 * @param  {string|*}           eventName  is the name of an event to test for (e.g. "resize")
 * @param  {(Object|string|*)=} element    is the element|document|window|tagName to test on
 * @return {boolean}
 */
bizagi.util.isEventSupported = function (eventName, element) {
    var needsFallback = !('onblur' in document.documentElement);
    var isSupported;

    if (!eventName) {
        return false;
    }
    if (!element || typeof element === 'string') {
        element = document.createElement(element || 'div');
    }

    // Testing via the `in` operator is sufficient for modern browsers and IE.
    // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and
    // "resize", whereas `in` "catches" those.
    eventName = 'on' + eventName;
    isSupported = eventName in element;

    // Fallback technique for old Firefox - bit.ly/event-detection
    if (!isSupported && needsFallback) {
        if (!element.setAttribute) {
            // Switch to generic element if it lacks `setAttribute`.
            // It could be the `document`, `window`, or something else.
            element = document.createElement('div');
        }
        if (element.setAttribute && element.removeAttribute) {
            element.setAttribute(eventName, '');
            isSupported = typeof element[eventName] === 'function';

            if (element[eventName] !== undefined) {
                // If property was created, "remove it" by setting value to `undefined`.
                element[eventName] = undefined;
            }
            element.removeAttribute(eventName);
        }
    }
    return isSupported;
};

/***
 * Removes the objects and dispose
 * @param obj
 */

bizagi.util.dispose = function (obj) {
    for (key in obj) {
        if (typeof (obj[key]) == "object") delete obj[key];
        if (typeof (obj[key]) == "function") { obj[key] = null; delete obj[key]; }
    }
};