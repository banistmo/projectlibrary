/*
*   Name: BizAgi Tablet Render Link Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the link render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.link.extend("bizagi.rendering.link", {}, {

    configureHandlers: function () {
        var self = this;
        var control = self.getControl();
        var properties = self.properties;
        var link = $(".ui-bizagi-render-link", control);

		// Call base
		self._super();

		//if link is editable, sets the link
        if (properties.editable) {
            link.attr("href", properties.value);
            link.attr("target", "_blank");
            link.attr("data-rel", "external");
        } else {
            link.addClass("ui-state-disabled");
        }
	},

    /*
    *   Method to render non editable values
    */
    postRenderReadOnly: function () {
        var self = this;

        // Execute configure handlers
        self.configureHandlers();
    }
});
