/*
*   Name: BizAgi Desktop Render Factory
*   Author: Diego Parra
*   Comments:
*   -   This script will define a render factory to create desktop versions of each render type
*/

bizagi.rendering.base.factory.extend("bizagi.rendering.desktop.factory", {}, {
    /* 
    *   Constructor
    */
    init: function (dataService) {
        var self = this;
        // Call base
        self._super(dataService);

        // Desktop implementation
        this.printVersion = false;

        // Extend jqgrid with localized resources
        /**
         * TODO: Remove the following lines

        var jqGridLanguage = bizagi.language;
        while ($.jgrid.l10n[jqGridLanguage] == null && jqGridLanguage && jqGridLanguage.length > 0) {

        // Retry with a lesser language
        var jqGridLanguageParts = jqGridLanguage.split("-");
        jqGridLanguageParts.pop();
        jqGridLanguage = jqGridLanguageParts.join("-");
        }
        if (jqGridLanguage.length == 0 || $.jgrid.l10n[jqGridLanguage] == null) {
        // Return default language
        jqGridLanguage = "en";
        }

        $.extend($.jgrid, $.jgrid.l10n[jqGridLanguage]);

        // Set datepicker i18n
        var datePickerRegional = bizagi.localization.getResource("datePickerRegional");
        $.datepicker.setDefaults(datePickerRegional);

        // Localize formatter days and months
        bizagi.util.monthNames = $.merge(datePickerRegional.monthNames, datePickerRegional.monthNamesShort);
        bizagi.util.dayNames = $.merge(datePickerRegional.dayNames, datePickerRegional.dayNamesShort);*/

        // Detect flash support
        self.hasFlash = false;

        // Flag to enable controls with flash
        self.enableFlashControls = false;

        try {
            var document = window.document;
            var swf = this.dataService.getUploadSwfLocation();
            var testDiv = $('<div id="flashtest"/>').appendTo("body", document);
            swfobject.embedSWF(swf, "flashtestUploader", "1", "1", '9.0.24', null, {}, { 'quality': 'high', 'wmode': 'transparent', 'allowScriptAccess': 'always' }, {}, function (obj) {
                self.hasFlash = obj.success;
                if (navigator.userAgent.indexOf("Chrome") > 0 && obj.version[0] == 11 && obj.version[1] == 2) {
                    // Actually this chrome flash version is bugged inside sharepoint
                    self.hasFlash = false;
                }
                testDiv.detach();
            });
        } catch (e) {
        }
    },
    /*
    *   Load all the templates used for rendering
    */
    loadTemplates: function () {
        var self = this;
        var defer = new $.Deferred();

        $.when(
        // Common
                self.loadTemplate("form-error", bizagi.getTemplate("bizagi.rendering.desktop.form-error")),
                self.loadTemplate("form-waiting", bizagi.getTemplate("bizagi.rendering.desktop.form-waiting")),
                self.loadTemplate("info-message", bizagi.getTemplate("common.bizagi.desktop.info-message")),
        // Containers
                self.loadTemplate("form", bizagi.getTemplate("bizagi.rendering.desktop.form") + "#ui-bizagi-render-form"),
                self.loadTemplate("panel", bizagi.getTemplate("bizagi.rendering.desktop.panel")),
                self.loadTemplate("contentPanel", bizagi.getTemplate("bizagi.rendering.desktop.contentPanel")),
                self.loadTemplate("tab", bizagi.getTemplate("bizagi.rendering.desktop.tab")),
                self.loadTemplate("tabItem", bizagi.getTemplate("bizagi.rendering.desktop.tabItem")),
                self.loadTemplate("collectionnavigator", bizagi.getTemplate("bizagi.rendering.desktop.collectionnavigator") + "#ui-bizagi-render-collectionnavigator"),
                self.loadTemplate("collectionnavigator-form-button", bizagi.getTemplate("bizagi.rendering.desktop.collectionnavigator") + "#ui-bizagi-render-collectionnavigator-form-button"),
                self.loadTemplate("collectionnavigator-control", bizagi.getTemplate("bizagi.rendering.desktop.collectionnavigator") + "#ui-bizagi-render-collectionnavigator-control"),
                self.loadTemplate("collectionnavigator-actions", bizagi.getTemplate("bizagi.rendering.desktop.collectionnavigator") + "#ui-bizagi-render-collectionnavigator-actions"),
                self.loadTemplate("collectionnavigator-preview", bizagi.getTemplate("bizagi.rendering.desktop.collectionnavigator") + "#ui-bizagi-render-collectionnavigator-preview"),
                self.loadTemplate("horizontal", bizagi.getTemplate("bizagi.rendering.desktop.horizontal")),
                self.loadTemplate("accordion", bizagi.getTemplate("bizagi.rendering.desktop.accordion")),
                self.loadTemplate("accordionItem", bizagi.getTemplate("bizagi.rendering.desktop.accordionItem")),
                self.loadTemplate("group", bizagi.getTemplate("bizagi.rendering.desktop.group")),
                self.loadTemplate("searchForm", bizagi.getTemplate("bizagi.rendering.desktop.searchForm")),
        // Renders
                self.loadTemplate("render", bizagi.getTemplate("bizagi.rendering.desktop.render")),
                self.loadTemplate("text", bizagi.getTemplate("bizagi.rendering.desktop.text")),
                self.loadTemplate("extendedText", bizagi.getTemplate("bizagi.rendering.desktop.extendedText")),
                self.loadTemplate("number", bizagi.getTemplate("bizagi.rendering.desktop.number")),
                self.loadTemplate("date", bizagi.getTemplate("bizagi.rendering.desktop.date")),
                self.loadTemplate("yesno", bizagi.getTemplate("bizagi.rendering.desktop.yesno")),
                self.loadTemplate("check", bizagi.getTemplate("bizagi.rendering.desktop.check")),
                self.loadTemplate("combo", bizagi.getTemplate("bizagi.rendering.desktop.combo")),
                self.loadTemplate("list", bizagi.getTemplate("bizagi.rendering.desktop.list")),
                self.loadTemplate("geolocation", bizagi.getTemplate("bizagi.rendering.desktop.geolocation")),
                self.loadTemplate("radio", bizagi.getTemplate("bizagi.rendering.desktop.radio")),
                self.loadTemplate("image", bizagi.getTemplate("bizagi.rendering.desktop.image") + "#ui-bizagi-render-image"),
                self.loadTemplate("image-flash", bizagi.getTemplate("bizagi.rendering.desktop.image") + "#ui-bizagi-render-image-control"),
                self.loadTemplate("image-noflash", bizagi.getTemplate("bizagi.rendering.desktop.image.noflash") + "#ui-bizagi-render-image-noflash"),
                self.loadTemplate("image-html5", bizagi.getTemplate("bizagi.rendering.desktop.image.html5") + "#ui-bizagi-render-image-html5"),
                self.loadTemplate("image-dialog", bizagi.getTemplate("bizagi.rendering.desktop.image.noflash") + "#ui-bizagi-render-image-dialog-noflash"),
                self.loadTemplate("image-item", bizagi.getTemplate("bizagi.rendering.desktop.image") + "#ui-bizagi-render-image-item"),
                self.loadTemplate("image-item-noflash", bizagi.getTemplate("bizagi.rendering.desktop.image.noflash") + "#ui-bizagi-render-image-item-noflash"),
                self.loadTemplate("image-item-html5", bizagi.getTemplate("bizagi.rendering.desktop.image.html5") + "#ui-bizagi-render-image-item-html5"),
                self.loadTemplate("upload", bizagi.getTemplate("bizagi.rendering.desktop.upload")),
        //self.loadTemplate("upload-ecm", bizagi.getTemplate("bizagi.rendering.desktop.ecm")),
                self.loadTemplate("upload-ecm-view-default", bizagi.getTemplate("bizagi.rendering.desktop.ecm") + "#biz-render-ecm-view-default"),
                self.loadTemplate("upload-ecm-view-default-metadata", bizagi.getTemplate("bizagi.rendering.desktop.ecm") + "#biz-render-ecm-view-default-metadata"),
                self.loadTemplate("upload-ecm-view-default-yesno", bizagi.getTemplate("bizagi.rendering.desktop.ecm") + "#biz-render-ecm-view-default-yesno"),
                self.loadTemplate("upload-ecm-view-default-edit-buttons", bizagi.getTemplate("bizagi.rendering.desktop.ecm") + "#biz-render-ecm-view-default-edit-buttons"),
                self.loadTemplate("document", bizagi.getTemplate("bizagi.rendering.desktop.document") + "#bz-document-generator"),
                self.loadTemplate("document-item", bizagi.getTemplate("bizagi.rendering.desktop.document") + "#bz-document-generator-item"),
                self.loadTemplate("document-column-item", bizagi.getTemplate("bizagi.rendering.desktop.document") + "#bz-document-generator-column-item"),
                self.loadTemplate("document-item-preview", bizagi.getTemplate("bizagi.rendering.desktop.document") + "#bz-document-generator-item-preview"),
                self.loadTemplate("document-item-preview-object", bizagi.getTemplate("bizagi.rendering.desktop.document") + "#bz-document-generator-item-preview-object"),
                self.loadTemplate("upload.noFlash", bizagi.getTemplate("bizagi.rendering.desktop.upload.noFlash")),
                self.loadTemplate("uploadHtml5", bizagi.getTemplate("bizagi.rendering.desktop.upload.html5")),
                self.loadTemplate("uploadItem", bizagi.getTemplate("bizagi.rendering.desktop.uploadItem")),
                self.loadTemplate("upload.dialog", bizagi.getTemplate("bizagi.rendering.desktop.upload.dialog")),
                self.loadTemplate("upload.template.dialog", bizagi.getTemplate("bizagi.rendering.desktop.upload.template.dialog")),
                self.loadTemplate("upload.ecm.dialog", bizagi.getTemplate("bizagi.rendering.desktop.upload.ecm.dialog")),
                self.loadTemplate("dialog-send-email", bizagi.getTemplate("bizagi.rendering.desktop.dialog-sendEmail")),
                self.loadTemplate("grid", bizagi.getTemplate("bizagi.rendering.desktop.grid")),
                self.loadTemplate("cell", bizagi.getTemplate("bizagi.rendering.desktop.cell")),
                self.loadTemplate("cell.readonly", bizagi.getTemplate("bizagi.rendering.desktop.cell.readonly")),
                self.loadTemplate("cell.summary", bizagi.getTemplate("bizagi.rendering.desktop.cell.summary")),
                self.loadTemplate("cell.upload", bizagi.getTemplate("bizagi.rendering.desktop.cell.upload")),
                self.loadTemplate("cell.button", bizagi.getTemplate("bizagi.rendering.desktop.cell.button")),
                self.loadTemplate("search", bizagi.getTemplate("bizagi.rendering.desktop.search")),
                self.loadTemplate("searchItem", bizagi.getTemplate("bizagi.rendering.desktop.searchItem")),
                self.loadTemplate("searchList", bizagi.getTemplate("bizagi.rendering.desktop.searchList")),
                self.loadTemplate("letter", bizagi.getTemplate("bizagi.rendering.desktop.letter")),
                self.loadTemplate("letter.readonly", bizagi.getTemplate("bizagi.rendering.desktop.letter.readonly")),
                self.loadTemplate("letter.dialog", bizagi.getTemplate("bizagi.rendering.desktop.dialog-letter")),
                self.loadTemplate("button", bizagi.getTemplate("bizagi.rendering.desktop.button")),
                self.loadTemplate("fileprint", bizagi.getTemplate("bizagi.rendering.desktop.fileprint") + "#ui-bizagi-render-fileprint"),
                self.loadTemplate("fileprint-object", bizagi.getTemplate("bizagi.rendering.desktop.fileprint") + "#ui-bizagi-render-fileprint-object"),
                self.loadTemplate("fileprint-iframe", bizagi.getTemplate("bizagi.rendering.desktop.fileprint") + "#ui-bizagi-render-fileprint-iframe"),
                self.loadTemplate("link", bizagi.getTemplate("bizagi.rendering.desktop.link")),
                self.loadTemplate("association", bizagi.getTemplate("bizagi.rendering.desktop.association")),

                (bizagi.override.enablePrintFromEditForm) ? self.loadTemplate("print-frame", bizagi.getTemplate("bizagi.workportal.desktop.widget.print") + "#bz-wp-widget-print-frame") : null, (bizagi.override.enablePrintFromEditForm) ? self.loadTemplate("print-frame-header", bizagi.getTemplate("bizagi.workportal.desktop.widget.print") + "#bz-wp-widget-print-frame-header") : null,

        //Bizagi grid
                self.loadTemplate("grid.bizagi", bizagi.getTemplate("bizagi.rendering.desktop.grid.component")),
                self.loadTemplate("bizagi.grid.grid", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid"),
                self.loadTemplate("bizagi.grid.waiting", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-waiting"),
                self.loadTemplate("bizagi.grid.table", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-table"),
                self.loadTemplate("bizagi.grid.table.empty", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-table-empty"),
                self.loadTemplate("bizagi.grid.column", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-column"),
                self.loadTemplate("bizagi.grid.column.special", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-column-special"),
                self.loadTemplate("bizagi.grid.row", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-row"),
                self.loadTemplate("bizagi.grid.row.buttons", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-row-buttons"),
                self.loadTemplate("bizagi.grid.cell", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-cell"),
                self.loadTemplate("bizagi.grid.cell.special", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-cell-special"),
        //Grig Pager based on inboxgrid pager
                self.loadTemplate("bizagi.grid.pagination", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-pagination"),
                self.loadTemplate("bizagi.grid.pager", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-pager"),
                self.loadTemplate("bizagi.grid.buttons", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-buttons"),
                self.loadTemplate("bizagi.grid.dynamicpager", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-dynamic-pager"),
                self.loadTemplate("bizagi.grid.summary", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-summary"),
                self.loadTemplate("bizagi.grid.summaryCell", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-summary-cell"),
        // Grouped grid
                self.loadTemplate("bizagi.grid.grouped.table", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-table"),
                self.loadTemplate("bizagi.grid.grouped.column", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-column"),
                self.loadTemplate("bizagi.grid.grouped.row", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-row"),
                self.loadTemplate("bizagi.grid.grouped.header.row", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-header-row"),                
                self.loadTemplate("bizagi.grid.grouped.summary", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-summary"),
                self.loadTemplate("bizagi.grid.grouped.summaryCell", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-summary-cell"),
                self.loadTemplate("bizagi.grid.grouped.emptyCell", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-grouped-empty-cell"),


                self.loadTemplate("bizagi.grid.exportOptions", bizagi.getTemplate("bizagi.rendering.desktop.grid.bizagi") + "#ui-bizagi-grid-export-options")).done(function () {

                    // Resolve when all templates are loaded
                    defer.resolve();
                });

        return defer.promise();
    },
    /*
    *   Returns the appropiate render based on the render type
    */
    getRender: function (params) {
        var self = this;
        var type = params.type;
        var data = params.data;
        var renderParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService
        });

        if (type == "image") {
            // Override for test purposes
            if (self.enableFlashControls && self.hasFlash) {
                return new bizagi.rendering.image(renderParams);
            } else {
                //Check if the engine is Windows 8 App 
                if (typeof Windows === "undefined") {
                    // TODO: Implement all functionalities of flash version in to uploadhtml5 control
                    return new bizagi.rendering.imageNoFlash(renderParams);
                } else {
                    //its windows 8
                    return new bizagi.rendering.imageHtml5(renderParams);
                }
            }
        }

        if (type == "upload") {
            // Upload control with flash now its deprecated
            // return new bizagi.rendering.upload(renderParams);

            //Check if the engine is Windows 8 App 
            if (typeof Windows === "undefined") {
                // TODO: Implement all functionalities of flash version in to uploadhtml5 control
                return new bizagi.rendering.uploadNoFlash(renderParams);
            } else {
                //its windows 8  
                if (typeof (bizagi.context.isOfflineForm) != "undefined" && bizagi.context.isOfflineForm) {
                    return new bizagi.rendering.upload.offline(renderParams);
                } else {
                    return new bizagi.rendering.uploadHtml5(renderParams);
                }
            }
        }

        if (type == "uploadecm") {
            return new bizagi.rendering.ecm(renderParams);
        }

        if (type == "grid") {
            var properties = data.properties;

            if (typeof (bizagi.context.isOfflineForm) != "undefined" && bizagi.context.isOfflineForm) {
                return new bizagi.rendering.grid.offline(renderParams);
            } else {
                return new bizagi.rendering.grid.bizagi(renderParams);
            }
        }

        if (type == "groupedgrid") {
            var properties = data.properties;
            return new bizagi.rendering.grid.grouped(renderParams);
        }

        if (type == "collectionnavigator") {
            return new bizagi.rendering.collectionnavigator(renderParams);
        }

        // Call base
        return self._super(params);
    },
    /*
    *   Returns the appropiate column based on the render type
    */
    getColumn: function (params) {
        var self = this;
        var type = params.type;
        var columnParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService,
            singleInstance: bizagi.util.isEmpty(params.singleInstance) ? true : params.singleInstance
        });

        if (type == "columnUpload") {
            //Check if the engine is Windows 8 App 
            if (typeof Windows === "undefined") {
                if (self.enableFlashControls && self.hasFlash) {
                    columnParams.decorated = bizagi.rendering.upload;
                } else {
                    columnParams.decorated = bizagi.rendering.uploadNoFlash;
                }
                return new bizagi.rendering.columns.upload(columnParams);
            } else {
                //its windows 8
                columnParams.decorated = bizagi.rendering.uploadHtml5;
                return new bizagi.rendering.columns.uploadHtml5(columnParams);
            }
        }

        if (type == "columnImage") {
            if (self.enableFlashControls && self.hasFlash) {
                columnParams.decorated = bizagi.rendering.image;
            } else {
                columnParams.decorated = bizagi.rendering.imageNoFlash;
            }

            return new bizagi.rendering.columns.image(columnParams);
        }

        // Call base
        return self._super(params);
    }
});