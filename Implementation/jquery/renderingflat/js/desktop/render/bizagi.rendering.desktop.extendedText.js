/*
 *   Name: BizAgi Desktop Render Text Extension
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will redefine the text render class to adjust to desktop devices
 */

// Extends itself
bizagi.rendering.extendedText.extend("bizagi.rendering.extendedText", {}, {

    postRender: function () {
        var self = this;
        self._super();
        if (bizagi.util.isIE8()) {
            self.textarea.attr("cols", "5000");
        }
    },
    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        var properties = self.properties;

        // Call base
        self._super();
        self.textarea.attr("rows", "6");

        // Define max length of element
        if (properties.maxLength > 0) {
            self.textarea.attr("maxlength", properties.maxLength);
            self.actualMaxLength = properties.maxLength;

            $(self.textarea).bind("keydown", function (e) {
                if (e.which === 8 || e.which === 13 || e.which === 32 || e.which > 46) {
                    var $textarea = $(this);
                    var string = $textarea.val();
                    var match = string.match(/\n/gm);
                    var numberEnters = match ? match.length : 0;

                    self.setMaxLength($textarea, numberEnters, string);
                }
            });

            $(self.textarea).bind("paste", function (e) {
                var $textarea = $(this);
                setTimeout(function () {
                    var string = $textarea.val();
                    var match = string.match(/\n/gm);
                    var numberEnters = match ? match.length : 0;

                    self.setMaxLength($textarea, numberEnters, string);

                }, 100);
            });

        }

        self.ready().done(function () {
            // auto extend
            if (properties.autoExtend) {
                var value = "";
                var minHeight = self.textarea.css("height");
                var valMinHeight;
                var fakeControl = $("<div />").css({
                    whiteSpace: "pre-wrap",
                    display: "none"
                });

                if (typeof minHeight == "string" && minHeight.search("px") !== -1) {
                    valMinHeight = Number(minHeight.split("px")[0]);
                    if (valMinHeight <= 150) {
                        minHeight = "150px";
                    }
                } else {
                    minHeight = "150px";
                }

                // set textarea styles
                self.textarea.css({
                    overflow: "hidden",
                    resize: "none",
                    minHeight: minHeight
                });

                fakeControl.insertAfter(self.textarea);

                var process = function () {
                    if (value === (value = self.textarea.val())) {
                        return;
                    }
                    fakeControl.text(value);
                    self.textarea.css("height", fakeControl.height());
                };

                setTimeout(process, 100);
                self.textarea.bind("keyup", process);
            }
        });

    },

    /*
    * Set dynamic max length
    */
    setMaxLength: function ($textarea, numberEnters, string) {
        var self = this;
        var properties = self.properties;
        var calculated = properties.maxLength - (3 * numberEnters); // 3 for \\n

        calculated = (calculated > 0) ? calculated : 0;

        if (string.length > calculated) {
            $textarea.val(string.substr(0, calculated));
        }

        if (self.actualMaxLength !== calculated) {
            $textarea.attr("maxlength", calculated);
            self.actualMaxLength = calculated;
        }
    },

    /*
    *   Sets the value in the rendered control
    */
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var decodedValue = bizagi.util.decodeURI(value);
        value = (typeof decodedValue == "string") ? decodedValue : "";

        // Call base
        this._super(value);

        // Set value in input
        if (value && properties.editable) {
            var resolvedValue = $.br2nl(value);

            var valueToDisplay = resolvedValue.replaceAll("\\n", "\n");

            self.textarea.val(valueToDisplay);
        }
    }
});
