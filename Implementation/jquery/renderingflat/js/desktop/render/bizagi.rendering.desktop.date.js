/*
*   Name: BizAgi Desktop Render Date Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the Date render class to adjust to desktop devices
*/

// Extends itself
bizagi.rendering.date.extend("bizagi.rendering.date", {}, {
    /*
    *   Update or init the element data
    */
    initializeData: function (data) {
        var self = this;
        // Call base
        this._super(data);

        // Fill default properties
        var properties = this.properties;

        // Fix dateFormat for datepicker  
        var dateFormat = self.getDateFormat(properties.dateFormat);
        // properties.dateFormat = dateFormat.datePickerFormat;
        properties.datePickerFormat = dateFormat.datePickerFormat;
        properties.timePickerFormat = dateFormat.timePickerFormat;
    },
    /**
    * Translate standar date format to jquery date picker
    * 
    */
    getDateFormat: function (format) {
        var datePickerFormat = format;
        var timePickerFormat = undefined;
        var init;

        var properties = this.properties;

        if (format.search("h") != -1 || format.search("H") != -1) {
            if (format.search("h") != -1) {
                init = format.search("h");
            } else {
                init = format.search("H");
            }
            datePickerFormat = format.substring(0, init - 1);
            timePickerFormat = format.substr(init);
        }

        // Fix month
        if (datePickerFormat.indexOf("MMMM") >= 0) {
            datePickerFormat = datePickerFormat.replaceAll("MMMM", "MM");

        } else if (datePickerFormat.indexOf("MMM") >= 0) {
            // Month name short
            datePickerFormat = datePickerFormat.replaceAll("MMM", "M");
        } else if (datePickerFormat.indexOf("MM") >= 0) {
            // Month two digits
            datePickerFormat = datePickerFormat.replaceAll("MM", "m");
        } else if (datePickerFormat.indexOf("M") >= 0) {
            // Month one digits
            datePickerFormat = datePickerFormat.replaceAll("M", "m");
        }

        // Fix years
        if (datePickerFormat.search("yyyy") != -1) {
            datePickerFormat = datePickerFormat.replaceAll("yyyy", "yy");
        } else {
            if (datePickerFormat.search("yy") != -1) {
                datePickerFormat = datePickerFormat.replaceAll("yy", "y");
            }
        }
        datePickerFormat = datePickerFormat.replaceAll("dddd", "DD"); // Fix days

        return {
            'datePickerFormat': datePickerFormat,
            'timePickerFormat': timePickerFormat
        };
    },
    getTimePickerFormat: function (timeFormat) {
        var i = 0;
        var c;

        // Define return object
        var returnObj = {
            show24Hours: false,
            showSeconds: false,
            separator: ":"
        };

        // Analize format
        var token, lastToken = "";
        while (i < timeFormat.length) {
            // Get next token from format string
            c = timeFormat.charAt(i);
            token = "";
            while ((timeFormat.charAt(i) == c) && (i < timeFormat.length)) {
                token += timeFormat.charAt(i++);
            }

            // Extract contents of value based on format token
            if (token == "hh" || token == "h") {
                lastToken = token;
                returnObj.show24Hours = false;
            }
            else if (token == "HH" || token == "H") {
                lastToken = token;
                returnObj.show24Hours = true;
            }
            else if (token == "mm" || token == "m") {
                lastToken = token;
            }
            else if (token == "ss" || token == "s") {
                lastToken = token;
                returnObj.showSeconds = true;
            }
            else if (token == "a") {
                lastToken = token;
            }
            else if (lastToken.toUpperCase() == "H" || lastToken.toUpperCase() == "HH") {
                returnObj.separator = token;
            }
            else if (lastToken.toUpperCase() == "S" || lastToken.toUpperCase() == "SS") {
                lastToken = token;
                returnObj.ampmPrefix = token;
            }
        }

        return returnObj;
    },
    /*
    *   Template method to implement in each device to customize each render after processed
    */
    postRender: function () {
        var self = this;
        var properties = self.properties;
        var timeControl = self.getTimeControl();

        // Call base 
        this._super();

        // Apply timeEntry plugin
        if (properties.showTime) {

            var timeFormatProperties;

            if (properties.timePickerFormat !== undefined) {
                timeFormatProperties = self.getTimePickerFormat(properties.timePickerFormat);
            } else {
                timeFormatProperties = bizagi.util.dateFormatter.analyzeTimeFormat(properties.timeFormat);
            }
            timeControl.timeEntry({
                showSeconds: timeFormatProperties.showSeconds,
                show24Hours: timeFormatProperties.show24Hours,
                separator: timeFormatProperties.separator,
                ampmPrefix: timeFormatProperties.ampmPrefix || "",
                useMouseWheel: (self.getMode() == "execution")
            });

            // DEPL: Fix plugin without touching it
            // The plugin is setting the image as an inline style, 
            // that means that we can't set it in the css files,
            // so we remove the inline style, and the css will do the rest
            $(".timeEntry_control", timeControl)
                    .css("height", "")
                    .css("width", "")
                    .css("display", "")
                    .css("background-color", "")
                    .css("background-image", "")
                    .css("background-position", "")
                    .css("background-repeat", "");
        }
    },
    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        var properties = self.properties;
        var dateControl = self.getDateControl();
        var initYear = 100;
        var endYear = 30;
        if (!isNaN(Date.parse(properties.minValue))) {
            initYear = (new Date).getFullYear() - $.datepicker.parseDate("mm/dd/yy", properties.minValue).getFullYear() || 100;
        }

        if (!isNaN(Date.parse(properties.maxValue))) {
            endYear = $.datepicker.parseDate("mm/dd/yy", properties.maxValue).getFullYear() - (new Date).getFullYear() || 100;
        }

        // Call base
        self._super();

        // Apply datepicker & mask plugin
        dateControl.datepicker({
            yearRange: "c-" + initYear + ":c+" + endYear,
            dateFormat: properties.datePickerFormat,
            changeYear: true,
            changeMonth: true,
            shortYearCutoff: (typeof BIZAGI_DEFAULT_DATETIME_INFO != "undefined" ) ? "+" + BIZAGI_DEFAULT_DATETIME_INFO.twoDigitYearMaxDelta : "+10",
            beforeShow: function () {
                // Set min & max value
                properties.editable = true;
                self.changeMinValue(properties.minValue);
                self.changeMaxValue(properties.maxValue);
                self.setDisplayValue(self.getValue());
            }
        });

        // Bind to change event to detect if the user clears the text
        dateControl.bind("change", function () {
            var value = dateControl.val();
            if (bizagi.util.isEmpty(value)) {
                self.setValue("");
            } else {
                if (bizagi.util.dateFormatter.getDateFromFormat(value, self.properties.dateFormat.substring(0, (self.properties.dateFormat.search("h|H") != -1) ? self.properties.dateFormat.search("h|H") - 1 : self.properties.dateFormat.length)) == 0) {
                    self.setValue("");
                    dateControl.val("");
                }
            }
        });
    },
    /*
    *   Template method to implement in each device to customize the render's behaviour when rendering in design mode
    */
    configureDesignView: function () {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();

        if (properties.showTime) {
            control.find(".timeEntry_control").unbind("mousedown");
            control.find(".timeEntry_control").unbind("mouseup");
            control.find(".timeEntry_control").unbind("mouseover");
            control.find(".timeEntry_control").unbind("mousemove");
        }
    },
    /*
    *   Gets the date control
    */
    getDateControl: function () {
        var self = this;
        self.dateControl = $(".ui-bizagi-render-date", self.getControl());
        return self.dateControl;
    },
    /*
    *   Gets the time control
    */
    getTimeControl: function () {
        var self = this;
        if (self.timeControl) {
            if (self.timeControl.hasClass("hasTimeEntry")) {
                self.timeControl = $(".ui-bizagi-render-time", self.getControl());
            }
        } else {
            self.timeControl = $(".ui-bizagi-render-time", self.getControl());
        }
        return self.timeControl;
    },
    /* 
    *   Changes the render min value
    */
    changeMinValue: function (value) {
        var self = this,
                properties = self.properties;
        var dateControl = self.getDateControl();

        // Set value in control
        if (value && properties.editable) {
            properties.minValue = value;

            dateControl.datepicker("option", "minDate", new Date(value));
        }
    },
    /* 
    *   Changes the render max value
    */
    changeMaxValue: function (value) {
        var self = this,
                properties = self.properties;
        var dateControl = self.getDateControl();

        // Set value in control
        if (value && properties.editable) {
            properties.maxValue = value;
            dateControl.datepicker("option", "maxDate", bizagi.util.dateFormatter.getDateFromInvariant(value));
        }
    },
    /*
    *   Sets the value in the rendered control
    */
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var dateControl = self.getDateControl();
        var timeControl = self.getTimeControl();

        // Call base
        this._super(value);

        // Set value in input
        if (value && properties.editable) {
            var dateObj;
            if (properties.timePickerFormat !== undefined) {
                if (self.getMode() != "execution") {
                    dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value);
                } else {
                    dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value, true);
                }
            } else {
                if (properties.showTime) {
                    var INVARIANT_FORMAT = "MM/dd/yyyy H:mm:ss";
                    if (typeof value == "string") {
                        if (value.length == INVARIANT_FORMAT.length) {
                    dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value, true);
                        } else {
                            dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value);
                            dateObj.setHours(0, 0, 0, 0);
                        }
                    } else {
                        dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value, true);
                    }
                } else {
                    dateObj = bizagi.util.dateFormatter.getDateFromInvariant(value);
                }

            }

            // Assert that the date could be parsed
            bizagi.assert(dateObj != 0, "The date could not be parsed", value);

            // Just set the value in the input for performance, when initializing the view
            if (self.internalSetInitialValueFlag) {
                dateControl.val(bizagi.util.dateFormatter.formatDate(dateObj, properties.dateFormat));
                timeControl.val(bizagi.util.dateFormatter.formatDate(dateObj, properties.timePickerFormat ? properties.timePickerFormat : properties.timeFormat));
                return;
            }

            // Set data in controls
            dateControl.datepicker("setDate", dateObj);
            if (properties.showTime) {
                timeControl.timeEntry("setTime", dateObj);
            }
        }
    },
    /*
    *   Focus on the current element
    */
    focus: function (time) {
        var self = this,
                dateControl = self.getDateControl();

        // Call base
        this._super(time);

        // Hide datepicker
        dateControl.datepicker("hide");
    },
    /* 
    *   Formats the date time value to the hidden control
    */
    onChangeHandler: function () {
        var self = this;
        var properties = self.properties;
        var dateControl = self.getDateControl();
        var timeControl = self.getTimeControl();
        var currentDate = dateControl.datepicker('getDate');

        if (properties.showTime && timeControl.length > 0) {
            var currentTime = timeControl.timeEntry('getTime');

            // Merge
            if (currentTime) {
                // If no date has been selected then we need to create a new one to hold the time
                if (currentDate == null)
                    currentDate = new Date();

                currentDate.setHours(currentTime.getHours());
                currentDate.setMinutes(currentTime.getMinutes());
                currentDate.setSeconds(currentTime.getSeconds());
            }
        }

        if (!currentDate || currentDate == 0)
            return;

        //Attemp set the value of the datepicker control. If not valid go back to the previous.
        var previousValue = self.value;
        self.setValue(bizagi.util.dateFormatter.formatInvariant(currentDate, properties.showTime));
        if (!self.isValid([])) {
            self.setValue(previousValue);
            self.setDisplayValue(previousValue);
        }

        // Set display value
        properties.displayValue = bizagi.util.dateFormatter.formatDate(currentDate, properties.fullFormat);
    },

    /**
    *  Clean control before to refreshed it
    */
    afterToRefresh: function () {
        var self = this;
        var dateControl = self.getDateControl();

        // Run validates
        var errors = [];
        self.isValid(errors);
        if (errors.length > 0 && self.properties.minValue) {
            self.setValue(self.properties.minValue);
        }
    }
});
