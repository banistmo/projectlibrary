/*
*   Name: BizAgi Desktop Render Link Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the link render class to adjust to desktop devices
*/

// Extends itself
bizagi.rendering.link.extend("bizagi.rendering.link", {}, {

    /*
    *   Template method to implement in each device to customize each render after processed
    */
    postRender: function () {

    },

    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        var control = self.getControl();
        var properties = self.properties;
        var link = $(".ui-bizagi-render-link", control);

        // Call base
        self._super();

        // Modify href
        if (properties.value) {
            link.attr("href", properties.value);
            link.attr("target", "_blank");
        } else {
            link.addClass("ui-state-disabled");
        }

    },

    /*
    *   Method to render non editable values
    */
    postRenderReadOnly: function () {
        var self = this;

        // Execute the same as post-render
        self.postRender();
        self.configureHandlers();
    }
});
