/*
*   Name: BizAgi Desktop querySearch
*   Author: Ivan Ricardo Taimal Narvaez
*   Comments:querySearch
*   -
*/

bizagi.rendering.search.extend("bizagi.rendering.querySearch", {}, {
    /*
     *  configure Handlers for check, column included in the response
     */
    configureHandlers: function () {
        var self = this;
        self._super();
        var checkIncluded = self.element.find(".ui-bizagi-render-control-included");
        checkIncluded.change(function () {
            self.properties.included = $(this).is(":checked");
        });
    },
    /*
     *   Add the render data to the given collection in order to send data to the server
     */
   /* collectData: function (renderValues) {
        var self = this;
        var properties = self.properties;
        var xpath = properties.xpath;
        var value = self.getValue();
        var filter =false;
        if (!bizagi.util.isEmpty(xpath)) {
            filter=typeof (value) !== "undefined" && !bizagi.util.isEmpty(value);
            if (filter || self.properties.included) {
                renderValues.push({
                    "value": (filter) ? value : null,
                    "visible": self.properties.included,
                    "xpath": properties.xpath,
                    "operation": properties.range
                });
            }
        }
    }*/
});

