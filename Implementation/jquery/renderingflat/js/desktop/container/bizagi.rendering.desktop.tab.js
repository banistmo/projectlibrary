﻿/*
*   Name: BizAgi Desktop Tab Container Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the container class to adjust to desktop devices
*/

// Auto extend
bizagi.rendering.tab.extend("bizagi.rendering.tab", {}, {

    /* 
    *   Template method to implement in each device to customize each container after processed
    */
    postRenderContainer: function (tab) {
        var self = this;

        // Call base
        self._super(tab);

        // Apply tabs widget
        tab.tabs({
            active: self.activeTab,
            activate: function (event, ui) {
                // BUGFIX for grids inside tabs
                setTimeout(
            		function () {
            		    self.resize({ width: self.container.width(), height: self.container.height() });
            		}, 50
            	);
            },
            beforeActivate: function (event, ui) {
                self.triggerHandler("selected", { index: ui.newTab.index(), tab: self.children[ui.newTab.index()] });
                if (self.children[ui.newTab.index()]) {
                    self.children[ui.newTab.index()].activate();
                    self.activeTab = ui.newTab.index();
                }
            }
        });

    },

    /*
    *   Template method to implement in each device to customize the container's behaviour to show layout
    */
    configureLayoutView: function () {
        // Do nothing
    }
});
