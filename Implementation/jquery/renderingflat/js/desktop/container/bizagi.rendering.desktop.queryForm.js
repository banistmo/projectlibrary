﻿/*
*   Name: BizAgi Desktop queryForm Extension
*   Author: Iván Ricardo Taimal Narváez
 *   Comments:
*   -   This script will redefine the container class to adjust to desktop devices
*   -   Will apply a desktop form template
*/

// Auto extend
bizagi.rendering.form.extend("bizagi.rendering.queryForm", {
    /*
     *    Creates a json array with key values to send to the server
     */
    collectRenderValuesForSubmitQueryForm: function (request) {
        var self = this;
        var data =[];
        // Collect data in array
        self.collectRenderValues(data);

        // Mark data collected as original values
        $.each(data, function (i,item) {
            var renders = self.getRenders(item.xpath);
            $.each(renders, function (i, render) {
                render.updateOriginalValue();
            });
        });
       request.parameters=JSON.stringify(data);
       return request;
    }
});
