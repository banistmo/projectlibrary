/**
* Extend base functionality to serve mocks of real calls to REST services
*
* Based on DiegoP code and modified to automatization process
*
* @author RicharU
*/

bizagi.loader.loadFile(
     { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false })
    .then(function () {
        var BIZAGI_RESPONSE_TIME = 0;

        // Define scenarios storage

        bizagi.scenarios = {
            name: "",
            description: "",
            tests: []
        };

        // DUMMIES
        if (BIZAGI_ENABLE_MOCKS) {
            $.mockjax({
                url: 'Rest/Users/CurrentUser',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.currentUser.txt"
            });

            $.mockjax({
                url: 'Rest/Authorization/MenuAuthorization',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.menuAuthorization.txt"
            });

            $.mockjax({
                url: 'Rest/Inbox/Summary',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.render.summary.txt"
            });

            $.mockjax({
                url: 'Rest/Util/Version',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.render.version.txt"
            });

            $.mockjax({
                url: 'Rest/Processes',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.render.processes.txt"
            });

            $.mockjax({
                url: /Rest\/Cases\/\d+\/Summary/,
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.rendering.caseSummary.txt"
            });          
        }
    });