var bizagiConfig = {
    /**
    * Define Server to REST services, if services is allocated in the
    * same server to static resources you must leave this field in blank.
    * Note: Without final / slash
    *
    * @default ""
    * @example proxyPrefix: "http://www.example.com/BizAgi-war"
    * @example proxyPrefix: "/REST"
    * @example proxyPrefix: ""
    */
    //proxyPrefix: "http://orc-jquery:8080/BizAgi-war",
    proxyPrefix: "",
    /**
    * Define relative path to root
    *
    * if your static resources is allocated in some directory (diferent to root)
    * you must define this var with relative path
    *
    * @default ""
    * @example http://www.example.com/index.html => pathToRoot: ""
    * @example http://www.example.com/BizAgi-war/index.html => pathToRoot: "../"
    * @example http://www.example.com/someDirectory/index.html => pathToRoot: "../"
    * @example http://www.example.com/someDirectory/other/index.html => pathToRoot: "../../"
    * @example http://www.example.com/someDirectory/other/andmore/index.html => pathToRoot: "../../../"
    */
    pathToRoot: "../../../../../../",
    /**
    * Set default language, it is used if not defined yet
    * List of available languages: en,es,fr,it,ja,nl,pl,pt,ru,zh
    *
    * @default "en"
    * @example defaultLanguage: "es"
    */
    defaultLanguage: "en",
    /**
    * Define bizagi environment
    *
    * @default release
    * @example environment: "release"
    * @example environment: "debug"
    */
    environment: "debug",
    /**
    * Enable or disable log
    *
    * @type boolean
    * @default false
    * @example  log: false
    */
    log: false,
    /**
    * Define the maximun number of bytes allowed to upload in render controls,
    * Affected controls: upload, ecm, image
    *
    *
    * @default 1048576 -> 1MB
    */
    uploadMaxFileSize: "1048576",

    /**
    * Enable mocks
    *
    * @type boolean
    * @default false
    */
    mocks: true,    

    /**
    * Define base path to json mocks, relative path
    *
    * @type string
    */
    smartphonePath: "jquery/renderingflat/js/test/tablet/",
    
    /**
    * Define module to start, workportal, activityform etc
    *
    * @type string
    */
    startModule: "render",

    /**
    * Set default device, it is used if not defined yet
    * List of available device: smartphone_ios, smartphone_android, tablet, tablet_android
    *
    * @default "en"
    * @example defaultLanguage: "smartphone_ios"
    */
    defaultDevice: "tablet_android",

    /**
    * Enable absolute path
    *
    * @type boolean
    * @default false
    */
    absolutePath: false
};