﻿// Unit test for webpart render

describe("Testing webpart Render", function () {
    var webpartRender;

    it("Executing webpart", function(done) {
        var workportal = bizagi.collection.get("workportal");

        workportal.executeWebpart({
            webpart: "render",
            canvas: $("body")
        }).done(function(wp) {
            webpartRender = wp.webpart;
            done();
        });
    });

    it("This webpart is an Object", function () {
        expect(typeof webpartRender).toEqual('object');
    });
});