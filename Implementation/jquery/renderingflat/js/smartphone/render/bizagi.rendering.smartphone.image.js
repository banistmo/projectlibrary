/*
*   Name: BizAgi Render Image Class
*   Author: Edward J Morales
*   Comments:
*   -   This script will redefine the image render class to adjust to smathphone devices
*/

// Extends itself
bizagi.rendering.image.extend("bizagi.rendering.image", {
    QUALITY_PICTURE: 80,
    LIMIT: 1
}, {

    renderSingle: function () {
        var self = this;
        var control = self.getControl();
        //var container = self.getContainerRender();
        //container.addClass("bz-command-edit-inline");
        $('.ui-bizagi-render-edition-arrow', self.getControl().parent()).hide();
        //bug render image control
        if (typeof (cordova) !== "undefined"){
            self.activeUploadNative();
        }else{
            $('.bz_rn_upload_container_upload_items_inline', control).hide();
        }
        if(control.find('img').length > 0){
            $('.ui-bizagi-render-image-wrapper', control).addClass('no-after-image');//removes little thumb
        }
        if (control.find(".image-wrapper").length == 1){
            return true;
        }
        $.when(self.renderControl()).done(function (html) {
            control.append(html);
        });
    },


    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        // Call base
        self._super();
    },

    activeUploadNative: function () {
        var self = this;
        var container = self.getContainerRender();

        var itemsUpload = {'buttons': [
                            {
                                'ordinal': 1,
                                'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-photo")
                            },{
                                'ordinal': 2,
                                'caption': bizagi.localization.getResource("workportal-actionsheet-upload-choose-photo")
                             }
         ], 'id': self.properties.id};

         var actionSheetUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionsheetUpload-tmpl"], { useWithBlock: false });
         var actionSheetUpload = actionSheetUploadTmpl(itemsUpload);
         $(container).append(actionSheetUpload);
         self.actionSheetUpload = $(".actionsheetUpload", container).kendoMobileActionSheet();
         $(".options", container).bind("click", function(e){
            e.preventDefault();
            e.stopPropagation();
            $(self.actionSheetUpload).data('kendoMobileActionSheet').open();
         });
        self.addListener();

    },

    addListener: function () {

        var self = this;

        //The action sheet link was changed to a kendo button because the action click wasn't working
        //Kendo should release a click event eventually
        $("a[data-bz-ordinal]", self.actionSheetUpload).kendoMobileButton({
            click: function (e) {
                bizagi.util.smartphone.startLoading();
                if(e.button.data("bz-ordinal") === 2){//select pic
                    navigator.camera.getPicture(function (dataImage) {
                        $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                            if (resp) {
                                self.saveImage(self, dataImage);
                            }
                            bizagi.util.smartphone.stopLoading();
                        });

                    }, self.onFail, {
                        quality: self.Class.QUALITY_PICTURE,
                        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
                    });
                }else if(e.button.data("bz-ordinal") === 1){//take pic
                    navigator.camera.getPicture(function (dataImage) {
                        $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                            if (resp) {
                                self.saveImage(self, dataImage);
                            }
                            bizagi.util.smartphone.stopLoading();
                        });
                    },
                    self.onFail,
                    { quality: self.Class.QUALITY_PICTURE });
                }
            }
        });

    },

    saveImage: function (context, dataImage) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = data;
        var ft = new FileTransfer();
        ft.upload(dataImage, properties.addUrl,
         function (r) {
            var response = JSON.parse(decodeURIComponent(r.response))
            if(response.type === 'error'){
                navigator.notification.alert(response.message, function(){}, response.errorType);
                bizagi.util.smartphone.stopLoading();
            }else{
                self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
            }
         },
         function (error) {
             bizagi.log("An error has occurred: Code = " + error.code);
         }
         , options);
    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);
        bizagi.util.smartphone.stopLoading();
    },

    onUploadFileCompleted: function (context, response) {
        var self = context;
        try {
            //self.submitOnChange();
            $.when(self.renderUploadItem()).done(function (htmlImage) {

                // Empty container and add new image            	
                var control = self.getControl();
                $('.ui-bizagi-render-image-wrapper', control).addClass('no-after-image');
                var imageWrapper = $(".ui-bizagi-render-image-wrapper", control);

                $(imageWrapper).html("");
                $(imageWrapper).append(htmlImage);

                //this is needed to update the state of the control and delete the required icon
                self.setValue([true]);

                //set to the new image the click event
                self.activeUploadNative();
                
                //refresh to have synchronized the image between sever and client, it is needed to view the image in the zoom view once it is uploaded
                self.refreshControl();
            });
        } catch (e) {
            self.getFormContainer().refreshForm();
        }
    },

    /*
    *   Template method to implement in each device to customize the render's behaviour when rendering in design mode
    */
    configureDesignView: function () {
        var self = this;
        // Call base
        self._super();
    },



    /*
    *   Template method to implement in each device to customize each render after processed in read-only mode
    */
    postRenderReadOnly: function () {
        var self = this;
        var container = self.getContainerRender();
        container.addClass("bz-command-edit-inline");

    },

    /*
    *
    */
    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURI(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {
                if (fileObj.size >= properties.maxSize) {
                    navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    defer.resolve(false);
                }
                else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },

    renderEdition: function () {
        var self = this;
        self.inputEdition = $.tmpl( self.renderFactory.getTemplate("image-preview"), {'url': self.value[0]} );
        self.kendoViewOptions = { 'zoom': true };
        self.renderEditionType = 'preview';
        self.titleLable = false;
        self.kendoViewFinish = function () {
            var originalImageWidth = self.inputEdition.width();
            var view = bizagi.kendoMobileApplication.view().element;
            var adjustedImageWidthScale = (view.width() < originalImageWidth) ? (view.width() / originalImageWidth) : 1;
            bizagi.kendoMobileApplication.scroller().movable.scaleTo(adjustedImageWidthScale);
            $('.ui-bizagi-container-inputs', view).css('padding', 0);
        }
    },
});
