﻿/*

*   Name: BizAgi smartphone Render Date Extension

*   Author: Oscar O

*   Comments:

*   -   This script will redefine the Date render class to adjust to tablet devices

*/

//queda pendiente el max y min fecha por que no hay mocks o un caso con esa opcion disponible observar comentarios dice como se realizaria.

bizagi.rendering.date.extend("bizagi.rendering.date", {}, {



    renderSingle: function () {

        var self = this;

        var properties = self.properties;

        var container = self.getContainerRender();

        var control = self.getControl();

        self.getArrow().addClass("bz-rn-input-icon-date");

        //the edition inline only available for ios 5 
        //typeof (cordova) !== "undefined" &&
        if (properties.editable && bizagi.util.isIphoneHigherIOS5()) {

            container.addClass("bz-command-edit-inline");

            var dateTmpl = self.renderFactory.getTemplate("edition.date");

            var input = (control.find("input").length == 0) ? $.tmpl(dateTmpl, { 'showTime': properties.showTime }) : control;
            self.input = input.find("input:first");
            if (properties.showTime) {
                self.inputTime = input.find("input:last");
                self.inputTime.appendTo(control);

                $(self.inputTime).bind("focusout", function (e) {
                    self.onChangeHandler();
                });
            }

            self.input.get(0).type = 'date';

            self.input.appendTo(control);
            $(self.input).bind("focusout", function (e) {
                self.onChangeHandler();
            });
            return;

        }



        var textTmpl = self.renderFactory.getTemplate("text");

        self.input = $.tmpl(textTmpl, { 'showTime': false }).appendTo(control);

        if (!properties.editable) {

            container.addClass("bz-command-not-edit");

            self.input.attr('readonly', "readonly");

        }



    },

    setDisplayValue: function (value) {

        var self = this;

        var properties = self.properties;

        var control = self.getControl();

        var date = bizagi.util.dateFormatter.getDateFromInvariant(value, self.properties.showTime);

        var dateFormated = null;
        if (date != 0) {
            if (bizagi.util.isIphoneHigherIOS5()) {
                properties.dateFormat = "yyyy-MM-dd";
                properties.timeFormat = "HH:mm";
            }
            var fullDateFormated = bizagi.util.dateFormatter.formatDate(date, properties.fullFormat);
            var dateFormated = bizagi.util.dateFormatter.formatDate(date, properties.dateFormat);
            var timeFormated = bizagi.util.dateFormatter.formatDate(date, properties.timePickerFormat ? properties.timePickerFormat : properties.timeFormat);
        }

        if (!bizagi.util.isIphoneHigherIOS5() || !properties.editable) { 
            if (properties.showTime) {
                self.input.prop('value', fullDateFormated);
            } else {
                self.input.prop('value', dateFormated);
            }
        } else {
            self.input.prop('value', dateFormated);
            if (properties.showTime) {
                self.inputTime.prop('value', timeFormated);
            }
        }

        self.setValue(value, false);

        //Ya no se necesita
        //        if (typeof (cordova) !== "undefined") {
        //            var objDate = bizagi.util.dateFormatter.getDateFromInvariant(value);
        //            var year = objDate.getFullYear();

        //            var month = objDate.getMonth() + 1;

        //            var day = objDate.getDate();

        //            if (month < 10) {

        //                month = '0' + month;

        //            }

        //            if (day < 10) {

        //                day = '0' + day;

        //            }

        //            self.input.prop('value', year + '-' + month + '-' + day);
        //        }


    },

    getDateControl: function () {

        var self = this;
        var inputDate = $("<input/>");
        if (self.properties.showTime && !self.inputEdition) {
            inputDate.val(self.value.split(" ")[0]);
            return inputDate;
        } else if (!self.properties.showTime && !self.inputEdition) {
            inputDate.val(self.value);
            return inputDate;
        } else {
            return self.inputEdition.children(".ui-bizagi-render-edit-date");
        }

    },

    getTimeControl: function () {

        var self = this;
        var inputTime = $("<input/>");
        if (self.properties.showTime && !self.inputTime) {
            inputTime.val(self.value.split(" ")[1]);
            return inputTime;
        } else {
            return self.inputTime;
        }

    },

    renderEdition: function () {

        var self = this;

        var properties = self.properties;

        var container = self.getContainerRender();

        var control = self.getControl();

        var textTmpl = self.renderFactory.getTemplate("edition.date");

        var timeTmpl = self.renderFactory.getTemplate("edition.time");

        self.inputEdition = $.tmpl(textTmpl, { 'showTime': false });

        self.inputTime = $.tmpl(timeTmpl);
        //endYear
        if (bizagi.util.isIphoneHigherIOS5()) {

            self.getDateControl().get(0).type = 'date';

            if (properties.showTime) {

                self.inputTime.appendTo(self.inputEdition);

            }

        } else {

            (properties.showTime) ? (presetType = 'datetime', widthtype = 60) : (presetType = 'date', widthtype = 80)

            self.getDateControl().scroller({ display: 'inline', endYear: (new Date().getFullYear() + 10), mode: 'mixed', theme: 'android-ics', preset: presetType, dateFormat: "mm/dd/yyyy", ampm: false, timeFormat: 'HH:ii:ss', seconds: true, showOnFocus: 'true', width: widthtype, onSelect: function (valueText, inst) { self.onSelect(valueText, inst); }, onClose: function (valueText, inst) { self.onClose(valueText, inst); } });

            self.getDateControl().hide();

            self.getTimeControl().hide();

        }



    },

    setDisplayValueEdit: function (value) {

        var self = this;

        var properties = self.properties;

        var objDate = bizagi.util.dateFormatter.getDateFromInvariant(value);

        if (!bizagi.util.isIphoneHigherIOS5()) {

            self.getDateControl().scroller('setDate', objDate, true);

            // self.getDateControl().hide();

            /*maxDate	Date	null	 Maximum date that can be selected

            minDate	Date	null	 Minimum date that can be selected*/

        }

        else {



            var year = objDate.getFullYear();

            var month = objDate.getMonth() + 1; // zero based

            var day = objDate.getDate();

            if (month < 10) {

                month = '0' + month;

            }

            if (day < 10) {

                day = '0' + day;

            }

            self.getDateControl().prop('value', year + '-' + month + '-' + day);

            // min="2011-01-01" max="2015-12-31"

            if (properties.showTime) {

                var hour = objDate.getHours();

                var minutes = objDate.getMinutes();

                var seconds = objDate.getSeconds();



                if (hour < 10) {

                    hour = '0' + hour;

                }

                if (minutes < 10) {

                    minutes = '0' + minutes;

                }

                if (seconds < 10) {

                    seconds = '0' + seconds;

                }

                self.getTimeControl().prop('value', hour + ':' + minutes + ':' + seconds);

            }

        }



    },

    onSelect: function (valueText, inst) {

        var self = this;

        self.setValue(valueText, false);

        self.input.html(valueText);
        self.input.val(valueText);
    },

    onClose: function (valueText, inst) {

        var self = this;

        var contexttmp = self.getFormContainer().container.find("#container-items-edit")

        $(".ui-bizagi-container-button-edit .ui-bizagi-cancel-btn", contexttmp).click();

    },

    actionSave: function () {

        var self = this;

        var properties = self.properties;



        if (bizagi.util.isIphoneHigherIOS5()) {

            var valueDate = self.getDateControl().val();

            var valueTime = self.getTimeControl();

            var currentTime = (properties.showTime && valueTime && valueTime.val() != "") ? " " + valueTime.val() : " 00:00:00";

            var fullDate = valueDate.length > 0 ? valueDate + currentTime : bizagi.util.dateFormatter.formatISO(new Date(), false) + currentTime;

            fullDate = bizagi.util.dateFormatter.getDateFromISO(fullDate, true);

            self.setValue(bizagi.util.dateFormatter.formatInvariant(fullDate, properties.showTime));

            self.input.html(bizagi.util.dateFormatter.formatDate(fullDate, properties.fullFormat));
            self.input.val(bizagi.util.dateFormatter.formatDate(fullDate, properties.fullFormat));
        }

        else {

            //replace for -

            var date = self.getDateControl().scroller('getDate');

            self.setValue(bizagi.util.dateFormatter.formatInvariant(date, properties.showTime));

            self.input.html(bizagi.util.dateFormatter.formatDate(date, properties.fullFormat)); //date);
            self.input.val(bizagi.util.dateFormatter.formatDate(date, properties.fullFormat));

            if (properties.showTime) {

                self.inputTime.val(bizagi.util.dateFormatter.formatDate(date, properties.timeFormat));

            }

            //self.getDateControl().val(bizagi.util.dateFormatter.formatDate(date, properties.fullFormat))

        }



        /* if (self.properties.submitOnChange)

        self.submitOnChange();*/



    },
    /*
    *   set the value when the control change only available for ios greather than 5
    */
    onChangeHandler: function () {
        var self = this;
        var properties = self.properties;
        var dateControl = self.input;
        var timeControl = self.inputTime;

        var currentDate = dateControl.val();
        var currentTime = (properties.showTime && timeControl.val() != "") ? " " + timeControl.val() : " 00:00:00";

        // If curren date is empty reset actual value
        if ((currentDate == "" && !properties.showTime) || (currentDate == "" && timeControl.val() == "")) {
            self.setValue("");
            properties.displayValue = "";
        } else {
            // If no date selected then use the current date
            var fullDate = currentDate.length > 0 ? currentDate + currentTime : bizagi.util.dateFormatter.formatISO(new Date(), false) + currentTime;

            // Convert ISO date into date object
            fullDate = bizagi.util.dateFormatter.getDateFromISO(fullDate, true);
            if (!fullDate || fullDate == 0) {
                return;
            }

            // Set date value
            self.setValue(bizagi.util.dateFormatter.formatInvariant(fullDate, properties.showTime));

            // Set display value
            properties.displayValue = bizagi.util.dateFormatter.formatDate(fullDate, properties.fullFormat);
        }
    }


}); 