﻿/*
*   Name: BizAgi Tablet Render Association
*   Author: RicharU (based on Edward Morales)
*   Comments: Defines the association control
*/

bizagi.rendering.association.extend('bizagi.rendering.association', {}, {
    /* POSTRENDER
    =====================================================*/
    postRenderSingle: function () {

        var self = this;
        var control = self.getControl();

        // Cache control
        self.association = $('.bz-wp-render-association', control);
        self.getArrowContainer().css("visibility", "hidden");

        // Add Events at Association Control
        self.addEventControl();

        if (!self.properties.editable) {
            self.association.find('input:checkbox').hide();
        }
    },

    /* ADD EVENT
    =====================================================*/
    addEventControl: function () {

        var self = this;
        var template = self.renderFactory.getTemplate("association");
        var control = self.getControl();

        // Bind check toggle
        self.association.delegate('input:checkbox', 'change', function () {

            var contextElement = $(this);

            if (contextElement.prop('checked')) {
                // Associate item
                self.associateElement(contextElement);
            } else {
                // Dissociate item
                self.dissociateElement(contextElement);
            }
        });

        // Bind flip actions
        self.association.delegate('.association-flip-button', 'click', function () {

            $(control).empty();

            // Define flipped
            self.properties.flipped = !self.properties.flipped;

            // Send flip data to server
            self.properties.flipstate = self.properties.flipped;

            self.dataService.getFlipAssociation(self.properties);

            // Render template
            $.tmpl(template, self.properties, {
                getColumnData: self.getColumnData,
                getRightAssociation: self.getRightAssociation,
                getLeftName: self.getLeftName
            }).appendTo(control);

            self.postRenderSingle();
        });
    },

    renderReadOnly: function () {
        var self = this;
        return self.renderControl();
    },

    /* RENDER READONLY VALUES
    =====================================================*/
    postRenderReadOnly: function () {
        var self = this;
        var control = self.getControl();
        var template = self.renderFactory.getTemplate("association");

        // Render template  
        $.tmpl(template, self.properties, {
            getColumnData: self.getColumnData,
            getRightAssociation: self.getRightAssociation,
            getLeftName: self.getLeftName
        }).appendTo(control);

        // Cache control
        self.association = $('.bz-wp-render-association', control);
        self.getArrowContainer().css("visibility", "hidden");

        // Bind event for flip botton
        self.association.delegate('.association-flip-button', 'click', function () {
            $(control).empty();
            // Define flipped
            self.properties.flipped = !self.properties.flipped;

            self.postRenderReadOnly();
        });
    },

    /* ASSOCIATE THE LEFT ITEM WITH THE ACTIVE PARENT
    =====================================================*/
    associateElement: function (element) {
        var self = this;
        var context = self.getControl();

        // Get the parent id
        var parentId = $(element.parents('ul'), context).data('parent-id');
        var itemAdd = element.data('id');

        // Update JSON object
        self.addElement(parentId, itemAdd);
    },

    /* DISSOCIATES AN ITEM FROM THE ACTIVE COLLECTION
    =====================================================*/
    dissociateElement: function (element) {
        var self = this;
        var context = self.getControl();

        // Get the parent id
        var parentId = $(element.parents('ul'), context).data('parent-id');
        var itemRemove = element.data('id');

        // Update JSON object
        self.removeElement(parentId, itemRemove);
    },

    /* SET-DISPLAY-VALUE
    =====================================================*/
    setDisplayValue: function (value) {
        var self = this;
        // Set internal value
        self.setValue(value, false);
    }
});