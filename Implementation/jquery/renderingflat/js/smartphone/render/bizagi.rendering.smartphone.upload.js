﻿/*
*   Name: BizAgi smartphone Render Label Extension
*   Author: oscaro
*   Comments:
*   -   This script will redefine the label render class to adjust to smartphones devices
*/

// Extends itself
bizagi.rendering.upload.extend("bizagi.rendering.upload", {
    BA_ACTION_PARAMETER_PREFIX: bizagi.render.services.service.BA_ACTION_PARAMETER_PREFIX,
    BA_CONTEXT_PARAMETER_PREFIX: bizagi.render.services.service.BA_CONTEXT_PARAMETER_PREFIX,
    QUALITY_PICTURE: 80,
    LIMIT: 1, //limit: The maximum number of audio clips,video clips,etc in the device user can record in a single capture operation.
    EXTENSIONSIMG: ["image/jpeg", "jpeg", "image", "png", "jpg"],
    EXTENSIONSVIDEO: ["video/quicktime", "quicktime", "qt", "mov"],
    EXTENSIONSAUDIO: ["audio/wav", "audio", "wav"]
}, {

    renderUploadImage: function(params){
        var self = this;

        var properties = self.properties;
        var parameters = {
                url: properties.dataUrl,
                xpath: properties.xpath,
                idRender: properties.id,
                xpathContext: properties.xpathContext,
                idPageCache: properties.idPageCache,
                property: "fileContent",
                dataType: "text",
                getRequest: true,
                fileId: params.fileid
            };

        bizagi.util.smartphone.startLoading();
        self.dataService.multiaction().getPropertyData(parameters, "text").done(function (data) {
            if (data[1] != "") {
                url = "data:image/png;base64," + data[0][1];
            }

            var createViewParams = {
                'idCase': bizagi.context.idCase,
                'displayName': params.displayName,
                'content': $.tmpl( self.renderFactory.getTemplate("image-preview"), {'url': url} ),
                'kendoViewOptions': { 'zoom': true, 'hide': function(){this.destroy();} },
                'titleLable': false,
                'kendoViewFinish': function () {
                    setTimeout(function(){
                        var view = bizagi.kendoMobileApplication.view().element;
                        var originalImageWidth = $('.ui-bizagi-render-preview', view).width();
                        var adjustedImageWidthScale = (view.width() < originalImageWidth) ? (view.width() / originalImageWidth) : 1;
                        bizagi.kendoMobileApplication.scroller().movable.scaleTo(adjustedImageWidthScale);
                        $('.ui-bizagi-container-inputs', view).css('padding', 0);
                    },200);
                },
                getNavigation: true,
                'callback': function(params){
                    bizagi.kendoMobileApplication.navigate(params.id);
                    params.navigation.setNavigationButtons(params.viewContainer, 'preview');
                },
            };
            bizagi.webpart.publish("createEditionView", createViewParams);
            bizagi.util.smartphone.stopLoading();
        });
    },

    postRenderSingle: function() {
        var self = this;
        var properties = self.properties;
        var container = self.getControl();
        var containerRender = self.getContainerRender();
        var body = $("body");
        self.configureHelpText();
        self.getArrowContainer().hide();

        if (!properties.editable) {
            containerRender.addClass("bz-command-not-edit");
        } else {
            containerRender.addClass("bz-command-edit-inline");
        }

        self.itemAddfile = $(".bz-rn-upload-show-menu", container);

        if (typeof (cordova) === "undefined" || properties.editable == false) {
            self.itemAddfile.hide();
            return;
        }


        self.getControl().find(".files > li a").bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            if($(this).data('upload-type') === 'image'){
                var params = {
                    fileid: $(this).data('fileid'),
                    displayName: $(this).attr('title')
                };
                self.renderUploadImage(params);
            }else{
                window.open(encodeURI(this.getAttribute("href")), '_blank', 'location=yes');
            }
        });
        
        var itemsUpload = self.checkExtensions();
        var actionSheetUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionsheetUpload-tmpl"], { useWithBlock: false });
        var actionSheetUpload = actionSheetUploadTmpl(itemsUpload);
        $(container).append(actionSheetUpload);
        self.actionSheetUpload = $(".actionsheetUpload", container).kendoMobileActionSheet();

        self.itemAddfile.bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(self.actionSheetUpload).data('kendoMobileActionSheet').open();
         });
        
        self.addListener();        
        self.checkMaxFiles();
    },

    addListener: function () {
        var self = this;

        $("a[data-bz-ordinal]", self.actionSheetUpload).kendoMobileButton({
            click: function (e) {
                bizagi.util.smartphone.startLoading();
                if(e.button.data("bz-ordinal") === 2){//select pic
                    navigator.camera.getPicture(function (dataImage) {
                        $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                            if (resp) {
                                self.saveImage(self, dataImage);
                            }
                            bizagi.util.smartphone.stopLoading();
                        });
                    }, self.onFail, {
                        quality: self.Class.QUALITY_PICTURE,
                        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
                    });
                }else if(e.button.data("bz-ordinal") === 1){//take pic
                    navigator.camera.getPicture(function (dataImage) {

                        $.when(self.checkMaxSize(dataImage)).done(function (responseInternal) {
                            if (responseInternal)
                                self.saveImage(self, dataImage);
                            bizagi.util.smartphone.stopLoading();
                        });
                    },
                   self.onFail,
                    { quality: self.Class.QUALITY_PICTURE,
                        sourceType: navigator.camera.PictureSourceType.CAMERA,
                        mediaType: navigator.camera.MediaType.PICTURE,
                        destinationType: Camera.DestinationType.FILE_URI,
                        correctOrientation: true,
                        saveToPhotoAlbum: true

                    });
                }else if(e.button.data("bz-ordinal") === 3){//capture video
                     navigator.device.capture.captureVideo(function (dataImage) {
                        $.when(self.checkMaxSizeVideo(dataImage)).done(function (responseInternal) {
                            if (responseInternal)
                                self.saveVideo(self, dataImage);
                            bizagi.util.smartphone.stopLoading();
                        });
                    }, self.onFail, { limit: self.Class.LIMIT });
                }else if(e.button.data("bz-ordinal") === 4){//capture audio
                    navigator.device.capture.captureAudio(function (mediaFiles) {
                        var audioUrl = mediaFiles[0].localURL;
                        $.when(self.checkMaxSize(audioUrl)).done(function (responseInternal) {
                            if (responseInternal)
                                self.saveAudio(self, mediaFiles);
                            bizagi.util.smartphone.stopLoading();
                        });
                                                          
                    }, self.onFail, { limit: self.Class.LIMIT });

                }
            }
        });
    },


    /*
    *   Template method to implement in each children to customize each control
    */
    renderControl: function () {
        var self = this,
        properties = self.properties;

        var template = self.renderFactory.getTemplate("upload");

        // Render template
        var html = $.fasttmpl(template, {
            xpath: bizagi.util.encodeXpath(self.getUploadXpath()),
            editable: properties.editable,
            noFiles: (self.filesCount == 0),
            allowSendInMail: properties.allowSendInMail
        });

        // Render current children
        var items = "";
        for (var i = 0; i < self.filesCount; i++) {
            var file = { id: self.files[i][0], fileName: self.files[i][1] };
            var item = self.renderUploadItem(file);
            items += item;
        }
        html = self.replaceFilesHtml(html, items);
        return html;
    },

    /* 
    *   Renders a single upload item 
    */
    renderUploadItem: function (file) {
        var self = this;
        var properties = self.properties;
        var mode = self.getMode();
        var image = false;


        var template = self.renderFactory.getTemplate("uploadItem");
        var url = self.buildItemUrl(file);

        // Don't render urls when not running in execution mode
        if (mode != "execution")
            url = "javascript:void(0);";

        if (self.checkImageFile(file.fileName)) {
            image = true;
        } 

        var html = $.fasttmpl(template, {
            url: url,
            image: image,
            allowDelete: properties.allowDelete,
            filename: file.fileName,
            id: file.id,
            mode: mode
        });

        return html;
    },

    checkImageFile: function (fileName) {
        var self = this;
        var validExtensions = self.Class.EXTENSIONSIMG;

        for (var i = 0; i < validExtensions.length; i++) {
            var image = fileName.indexOf('.' + validExtensions[i]);

            if (image >= 0) {
                return true;
            }
        }
        return false;
    },

    getTemplateName: function () {
        return "upload";
    },

    getTemplateItemName: function () {
        return "uploadItem";
    },

    getTemplateEditionName: function () {
        return "edition.upload";
    },

    getTemplateEditionMenu: function () {
        return "edition.upload.menu";
    },

    saveImage: function (context, dataImage) {
        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        
        


	

        
        















        
        options.mimeType = "image/jpeg";
        options.params = data;
        var ft = new FileTransfer();
        ft.upload(dataImage, properties.addUrl,
         function (r) {
            var response = JSON.parse(decodeURIComponent(r.response))
            if(response.type === 'error'){
                navigator.notification.alert(response.message, function(){}, response.errorType);
            }else{
                self.onUploadFileCompleted(context, response);
            }
         },
         function (error) {
             bizagi.log("An error has occurred: Code = " + error.code);
         }
         , options);
    },

    saveAudio: function (context, dataAudio) {

        var self = context;

        var properties = self.properties;

        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());

        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataAudio[0].name;

        //options.mimeType = "audio/wav";

        options.params = data;

        var ft = new FileTransfer();

        ft.upload(dataAudio[0].fullPath, properties.addUrl,
                 function (r) {
                     var response = JSON.parse(decodeURIComponent(r.response))
                    if(response.type === 'error'){
                        navigator.notification.alert(response.message, function(){}, response.errorType);
                    }else{
                        self.onUploadFileCompleted(context, response);
                    }
                 },

                 function (error) {
                     bizagi.log("An error has occurred: Code = " + error.code);
                 }

                 , options);

    },

    saveVideo: function (context, dataVideo) {

        var self = context;

        var properties = self.properties;

        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());

        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataVideo[0].name;

        // options.mimeType = "video/quicktime";

        options.params = data;

        var ft = new FileTransfer();

        ft.upload(dataVideo[0].fullPath, properties.addUrl,

                 function (r) {
                     var response = JSON.parse(decodeURIComponent(r.response))
                    if(response.type === 'error'){
                        navigator.notification.alert(response.message, function(){}, response.errorType);
                    }else{
                        self.onUploadFileCompleted(context, response);
                    }

                 },

                 function (error) {

                     bizagi.log("An error has occurred: Code = " + error.code);

                 }

                 , options);

    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);
        bizagi.util.smartphone.stopLoading();
    },

    onUploadFileCompleted: function (context, response) {
        var self = context,

         control = self.getControl();

        var uploadWrapper = $(".bz-rn-upload-show-menu", control);

        var result = response;

        if (result.id && result.fileName) {
            var newItem = self.renderUploadItem(result);

            self.files.push([result.id, result.fileName]);

            // Locate it before the upload wrapper

            $(newItem).insertBefore(uploadWrapper);

            // Increment counter

            self.filesCount = self.filesCount + 1;

            self.triggerRenderChange();
                               
           $('a[data-fileid="'+result.id+'"]', self.control).bind("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                if($(this).data('upload-type') === 'image'){
                    var params = {
                        fileid: $(this).data('fileid'),
                        displayName: $(this).attr('title')
                        };
                    self.renderUploadImage(params);
                }else{
                    window.open(encodeURI(this.getAttribute("href")), '_blank', 'location=yes');
                }
            });

            // Check maxFiles

            self.checkMaxFiles();
        } else {
            bizagi.log("E:" + result.message);

        }
    },

    checkMaxFiles: function () {
        var self = this;
        var properties = self.properties;
        var maxFiles = properties.maxfiles;
        var actualFiles = properties.value.length;

        if (maxFiles != 0 && (actualFiles >= maxFiles)) {
            self.itemAddfile.hide();
        }
    },

    checkExtensions: function () {
        var self = this;
        var properties = self.properties;
        var validExtensions = properties.validExtensions;
        
        var enableVideo =  enableAudio =  enableImage = false;
        
        if (typeof validExtensions === "undefined" || validExtensions == "") {
        	 enableVideo =  enableAudio =  enableImage = true;        	 
        }else{

	        validExtensions = validExtensions.split(";");
	
	        if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
	        	enableVideo =  enableAudio =  enableImage = true;	        	
	        }else{	        
		
		        for (var i = 0; i < validExtensions.length; i++) {
		            validExtensions[i] = validExtensions[i].replace("*.", "");
		            var image = self.Class.EXTENSIONSIMG.toString().indexOf(validExtensions[i]);
		            var audio = self.Class.EXTENSIONSAUDIO.toString().indexOf(validExtensions[i]);
		            var video = self.Class.EXTENSIONSVIDEO.toString().indexOf(validExtensions[i]);
		
		            if (image >= 0) {
		                enableImage = true;
		            }
		            if (audio >= 0) {
		                enableAudio = true;
		            }
		
		            if (video >= 0) {
		                enableVideo = true;
		            }
		
		        }
	        }
        }

        var itemsUpload = {'buttons': [], 'id': properties.id};
        
        //add image options 
        if (enableImage) {
        	itemsUpload.buttons.push(
                   {
                       'ordinal': 1,
                       'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-photo")
                   });
        	itemsUpload.buttons.push( 
        			{
                       'ordinal': 2,
                       'caption': bizagi.localization.getResource("workportal-actionsheet-upload-choose-photo")
                    });
        }
        //add video options         
        if (enableVideo){
        	itemsUpload.buttons.push(          
        			{
                       'ordinal': 3,
                       'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-video")
                    });
        }
        //add audio options         
        if (enableAudio){
				itemsUpload.buttons.push(
				{
				   'ordinal': 4,
				   'caption': bizagi.localization.getResource("workportal-actionsheet-upload-record-audio")
                });
        }
                    
        return itemsUpload;


    },
    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURL(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {

                if (fileObj.size >= properties.maxSize) {
                    navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    defer.resolve(false);
                }
                else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },

    checkMaxSizeVideo: function (objectVideo) {

        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        var size = objectVideo[0].size;

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        if (size >= properties.maxSize) {
            navigator.notification.alert(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize)); //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
            defer.resolve(false);
        }
        else {
            defer.resolve(true);
        }



        return defer.promise();

    }

    //TODO:android no arroja la extencion solo permite por el tipo imagen o no
    /* checkExtensionsInFile: function(extFile){
    var self = this;
    var properties = self.properties;
    var defer = new $.Deferred();
        
    if (typeof validExtensions === "undefined" || validExtensions == "") {
    defer.resolve(true);
    }
        
    validExtensions = validExtensions.split(";");
        
    if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
    defer.resolve(true);
    }   
        
    for (element in validExtensions) {
    if(extFile.indexOf(element) >= 0 )
    {
    defer.resolve(true);
    }
    }
    alert("not posible in cordova")
    defer.resolve(false);     
    return defer.promise();
    	
    }*/



});
