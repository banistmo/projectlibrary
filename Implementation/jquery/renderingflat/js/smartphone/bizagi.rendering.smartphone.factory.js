﻿/*
*   Name: BizAgi Smartphone Render Factory
*   Author: Oscar O
*   Comments:
*   -   This script will define a render factory to create smartphone versions of each render type
*/

bizagi.rendering.base.factory.extend("bizagi.rendering.smartphone.factory", {}, {

    /*
    *   Load all the templates used for rendering
    */
    loadTemplates: function () {
        var self = this;
        var defer = new $.Deferred();

        $.when(
        // Containers
            self.loadTemplate("form", bizagi.getTemplate("bizagi.renderingflat.smartphone.form") + "#ui-bizagi-render-form"),
            self.loadTemplate("complexgateway", bizagi.getTemplate("bizagi.renderingflat.smartphone.form") + "#ui-bizagi-render-complexGateway"),
            self.loadTemplate("panel", bizagi.getTemplate("bizagi.renderingflat.smartphone.panel")),
            self.loadTemplate("tab", bizagi.getTemplate("bizagi.renderingflat.smartphone.tab")),
            self.loadTemplate("tabItem", bizagi.getTemplate("bizagi.renderingflat.smartphone.tabItem")),
            self.loadTemplate("horizontal", bizagi.getTemplate("bizagi.renderingflat.smartphone.horizontal")),
            self.loadTemplate("group", bizagi.getTemplate("bizagi.renderingflat.smartphone.group")),
            self.loadTemplate("editRender", bizagi.getTemplate("bizagi.renderingflat.smartphone.edit")),
            self.loadTemplate("searchForm", bizagi.getTemplate("bizagi.renderingflat.smartphone.searchForm")),
            self.loadTemplate("accordion", bizagi.getTemplate("bizagi.renderingflat.smartphone.accordion")),
            self.loadTemplate("accordionItem", bizagi.getTemplate("bizagi.renderingflat.smartphone.accordionItem")),
            self.loadTemplate("contentPanel", bizagi.getTemplate("bizagi.renderingflat.smartphone.contentPanel")),
        // Renders
            self.loadTemplate("render", bizagi.getTemplate("bizagi.renderingflat.smartphone.render")),
            self.loadTemplate("text", bizagi.getTemplate("bizagi.renderingflat.smartphone.text")),
            self.loadTemplate("number", bizagi.getTemplate("bizagi.renderingflat.smartphone.number")),
            self.loadTemplate("numberScientificNotation", bizagi.getTemplate("bizagi.renderingflat.smartphone.numberScientificNotation")),
            self.loadTemplate("extendedText", bizagi.getTemplate("bizagi.renderingflat.smartphone.extendedText")),
            self.loadTemplate("yesno", bizagi.getTemplate("bizagi.renderingflat.smartphone.yesno")),
            self.loadTemplate("check", bizagi.getTemplate("bizagi.renderingflat.smartphone.check")),
            self.loadTemplate("button", bizagi.getTemplate("bizagi.renderingflat.smartphone.button")),
            self.loadTemplate("link", bizagi.getTemplate("bizagi.renderingflat.smartphone.link")),
            self.loadTemplate("geolocation", bizagi.getTemplate("bizagi.renderingflat.smartphone.geolocation")),
            self.loadTemplate("radio", bizagi.getTemplate("bizagi.renderingflat.smartphone.radio")),
            self.loadTemplate("upload", bizagi.getTemplate("bizagi.renderingflat.smartphone.upload") + "#bz-rn-upload-container"),
            self.loadTemplate("uploadItem", bizagi.getTemplate("bizagi.renderingflat.smartphone.upload") + "#bz-rn-upload-item"),
            self.loadTemplate('association', bizagi.getTemplate('bizagi.renderingflat.smartphone.association')),
            self.loadTemplate("ecm", bizagi.getTemplate("bizagi.renderingflat.smartphone.ecm") + "#bz-rn-ecm-view-default-item"),
            self.loadTemplate("ecm-metadata", bizagi.getTemplate("bizagi.renderingflat.smartphone.ecm") + "#bz-rn-ecm-metadata"),

            self.loadTemplate("image", bizagi.getTemplate("bizagi.renderingflat.smartphone.image") + "#ui-bizagi-render-image"),
            self.loadTemplate("image-item", bizagi.getTemplate("bizagi.renderingflat.smartphone.image") + "#ui-bizagi-render-image-item"),
            self.loadTemplate("image-preview", bizagi.getTemplate("bizagi.renderingflat.smartphone.image") + "#bz-rd-upload-render-edition"),
            self.loadTemplate("document", bizagi.getTemplate("bizagi.renderingflat.smartphone.document") + "#bz-document-generator"),
            self.loadTemplate("document-item", bizagi.getTemplate("bizagi.renderingflat.smartphone.document") + "#bz-document-generator-item"),
            self.loadTemplate("document-item-preview", bizagi.getTemplate("bizagi.renderingflat.smartphone.document") + "#bz-document-generator-item-preview"),

        //Renders Edition
            self.loadTemplate("edition.text", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.text")),
            self.loadTemplate("edition.extendedText", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.extendedText")),
            self.loadTemplate("edition.number", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.number")),
            self.loadTemplate("edition.date", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.date") + "#bz-rn-date"),
            self.loadTemplate("edition.time", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.date") + "#bz-rn-time"),
            self.loadTemplate("combo", bizagi.getTemplate("bizagi.renderingflat.smartphone.combo")),
            self.loadTemplate("edition.combo", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.combo")),
            self.loadTemplate("edition.search", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.search") + "#bz-rn-search"),
            self.loadTemplate("edition.search.item", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.search") + "#bz-rn-search-item"),
            self.loadTemplate("edition.link", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.link")),
            self.loadTemplate("edition.list", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.list")),
            self.loadTemplate("edition.letter", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.letter")),
            self.loadTemplate("edition.upload.menu", bizagi.getTemplate("bizagi.renderingflat.smartphone.edition.upload") + "#bz-rn-upload-menu"),

        //Renders commons
            self.loadTemplate("form-error", bizagi.getTemplate("bizagi.renderingflat.smartphone.form-error")),
            self.loadTemplate("form-waiting", bizagi.getTemplate("bizagi.renderingflat.smartphone.form-waiting")),


        //GRIDS SMARTPHONES
            self.loadTemplate("grid", bizagi.getTemplate("bizagi.renderingflat.smartphone.grid")),
            self.loadTemplate("cell", bizagi.getTemplate("bizagi.renderingflat.smartphone.cell")),
        	self.loadTemplate("cell.readonly", bizagi.getTemplate("bizagi.renderingflat.smartphone.cell.readonly")),
        	self.loadTemplate("cell.upload", bizagi.getTemplate("bizagi.renderingflat.smartphone.cell.upload")),

        //searchForm

        //grid items
            self.loadTemplate("bizagi.grid.grid", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid"),
        	self.loadTemplate("bizagi.grid.waiting", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-waiting"),
        	self.loadTemplate("bizagi.grid.table", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-table"),
        	self.loadTemplate("bizagi.grid.table.empty", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-table-empty"),
        	self.loadTemplate("bizagi.grid.column", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-column"),
        	self.loadTemplate("bizagi.grid.column.special", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-column-special"),
        	self.loadTemplate("bizagi.grid.row", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-row"),
        	self.loadTemplate("bizagi.grid.row.buttons", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-row-buttons"),
        	self.loadTemplate("bizagi.grid.cell", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-cell"),
        	self.loadTemplate("bizagi.grid.cell.special", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-cell-special"),
        	self.loadTemplate("bizagi.grid.pager", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-pager"),
        	self.loadTemplate("bizagi.grid.buttons", bizagi.getTemplate("bizagi.renderingflat.smartphone.bizagi.grid") + "#ui-bizagi-grid-buttons")

        ).done(function () {

            // Resolve when all templates are loaded
            defer.resolve();
        });

        return defer.promise();
    }
    ,
    /*
     *   Returns the appropiate render based on the render type
     */
    getRender: function (params) {
        var self = this;
        var type = params.type;
        var data = params.data;
        var renderParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService
        });

        if (type == "number") {
            var properties = data.properties;
            if (properties && properties.dataType === 29) {
                return new bizagi.rendering.numberScientificNotation(renderParams);
            }

        }

        return self._super(params);
    },

    /*
     *   Returns the appropiate column based on the render type
     */
    getColumn: function (params) {
        var self = this;
        var type = params.type;
        var columnParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService,
            singleInstance: bizagi.util.isEmpty(params.singleInstance) ? true : params.singleInstance
        });

        if (type == "columnNumber") {
            var data = params.data;
            var properties = data.properties;
            if(properties && properties.dataType === 29) {
                columnParams.decorated = bizagi.rendering.numberScientificNotation;
                return new bizagi.rendering.columns.numberScientificNotation(columnParams);
            }
        }

        // Call base
        return self._super(params);
    }
});
