
/*
*   Name: BizAgi Render Number Scientific notation Class
*   Author: Laura Ariza
*   Comments:
*   -   This script will define basic stuff for scientific notation numbers
*/

bizagi.rendering.render.extend("bizagi.rendering.numberScientificNotation", {
    SD_LIMIT: 38,
    EXP_MIN_LIMIT: -125,
    EXP_MAX_LIMIT: 125

}, {
    /*
     *   Update or init the element data
     */
    initializeData: function (data) {
        var self = this;
        // Call base
        this._super(data);

        // Fill default properties
        var properties = self.properties;

        properties.decimalSymbol = typeof (BIZAGI_DEFAULT_CURRENCY_INFO) !== "undefined" ? BIZAGI_DEFAULT_CURRENCY_INFO.decimalSeparator : format.decimalSymbol;
    },

    renderControl: function () {
        var self = this;
        var template = self.renderFactory.getTemplate("numberScientificNotation");

        // Render template
        var html = $.fasttmpl(template, {});
        return html;
    },

    /*
     * Template method to implement in each device to customize each render after processed
     */
    postRender: function () {
        var self = this;
        var control = self.getControl();
        self.numericInput = control.find("input");
        // Add numeric plugin to avoid invalid keystrokes
        self.numericInput.numericSN(self.properties.decimalSymbol);

    },

    postRenderReadOnly: function(){
        var self = this;
        console.log('read only');
    },

    getDisplayValue: function(){
        var self = this;
        return (self.getValue() > 0 ) ? self.getValue().replace('.', self.properties.decimalSymbol) : self.getValue();
    },

    setValue: function (value) {
        var self = this;
        value = (value || '').toString().replace(self.properties.decimalSymbol, '.');
        self._super(value);
    },

    scientificNotationFormat: function(number, decimalSymbol, sdLimit){
        var self = this;
        number = self.getStandardNotation(number, decimalSymbol);
        var decimalSymbol = decimalSymbol || '.';
        var expSimbol = 'e';
        var sNregex = new RegExp('[0-9]+(' + decimalSymbol + '[0-9]+)?(e[+-]?[0-9]+)?');
        var sNFormat = number.match(sNregex);
        sdLimit = ( sdLimit > 0 ) ? sdLimit : self.Class.SD_LIMIT;
        var expMinLimit = self.Class.EXP_MIN_LIMIT;
        var expMaxLimit = self.Class.EXP_MAX_LIMIT;
        //get que prefix
        var exp = 0;

        var prefix = number[0] === '-'? number[0]: '';
        var originalPointIndex = 0;

        number = number.replace(prefix, '');

        //if the number is not in scientific notation
        if(sNFormat && sNFormat[0] && sNFormat[2]){
            var tempParts = sNFormat[0]?(sNFormat[0].replace(sNFormat[2], '').split(decimalSymbol)):'';

            var significantDigits = '';
            var intPart = '';

            //calculate significant digits
            //remove zeros at the left
            tempParts[0] = tempParts[0].replace(/^0+/g,'');

            if (tempParts[0].length > 1){
                significantDigits = tempParts[0].slice(1);
                originalPointIndex = significantDigits.length;
                intPart = tempParts[0][0] || '';
                significantDigits = significantDigits.concat(sNFormat[1]||'');
            }
            else if (tempParts[0].length === 1){
                intPart = tempParts[0][0] || '';
                significantDigits = significantDigits.concat(tempParts[1]||'');
            }
            else{
                originalPointIndex = tempParts[1].length;
                tempParts[1] = tempParts[1].replace(/^0+/g,'');
                originalPointIndex = (tempParts[1].length - 1) - originalPointIndex;
                intPart = tempParts[1][0];
                significantDigits = tempParts[1].slice(1);
            }
            //remove . and zeros from significant digits
            significantDigits = significantDigits.replace(decimalSymbol, '').replace(/0+$/g,'');

            if(significantDigits.length >= sdLimit){
                significantDigits = significantDigits.slice(0, sdLimit-1);
            }
            exp = Number(sNFormat[2].replace(expSimbol, '')) + originalPointIndex;
            exp = (exp>expMaxLimit)?expMaxLimit:(exp<expMinLimit)?expMinLimit:exp;
            return prefix + intPart + (significantDigits?(decimalSymbol + significantDigits):'') + expSimbol + (exp>0?'+'+exp:exp);
        }
        else if(number){

            originalPointIndex = number.indexOf(decimalSymbol);
            var from = 0;
            var to = number.length-1;

            //remove decimal point
            number = number.replace(decimalSymbol, '');

            //from
            var i = from;
            while(!significantFlag){
                if(number[i]!== '0'&& number[i]!== decimalSymbol ){significantFlag = true;from = i;}
                else {i++;}
            }
            //to
            var significantFlag = false;
            var j = to;
            while(!significantFlag){
                if(number[j]!== '0'&& number[j]!== decimalSymbol && '-'){significantFlag = true; to = j + 1;}
                else {j--;}
            }
            to = to>sdLimit+from?sdLimit+from:to;
            if(originalPointIndex !== -1){
                //find significant digits
                var significantFlag = false;
                var significantDigits = number.slice(from, to);
                var pointIndex = from + 1;
                exp = originalPointIndex - pointIndex;
                if(significantDigits.length > 0 && !isNaN(Number(significantDigits[0]))){
                    return prefix + significantDigits[0] + (significantDigits.length > 1? decimalSymbol + significantDigits.slice(1):'') + expSimbol + (exp>0?'+'+exp:exp);
                }
                else{ return '0';}

            }
            else{
                //Int numbers
                var firstDigit =  number.slice(from, from + 1);
                var restDigits =  number.slice(from + 1, number.length);
                exp = restDigits.length;
                restDigits = restDigits.replace(/0+$/g,'');
                if(restDigits.length > sdLimit){
                    restDigits =  number.slice(from + 1, sdLimit + from);
                }
                if(Number(firstDigit)){
                    return prefix + firstDigit + (restDigits.length > 0? decimalSymbol + restDigits: '') + expSimbol + (exp>0?'+'+exp:exp);
                }else{ return '0';}
            }
        }
        else{ return ''; }
    },

    //remove all the character except the +, -, decimal point and the numbers
    getStandardNotation: function(number, decimalSymbol){
        var plusCharacter = '+';
        var minusCharacter = '-';
        var decimalSymbol = decimalSymbol || '.';
        var expSymbol = 'e';
        var tmp = '';
        //remove all the character except the +, -, decimal point and the numbers
        var decimalNumber = false;
        var exponentialNumber = false;
        number = (number.length>0?number.toString().toLowerCase():'').replace(/ /g,'');
        for(var i = 0; i < number.length; i++){
            if((number[i] === plusCharacter || number[i] === minusCharacter) && (i === 0 || number[i-1] == expSymbol)){
                tmp += number[i];
            }else if(!isNaN(Number(number[i]))){
                tmp += number[i];
            }
            else if((number[i] === decimalSymbol) && !decimalNumber){
                tmp += number[i];
                decimalNumber = true;
            }
            else if((number[i] === expSymbol) && !exponentialNumber){
                tmp += number[i];
                exponentialNumber = true;
            }
        }

        return tmp;
    }
});