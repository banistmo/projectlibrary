/*
 *   Name: BizAgi Ajax Services
 *   Author: Diego Parra
 *   Comments:
 *   -   This class will provide a low level ajax handling over jquery
 *       that will let us switch between plain ajax and json-rpc 2.0 protocol
 *   -   http://groups.google.com/group/json-rpc/web/json-rpc-2-0
 */

// Create or define namespace
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.services = (typeof (bizagi.services) !== "undefined") ? bizagi.services : {};
bizagi.services.ajax = (typeof (bizagi.services.ajax) !== "undefined") ? bizagi.services.ajax : {};
bizagi.log = bizagi.log || function () {};
bizagi.assert = bizagi.assert || function () {};
// Global variables
bizagi.services.ajax.useAbsolutePath = typeof (BIZAGI_USE_ABSOLUTE_PATH) !== "undefined" ? BIZAGI_USE_ABSOLUTE_PATH : false;
bizagi.services.ajax.pathToBase = bizagi.services.ajax.useAbsolutePath ? (bizagi.loader.getLocationPrefix()) : (typeof (BIZAGI_PATH_TO_BASE) !== "undefined" ? BIZAGI_PATH_TO_BASE : "");
bizagi.services.ajax.loginPage = bizagi.services.ajax.pathToBase || "default.htm";
bizagi.services.ajax.errorPage = bizagi.services.ajax.pathToBase + "error.html";
bizagi.services.ajax.rpcEnabled = false;
bizagi.services.ajax.rpcHandler = "RestServices/JsonRpcHandler.ashx";
bizagi.services.ajax.logEnabled = true;
bizagi.services.ajax.logSuccess = true;
bizagi.services.ajax.logSubmits = true;
bizagi.services.ajax.logFail = true;
bizagi.services.ajax.batch = {};
bizagi.services.ajax.batchCount = 0;
bizagi.services.ajax.batchTimer = 100;
bizagi.services.ajax.batchTimeoutInstance = null;
// $.xhrPool and $.ajaxSetup are the solution
bizagi.services.ajax.xhrPool = [];
/*
 *   Ajax prefilter to enable/disable globally rpc and fix urls
 */
$.ajaxPrefilter("json", function (options, originalOptions, jqXHR) {
    bizagi.services.ajax.xhrPool.push(jqXHR);
    var rpcEnabled = (options.rpcEnabled !== undefined) ? options.rpcEnabled : bizagi.services.ajax.rpcEnabled;
    if (options.triggerBatch)
        return;
    if (rpcEnabled) {

        // If there is no timeout instance create one
        if (bizagi.services.ajax.batchTimeoutInstance == null) {
            bizagi.services.ajax.batchTimeoutInstance = setTimeout(function () {
                    // Call ajax rpc service
                    bizagi.services.ajax.fireBatch();
                },
                bizagi.services.ajax.batchTimer);
        }

        // Enqueue requests
        var newDeferred = jQuery.Deferred();
        newDeferred.promise(jqXHR);
        bizagi.services.ajax.addToBatch(options, newDeferred, jqXHR);
        // Change transport
        options.dataType = "jsonrpc";
        options.dataTypes = [];
        options.dataTypes.push(options.dataType);
    }
});
/*
 *   Ajax prefilter to to fix text "& scripts urls
 */
$.ajaxPrefilter("*", function (options, originalOptions, jqXHR) {

    var rpcEnabled = (options.rpcEnabled !== undefined) ? options.rpcEnabled : bizagi.services.ajax.rpcEnabled;
    // Don't convert the trigger batch request, neither the non-rpc requests
    if (options.triggerBatch || !rpcEnabled) {
        // Fix urls 
        var finalUrl = options.url.indexOf("http") == 0 ? options.url : bizagi.services.ajax.pathToBase + options.url;
        options.url = finalUrl;
        if (options.dataType == "json" && !options.cache) {
            options.cache = false;
        }

        // Add log option
        if (bizagi.services.ajax.logEnabled) {
            if (bizagi.services.ajax.logSubmits && finalUrl.indexOf("tmpl") == -1 && finalUrl.indexOf("resources.json") == -1) {
                bizagi.log("Sending data to " + finalUrl, options.data, "success");
            }

            if (options.crossDomain == true) {
                options.xhrFields = {
                    withCredentials: true
                };
            }

            jqXHR.done(function (result) {
                if (bizagi.services.ajax.logSuccess && finalUrl.indexOf("tmpl") == -1 && finalUrl.indexOf("resources.json") == -1) {
                    bizagi.log(finalUrl + " succeded", result, "success");
                }
            }).fail(function (_, __, message) {
                // Log data sent
                bizagi.log("Sent data to" + finalUrl, options.data, "error");
                // Log error
                if (bizagi.services.ajax.logFail) {
                    // Log result
                    if (message.name == "SyntaxError") {
                        bizagi.log(finalUrl + " failed - syntax error", arguments[0].responseText, "error");
                    } else {
                        bizagi.log(finalUrl + " failed", message, "error");
                    }
                }
            });
        }
    }
    return;
});
/*
 *   Create ajax transport to handle jsonrpc fake calls
 */
$.ajaxTransport("jsonrpc", function (options, originalOptions, jqXHR) {
    return {
        send: function (headers, completeCallback) {
            // We don't do anything here, because this is a fake request
        },
        abort: function () {
            // We don't do anything here, because this is a fake request
        }
    };
});
/*
 *   Extend parse JSON to parse error messages from server
 *   then apply a new converter and assign the new function
 */
bizagi.services.ajax.parseJSON = function (data) {
    var result = {};
    // Remove break line
    data = data.replace(/(\r\n|\n|\r)/gm, "");

    try {
        result = $.parseJSON(data);

    } catch (e) {

        try {
            // Try to fix some known problems regarding rare ascii characters
            result = $.parseJSON(data.replace(/\x1d/g, "."));
        } catch (ex) {
            result.type = "error";
            result.code = e.errorCode;
            result.message = data;
            //Throw exception in next layer 
        }
    }
    if (true) {};

    if ((result.type && result.type == "error") || (result.status && result.status == "error")) {
        if (result.code == "AUTH_ERROR" || result.code == "AUTHENTICATION_ERROR") {
            // Redirect to login page
            if (typeof bizagi.services.ajax.loginPage === "string") {
                // Remove session storage of authentication data
                sessionStorage.removeItem("bizagiAuthentication");
                if (localStorage.getItem("clientLog") != "true") {
                    // Reload page
                    window.location = bizagi.services.ajax.loginPage;

                    if (!bizagi.util.isIE()) {
                        //Redirect to home
                        window.location.href = location.origin + location.pathname;
                    }
                }
            } else {
                bizagi.services.ajax.loginPage();
            }
        }

        if (result.code == "licenseError") {

            window.location = bizagi.services.ajax.errorPage + "?message=" + result.message + "&type=" + result.type;
        };
        var message = (result.code ? result.code : "") + (result.message ? "" + result.message : "");
        jQuery.error(message);
    }
    localStorage.setItem("clientLog", "false");
    return result;


};

bizagi.services.ajax.xhrPool.abortAll = function () {
    $(this).each(function (idx, jqXHR) {
        jqXHR.abort();
    });
    bizagi.services.ajax.xhrPool.splice(0, bizagi.services.ajax.xhrPool.length);
};

$.ajaxSetup({
    /*crossDomain: 'true',
    xhrFields: {
    withCredentials: true
    },*/
    converters: {
        "text json": bizagi.services.ajax.parseJSON
    },
    complete: function (jqXHR) {
        var index = bizagi.services.ajax.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            bizagi.services.ajax.xhrPool.splice(index, 1);
        }
    }
});
/*
 *   Enable disable batch
 */
bizagi.services.ajax.enableBatch = function (enabled) {
    // If switching off, fire batch first and cancel timeout
    if (bizagi.services.ajax.rpcEnabled && !enabled) {
        bizagi.services.ajax.fireBatch();
    }

    bizagi.services.ajax.rpcEnabled = enabled;
};
/*
 *   Add to batch function
 *
 *   -   This method will add a single ajax call to the batch so
 *   -   we can trigger them later
 */
bizagi.services.ajax.addToBatch = function (options, deferred, jqXHR) {
    // Create a deferred object
    deferred = deferred || $.Deferred();
    var id = bizagi.services.ajax.batchCount;
    bizagi.services.ajax.batch[id] = {
        deferred: deferred,
        jqXHR: jqXHR,
        options: options,
        id: id
    };
    bizagi.services.ajax.batchCount++;
    return deferred.promise();
};
/*
 *   Fire batch function
 *
 *   -   This method will fire all the batched ajax calls and will dispatch
 *       all the original callbacks
 */
bizagi.services.ajax.fireBatch = function () {
    // Create JSON RPC structure
    var jsonRPC = [];
    // Build mega JSON
    $.each(bizagi.services.ajax.batch, function (i, item) {
        jsonRPC.push(buildJsonRpcItem(item.options, item.id));
    });
    // Clear batch and store a backup locally
    var sentBatch = bizagi.services.ajax.batch;
    bizagi.services.ajax.batch = {};
    bizagi.services.ajax.batchCount = 0;
    // Call ajax
    var promise = $.ajax({
        url: bizagi.services.ajax.pathToBase + bizagi.services.ajax.rpcHandler,
        type: "POST",
        dataType: "json",
        triggerBatch: true,
        data: JSON.encode(jsonRPC)
    });
    // Resolve responses
    $.when(promise).done(function (responses) {
        $.each(responses, function (i, response) {
            var deferred = sentBatch[response.id].deferred;
            // Execute deferred
            if (response.result) {
                deferred.resolve($.parseJSON(response.result));
            } else if (response.error) {
                // Log data sent
                bizagi.log("Sent data to" + sentBatch[response.id].options.url, sentBatch[response.id].options.data, "error");
                // Log error
                if (bizagi.services.ajax.logFail) {
                    // Log result
                    bizagi.log(sentBatch[response.id].options.url + " failed", response.error, "error");
                }

                deferred.reject(null, "error", response.error);
            }
        });
    });
    //  Remove timeout
    bizagi.services.ajax.batchTimeoutInstance = null;
    // Helper method
    function buildJsonRpcItem(options, id) {
        return {
            "jsonrpc": "2.0",
            "method": options.url,
            "params": options.data,
            "id": id
        };
    }

    // Return promise
    return promise;
};
// Add x-domain support for ie7-ie8-ie9
if (typeof jQuery.browser != "undefined" && jQuery.browser.msie && window.XDomainRequest && (document.documentMode == 7 || document.documentMode == 8 || document.documentMode == 9)) {
    jQuery.support.cors = true;
}