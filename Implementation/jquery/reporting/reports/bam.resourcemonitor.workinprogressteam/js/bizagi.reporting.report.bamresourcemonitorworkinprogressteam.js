﻿/*
*   Name: BizAgi Report for BAM - Process - Work in Progress
*   Author: David Romero
*   Comments:
*   -   This script draws a specific report for BAM - Resource Monitor - User
*/

bizagi.reporting.report.bamresourcemonitorworkinprogressuser.extend("bizagi.reporting.report.bamresourcemonitorworkinprogressteam", {}, {

    /*
    * Initialize report
    */
    init: function (params) {

        this._super(params);

        var id = params.myTeam.id,
            values = params.myTeam.items;

        this.model = { "dimension": [{ id: id, values: values, includeEmptyValues: false, type: "system"}] };
    },

    /*
    *   Templated render component
    */
    renderContent: function () {

        var self = this;
        var main = self.getTemplate("main");

        // Render main template
        self.content = $.tmpl(main);

        return $.when(self.loadComponents()).pipe(function () {

            var filter = "filters=" + JSON.stringify(self.model);

            //draw report
            self.drawReport(filter);

            return self.content;
        });

    },

    /*
    * Post Render
    */
    postRender: function () {
        var self = this;

        //Bind event to export to excel
        self.bindExportToExcel("bam.resources.myteam");
    }

});
