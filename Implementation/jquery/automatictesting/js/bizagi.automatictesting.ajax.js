﻿/*
*   Adds an ajax prefilter in order to record a test case
*/
// Create or define namespace
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.automatictesting = (typeof (bizagi.automatictesting) !== "undefined") ? bizagi.automatictesting : {};
bizagi.automatictesting.ajax = (typeof (bizagi.automatictesting.ajax) !== "undefined") ? bizagi.automatictesting.ajax : {};

// Global variables
bizagi.automatictesting.ajax.requests = [];

$.ajaxPrefilter("*", function (options, originalOptions, jqXHR) {
    var url = options.url;

    if (options.serviceType == "NEWCASE" || options.serviceType == "CREATECASES") {
        if (options.serviceType == "NEWCASE") {
            recorder.closeNewCaseDialog();
            recorder.closeUploadDialog();
        }
        if (options.serviceType == "CREATECASES") {
            recorder.closeSOADialog();
        }

        bizagi.automatictesting.state.startRecording();
        recorder.addQuickLoginRequest();
    }

    // If not enabled don't do anything
    if (bizagi.automatictesting.state.isRecording() == false) { return; }
    // Don'record tmpl or resources url
    if (url.indexOf("tmpl") != -1 || url.indexOf("resources.json") != -1) { return };

    if (!options.serviceType) { options.serviceType = "UNDEFINED"; };

    // Calculate absolute path
    if (bizagi.services.ajax.useAbsolutePath == false) {
        url = bizagi.loader.basePath + url.replaceAll("../", "");
    }

    // Log requests
    var request = {
        url: url,
        data: options.data,
        method: options.type,
        response: undefined,
        success: false,
        errorMessage: undefined,
        serviceType: options.serviceType
    };

    // Attach async handlers
    jqXHR.done(function (result, status, xhr) {
        // Save success
        var contentType = xhr.getResponseHeader("Content-Type");
        var responseText = contentType.indexOf("xml") > 0 ? xhr.responseText : result;

        request.response = bizagi.clone(responseText);
        request.success = true;

        // Add to stack
        if (request.serviceType != "UNDEFINED") {
            if (request.data.indexOf("h_propertyName=fileContent") == -1) {
                bizagi.automatictesting.ajax.requests.push(request);
            }
        }

    }).fail(function (_, __, message) {
        // Save failure
        request.response = arguments[0].responseText;
        if (message.name != "SyntaxError") {
            request.errorMessage = message;
        }

        // Add to stack
        if (request.serviceType != "UNDEFINED") {
            bizagi.automatictesting.ajax.requests.push(request);
        }
    });
    return;
});