﻿$.Class.extend("bizagi.automatictesting.recorder", {}, {

    init: function () {
        var self = this;
        $(document).ready(function () {
            if (!bizagi.automatictesting.state.isRecording()) {
                var queryStringParams = bizagi.automatictesting.host.getQueryStringParams();
                if (queryStringParams["userName"] != undefined && queryStringParams["domain"] != undefined) {
                    bizagi.automatictesting.state.setQuickLogin(queryStringParams["userName"], queryStringParams["domain"]);
                } else {
                    bizagi.automatictesting.state.setQuickLogin(undefined, undefined);
                }
            } else {
                recorder.addQuickLoginRequest();
            }
            window.setTimeout(function () { recorder.openQuickLoginDialog(); }, 1000);
        });
    },

    openNewCaseDialog: function () {
        var self = this;
        $.when(workportal.ready())
        .done(function () {
            newCaseDialog = workportal.getMainController().showDialogWidget({
                widgetName: "newCase",
                dialogClass: "no-close",
                closeVisible: false,
                closeOnEscape: false,
                modalParameters: { title: bizagi.localization.getResource("bizagi-autotesting-recorder-title"), width: "400", height: "500" },
                buttons: [
                    {
                        text: bizagi.localization.getResource("bizagi-autotesting-recorder-soabased"),
                        click: function () {
                            newCaseDialog.dialogBox.dialog('close'); 
                            self.openSOADialog();
                        }
                    },
                    {
                        text: bizagi.localization.getResource("bizagi-autotesting-recorder-continuefromexisting"),
                        click: function () {
                            newCaseDialog.dialogBox.dialog('close'); 
                            self.openUploadDialog();
                        }
                    },
                    {
                        text: bizagi.localization.getResource("bizagi-autotesting-recorder-changeuser"),
                        click: function () {
                             self.logout();
                        }
                    }
                ]
            });
        });
    },

    loadUsers: function () {
        var userlist = $("#cbUsers");
        var users = bizagi.automatictesting.host.getUsers();
        if (users) {
            for (var i = 0; i < users.length; i++) {
                userlist.append('<option>' + users[i].name + '</option>');
            }
        }
    },

    closeNewCaseDialog: function () {
        newCaseDialog.dialogBox.dialog('close');
    },

    flushModal: function () {
        $('.ui-widget-overlay').hide();
    },

    blockBackground: function () {
        $('.ui-widget-overlay').show();
    },

    loadRecorderTemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/recorder.tmpl.html");
    },

    loadUploadTemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/uploadTemplate.tmpl.html");
    },

    loadLoginTemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/login.tmpl.html");
    },

    loadQuickLoginTemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/quickLogin.tmpl.html");
    },

    loadSOATemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/soa.tmpl.html");
    },

    loadWaitingTemplate: function () {
        return bizagi.templateService.getTemplateWebpart("jquery/automatictesting/tmpl/waiting.tmpl.html");
    },

    openUploadDialog: function () {
        $('.ui-widget-overlay').show();
        $("#dialog-upload").dialog("open");
    },

    closeUploadDialog: function () {
        $('.ui-widget-overlay').hide();
        $("#dialog-upload").dialog("close");
    },

    openSOADialog: function () {
        $('.ui-widget-overlay').show();
        $("#dialog-soa").dialog("open");
    },

    closeSOADialog: function () {
        $('.ui-widget-overlay').hide();
        $("#dialog-soa").dialog("close");
    },

    closeWaitingOverlay: function () {
        $('.bz-at-loading-container').hide();
    },

    closeLoginDialog: function () {
        $('.ui-widget-overlay').hide();
        $("#dialog-login").dialog("close");
    },

    openDownload: function (response) {
        if (response.location) {
            $.fileDownload(location);
        }
    },

    openLoginDialog: function () {
        var userName = bizagi.automatictesting.state.getUser();
        var password = bizagi.automatictesting.state.getPassword();
        var domain = bizagi.automatictesting.state.getDomain();
        if (userName != "undefined" && password != "undefined" && domain != "undefined") {
            recorder.login(userName, password, domain);
        } else {
            $('.ui-widget-overlay').show();
            $("#dialog-login").dialog("open");
        }
    },

    openQuickLoginDialog: function () {
        var user = bizagi.automatictesting.state.getUser();
        var domain = bizagi.automatictesting.state.getDomain();
        if (user !== "undefined") {
            recorder.quickLogin(user, domain);
        } else {
            $('.ui-widget-overlay').show();
            $("#dialog-login").dialog("open");
        }
    },

    login: function (userName, password, domain) {
        $.ajax({
            url: "Rest/Users/UserAuthentication?userName=" + userName + "&password=" + password + "&domain=" + domain,
            type: "GET",
            dataType: "json"
        }).done(function (response) {
            if (response.isAuthenticate == "true") {
                bizagi.automatictesting.state.setLogin(userName, password, domain);
                workportal = new bizagi.workportal.facade();
                workportal.execute();
                $(".bz-at-recorder-stop").show();
                $.when(workportal.ready())
                        .done(function () {
                            if (!bizagi.automatictesting.state.isRecording()) {
                                recorder.openNewCaseDialog();
                                recorder.closeLoginDialog();
                            }
                            else {
                                recorder.flushModal();
                            }
                        });

            }
            else {
                bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-login"));
            }
        }).fail(function (jqXHR, textStatus) {
            bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-loginfields"));
        });
    },

    quickLogin: function (user, domain) {
        var userData = { user: user, domain: domain, loginOption: "AutoTestingMode" };

        $.ajax({
            url: "Rest/Authentication/User",
            type: "POST",
            data: userData,
            dataType: "json"
        }).done(function (response) {
            if (response.isAuthenticate === "true") {
                bizagi.automatictesting.state.setQuickLogin(user, domain);
                workportal = new bizagi.workportal.facade();
                workportal.execute();
                $(".bz-at-recorder-stop").show();
                $.when(workportal.ready())
                        .done(function () {
                            if (!bizagi.automatictesting.state.isRecording()) {
                                recorder.openNewCaseDialog();
                                recorder.closeLoginDialog();
                            }
                            else {
                                recorder.flushModal();
                            }
                        });
            }
            else {
                bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-login"));
            }
        }).fail(function (jqXHR, textStatus) {
            bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-login"));
        });
    },

    logout: function () {
        $.ajax({
            url: "Rest/Authentication",
            type: "DELETE",
            dataType: "json"
        }).done(function () {
            window.location = location.protocol + "//" + location.host + location.pathname;
        }).fail(function (jqXHR, textStatus) {
            window.location = location.protocol + "//" + location.host + location.pathname; ;
        });
    },

    render: function () {
        var self = this;
        $.when(self.loadRecorderTemplate())
               .done(function (template) {
                   var content = $.tmpl(template);
                   content.appendTo("body");
                   $(".bz-at-recorder-stop", content).click(function () {
                       self.stop();
                   });
                   $(".bz-at-recorder-stop").hide();
               });

        $.when(self.loadSOATemplate())
            .done(function (template) {
                var content = $.tmpl(template);
                $.when(content.appendTo("body"))
                .done(function () {
                    var fileInput = $(".bz-at-soap-upload");
                    var xmlText = $(".bz-at-soap-xml");
                    fileInput.bind('change', function (e) {
                        if (fileInput.length === 0 || fileInput[fileInput.length - 1].files.length === 0) {
                            bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-scenariofile"));
                            return;
                        }
                        var file = fileInput[0].files[0];
                        var textType = '/text.*/';
                        if (file.type.match(textType)) {
                            var reader = new FileReader();
                            reader.onload = function (oFrEvent) {
                                xmlText[0].innerText = oFrEvent.target.result;
                            };
                            reader.readAsText(file);
                        } else {
                            xmlText[0].innerText = "File not supported!";
                        }
                    });
                })
                    .done($("#dialog-soa").dialog({
                        title: bizagi.localization.getResource("bizagi-autotesting-recorder-soa-title"),
                        autoOpen: false,
                        height: 400,
                        width: 350,
                        modal: false,
                        closeOnEscape: false,
                        buttons: [
                            {
                                text: bizagi.localization.getResource("bizagi-autotesting-recorder-continue"),
                                click: function () {
                                    bizagi.automatictesting.host.executeSOAPRequest($(".bz-at-soap-xml")[0].value);
                                    self.blockBackground();
                                }
                            },
                            {
                                text: bizagi.localization.getResource("bizagi-autotesting-recorder-cancel"),
                                click: function () {
                                    self.closeSOADialog();
                                }
                            }],
                        close: function () {
                            self.openNewCaseDialog();
                        }
                    }));
            });

        $.when(self.loadUploadTemplate())
               .done(function (template) {
                   var content = $.tmpl(template);
                   $.when(content.appendTo("body"))
                       .done($("#dialog-upload").dialog({
                           title: bizagi.localization.getResource("bizagi-autotesting-recorder-upload-title"),
                           autoOpen: false,
                           height: 200,
                           width: 350,
                           modal: false,
                           closeOnEscape: false,
                           buttons: [{
                               text: bizagi.localization.getResource("bizagi-autotesting-recorder-continue"),
                               click: function () {
                                   var file = $(".bz-at-scenario-upload");
                                   if (file.length === 0 || file[file.length - 1].files.length === 0) {
                                       bizagi.showMessageBox(bizagi.localization.getResource("bizagi-autotesting-messages-error-scenariofile"));
                                       return;
                                   }
                                   bizagi.automatictesting.host.runScenario(file[file.length - 1].files[0]);
                                   self.closeUploadDialog();
                                   self.blockBackground();
                               }
                           }, {
                               text: bizagi.localization.getResource("bizagi-autotesting-recorder-cancel"),
                               click: function () {
                                   self.closeUploadDialog();
                               }
                           }],
                           close: function () {
                               self.openNewCaseDialog();
                           }
                       }));
               });
        $.when(self.loadQuickLoginTemplate())
                   .done(function (template) {
                       var content = $.tmpl(template);
                       $.when(content.appendTo("body"))
                        .done(self.loadUsers())
                           .done(function () {
                               $("#dialog-login").dialog({
                                   autoOpen: false,
                                   width: 250,
                                   modal: false,
                                   closeOnEscape: false,
                                   dialogClass: "no-close",
                                   resizable: false,
                                   buttons: [{
                                       text: bizagi.localization.getResource("bizagi-autotesting-recorder-login"),
                                       click: function () {
                                           var userInput = $("#cbUsers").val();
                                           var domain = userInput.split("\\")[0];
                                           var user = userInput.split("\\")[1];
                                           self.quickLogin(user, domain);
                                       }
                                   }],
                                   close: function () {
                                   }
                               });
                               $("#dialog-login input").on("keydown", function (e) {
                                   if (e.keyCode == 13) {
                                       var buttons = $("#dialog-login").dialog("option", "buttons");
                                       buttons[0].click();
                                   }
                               });

                           }
                           );
                   });
        return;
    },

    stop: function () {
        var self = this;
        // Disable logging, and fetch recorded messages
        var requests = bizagi.automatictesting.ajax.requests;
        bizagi.log("Requests recorded", requests);
        bizagi.automatictesting.ajax.requests = [];

        // Save information externally
        self.blockBackground();
        $.when(self.loadWaitingTemplate())
            .done(function (template) {
                var content = $.tmpl(template);
                $.when(content.appendTo("body")).done(function () {
                    $.when(bizagi.automatictesting.host.saveTestCase(requests))
                        .done(function () {
                            self.flushModal();
                            self.closeWaitingOverlay();
                            self.openNewCaseDialog();
                        });
                });
            });
    },

    addLoginRequest: function () {
        var request = {
            url: "Rest/Users/UserAuthentication",
            data: "userName=" + bizagi.automatictesting.state.getUser() + "&password=" + bizagi.automatictesting.state.getPassword() + "&domain=" + bizagi.automatictesting.state.getDomain(),
            method: "GET",
            response: "{}",
            success: false,
            errorMessage: undefined,
            serviceType: "LOGIN"
        };
        bizagi.automatictesting.ajax.requests.push(request);
    },

    addQuickLoginRequest: function () {
        var request = {
            url: "Rest/Authentication/User",
            data: "user=" + bizagi.automatictesting.state.getUser() + "&domain=" + bizagi.automatictesting.state.getDomain(),
            method: "POST",
            response: "{}",
            success: false,
            errorMessage: undefined,
            serviceType: "QUICKLOGIN"
        };
        bizagi.automatictesting.ajax.requests.push(request);
    }

});

